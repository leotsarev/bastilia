<?php

if (!defined('ROOT'))
	die ('Error!');

require_once 'common.php';

 define ('GROUP', '��� &laquo;��������&raquo;');
 define ('KEYWORDS', '��������, ������� ����, �����-���������');
 define ('DOCTYPE', '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">');
 define ('CONTENTTYPE','<meta http-equiv="Content-Type" content="text/html; charset=windows-1251">');

 define ('BEFREE', '������������� ���� ��-���������� ��������� ������� ����� ������ � ������...<br/> <strong>����� ���������� � ��������!</strong>');
 define ('AKUNIN', '������� ���� <br /> <strong> ��������� ������ </strong> <br /> <em>10-13 ������� 2006 ����, ������������� �������</em>');
 define ('NERON', '������� ���� <br /> <strong> ���� ������ </strong> <br /> <em>6-9 ���� 2006 ����, ������������� �������</em>');
 define ('REVOLUTION', '������� ���� <br /> <strong> ������� � ����� </strong> <br /> <em>13-16 ���� 2006 ����, ������, �� &laquo;���������� ���&raquo;</em>');
 define ('HELLAS', '������� ���� <br /> <strong> ���� ��������� </strong> <br /> <em> 19-22 ���� 2007 ����, ������������� �������</em>');
 define ('LYNCH', '������� ���� <br /> <strong> �������� </strong> <br /> <em> 17-19 ������� 2007 ����, ������������� �������</em>');
 define ('FALCONS', '���������� ������� <br /> <strong>Falmouth Falcons</strong> <br /> <em>�� ��������; ���� ��� - �������� ���� �����</em>');
 define ('PROJECTS', '<strong>����������� �����</strong> <br /> <em>�������, ������� �� ����� � �������� ������� �����</em>');
 define ('ALASKA', '������� ���� <br /> <strong> ��������: ������� ���������</strong> <br /> <em>23-25 ������� 2008 ����, ������������� �������</em>');
 define ('HOBBIT', '������� ���� <br /> <strong> ������</strong> <br /> <em>8-10 ������� 2008 ����, ������������� �������</em>');
 define ('XVI', '������� ���� <br /> <strong>XVI. ��� � ����������</strong> <br /> <em>8-12 ���� 2009 ����, �� &laquo;�������� � ����&raquo; � ������</em>');
 define ('LUNA', '������� ���� <br /> <strong>�������� �� ����</strong> <br /> <em>28-31 ��� 2009 ����, ������������� �������</em>');
 define ('BSG', '������� ���� <br /> <strong>BSG75 ����������</strong> <br /> <em>3-5 �������� 2010 ����, ������������� �������</em>');
 define ('MORIA', '������� ���� <br /> <strong>������ �����: ����� ������</strong> <br /> <em>6�11 ���� 2010 ����, ���������� �������, �� ������ �����</em>');

 define ('CSV_HARD_LIMIT', 100);
 define ('FAVICON', '<link rel="icon" href="/favicon.ico">');
 define ('RULER', '<table class="ruler"><tr><td></td></tr></table>');
 define ('DOCICON', '<img src="/images/article.gif" alt="[doc]" width="18" height="16" />');

 define ('HELLAS_BANNER', '<a href="http://cuba.romulrem.ru" onClick="javascript:urchinTracker(\'/outgoing/cuba_romulrem_ru\');"><img src="/images/banners/cuba.gif" width="88" height="31" alt="Guantamera. ��� 2007" style="border:0;"/></a>');
 define ('COMCON_BANNER','<a href="http://www.comcon.su" onClick="javascript:urchinTracker(\'/outgoing/comcon_su\');"><img src="/images/banners/comcon.gif" width="88" height="31" alt="������ - ������� ��� �����" style="border:0;" /></a>');

 setlocale(LC_ALL, "ru_RU.CP1251");

date_default_timezone_set('Europe/Moscow');

function open_file($name) {
	return @fopen(ROOT . $name,"rb");
}

function show_doc($icon, $name, $link, $text) {
 echo("<p>$icon<a href=\"$link\">$name</a> $text");
}

function show_menu_file ($handle)
{
	echo '<table cellpadding="0" cellspacing="7" class="menu">';
	$array = fgetcsv ($handle, CSV_HARD_LIMIT);
	while ($array)
	{
	  $link = $array[0];
	 if ($link != "")
	 {
    $title = $array[1];
    $text = $array[2];
    if ($link == "*")
    {
      echo "<tr><td title=\"$title\" class=\"label\"><strong>$text</strong></td></tr>\n";
    }
    else
    {
      echo "<tr><td><a href=\"$link\" title=\"$title\">$text</a></td></tr>\n";
	 	}
	 }
	 $array = fgetcsv ($handle, CSV_HARD_LIMIT);
	}
  echo '</table>';
}

function show_header($name, $longname) {
	echo DOCTYPE;
	echo "\n<html lang=\"ru\">\n<head>\n<title>" . GROUP . " :: $name </title>\n";
	echo CONTENTTYPE ."\n" . FAVICON . "\n";
?>
<link media="all" rel="stylesheet" href="http://bastilia.ru/css/def.css" type="text/css">
<link rel="alternate" type="application/rss+xml" title="RSS" href="http://feeds.feedburner.com/bastilia/">
</head>
<body>
<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-1194519-1";
urchinTracker();
</script>
<table cellpadding="2" cellspacing="0" width="100%">
 <tr> <td class="side"></td> <td colspan="3" class="line"></td> <td class="side"></td> </tr>
  <tr>
    <td class="logo">
    <a href="http://bastilia.ru">
    	<img src="/images/logo-low.jpg" alt="�������� ��������" /></a></td>

    <td rowspan="3" class="chain">&nbsp;</td>
    <td class="globalheader">
<? echo($longname); ?>
    </td>
    <td rowspan="3" class="chain">&nbsp;</td>
    <td class="side">&nbsp;</td>
   </tr>
      <tr>
   	  <td class="side">
<?
}
function show_footer (){
?>
	    <tr>
    	<td class="side" rowspan="2">&nbsp;</td>
    	<td class="bottom">
       <table class="ruler"><tr><td></td></tr></table>
       <div class="small">
������ �����
<?php lj_user2('scrackan'); echo(", "); lj_user2('krom'); echo(" &amp; "); lj_user2('leotsarev'); echo " ������������� <a href=\"mailto:admin@bastilia.ru\">scrackan</a>"; ?>
       </div>
      </td>
      <td class="side" rowspan="2">&nbsp;</td></tr>
    <tr><td colspan="3" class="line"></td></tr></table>

    	</body>

    	</html>
<?}

function show_menu($menufile){
	$handle=open_file($menufile);
	if ($handle==FALSE) die("NO MENU $menufile");
	show_menu_file ($handle);
  echo('</td>');
}

function show_banner()
{
	echo '<br />';
  echo '<br />';
  include('inc/right-banner.inc');
  echo '<br />';
}

function show_local_menu ($menu)
{
	$handle = open_file("inc/$menu.menu");
	if ($handle === FALSE)
		return FALSE;
	show_menu_file ($handle);
	return TRUE;
}

function try_show_admin ()
{
	if ( is_admin (get_username ()))
  {
  	echo '<strong>����������</strong>';
  	show_local_menu ('admin');
  	return TRUE;
  }
  return FALSE;
}

function right_block ($menu, $show_admin = FALSE)
{
	echo '<td class="side">';

  $clear = TRUE;
  $clear = !show_local_menu ($menu) && $clear;

	if ( $show_admin )
		$clear = !try_show_admin () && $clear;

	if ($clear)
		show_banner();
	echo '</td>';

}

function show_login_link ()
{
	if ( is_admin (get_username ()))
		return;
?>
  <div class="small">
          <strong><a href="/news/openid/">����� � ������ ���������</a></strong>
         </div>
         <table class="ruler"><tr><td></td></tr></table>

<?php
}

function show_menu_ex($menufile) {
	echo '<td class="side">';
	show_menu ($menufile);
	echo '</td>';
}

function lj_user($ljuser)
{
  show_lj_user($ljuser, false, true);
}
function lj_comm($ljuser)
{
	show_lj_user($ljuser, true, true);
}
function lj_user2($ljuser)
{
	show_lj_user($ljuser, false, false);
}

function show_lj_user($ljuser, $comm, $icon)
{
  	$image =$comm ? 'http://bastilia.ru/images/lj/community.gif' : 'http://bastilia.ru/images/lj/userinfo.gif';
	$link = get_lj_path ($ljuser, $comm);
	$img = $icon ? "<img src='$image' width='17' height='17' style='border: 0;' alt='LJ'/>" : '';
	echo ("<span style=\"white-space: nowrap\">$img<a href='$link/profile' onClick='javascript:urchinTracker(\"/outgoing/$link/profile\");'><b>$ljuser</b></a></span>");
}



function show_icq($icq)
{
	echo "<span style=\"white-space: nowrap\"><img height=\"18\" alt=\"ICQ status\" src=\"http://web.icq.com/whitepages/online?icq=$icq&amp;img=5\" width=\"18\"><strong><a href=\"http://wwp.icq.com/$icq\">$icq</a></strong></span>";
}
?>