<?php
define ('ROOT', '../../../');
require_once ROOT . 'funcs.php';
show_header ('������ :: ������� :: �������� ����', HOBBIT);
show_menu ('inc/main.menu');
?>
<td class="box">
<div class="boxheader"><a href="http://bastilia.ru/hobbit/">������</a> :: <a href="http://bastilia.ru/hobbit/regions/">�������</a> :: �������� ����</div>
<div style="margin:1em">
	<div style="text-align:center">
	<h3>�������� ����</h3>
	<img src="misty.jpg" />
</div>
<p>�&nbsp;����� ������� ������ ����, �&nbsp;�������� �����, ������� ��� ������ �&nbsp;������ ��� ������. ��&nbsp;�&nbsp;�����-�� ������ �������� ������, ��� ����� ������� ����, ������� �������� ����� �&nbsp;��&nbsp;������� ��&nbsp;����� ������� ���-�� �������� �����. ������ ���! �&nbsp;���� ������ ���� ���������� ������, ��������� ������ ������ �����, �&nbsp;������ ������������� �������, ����� �&nbsp;������ ������������� ���� �&nbsp;������ �&nbsp;������������ ��������� ���. ���� ��&nbsp;��������� �������� ��&nbsp;���, ���&nbsp;�� ������ &laquo;������ �������&raquo; �&nbsp;�����, ��&nbsp;������ ����� ����&nbsp;�� ������ ������ ������. �&nbsp;������ ���������������.</p>

<p>��� ������ ��� ����� ������� ����������. ��&nbsp;���� ��������� �&nbsp;������� �������� �������� �������, ����� ������, ��&nbsp;����� �������. ��&nbsp;����� ��������� ���, ��� ���������� ������ ��������� �&nbsp;����������� ����� ��&nbsp;���������, ��� ������ ��� ������, ��� �&nbsp;������, �&nbsp;��������, ��� ������ �������� ������� ��� ������ ��� ��������� ���� ��&nbsp;���� ����. ������ ��&nbsp;��������. �������, ���������� ���, ��������� �&nbsp;�&nbsp;����������� ��&nbsp;������� ���. ������ �������&nbsp;&#8212; ������ ���. ����� ����-��, �������� ��������, ��� �&nbsp;��������� ��������, ��&nbsp;������� ��� ��&nbsp;�������, ��&nbsp;���� �&nbsp;���� ����������. �&nbsp;��� ������ ������� �� ������ ����������...</p>

<p style="text-align:center">***</p>

<p><strong>������ �������&nbsp;&#8212; ������ ���.</strong><br />
�&nbsp;���������� ������� ����� ����� �&nbsp;���� ���. ������� �������� ����� �������� ���� ��������� ��&nbsp;������������ ����� ��������, ������� ��������� ����� �������� �����������. ���&nbsp;&#8212; ������� ��� ������� �&nbsp;������, ����� ���������. ������ �������&nbsp;&#8212; ������ ���. ���� ��� �������� ���� 5&nbsp;��� �&nbsp;���� ��� ����� �����, ��&nbsp;��� ������� ��� ����� ������������ ������������. �&nbsp;��� 5&nbsp;��� �&nbsp;���� ���� ������ ������. ��� ��������� �&nbsp;����������� ��&nbsp;������ �������. �&nbsp;�������, ��� ������ ����� �&nbsp;����� ��������! ��� ����� �������� ���. ����&nbsp;�� �������� ����� �������� ����� �������� ���� ������ ������? �������, ��� ����� �&nbsp;���������. ������� �&nbsp;������ �&nbsp;����� ���� (�&nbsp;��������) ����� ������ ����� �������� ����. �&nbsp;�������� ���������������� ������ ������� ������ ����������, ������� ����� ���� ������, ���� ������. ���� ��� ��������� �������&nbsp;&#8212; ��������, ��������.</p>

<p><strong>���������� �������.</strong><br />
������� �������� ����� ���, �&nbsp;������������, ����� �&nbsp;��������� ����� �������� ���&nbsp;&#8212; ��� ������� �&nbsp;������� ���, �&nbsp;������, �&nbsp;�����-�� ������,&nbsp;&#8212; ������ ��������� �����. ���� ���������� �����, �&nbsp;���� ������ ���������� �������. ������ ��� ��&nbsp;���������. ��&nbsp;�&nbsp;����� ������, ���� �������� ��������� �������, ��� ��������� �&nbsp;������� ��������� ��&nbsp;����.</p>

</div>
</td>
<?php
	right_block('hobbit');
	echo '</tr>';
	show_footer();
?>

