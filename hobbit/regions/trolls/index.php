<?php
define ('ROOT', '../../../');
require_once ROOT . 'funcs.php';
show_header ('������ :: ������� :: ������', HOBBIT);
show_menu ('inc/main.menu');
?>
<td class="box">
<div class="boxheader"><a href="http://bastilia.ru/hobbit/">������</a> :: <a href="http://bastilia.ru/hobbit/regions/">�������</a> :: ������</div>
<div style="margin:1em">
	<div style="text-align:center">
	<h3>������</h3>
	<img src="trolls.jpg" />
</div>
<p>������&nbsp;&#8212; ��� �������� �&nbsp;������ �������� ����������, ������� ������ �&nbsp;�������� ������ �&nbsp;���, ��� ����-������ ���������, ������� �&nbsp;�������� ���&nbsp;�� ��&nbsp;����. �&nbsp;�����, ��� �������� �&nbsp;��� ��&nbsp;����� �&nbsp;���� �&nbsp;�������. �����, ��&nbsp;�������� ��&nbsp;������� ��������� ������-���� �������, �������� ����-�� �&nbsp;������� ���. ������ ���������� �������� �&nbsp;��������� ��������� ���� �&nbsp;������. ������, ��� ��� ��&nbsp;������ ������� ��� ����� �������� ���� ������&nbsp;&#8212; �������� �&nbsp;���������, ���� �������� ������� (������� ��� ������� ��������� ��&nbsp;������ ��������, ��������� �����������).</p>

<p>�&nbsp;������� ���� ������. �&nbsp;��� ������ ��� ���������� �� ������������. �&nbsp;�������������, ��� ����� ������� ����������. ������, ���� ������� ������� ��&nbsp;��������� (�&nbsp;���� ��� ������ ���������), �&nbsp;������� �������: <strong>��&nbsp;��� ����� ������� ������ ���� ����</strong>. ����������, ����� ���������� �&nbsp;������ �������, ���� ������� ������� �&nbsp;��� �&nbsp;����, ��&nbsp;������� ������� ��&nbsp;���������.</p>

<p>������� �&nbsp;������� �&nbsp;���� ����� ����� ������, ���� ��� ������ ���� ������:</p>

<p><ul><li>���� ������ ���� �������� ������� (���������, ��������), ��&nbsp;��&nbsp;�������, ���� ���� ������ ��&nbsp;������</li><br />
<li>���������, ��� ��&nbsp;������ ������� ����� ������.</li></ul></p>

<p>������ ��������� ��&nbsp;��� �������&nbsp;&#8212; ������ ��������, ��� ����� ����������� ������ ������ ����������, ���� ��&nbsp;����� �&nbsp;������� �����������.</p>



</div>
</td>
<?php
	right_block('hobbit');
	echo '</tr>';
	show_footer();
?>

