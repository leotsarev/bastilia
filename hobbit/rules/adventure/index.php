<?php
define ('ROOT', '../../../');
require_once ROOT . 'funcs.php';
show_header('������ :: ������� ::  ������� �� ������������', HOBBIT);
show_menu("inc/main.menu"); ?>
<td class="box">
<div class="boxheader"><a href="http://bastilia.ru/hobbit/">������</a> :: <a href="/hobbit/rules/">�������</a> :: ������� �� ������������ </div>
<h2 align="center">������� �����������</h2>
<h3 align="center">��&nbsp;������</h3>
<p>�� ������������ ��&nbsp;���� �&nbsp;���������, ��&nbsp;��������� ������� ��&nbsp;�����. ������� �����������&nbsp;&#8212; ���� ��� ��� �����������, �&nbsp;������� ��������� ������ ������������. ��� ������ ���������� ����������� �&nbsp;�&nbsp;���������. ��&nbsp;���� ����� ��������� ������. ��&nbsp;���� �&nbsp;����� ������� ���� ������ �������� ����� ������ �������. �&nbsp;������ ��&nbsp;��� ������ ��������� ��� ��������� �����������. ���� ������ ������ ������������ ������ ��� �����������, ��� ������� �����-������ �������� ��� �������� ����������, ��� ������ ����������� ������ ������ �������.</p>
<h4>����</h4>
<p>����� ����� ���� ������ ����� ����������. ��&nbsp;����� ��&nbsp;������������ ����� �������, ��� ������ ���������, ������� �� ������ ��&nbsp;����. ����� ��&nbsp;������� �&nbsp;���� �&nbsp;��&nbsp;�������� �������� �����������, ������ ������������ ����� ����� ��&nbsp;������� �&nbsp;������� ������ ��&nbsp;���������� ������������� ������� ��� ������ ��&nbsp;�����������.</p>
<h4>������� ��&nbsp;������</h4>
<p>������ ��&nbsp;����� ������ ��&nbsp;����� ������� ��&nbsp;������&nbsp;&#8212; ��&nbsp;�����, ��&nbsp;�������. �&nbsp;������, ��&nbsp;��������� ��&nbsp;������ ��������� ���� �&nbsp;����� �����-�� ��������� ��������. ���� ���� ������ ������ ��������� ������ ��������. ��&nbsp;�� ����� �&nbsp;��&nbsp;������������ ��&nbsp;���� ��������� ���������. ����������������� ����� ������ �������������.</p>
<h3 align="center">�&nbsp;��������</h3>
<p>����� �&nbsp;������� ������ ����������� ������ ����������� ������� ��� ������������. ��� ��� ��&nbsp;����� �����&nbsp;&#8212; ������ ����� ������ ���� ��&nbsp;����������, ���� �����-�� ��� ��������. ��&nbsp;����������� ������� ������ ���� ���������� �&nbsp;�������� ��,&nbsp;��� ��&nbsp;�����, ���� �����������, �&nbsp;�&nbsp;���� ������ ��&nbsp;������ �&nbsp;������� ������� ���� �������. ������&nbsp;�� �������� ������.</p>
<h4>��������� ��&nbsp;�������</h4>
<p>���� ����������� ����� ������ ������� ��&nbsp;�������, ��&nbsp;���� �������� ���������: ������ �������������� �&nbsp;������� ����, �.�. ����������� ������������� ����� �&nbsp;������� �������� ��������������������� ��������, �&nbsp;��� ��&nbsp;���� ���. �&nbsp;���� ��������� �����-�� ���, ��&nbsp;�&nbsp;��� ������ ��������� ������� �������. ��� �&nbsp;����� ������ ����� ���� ������ ����������. �&nbsp;�������, ���� ������� ��&nbsp;�������� �����, ����� ������ ����� ����� �&nbsp;�������� �&nbsp;���� ��� ��&nbsp;������ �������, ��&nbsp;��� ���� ������ ��� ����� �����, ��� ����� �������� �&nbsp;����� &laquo;�����&raquo;.</p>
<h4>�������� �&nbsp;������������</h4>
<p>�� ����� ����������� ����� ���������� ������ ������. ��� �� ��� ���� ����� �����������, �&nbsp;������������ �������������� �&nbsp;�����������. ������, ����������� ����� ��������� ����� ������ �&nbsp;����� ����, ����� ����� �������, ���� �&nbsp;������ ��� ��������� ��&nbsp;��� �����. ������ ���� ������ ����� �������� �������� �&nbsp;������ (��������� ��.&nbsp;�&nbsp;�������� ��&nbsp;�������� ����).</p>
<h4>��������� ����������</h4>
<p>���� ��������, ��� ���������� ���������� �&nbsp;������ ��&nbsp;������� ������������, �&nbsp;����������� �&nbsp;����������. ��� ����, ����� �������� ��� ��������� ���������� �������: ���������� ������ ����. ��,&nbsp;�,&nbsp;������� �������, ��� ��&nbsp;�����, ��&nbsp;��������� �&nbsp;���� ���������� ������.</p>
<h4>�����</h4>
<p>�� ���� ����� ������ �����. ��� �����, ��� �&nbsp;����� ��� �������. ��&nbsp;�&nbsp;��� ����� ������ �����, ������ ��� ����� ��&nbsp;������. ��� ����������. ��&nbsp;�������, ��&nbsp;�������, ��&nbsp;����� ��� ������.</p>
<h2 align="center">��������� �����������</h2>
<h4>������</h4>
<p>� ��������� ����������� ����� ������ ������ ��������. �������, ���������� �������� ���� �&nbsp;��������� ����������� ����������� ���� ��� �������, ���� ������� ��&nbsp;��������� (��.&nbsp;������� ��&nbsp;����������� ��������). ������� ��&nbsp;����� ���� ��&nbsp;�������, ���� �&nbsp;���� ��� <em>����������� ��������</em>. ��� ������ ������� ���� ������� ����:</p>

<ul>
<li>��������&nbsp;&#8212; �������� �����</li>
<li>������&nbsp;&#8212; ������</li>
<li>�������&nbsp;&#8212; �����</li>
<li>�������� �����&nbsp;&#8212; ������ ������</li>
<li>��������� �������� �����&nbsp;&#8212; ������� �&nbsp;��������</li>
<li>�������� ����&nbsp;&#8212; ������ �����</li>
<li>������ ���&nbsp;&#8212; ���������<br />
</li>
</ul>
<p>���������� ���������� ��������� �&nbsp;������ ������� ���������� �&nbsp;������� ������, ��� ���������� ������� ���� �������. ����� �������, ������� ������� ��&nbsp;��������� ������.</p>
<h4>�&nbsp;�������</h4>
<p>���-�� ��������� ��������, �&nbsp;���-�� ��&nbsp;������ ��� ��&nbsp;������. ��&nbsp;���������, ���� ��&nbsp;����� ��&nbsp;�������� �&nbsp;��������� �����������, ��&nbsp;��&nbsp;������� �&nbsp;������ ������� ���� ����� ����� ��� ����� �����������. ����� ��� �����&nbsp;&#8212; ������� ��&nbsp;������ �������. ������ ������, ��� ������ ��&nbsp;��� ����� ��&nbsp;������.</p>
<h4>��������</h4>
<p>�������, ������� ���������� � ��������� �����������, ����� ������� � ���� ����� �������, �� ����� ������� �� ��� �� ������, ������ ������� ��� �������� ������ ��������� ������ ��� ������� �������.</p>
<h4>������ �������������� �&nbsp;����</h4>
<p>������� ������, ����� ���������� ����������� �&nbsp;�������� ��&nbsp;������, ��&nbsp;���� ����. ����� ��� ����� ���� ������ ����������, �.�. ��&nbsp;�������. ���� ��&nbsp;�������� ��������� ���������, ��&nbsp;���������� ������� ������ �������������� ���������� ��&nbsp;���, ��� ����, ���� ��������� ��&nbsp;������������ �&nbsp;���������� ��������� �������� ��������. �&nbsp;����� ������ ������� ���� ��&nbsp;�������� �������� ���.</p>

</td>
<?php
	right_block('hobbit');
	echo '</tr>';
	show_footer();
?>



