<?php
define ('ROOT', '../../');
require_once ROOT . 'funcs.php';
show_header ('������ :: ������� :: ��������', HOBBIT);
show_menu ('inc/main.menu');
?>
<td class="box">
<div class="boxheader"><a href="http://bastilia.ru/hobbit/">������</a> :: <a href="http://bastilia.ru/hobbit/regions/">�������</a> :: ��������</div>
<div style="margin:1em">
	<div style="text-align:center">
	<h3>��������</h3>
	<img src="hob-pic.jpg" width="400" heigh="254" />
</div>
<p>��� ��� �&nbsp;���� ��� ������ ������. ����� ��� ����� ����������. ���� ���� ������ ���������� ������� �&nbsp;���������� �������� ������. ��&nbsp;������ ������� �������, ��&nbsp;������� ��&nbsp;������� ����� �����, ����������� �&nbsp;�����, ����������� ��&nbsp;������. �&nbsp;��������� ��� ��� ���� ����������� �������� ����������� ���������� ��������, ������� ��&nbsp;��������� ���������� ��&nbsp;���� �������������, ����� �������� ���� ������������. ��� ������, ����� ����� �������� ������� �������, ��������� ������ �&nbsp;����� �&nbsp;������ �������. ����� ��������, ��� ������ ����� ����������� ������� ��������������� ��&nbsp;����, ������� ��&nbsp;��������������� ��������� �&nbsp;����� �������, ��� �&nbsp;�������� ����� ����� ���������. ����� ������� �������� ������ ���������� ��������� ������� ������� ������, �&nbsp;����� ����������� ������ ����, ������� ������ ����� �&nbsp;��� �����, ��������� ������� ������ �&nbsp;���� ������������, ������� ��&nbsp;���������, �&nbsp;������� ��������� ���������� �����&nbsp;&mdash; ������ ��&nbsp;������� �����.</p>

<p>��� ��&nbsp;��������, ������ ���������� �������� ����� ������ �������� <strong>������</strong> �������� ����. ����� ����� �&nbsp;����� ������ ������ �&nbsp;��������� ���-���� <strong>��������</strong>. ��� ����� ���� ��� ������ �����, ��� �&nbsp;������.</p>

<p>��������, ����� ������� �������� ���, ��� �&nbsp;���� ����� ��������� ������, �&nbsp;��� ���������&nbsp;&mdash; ���, ��� �&nbsp;��� ���� ������ ���������� ��������, ����������� ��� ����������� ��&nbsp;������ ����. ������ ����� ��������� �����-���� ���������, �������, ������ ��������� ��� ���������� ���������, ������� ��������� �&nbsp;���� �&nbsp;���������.</p>

<p>����� ����, �������� �������� ��&nbsp;���������� ��� ����� �����������, ��������� ����. �&nbsp;����, ��� �&nbsp;�&nbsp;������� �������, ���� ������� ������������ �������, ������� ����� �������� ��� ��� ��&nbsp;���.</p>

<p>��-������, ��� <strong>������������</strong> ���������� ����, ����� �������� �������� ��� ������� ��������, ��� ������� �������� ���� ������ ��&nbsp;����� ������. ��� ���� ����������� ��������� ��&nbsp;�������, ��� ����� ��� �&nbsp;���� ����� ������.</p>

<p>��-������, ��� ��&nbsp;<strong>��������������</strong> �������� �������������������, ������� ���������� ��������� ����� ����������� ����� ������ ��������� ���������.</p>

<p>��������, ��� ������������� ��������� ������������ ������, �&nbsp;�������������� �������&nbsp;&mdash; ��� ����������� ��� ������� �������� ������� �����, ������� ��&nbsp;����� ��������������� �&nbsp;����� ����� ��&nbsp;������ ����������.</p>

<p>�������������, ��� ��� ������ ������, ��� ������ ��� ��������� ��&nbsp;�������������� �&nbsp;������������. ��������, ������� �������� ����� ������ �&nbsp;������-������� ��&nbsp;���� ����� ��&nbsp;���, ������ ��� ������ ����������� ��� ��� �������� ������������. �&nbsp;������ ���������� ��� ���������� ��������, ���� ��&nbsp;���������, ���&nbsp;&mdash; ��&nbsp;��� ���&nbsp;&mdash; ���� ��&nbsp;�������.</p>

<p>����� �������, ������ ������� ����� ������� ���������� �����, ����, ��� ��&nbsp;�������� ����� ������, �&nbsp;����� �&nbsp;���� ���������� �����, ������� �������, ������ ������������� �&nbsp;��������������� ������ ��&nbsp;����� �&nbsp;������� ���.</p>

<p>���� ��&nbsp;��� ��&nbsp;��&nbsp;����� �������, �&nbsp;��� ������� �������� ���������� �����, ����������� ���������� ��������� �&nbsp;������ (������� �&nbsp;���, ����������, ������ ����) �&nbsp;������ ������� ���, �����&nbsp;�� ����� ��������� ����� ������.</p>



</div>
</td>
<?php
	right_block('hobbit');
	echo '</tr>';
	show_footer();
?>

