<?
define("ROOT","../../../");
require_once(ROOT . 'funcs.php');
show_header("����� :: ��������",KRYNN);
show_menu("inc/main.menu"); ?>

	    <td class="box">
 <div class="boxheader"><a href="/">�����: ����� �����</a> :: �������� </div>
 
<p><span id="internal-source-marker_0.05880284196102159">����� ��&nbsp;���� ���������� </span><strong>600</strong> ������ (��� ������� ������������� �����).
<p>��������� ������ ���� ��&nbsp;��������� �������:
<ul>
<li>����������� ��������� </li>
<li>������������� (�����, �������, �����, �������, ������, �&nbsp;�.�.) ��� <strong>���������</strong> ������� ���������� </li>
<li>������������ �&nbsp;������������� ������ </li>
<li>������ ������� �&nbsp;������� ��������� </li>
<li>��������� ���������� (������, ������� ���������) </li>
<li>��������� ��������� ��� �����������, �������� �&nbsp;��������� </li>
<li>������� ��� ������������� ������ </li>
<li>��������������� ����� ����� ��&nbsp;������� �&nbsp;������� </li>
</ul>
<p>����� ���� ����� ����������� ������� ���������� ����� (��.<a href="http://bastilia.ru/akunin/post/money/"> &laquo;��������� ������&raquo;</a>,<a href="http://bastilia.ru/hellas/final/money/"> &laquo;���� ���������&raquo;</a>,<a href="http://bastilia.ru/lynch/final/money/"> &laquo;��������&raquo;</a>,<a href="http://bastilia.ru/alaska/final/money/"> &laquo;��������: ������� ���������&raquo;</a>,<a href="http://bastilia.ru/luna/final/money/"> &laquo;�������� ��&nbsp;����&raquo;</a>, <a href="http://bsg.bastilia.ru/final/money/">&laquo;Battlestar Galactica: �&nbsp;������� ����&raquo;</a>, <a href="http://ice.bastilia.ru/final/money/">&laquo;˸�&raquo;</a>).
<h4>����� �����</h4>
<ul>
<li>��&nbsp;30&nbsp;����&nbsp;&mdash; <strong>600</strong>&nbsp;�.</li>
<li>�&nbsp;1&nbsp;���� ��&nbsp;12&nbsp;�������&nbsp;&mdash; <strong>800</strong>&nbsp;�.</li>
<li>�&nbsp;13&nbsp;�������&nbsp;&mdash; <strong>1000</strong>&nbsp;�.</li>
</ul>
<h4>��� �������� ����� �����</h4>
<p>�&nbsp;<strong>�����-����������</strong> ������ ����� ����� ������ ��&nbsp;<a href="http://krynn.bastilia.ru/masters/">��������</a> ��&nbsp;������ �&nbsp;<a href="http://maps.yandex.ru/?text=%D0%A0%D0%BE%D1%81%D1%81%D0%B8%D1%8F%2C%20%D0%A1%D0%B0%D0%BD%D0%BA%D1%82-%D0%9F%D0%B5%D1%82%D0%B5%D1%80%D0%B1%D1%83%D1%80%D0%B3%2C%20%D1%81%D0%B0%D0%B4%20%D0%9F%D1%80%D1%83%D0%B4%D0%BA%D0%B8&amp;sll=30.367458%2C59.937337&amp;sspn=0.065882%2C0.033059">������������ ������</a>, �&nbsp;����� ��&nbsp;�������� �������������� �������� (����������� �&nbsp;����� �&nbsp;������� �������� �&nbsp;<a href="http://bastilia.ru/news/tag/krynn/">��������</a> ��� <a href="http://community.livejournal.com/krynn2011/">����������</a>).</p>
<p>�&nbsp;<strong>������</strong> ������ ����� ����� ������� <a href="http://krynn.bastilia.ru/masters/">��������</a> (����, ������&nbsp;��), �&nbsp;����� �����. ����� �&nbsp;����� ������� �������������� ��������.</p>
<h4><a name="remote" id="remote"></a>��� �������� ����� ��������</h4>
<p>�������� �����, ��&nbsp;���������� �&nbsp;���������, ����� ����������� ���������:</p>
<ul>
<li><a href="http://krynn.bastilia.ru/vznos/#remote1">����� �������� &laquo;�����-�����&raquo;</a></li>
<li><a href="http://krynn.bastilia.ru/vznos/#remote2">����� ��������-���� &laquo;�����-�����&raquo; ��� ����� �����</a></li>
<li><a href="http://krynn.bastilia.ru/vznos/#remote3">����� ��������� ������ ������� �����</a></li>
</ul>
<p><strong><a name="remote1" id="remote1"></a>������ &#8470;&nbsp;1. ����� �������� &laquo;�����-�����&raquo;</strong></p>
<p><strong>�����:</strong> ������������, ���������� ��������. <u>���������� ����� ��� ����� ��&nbsp;�����</u>!</p>
<p><strong>������:</strong> ���������� ��������� ��&nbsp;��������� &laquo;�����-�����&raquo; (�������, �&nbsp;�����-���������� ��&nbsp;����� 30&nbsp;��; �&nbsp;������&nbsp;&mdash; 70; ������� ������������ ���� ��&nbsp;���� ���������� ��)</p>
<p>������� ��������:</p>
<ol>
<li>�������� ��� ����� �&nbsp;�������<a href="http://alfabank.ru/atm/"> &laquo;���� ���������� �&nbsp;������&raquo;</a></li>
<li>�������� ��� �������� &laquo;���������� ����� ��&nbsp;������ �����&raquo;</li>
<li>��&nbsp;��������� ����������� ������ ���������� ���������� (�&nbsp;�.�. �&nbsp;��������� �&nbsp;�������� �����)</li>
<li>�&nbsp;��������� ��������� &laquo;�������� ��������&raquo; �&nbsp;������� ����� ����� ���������� <strong>40817.810.7.0401.0194529</strong>. ���� �� ������� ���������, ��&nbsp;������ ���������� &laquo;������� ����� ���������&raquo;</li>
<li>������������ ��&nbsp;����������� ��������� ��&nbsp;��� ���, ���� ������ ��&nbsp;����� ������������ ���������.</li>
</ol>
<p>����� ����� �������� ����� ���������, ����������, SMS �������� ��&nbsp;����� <nobr class="phone"><strong>+7(911)278-22-69</strong> &mdash;</nobr> <em>�, �����-�� �����-��, ������ ��� ������� �������-�� ����� �����-����</em>. ��� �����, ��-������, ��� ����, ����� ��&nbsp;������, ��&nbsp;���� ������ ������ ������; ��-������, ����� ������� ����� ��������, ��� �� ��������� �����������.</p>
<p><strong><a name="remote2" id="remote2"></a>������ &#8470;&nbsp;2. ����� ��������-���� &laquo;�����-�����&raquo; ��� ����� �����</strong></p>
<p><strong>�����:</strong> ��&nbsp;���� �������� ��&nbsp;����.</p>
<p><strong>������:</strong> ��������-���� ���� ��&nbsp;�&nbsp;����. ���� �&nbsp;��� ��&nbsp;&laquo;�����-����&raquo;, ����������� �������� �������� (������ ������� ��&nbsp;�����), �&nbsp;������ ������ ��&nbsp;��������� (������ <nobr>1-2 ���,</nobr> ��&nbsp;������ �&nbsp;������)</p>
<p><strong>���������:</strong></p>
<ul><li><u>���:</u> ������� ����� ���������</li>
<li><u>����� �����:</u> 40817810704010194529</li>
<li><u>���� ����������:</u> ��� &laquo;�����-����&raquo;</li>
<li><u>���:</u> 044525593</li>
<li><u>���. ����:</u> 30101810200000000593</li>
<li><u>��� �����:</u> 7728168971</li>
<li><u>��� �����:</u> 775001001</li>
</ul>
<p>����� ����� �������� ����� ���������, ����������, SMS �������� ��&nbsp;����� <nobr class="phone"><strong>+7(911)278-22-69</strong> &mdash;</nobr> <em>�, �����-�� �����-��, ������ ��� ������� �������-�� ����� �����-�� ����</em>. ��� �����, ��-������, ��� ����, ����� ��&nbsp;������, ��&nbsp;���� ������ ������ ������; ��-������, ����� ������� ��������, ��� �� ��������� �����������.</p>
<p><strong><a name="remote3" id="remote3"></a>������ &#8470;&nbsp;3. ����� ��������� ������ ������� �����</strong></p>
<p>����������, ��������� ������ ����� �&nbsp;����� ��������� ������ �����. ��&nbsp;��&nbsp;����� ����������� ���� ������� ��������&nbsp;&mdash; ��������������� ���������� ������ �������.</p>
<p><img src="http://bastilia.ru/images/article.gif" width="18" height="16" /><a href="http://krynn.bastilia.ru/vznos.rtf">��� ��������� �&nbsp;RTF-�����</a>. ������������ �&nbsp;�������� �&nbsp;����� �&nbsp;����.</p></td>
<? right_block('krynn'); ?>
	     </tr>
<? show_footer(); ?>

