<?
define("ROOT","../../../");
require_once(ROOT . 'funcs.php');
show_header("����� :: ���������� ������",KRYNN);
show_menu("inc/main.menu"); ?>

	    <td class="box">
 <div class="boxheader"><a href="/">�����: ����� �����</a> :: ���������� ������</div>
 <p>&nbsp;</p>
 <table border="0" align="center" cellpadding="1" cellspacing="1">
   <tr>
     <td><table border="2" frame="border" rules="all" cellspacing="2" cellpadding="2">
       <tr>
         <td colspan="2"><div align="center"><strong>�������������� ���������� </strong></div></td>
         </tr>
       <tr>
         <td>����������</td>
         <td><a href="#leo">���</a></td>
       </tr>
       <tr>
         <td>�����</td>
         <td><a href="#atana"><strong>�����</strong></a>, <a href="#dagny">�����</a>, <a href="#clair">����</a>, <a href="#ksiontes">��������</a> </td>
       </tr>
       <tr>
         <td>����������, ������ </td>
         <td><a href="#wer"><strong>���</strong></a></td>
       </tr>
       <tr>
         <td>��������</td>
         <td><a href="#xenia"><strong>������ ��</strong></a>, <a href="#leo">���</a> </td>
       </tr>
       <tr>
         <td>���������</td>
         <td><a href="#scrackan"><strong>�������</strong></a>, <a href="#leo">���</a></td>
       </tr>
       <tr>
         <td>�����</td>
         <td><strong>���</strong>, <a href="#leo">���</a> </td>
       </tr>
       <tr>
         <td>������ � ��������� </td>
         <td><a href="#clair"><b>����</b></a> </td>
       </tr>
       <tr>
         <td>������<br><a href="mailto:krynn-fight@bastilia.ru">krynn-fight@bastilia.ru</a></td>
         <td><a href="#leo"><strong>���</strong></a>, ����� </td>
       </tr>

       <tr>
         <td>�������</td>
         <td><a href="#ksiontes">��������</a></td>
       </tr>
       <tr>
         <td>���</td>
         <td><a href="#scrackan">�������</a> </td>
       </tr>
       <tr>
         <td>PR</td>
         <td><a href="#ksiontes">��������</a></td>
       </tr>
     </table></td>
     <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
     <td><table border="2" frame="border" rules="all" cellspacing="2" cellpadding="2">
       <tr>
         <td colspan="2"><div align="center"><strong>�������</strong></div></td>
         </tr>
       <tr>
         <td>������</td>
         <td><a href="#ksiontes">��������</a></td>
       </tr>
       <tr>
         <td>����� ����
           </td>
         <td><a href="#dagny">�����</a></td>
       </tr>
       <tr>
         <td>�����</td>
         <td><a href="#clair">����</a></td>
       </tr>
       <tr>
         <td>��� �� (�������) </td>
         <td><a href="#atana">�����</a></td>
       </tr>
       <tr>
         <td>���������� (�����) </td>
         <td><a href="#atana">�����</a></td>
       </tr>
       <tr>
         <td>��� ������ (�����)</td>
         <td><a href="#atana">�����</a><a href="#xenia"></a> </td>
       </tr>
       <tr>
         <td>����-�����</td>
         <td><a href="#wer">���</a></td>
       </tr>
       <tr>
         <td>����-������ (�����) </td>
         <td><a href="#scrackan">�������</a></td>
       </tr>
       <tr>
         <td>����-������ (�����������)</td>
         <td><a href="#wer">���</a></td>
       </tr>
       <tr>
         <td>����� �����</td>
         <td><a href="#murphy"><strong>�����</strong></a>, <a href="#clair">����</a> </td>
       </tr>
       <tr>
         <td>������� (�����������)</td>
         <td><a href="#murphy">�����</a></td>
       </tr>
     </table></td>
   </tr>
 </table>
 <p>&nbsp;</p>
 <p align="center"><strong>�������������� � �������� </strong></p>
 <table border="2" align="center" cellpadding="2" cellspacing="2" frame="border" rules="all">
   <tr>
     <td width="33%"><a name="murphy" id="murphy"></a><table  width="100%" border="0" cellspacing="1" cellpadding="1">
    <tr>
     <td style="background-color: lightgray; font-weight: bold" colspan="2">Lawful Good </td>
   </tr>
   <tr>
     <td colspan="2">�����</td>
   </tr>
  
   <tr>
     <td width="110"><div align="left"><img src="photo/mjurphy.jpg" width="100" height="100" /></div></td>
          <td>
Wizard <br>          
STR&nbsp;12<br />
DEX&nbsp;13<br />
CON&nbsp;13<br />
INT&nbsp;14<br />
WIS&nbsp;17<br />
CHA&nbsp;12</td>
   </tr>
   <tr>
     <td colspan="2"> <img src="http://bastilia.ru/images/icons16x16/phone.png" width="16" height="16" />&nbsp;7-921-319-08-01<br />       
       <img src="http://bastilia.ru/images/icons16x16/mail.png" width="16" height="16" />&nbsp;<a href="mailto:murphy@bastilia.ru">murphy@bastilia.ru</a><br />
       <img src="http://bastilia.ru/images/icons16x16/jabber.png" width="16" height="16" />&nbsp;murmail@gmail.com<br />
       <?php lj_user('mjurphy'); ?>
       <br /><br>
       <img src="http://bastilia.ru/images/icons16x16/blank_icq_1x18.gif" />&nbsp; </td>
   </tr>
 </table></td>
     <td  width="33%"><a name="dagny" id="dagny"></a><table  width="100%" border="0" cellpadding="1" cellspacing="1">
   <tr>
     <td style="background-color: lightgray; font-weight: bold" colspan="2">Neutral Good </td>
   </tr>
   <tr>
     <td colspan="2">�����</td>
   </tr>

   <tr>
     <td width="110"><div align="left"><img src="photo/dagny.jpg" width="100" height="100" /></div></td>
          <td>
Rogue<br>
STR&nbsp;9<br />
DEX&nbsp;15<br />
CON&nbsp;10<br />
INT&nbsp;13<br />
WIS&nbsp;11<br />
CHA&nbsp;13</td>
   </tr>
   <tr>
     <td colspan="2"> <img src="http://bastilia.ru/images/icons16x16/phone.png" width="16" height="16" />&nbsp;7-911-249-97-23<br />       
       <img src="http://bastilia.ru/images/icons16x16/mail.png" width="16" height="16" />&nbsp;<a href="mailto:dagny@bastilia.ru">dagny@bastilia.ru</a><br />
       <img src="http://bastilia.ru/images/icons16x16/jabber.png" width="16" height="16" />&nbsp;dagny@bastilia.ru<br />
      <?php lj_user('melory-noks'); ?><br /><img src="http://bastilia.ru/images/icons16x16/blank_icq_1x18.gif" />&nbsp;            <br><br> </td>
   </tr>
 </table></td>
     <td  width="33%"><a name="clair" id="clair"></a><table width="100%" border="0" cellspacing="1" cellpadding="1">
   <tr>
     <td style="background-color: lightgray; font-weight: bold" colspan="2">Chaotic Good</td>
   </tr>
   <tr>
     <td colspan="2">����</td>
   </tr>
   
   <tr>
     <td width="110"><div align="left"><img src="photo/clair.jpg" width="100" height="100" /></div></td>
		  <td>
Wizard<br>
STR&nbsp;9<br />
DEX&nbsp;10<br />
CON&nbsp;12<br />
INT&nbsp;17<br />
WIS&nbsp;12<br />
CHA&nbsp;13</td>
   </tr>
   <tr>
     <td colspan="2">
      <img src="http://bastilia.ru/images/icons16x16/phone.png" width="16" height="16" />&nbsp;7-911-234-16-79  (���)<br />       
      <img src="http://bastilia.ru/images/icons16x16/phone.png" width="16" height="16" />&nbsp;7-985-412-69-53 (���)<br />       
       <img src="http://bastilia.ru/images/icons16x16/mail.png" width="16" height="16" />&nbsp;<a href="mailto:clair@bastilia.ru">clair@bastilia.ru</a><br />
       <img src="http://bastilia.ru/images/icons16x16/jabber.png" width="16" height="16" />&nbsp;clair@bastilia.ru <br />
                <?php show_icq('272732532'); ?> <br />
                 <?php lj_user('le-ange-clair'); ?>              </td>
   </tr>
 </table></td>
   </tr>
   <tr>
     <td><a name="scrackan" id="scrackan"></a><table  width="100%" border="0" cellspacing="1" cellpadding="1">
   
   <tr>
     <td style="background-color: lightgray; font-weight: bold" colspan="2">Lawful Neutral</td>
   </tr>
   <tr>
     <td colspan="2">�������</td>
   </tr>
   <tr>
     <td width="110"><div align="left"><img src="photo/scrackan.jpg" width="100" height="100" /></div></td>
          <td>
Barbarian<br>
STR&nbsp;16<br />
DEX&nbsp;9<br />
CON&nbsp;16<br />
INT&nbsp;10<br />
WIS&nbsp;12<br />
CHA&nbsp;14</td>
   </tr>
   <tr>
     <td colspan="2"> <img src="http://bastilia.ru/images/icons16x16/phone.png" width="16" height="16" />&nbsp;7-911-278-22-69<br />       
       <img src="http://bastilia.ru/images/icons16x16/mail.png" width="16" height="16" />&nbsp;<a href="mailto:admin@bastilia.ru">admin@bastilia.ru</a><br />
       <img src="http://bastilia.ru/images/icons16x16/jabber.png" width="16" height="16" />&nbsp;boris.zemskov@gmail.com <br />
                <?php show_icq('109612345'); ?><br />
      <?php lj_user('scrackan'); ?>             </td>
   </tr>
 </table></td>
     <td><a name="ksiontes" id="ksiontes"></a><table  width="100%" border="0" cellspacing="1" cellpadding="1">
      <tr>
     <td style="background-color: lightgray; font-weight: bold" colspan="2">True  Neutral</td>
   </tr>
   <tr>
     <td colspan="2">��������</td>
   </tr>
   <tr>
     <td width="110"><div align="left"><img src="photo/ksi.jpg" width="100" height="100" /></div></td>
          <td>
Bard<br>
STR&nbsp;8<br />
DEX&nbsp;13<br />
CON&nbsp;11<br />
INT&nbsp;16<br />
WIS&nbsp;10<br />
CHA&nbsp;17</td>
   </tr>
   <tr>
     <td colspan="2"> <img src="http://bastilia.ru/images/icons16x16/phone.png" width="16" height="16" />&nbsp;7-911-208-85-94<br />       
       <img src="http://bastilia.ru/images/icons16x16/mail.png" width="16" height="16" />&nbsp;<a href="mailto:ksi@bastilia.ru">ksi@bastilia.ru</a><br />
       <img src="http://bastilia.ru/images/icons16x16/jabber.png" width="16" height="16" />&nbsp;ksiontes@gmail.com <br />
       <?php show_icq('241389124'); ?>
       <br />
       <?php lj_user('ksiontes'); ?></td>
   </tr>
 </table></td>
     <td><a name="wer" id="wer"></a><table  width="100%"  border="0" cellspacing="1" cellpadding="1">
  <tr>
     <td style="background-color: lightgray; font-weight: bold" colspan="2">Chaotic Neutral</td>
   </tr>
   <tr>
     <td colspan="2">���</td>
   </tr>
   
   <tr>
     <td  width="110"><div align="left"><img src="photo/wer.jpg" width="100" height="100" /></div></td>
          <td>
Fighter<br>STR&nbsp;11<br />
DEX&nbsp;13<br />
CON&nbsp;15<br />
INT&nbsp;14<br />
WIS&nbsp;14<br />
CHA&nbsp;6</td>
   </tr>
   <tr>
     <td colspan="2"> <img src="http://bastilia.ru/images/icons16x16/phone.png" width="16" height="16" />&nbsp;7-911-702-34-80<br />       
       <img src="http://bastilia.ru/images/icons16x16/mail.png" width="16" height="16" />&nbsp;<a href="mailto:wer@bastilia.ru ">wer@bastilia.ru </a><a href="mailto:admin@bastilia.ru"></a><br />
       <img src="http://bastilia.ru/images/icons16x16/jabber.png" width="16" height="16" />&nbsp;wer@bastilia.ru <br />
      <?php lj_user('wer'); ?>
      <br />
      <img src="http://bastilia.ru/images/icons16x16/blank_icq_1x18.gif" /></td>
   </tr>
 </table></td>
   </tr>
   <tr>
     <td><a name="leo" id="leo"></a><table  width="100%" border="0" cellspacing="1" cellpadding="1">
   <tr>
     <td style="background-color: lightgray; font-weight: bold" colspan="2">Lawful Evil </td>
   </tr>
   <tr>
     <td colspan="2">���</td>
   </tr>
  
   <tr>
     <td width="110"><div align="left"><img src="photo/leo.jpg" width="100" height="100" /></div></td>
          <td>
Cleric<br>
STR&nbsp;10<br />
DEX&nbsp;9<br />
CON&nbsp;14<br />
INT&nbsp;16<br />
WIS&nbsp;10<br />
CHA&nbsp;14</td>
   </tr>
   <tr>
     <td colspan="2"> <img src="http://bastilia.ru/images/icons16x16/phone.png" width="16" height="16" />&nbsp;7-921-777-68-55<br />       
       <img src="http://bastilia.ru/images/icons16x16/mail.png" width="16" height="16" />&nbsp;<a href="mailto:leo@bastilia.ru">leo@bastilia.ru</a><br />
       <img src="http://bastilia.ru/images/icons16x16/jabber.png" width="16" height="16" />&nbsp;leo@bastilia.ru<br />
      <?php lj_user('leotsarev'); ?>
      <br />
      <img src="http://bastilia.ru/images/icons16x16/blank_icq_1x18.gif" />&nbsp; </td>
   </tr>
 </table></td>
     <td><a name="atana" id="atana"></a><table  width="100%" border="0" cellspacing="1" cellpadding="1">
   <tr>
     <td style="background-color: lightgray; font-weight: bold" colspan="2">Neutral Evil </td>
   </tr>
   <tr>
     <td colspan="2">�����</td>
   </tr>
   
   <tr>
     <td width="110"><div align="left"><img src="photo/atana.jpg" width="100" height="100" /></div></td>
          <td>
Ranger<br>
STR&nbsp;10<br />
DEX&nbsp;10<br />
CON&nbsp;8<br />
INT&nbsp;15<br />
WIS&nbsp;13<br />
CHA&nbsp;17</td>
   </tr>
   <tr>
     <td colspan="2"> <img src="http://bastilia.ru/images/icons16x16/phone.png" width="16" height="16" />&nbsp;7-911-252-76-41<br />       
       <img src="http://bastilia.ru/images/icons16x16/mail.png" width="16" height="16" />&nbsp;<a href="mailto:atana@bastilia.ru">atana@bastilia.ru</a><br />
       <img src="http://bastilia.ru/images/icons16x16/jabber.png" width="16" height="16" />&nbsp;aleksvlasova@ya.ru <br />
       <?php show_icq('299207403'); ?>
       <br />
       <?php lj_user('aleks'); ?></td>
   </tr>
 </table></td>
     <td><a name="xenia" id="xenia"></a><table  width="100%" border="0" cellspacing="1" cellpadding="1">
   <tr>
     <td style="background-color: lightgray; font-weight: bold" colspan="2">Chaotic Evil </td>
   </tr>
   <tr>
     <td colspan="2">������ ��</td>
   </tr>
   
   <tr>
     <td width="110"><div align="left"><img src="photo/xeniaku.jpg" width="100" height="100" /></div></td>
          <td>
Cleric<br>
STR&nbsp;12<br />
DEX&nbsp;10<br />
CON&nbsp;15<br />
INT&nbsp;17<br />
WIS&nbsp;18<br />
CHA&nbsp;13</td>
   </tr>
   <tr>
     <td colspan="2"> <img src="http://bastilia.ru/images/icons16x16/phone.png" width="16" height="16" />&nbsp;7-903-229-20-59<br />       
       <img src="http://bastilia.ru/images/icons16x16/mail.png" width="16" height="16" />&nbsp;<a href="mailto:xeniaku@ya.ru">xeniaku@ya.ru</a><br />
       <img src="http://bastilia.ru/images/icons16x16/jabber.png" width="16" height="16" />&nbsp;xeniaku@ya.ru <br />
                <?php show_icq('480788310'); ?><br />
      <?php lj_user('xeniaku'); ?>             </td>
   </tr>
 </table></td>
   </tr>
 </table> <p>&nbsp;</p></td>
<? right_block('krynn'); ?>
	     </tr>
<? show_footer(); ?>

