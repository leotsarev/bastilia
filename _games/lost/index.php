<?
define("ROOT","../../");
require_once(ROOT . 'funcs.php');
show_header("���� :: � ��� ����",LOST);
show_menu("inc/main.menu"); ?>

	    <td class="box">
<div class="boxheader"><a href="http://lost.bastilia.ru/">����</a> :: � ��� ����</div>
<p>���� ����&nbsp;&mdash; ������� �&nbsp;��������� �&nbsp;��������� ������. ��&nbsp;����� ������
��� �������������, ��&nbsp;����� ���������� ��&nbsp;�����. ��������� ��&nbsp;�������,
������� ��� �������, ��������, ���� ���������� ��&nbsp;��������&nbsp;&mdash; �&nbsp;�����,
������� ��������� ������ &laquo;LOST&raquo;, ��� ��&nbsp;��� ��������.</p>
<p>��� ����� ��&nbsp;������� ������. ��� ��� �&nbsp;��� ��� �������� ���������, ��&nbsp;��&nbsp;����������� ������� ��� �������� �&nbsp;����� �������.
<p><div style="text-align: center;"><img alt="" border="1" src="images/lost.jpg" title="" /></div>
<p>����� ���������� �������, ��&nbsp;��� ����� �������, ������� ������������ ���� ����. ���������, ��� ������.

<p>����� ������ ������:

<p><ol><li>����� �&nbsp;����� ��������&nbsp;&mdash; ������ �&nbsp;������, ���� ��� <nobr>(19-21</nobr> ���� 2013&nbsp;�.)</li><li>������/��������� �������� ��&nbsp;������ �&nbsp;������ ���������� �������������� ���������.</li><li>������ ����&nbsp;&mdash; �&nbsp;������� �����, �����&nbsp;&mdash; �&nbsp;����������� �������. </li><li>�����&nbsp;&mdash; ������������� ����������������, ����� ������ ��&nbsp;�����-����������. </li><li>�������
����������&nbsp;&mdash; ����� ����� ��������� ��������, ����� ��������������
���������� ����� ���������. ������ �������� ��������� ��&nbsp;���������
�������, ���&nbsp;&mdash; �������� &laquo;��&nbsp;����&raquo; (��&nbsp;���������&nbsp;&mdash; ���� ���� ��&nbsp;�
������-������, �&nbsp;�&nbsp;����� ��������� ����).</li><li>�������� �����������&nbsp;&mdash; ������ <a href="http://rutracker.org/forum/tracker.php?nm=%EE%F1%F2%E0%F2%FC%F1%FF+%E2+%E6%E8%E2%FB%F5+LOST+%E0%E1%F0%E0%EC%F1">&laquo;LOST&raquo;</a>.</li></ol>
<p align="center"><em>E-mail ��� ����� �&nbsp;��: <a href="mailto:lost@bastilia.ru">lost@bastilia.ru</a></em>
  <? right_block('lost'); ?>
  </tr>
  <? show_footer(); ?>
