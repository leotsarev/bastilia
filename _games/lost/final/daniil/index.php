<?
define("ROOT","../../../../");
require_once(ROOT . 'funcs.php');
show_header("���� :: ����� ���� :: ������ ������� (������� �����)&nbsp;&mdash; �����",LOST);
show_menu("inc/main.menu"); ?>

	    <td class="box">
<div class="boxheader"><a href="http://lost.bastilia.ru/">����</a> :: <a href="http://lost.bastilia.ru/final/">����� ����</a> :: ������ ������� (������� �����)&nbsp;&mdash; �����</div>



<!--[if gte mso 9]><xml>
 
  
  
 
</xml><![endif]-->

<p class="MsoSubtitle" style="text-align:center" align="center"><span style="font-size:18.0pt;line-height:115%">����� � ���������� ������, ��
�����������, ��������, �����������, ��������� �� ��������, �������� � ���������
;)</span></p>

<p class="MsoNormal" style="text-align:right" align="right">����� ����� ����������
� ����</p>

<p class="MsoNormal" style="text-align:right" align="right">� ����������� ��������
������,</p>

<p class="MsoNormal" style="text-align:right" align="right">����� ������ ��������
����,</p>

<p class="MsoNormal" style="text-align:right" align="right">� ����� ��� ����� ���
�� �����</p>

<p class="MsoNormal" style="text-align:right" align="right">(�) ������� ����� (��
�������� ����������� ��������������� ��� ���. ��������� �.�. ��� 1.)</p>

<p class="MsoNormal" style="text-align:right" align="right">���� ������� �� �
������� ������ </p>

<p class="MsoNormal" style="text-align:right" align="right">(�) ����� � ������
�����</p>

<h1 align="center">����� ������� �������� � ���� ����һ � ������� ������.</h1>

<h2><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>����������, �������,
������� ��, �� ���� ������. ��� ���� ���������� � ���������� ����� � ����� 1.
���� �� ���-���� ������ ��� ���������, �� ����� �� ������ ��� ��������� � �����
� �������. ����� �� ����. </h2>

<p class="MsoNormal" style="text-indent:35.4pt">� ����� �� ���� ����� ��������
������� ������ ������� ��� ��������� �� �����! � ��� ����� �������� ����������.
��� � ��� � � ��� ����� �����? �� ����, ��� ����� � ��� ����� �� ������� � ��
����������� ����� ������ ���� ������������ ����� ������ ������� � �������� ��
������. � �� ��� ������ ����� �������������� ������� ����� � ������������, ��
����� ������ �������<span style="mso-spacerun:yes">&nbsp; </span>� ������ ������� �
��� ��� ����� ������ ����� ������ ������ ����� � ���, � ��� �� �� ���������� �
���� � �����-�� ������ �� ������ </p>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>�����
������� ������� ���� � ��� ����� �� ��� �������. ��� ����� � ��� ��������.
����� ������� ������, ������������� � ������ ���������, � ���� � �������� ����
����� ��� ����� ����������, ������������ �������� ������, � � ������ ������,
�������������� ������ ���������, ���� � ������ ������ �������������, ���������
��� ����� ������ ����, ������� �������������� ������� ������� � �����������
��������, ������� �� �������� ����� ��������� �� ��� ������ ����� ����� ��
�����������. </p>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>���
�� ���� ��������� ����, ��� ��������, ������� � ����� ������� ����, ��� ������
������ ��������, ������ ����������, � ������ ��� �������� ������ ���������� ��
����������� ��� ��� �������� ���������� ���� �� ������� ���� ����, ��� </p>

<h2 align="center">���������� ����� ������ ���� � ��������� ���� �� ���� ����һ.</h2>

<p align="center">����� ����������.</p>

<p class="MsoNormal">&nbsp;</p>

<h1 align="center">���������� ����� ������ ���� � ��������� ���� �� ���� ����һ.</h1>

<p class="MsoNormal">&nbsp;</p>

<h2>������.</h2>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>�,
�� ������ ������� ������ ������, ���� ������� ���, <s>����������� ��������
���������</s> ������� �������� ���� ����, ���� ������� �������! ������ ��� ��
����� ������� ��� �����. � ���� ���� ���� ����� �������</p>

<p class="MsoNormal" style="text-align:justify">������� �������� �� ������� �
����. </p>

<p class="MsoNormal" style="text-align:justify">������� ������ ������� (������)
�� ������ � �����.</p>

<p class="MsoNormal" style="text-align:justify">������� ���� (��� �������) ��
������ �������� ����� �����.</p>

<p class="MsoNormal" style="text-align:justify">������� ����� (������ �������) ��
��������� ��������.</p>

<p class="MsoNormal" style="text-align:justify">������� ����� (�������) ��
���������.</p>

<p class="MsoNormal" style="text-align:justify">������� ������� (�����) ��
������� � ���������� ������.</p>

<p class="MsoNormal" style="text-align:justify">������� ���� (����� ������) �� ����
� ��������� � ������ ������</p>

<p class="MsoNormal" style="text-align:justify">������� ������ (��, � �����������
�������� ���) �� ������ � �����</p>

<p class="MsoNormal" style="text-align:justify">������� ������ ����������
(������) �� ����������� �� ����</p>

<p class="MsoNormal" style="text-align:justify">������� �������� (�����) ��
�������, ���������� � ���������</p>

<p class="MsoNormal" style="text-align:justify">������� ���� ��������� (�����) ��
������ � �����.</p>

<p class="MsoNormal" style="text-align:justify">������� ���� �������� �� ��� (??)
�� ���������� �������</p>

<p class="MsoNormal" style="text-align:justify">������� ��� (������ ����������)
�� ����� �������� ������ � ������ �� ���� ������</p>

<p class="MsoNormal" style="text-align:justify">������� ������� (�����) ��
���������� ���������� � ������</p>

<p class="MsoNormal" style="text-align:justify">������� �������� (??) ��
������������� ����</p>

<p class="MsoNormal" style="text-align:justify">������� ?�������? (??) ��
��������� ������� ���</p>

<p class="MsoNormal">������� ������� (����� ��������) �� ���������</p>

<p class="MsoNormal">������� ������ (���� ��������) �� ������ � ������ ���� ;)</p>

<p class="MsoNormal">������� ������� (������ �������) �� ������</p>

<p class="MsoNormal">������� ������ ��������� (??) �� ������������� ������</p>

<p class="MsoNormal">������� ����� ������������� (??) �� ����������������</p>

<p class="MsoNormal">������� ������� ���������� (������ ������) �� �������
�������</p>

<p class="MsoNormal">������� ������� ���������� (??) �� ������������� �� ����</p>

<p class="MsoNormal">������� ��� (����� �����������?) �� ����� �� �������</p>

<p class="MsoNormal">������� (�������������� ������������ ��������) �� ��� ;)</p>

<p class="MsoNormal">������� (��������� �������) �� ������� �� �������, ������� �
����������</p>

<p class="MsoNormal">������� ������� ������ (��������) �� �������� ���� ��� ��
�������</p>

<p class="MsoNormal">������� ������������ (������������) � ����� �� ��� �����
�������� �������</p>

<p class="MsoNormal">������� ����, ���� � ������ �����, �� ���� ���� ��������
����, ��� ���� ��������� � �� ���� �������� ���� ��� ��� ���, ���� � ��� ��
�������</p>

<h1 align="center">����� 1. ������.</h1>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">������������ ������ ����� ����� ������� �����,</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">����� � ���� ��� ��������, ������ � ��������� �����,</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">������� ������ ������� � ������� ����� ������,</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">��� �����, �, ��������, �������������.</p>

<p class="MsoNormal" style="text-align:right" align="right">(�) ������ ������� (�� �������������)</p>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>����������.
��� ������� ��� ������� � ����� ����������� ���������� ����� � ����� �� �����
�� ����� ��� ���������� ����� �� �������, ��������� � ������� ��������,
���������� ��� �����, ��� � ����� �� �������, �������� ����� � �������
������������� ������ ����������� �� ��� ���, ���� ���������, ������,
������������� ����� � �������, ������������� ��������� ���������� ��������
��������� ��� ����, ����� ������ �� �����������, ����������� ���� �� ������ �
�������� �� �����, ���������� ������ ������� �������� ������������� �������.
������ ���������� ����������, ������, �� � ����� �����, � �������� ���������.
����� �������� ����� ������� � ������ ����������� ��� ������� �� ��� ���������,
� ���� ��� ��� ���������� ���� � ���� ���� ���������� � �������. ��� ���� ����
�� ������ ������, ���������� ����. ���� �� ����� � ���� ���-�� ������. � ��� �
����� �� ���������, �� ����� ������, �� ������� ��� � ���� ������. � ��� ������
���� �����, ���� �� ������ ������� �� � ������ ����������. � �� �� ���?</p>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>�
��� ��� �������� �����, ��������� ���� ��������� ��������, ���� ����� � ����
���������� �� ������ �����, ����������� �������, �������, ������ ������ ������
� �������. �����������. ������� �� ������� �������� ����������. ��������� �
������ �������� ������� ����� �� �������. ���� ������ ���������� ����� �����
�������, ����� ���� �������� ��� ����������� ������, � �� ����� �� ����, �
������ ������� ����� ������� � ����. ������������� � �� ���� �������� �����
������. ��� �� �� ������? ���� �������� � ������ �� ������ ��� � ���� � ��� �
��� ����� ������ � ����������� ��������� � ������, � � ����� �� ���� �� �������
�, �� �� ���������<span style="mso-spacerun:yes">&nbsp; </span>������ ������,
������� �������������� �������. ���� �����, �������� � ������, ��� ���
���������� �� � ��� ��������. ���� �����. ��� � ��� �������� ����� � �����
��������� ����� � ������ �����, ����� ����� ��� �����, � ��� ���� �� ����
�����, ���� ����������, �������. ���� �������� ��� �� ��������� ��������� �
���� ����� �����, � ��� �� �������� ������ �������, ��� ������������ �����
��������. �, ��� �� �� ������? ����� ������� ����������� ��������, �� ��� � ���
����� � �����. ��� ��� � � �������, ��� ���� ������� ������������� �����
�������, ��� ����� �������� ���� ���������� ������. ���� ���� ����� �������.
�����������, ����� ������� � ����� ������. � �����-�� ��� ��������� ��������
���������� ��� � ������� � ��� �� �����. </p>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>�����
���� ���������� ������������� ����� �� ������ ��� ���-���-��� � ����������
����� � ������ ������� � ���������� ����� ���� �� 15 �����. ����������� ��
������� ����������� ������� ���� �������, ������ ����� ������������ � � �����
�� ������ �� �� ����� ����� ����� 5, ��? ��� ����� ���? �� �����������? � ���,
��� ��� ����������. ������ ��� ���� ��������? </p>

<p class="MsoNormal" style="text-align:justify;text-indent:35.4pt">� �����
�������, ��� ��� ������ ��������� ������ ������������� ���� �����. ������
��������� ������ � ������� �� �� �������� ������� ������������ � �����������
����� ����� � ��������� �������. ��������� � ���������� ������ ���-��? ���? ��
� ���� � ���. ���, ������, �������� ����. </p>

<p class="MsoNormal" style="text-align:justify;text-indent:35.4pt">- ����. ����
�����. </p>

<p class="MsoNormal" style="text-align:justify;text-indent:35.4pt">- ������ �
����������� ���������� ������� � ���������. ���� � ��������, �� ���� �����
����������� � �� ��� �������. �� � �� �� ����, ����, ����� ��� �������
���������� ��� �� ���, �� ���. ����� ������ ������ � ��������. ��� ������ ���
� � ������� ������������ ������ ����� �������� �����. </p>

<p class="MsoNormal" style="text-align:justify;text-indent:35.4pt">���� �
�������� ����. ��� �� �����������, � ��, � ���, � ����� � ������ ���� �������
����������. � ���� ������������ ������� �����. ������ ������� �����, �� ����
��������� ��������������, �� ���� �� ��������� ����������, �� �����������
���������� ����� ������ ���� � �����, ��������� � �������� ����� �� �� ���
����� �������� ���� ������ ������ � ������, ���������� ������������
�������������. </p>

<p class="MsoNormal" style="text-align:justify;text-indent:35.4pt">�������.
������ ������� ����.<span style="mso-spacerun:yes">&nbsp; </span>��� �� ������, ���
��� ������ �������. ��� ����, ������ ��� �����. ��� ������ ����������� ��������
� �������. </p>

<h2>������� �� �������� ����� 1. </h2>

<p class="MsoNormal" style="text-align:justify"><i style="mso-bidi-font-style:
normal"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>��������� �
������. ����� ��� �� ���� ���� � ������ ��� ���� ���������. ��� ���� ��� ���
��������, �������� ��������� �������� �������, ������ ������ �� ������� �������
����.<span style="mso-spacerun:yes">&nbsp; </span>�� ����� ������� �������������
������, ������������ ���� ������� ����� ������ � ������ ����� ���������, �� �
���� � ���� � ��� ����� � ��� ���� �� � ����. ���� ���� ���� � ��� �� ����. </i></p>

<p class="MsoNormal" style="text-align:justify"><i style="mso-bidi-font-style:
normal"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>������ ��������� �
������ ����� ��������� � ������� ��������. �������� �� �������� ���: �</i><i style="mso-bidi-font-style:normal"><span style="font-size:10.0pt;line-height:
115%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#222222;background:white">�
�������������. ����� �������� �������������, ������ ��� �� 5-6 ������������ �
����, ��������� �������, ���������� �����, ������� �������... ������ �����. ���
���� ������� �������� ���������� ���������, ����������� ������ ���� ��������.�</span></i></p>

<p class="MsoNormal" style="text-align:justify"><i style="mso-bidi-font-style:
normal"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>� ������ �������
������ �����, ��� ����� ��������� ����� ������� � ��� ��������� �������. ���
������ ������, ������ ���������������� � ��� � ��� ����� � ������, ������
������� �������� �����. � ������� �������� ���������� �� ��������� � �������
��������, � ��������, ��� ����� ���� ������� � ������� ���������� ����� �
������� ������� ������ �� ������ �� ����. � ���������� ������ ���� �������
������ ��������� �� ��� ��������, ������� ����� ��� ������, ������� � ����
������� �������� ����� ���������. ��� � ������������ � ����� �������. </i></p>

<p class="MsoNormal" style="text-align:justify"><i style="mso-bidi-font-style:
normal"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>������ � ��� ��
������, ��� ����� ��� ���. ������� ����� ������ ��������� ���� ������, ��������
� ��������� ������� ���� ������� � ���� ����, ����� ���� ������� ������������
���� ���������. �, ��, ��� ��� ������� ������ � ����� �������, �� � � ���� ����
����� ������������� � ������� ����. </i></p>

<h2>���������� �������� ���� ������. </h2>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right;line-height:normal" align="right">����� �� ��, �� ��, �� ���, </p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right;line-height:normal" align="right">� ������ ���� �� �����,</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right;line-height:normal" align="right">�����, �� ���� ����� �����,</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right;line-height:normal" align="right">����� ���� ������� ������,</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right;line-height:normal" align="right">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right;line-height:normal" align="right">� ����� ���, �������, �����,</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right;line-height:normal" align="right">���� ������, ������, �����</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right;line-height:normal" align="right">����� ��� ����� ����� ���</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right;line-height:normal" align="right">�� ��� ������ ������ ����?</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right;line-height:normal" align="right">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right;line-height:normal" align="right">����� �������� ���� �����?</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right;line-height:normal" align="right">�������, ����� �� ������?</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right;line-height:normal" align="right">���� �������. ����� �����.</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right;line-height:normal" align="right">����� � ������ ������?</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right;line-height:normal" align="right">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right;line-height:normal" align="right">(�) ����� �.�. (�� �������� �����������
��������������� ��� ���. ��������� �.�. ��� 1.)</p>

<p class="MsoNormal">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;line-height:
115%;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-fareast-language:RU">���� ���� ����� ���. �� ������������� � ���������.
������������ ��������� ��� � �������. �� �� ��� ��� �� ���������, ����� �����
�� ���������� ����� ��� �������� ������, ��� ���, ��� �� ������������ �� ��
�������. ����� �� �� ���������� �������, �� ������ ��������� �� ����, ����� �
����� ������ � �����. ������ ��� ������� � �� ����� � ����� ��� ���, � ���, �
����, �� ����������� � ������ ������ ��� ���������� �������, ������������
���������, ������������ ����� ����� ����� � �������� ���������. ��� �� � ����
�����! ��� ��!!!� </span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">&nbsp;</span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU"><span style="mso-spacerun:yes">&nbsp;</span>�� ������ ���� ����� �, �� ��������� �����,
����� ����� ������� �������, �� ����������. ��� �������� ���� ���. ��� ��
��������� �� ���� ����� � ��� ����� ���� �������, �����, ������ �����������
������, ������ ���������� ������� �������� ��� ����� ������������ �����.</span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">&nbsp;</span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">�� ��� � �� �����, ����� ��� ���
���������� ����� �� ��������� �������� ���� �����. � ����� ���, �������,
������� ��� ����� ��� ��� ���� ����� �����, ������� ����� � ������� �����
���������, ���� ������� ����������, � ����� ������ �����������. ����� ���-��
����������?</span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">&nbsp;</span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">���, �� �� ���������. � ����
�������, ��� ������� �������� ������������, �� ��� ������ �������� ��������
����� ��������. � ����� ���� �� ���� ������, ����� ������, ������� ������ ���
���������� ������� � �����������. ������ �� �������� ���, ���� �������� � �����
������� (�, ��, �� ���� ���� � ������ ��������), ����� �� ������ ������ �
��������� �����, ��������� 50 ����� �� ����� � ������� �� ���� </span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">&nbsp;</span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">&nbsp;</span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">��� �� ����� ������������ �����.
����� �� ��� �� �������, ��� �� ��� ��� ������ ��� ������, �� ����� �������,
��� ����� ���� ���� � ����� ����. � �� ���������� ������������ � ��� ��������,
������ �� ������ ��������� </span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">&nbsp;</span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">�� ���� ������� ���������������.
������ ��������� �������� - <span style="color:#222222;background:white">������
��� �� 5-6 ������������ � ����, ��������� �������, ���������� �����, �������
�������... ������ �����. ������� � ������������� ������������� ���� �����
�������� ��������� ������ ������ ����� </span></span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">&nbsp;</span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">14 ������� ��, ��� ������, ���������
��������� �� ����� ������, �� ��������� � ���� � �������� � �������. ����� ��
�������� �����, �������� �� �������� �� ������ ����� �����, ������, ������
������� � ����� ������ ���� �� ������ �� ����, �� ������� ���-�� ������� ����
����, ��� ������� ������� � ����� ���������� � � ������� �����<span style="mso-spacerun:yes">&nbsp; </span>���-�� �������� ����������� ��� �����. �����
����� �������� ������������ ���� ����� �������. ���� ������� �������, �������
����, �������� ���������� �� �����, �, ��������� ����� � ���� �������, �����
���-�� ��� ���������. </span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">&nbsp;</span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">��������� ������� � �������� �������
�������� ���� ������ ����, ��� � ���������, �� ������� ������ ���-�� �������,
��������� ����� ���������. </span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">&nbsp;</span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">� ������ ���� ����������� ����� �
�����, ��� ����� ������ �������� ��������� ��� ����������, ������ �� ���
��������� ��� ���������. ���� �����- ���� ����� �� ����� ����� �������������� �
�������� �������, �������� � ������. </span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">&nbsp;</span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">- ���������?</span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">-���? � �� ���������, ��� �� ����� �
��������, ��� �� �������?</span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">- �� ���� ��������� � ����� ��������
��� ������� � �� ��� ���, �������?</span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">�� ������� ������ ��������� ����
����, ��� ������ ���� ���. ���, �� ���, �����-�� �������, ������ ������� ��
��� ��� ����� ���, ��� ������ �� ��������� ���� ������ � ������� </span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">- ��� � ����� ������? � ���������
���� ��� �� ����� ��������� ��� �������� � ����������� �����. � ���� � ���� �
���� �����</span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">- �� � ������� �� ���-�� �������
�����, �� ����� ���������, ��� �� ����������� ���� ������ � ��� ���. ���� ���
����. </span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">�� ��� �������� ������ ����� ����-��
������ � ���������� � ����� ����� �� ������� ���� �� �� ��������� ������ ���
������ ��� ����� </span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">&nbsp;</span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">����� ��� ��� ���� ��������� �
�������, ���<span style="mso-spacerun:yes">&nbsp; </span>��� ������� ������, � �����
�� ��� ���� ������� ��� ��� </span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU"><br>
����� ������ �� ��������� ���� � ����������� �������. ������������ ������� ����
� ��������. ��� ���������� �����, � ��������������� �������. �����������, ��
������ ���� ������ � �����, �, �������, ������� ��������� ��� ������, � �����
������ ����� �� �����������</span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU"><br>
� ��������� �� ������ ����� ����.</span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">&nbsp;</span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">� ��� ��� ������ �� ����.<span style="mso-spacerun:yes">&nbsp; </span>���� �������, ���-�� � �������������
��������������� �����, ������ ������ ���� ���� ������ ������, ����������
����������� ���� ���� ������ ������ ������ � ������ �����. �� �� ����� � ����
� �� ������� � ���������� �����, ������ ����������� ���� ����� �� ��������.
����� �� �����, ��� ������� �������� �� �����. ������� ����������� �������� �
��, ����������� ����, ����� � �������. � ��� ���� ������ ������ ����� ��������
� ������������ ������� �� ���������.. ��������� ������, �� ���� ������ ��������
��� ���� ����������� ��������. ������� �� ����� ������.<span style="mso-spacerun:yes">&nbsp; </span>�� ������� ����, �� ��� �� ������ ���� �����.
������ ������� �� ������ �����, �, ������� �������, ����������� � ������� �
�������� �� ��������� � �������</span></i></p>

<h1 align="center">����� 2. �������� �� ������.</h1>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">������ � ������ ����, �������� � �������, ������� ���� �����,</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">�������� ������, ������� ������� �� ������ ���������� �����,</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">�� ������, ��� �������, � ��-�� ������-����� �������� "�
�����"...</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">������������� ���, � �������, ��� ��� �����, �� �����������
�� �����... </p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">����� ������������ � ���� �� ����� �� �����, �� � ���� �� �
���,</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">�������� �� ����� ���������-��������� �����, ������� �
��������,</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">���� ��������� �������� � ��������, � ���� � ��� ������
�����,</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">� ��������� �������, �������, �������, ������� �������� -
����� - �� �����.</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">(�) ������ ������� (�� ��������)</p>

<p class="MsoNormal"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></p>

<p class="MsoNormal" style="text-align:justify;text-indent:35.4pt">�������
��������� ��� �������� ��������� ��<span style="mso-spacerun:yes">&nbsp;
</span>����� � �����������. ������� ���������� ������ �� ������ � ���������
�������, �������, ������ � ������� �, ����� ����������, �� ����� ������ ������.
������, �� ��� ������� � ����� ����� ��������� �� �������.<span style="mso-spacerun:yes">&nbsp; </span>���� ������ ���������� � ����������� ������ �
���������� ���������� �������. �������, ����, �, ��� ���������� � ��������� ���
���, � �� �������� �� ��� ������� ����� ������ � ������ �� � ��� ���, ��� �����
��������� ���� ������� �������, ��� ������� ����� ���������. �� ����� � ����,
������������ �����������, �������� ������ ���� ���-�� ��� ������ ������ ���
������� ������������� ��� �� ����� ��������� �� �������, ��� ���� ����� ������
������� �� ����������� � ���� ���������. </p>

<p class="MsoNormal" style="text-align:justify;text-indent:35.4pt">�� ������ �
��������, ��� ������ ������ �� ���� �������, � ������� ����� �, ������, ���
���� ������� � ��� �������, ��������� ������ �� ������ ��� �����������. ���� ��
������� ������������� ��� �����, ������ ��������� ���� ���-�� �������������, ��
����� �� ��� ������ ����� �� �������������?<span style="mso-spacerun:yes">&nbsp;
</span>� ����� ������ � ����� �� ���, ������� ���� ��������� �� �������. �����
�����, ����� �� ����������� �� ��������� �������, �� ����� �� �������� � ����
������ �����, ����� �������� �� �� �����, ��� ������� ��� �� ��������� ����� �
��������� ������� ��� ����������. ����� ����, ������� ���-�� ����� ����������,
����, ����� ��������� ������������ �� ��� ������ �, ������ ���� ��������,
�������� ��� ������� ����������� ���������� ������� �� ����� �������.
�����������, ��� ��� ���� ��� ��� � �������� ��������. �����, ����� �
����������� ����� ��������, ��������� �������� �� ���� ������� ���, � �����
�������, �� � ��� ����� �������. ��� �����, ��� ����� �����, �� �� ���������,
��? ����� ���� �������, ��� �� �� �� ����. </p>

<p class="MsoNormal" style="text-align:justify;text-indent:35.4pt">�� ��
��������� �� ���� � ���������� ������� ��� ���-�� ��� ����, � ���� �� ������� �
�����. ������������� ������ ������ ����� ��� 50 � ������ �������� � ������
�������. �������, ����� �� ��� ������ � �������� �������, ��� ������ � �����
��������������� ������� �� ����� ��������� �� �������� ����� � � �����?<span style="mso-spacerun:yes">&nbsp; </span>��������, ����� ����� ������ ���� ���� � �.�.
�������� ����� ����� ���, �� �� ��������� ��� ����� �������� ������ �������
��� ����������� � ������ ������������ ����. ��� ����� ��� ��� ���������� �
����������� �������. ��� � ���� � ������ ��� ������� � �������� ������, �������
� ��� ��������. ������ ����� � �������. ��� � ����������� ������� </p>

<p class="MsoNormal" style="text-align:justify;text-indent:35.4pt">����������,
��������. ��� ������� � ��������� ����� �� ����� ���� � ����� ���� �� �����, �
����� � ���. ���������� � ������, ������ ����� ���� � �������� ���������� �
������, �������, �������, ����� � ������� �� ����. ������� �����, ����� </p>

<p class="MsoNormal" style="text-align:justify;text-indent:35.4pt">- �������, �
����� ����� � ������� ����? � ������� ��������� ����� �� ��������� ���������,
��� ����� ���� �� ������� ������ ��� ������, ������� ���, �� �� �������
����������, � �� ��� �� ����� ��� ��������, ���, ���� ��, ��� ����������? � �,
�������?</p>

<p class="MsoNormal" style="text-align:justify;text-indent:35.4pt">- ���,
�������. � ������� ������ ���������� � ���� ����������� ���� �� �����������
����� � ���� ������ �������. �� ��� ������� ������� � ������� �����!</p>

<p class="MsoNormal" style="text-align:justify;text-indent:35.4pt">- �������, �
����� �� � ���� � ��� ��� ����� �������������� � ������� � ��� �����?</p>

<p class="MsoNormal" style="text-align:justify;text-indent:35.4pt">- ���, ����
�������� �����. ���� ������� ���������! �� ������! � ������� ���������� � �
���� ������� ���� � ��� ����� ������ ���� � ����� ���, ����� ������� ��� ��
�������� </p>

<p class="MsoNormal" style="text-align:justify;text-indent:35.4pt">�������������.
������ ������, �������� ������, ��� ������. ��������� ��������� ����� ������
����������� ������. ������� ��������� ����� �� ���� ����, ������� ��� ����
���������� ��������� � ������ ������ �����. ���� �� ������� ��� ���� � �����
������ ��� �� ������, ��������� ���� ����� � ������. ����� ��� �������������
��, ���� �� ������� � ������ ����������� ������� ����� ��������? � ������
������ � ������ �������� �������� ������ � ��� ���� �������� ������� ������, ������
����? ������, ���� ��������� ���� ������ ��� ��������, ��� ��� �����. ���, ��
������ ����, ��������, ������ ����� � ����.</p>

<p class="MsoNormal" style="text-align:justify;text-indent:35.4pt">�� �������
���� ������� ����� ��������. �������� ����� � ��� �� ������. ���� ��������,
������� �� �����, ����������� �����, �������� ������� ���������. ���������
���������� �����, ��� ������ �������� ����� �<span style="mso-spacerun:yes">&nbsp;
</span>�� ����. ����� ���� � �����, ������ � �����. ������� ���-�� ������ ���
��������� � ������� ����� ������ ��� �� ���� �������� ��������� ��� �������
����� � ����� ����������� ������ ��� ���������. � ���� ������ �����
������������� ��� ���, �� ������ �, ������, ��������� ������. �� ������� � ����
������� �������� ���������� ������ ����. ��� ������? �, ������������� ��������
�������� � ������ ���, ����� �������. ����� �� ��������? ������� </p>

<h2>������� �� �������� ����� 2.</h2>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><i style="mso-bidi-font-style:normal">��������, �������� ��� ��������� � ���
������ ���������� ����. ��� � ������ ������� �� ������� � ���� ����
��������������� �����. ����, ��� � �� ������ ������� ������ ����������� ��� �
������ ������ �� ������� �������, � ������ �������� ���-�� ���� ������, ���
������������� � ��� ��� �������� ��� ����� �������������� �� ������ ��������� �
����� �������. �� ������ ������ ��������� ������ ���. �������: ��� ����� ���� �
��������� � ������, �������� � ������ � �������� ����� ������ �� ����, ��������
��� ��?� � ��� ����� ������������ ��� ����� ���� ������. ������ � �� ����, ��
�������� ����<span style="mso-spacerun:yes">&nbsp; </span>���������? ����� ������
���� � ������. � ���� ���� �� ����� �� ��, ���� �� ��� ���� ���� � ��� �� ����
�� �������</i></p>

<p class="MsoNormal" style="text-align:justify"><i style="mso-bidi-font-style:
normal"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>�����, ������, ��
��� ��� ���� �������. ��� ���� ������ ���� ��������� ����������� �� �������� �
�����. �, ��������, ������� �������� ��������, ������ �� ������������� �������
�� ����� ��������� � �������� ����� � ���� �����������. �� ����� ������������
��������� ����� �� ���� ����� ��� (� ����� ������� ��������) � ����� �������
��� ���������, ���� ������ ������ ������ ������� (�����-���� ����� �������
�������� � ������ ������� �� ��� � ��������). ������ � � �������, ��� � �����
�������� ���� ���-�� �� ������ ��������, �� ��� � ��� �� ������ ��������.
������� ��������� �������, ��� ���� ������ ��� ������ ��� ����������� ������
��������, �� �� ���� � ����� �� ����� (�� �����, ������ � ������).</i></p>

<h1 align="center">����� 3. �� ������. </h1>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">������ ������� � ������ ������� ����, </p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">�������� ���� � ����� � �������� � ����,</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">�������� ������ �� ����� � ���������� ����,</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">��� � ����� ������ � ����� �� ����. </p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">������ � ������ �� ����� � �������� �����. </p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">������ ������, �� ���� ������� �����. </p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">����� �� ������, ������� � ������ ������,</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">� ����� ���� ��������� ������ ������.</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">�� � ����� ��� ������ ���� ����-������, �����?</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">�������, � ����� ������ ��� �� � ����,</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">��� �� ������ ����� ����� �� �������� ������,</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">�� �� ������ ��� ������ ������ ������</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">(�) ������ ������� (�� ����������������)</p>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></p>

<p class="MsoNormal" style="text-align:justify;text-indent:35.4pt">��� ����������
�������� ���������. ������ ����, ��������� �������������� ��� �������������
������� ���������� � �� �������� ��� ������� ���� ����� ����������, ����������
���� � ������� ���������� ���, ��������� � �������� � ��� �����.�<span style="mso-spacerun:yes">&nbsp; </span>��������, � �� � ������ ����� ��� �� ������,
������ � ������ ����� ��������� ������, ���� �� �� ��� ����. � ������ �� ��� <span style="mso-spacerun:yes">&nbsp;</span>� ��� ����� �������.</p>

<p class="MsoNormal" style="text-align:justify;text-indent:35.4pt"><span style="mso-spacerun:yes">&nbsp;</span>���-�� � �������� ���� � ��� ����� �������
������� � �����. ���, ���� �� ���. ������� �� �������, ��� �� �������������,
������� ��������, �� ���-����, ��������� ������ ����. ����� ������ ��� �� �����
��������, � ������ ����� ������� �����������, ��� ������������� ��������
��������, � ��� �� ���� ���� ����� ����, �������� ��������� ������
����������<span style="mso-spacerun:yes">&nbsp; </span>����������� ����
������������� ������� � ������� ����������� �� ����. ��� ���������� �����������
�� ��� ���� ������. ������ � ���� ���������� ���� ��, ��������� � ��������,
������� � ��� ���������, � ��� �� �������, ������ � ���� ����������� �� �����
�������� ��� � �������, ������������ ��� ������ ��� ��������� (�������)
���������� ��������. ���� ����� �� �������� ���� �������� �������� � ��� ������
�� � ���� �� ��������, ���� ����� ��� ���������� ��������� �� ����� ��������
�����, ���� ����� (� � ���� ������� �� ��� ������ ���� ������ ����) �����
�������� ��� ������ ������������� �� �����������, �������� ������ ������������
����, ������ ���������� ����</p>

<p class="MsoNormal" style="text-align:justify;text-indent:35.4pt">���� �� ��
������� ����� �������, ����� �� �������� �� ��� ���� �������� �������� �
���������� ��������. �� ���� �������� ������� � ��� ��, ��� ����������� �
������ ����� ��� ���������� �� �������, ��� �� �������� ������, ������ �� �����
�� �����, � ��� ������������ ����� ��� ������ ���, ����� ���� �����, ��� �����
�� ��� ��������� ���������� � ������ �������� ����������. ������, �����������
�� �������� ���� ���� ��������� ������� ������, ��� ��� ������� ��� ����� ����
�� ���������������� ������� 3 ������� ����������. </p>

<p class="MsoNormal" style="text-align:justify;text-indent:35.4pt">���������, ���
������ �� ������ � ������ � ��������� ������ ������� � ��� ����� �������
������� ������������ �������, ��� ��� � ������ �� ���������. �� ��� ��������
������, �������� ������ ����� ������������ � ��������. ������� ������ ���������
������ ���������� ��� ������ ���������� ��� �����, �� �� ���� ����������,
�������, ��-������ ������ �������� ������ ������ ������� �� ��������, �
��-������ ������� � ��� ������ � �������� ��� ����� ������ ����� ������, ������
� �������� ��� ����. � ����� ����� �� ���������� ������ �����, � ��� ��� ��
�������� �� ������� �����, �� ��������� ���� ������, � � �� ������� �������� ��
����. </p>

<p class="MsoNormal" style="text-align:justify;text-indent:35.4pt">���� ��������,
��� ��� ��� ����� � ���� ������ ������ ����, ��� �� ��� ��� ������ ���� �� ���
���������, �� � ����������� ����-�� ������������ ����, ���� �� ������, ��, ���
�������, ���� ��������. � �� �� ������ �������, ��� ����, ����������� �����
���������� �������, ������������ � ������ � ��������� �� ��������� ������ �
������ ������������� �������� �����. �� ������ ������, ��� ����� ��� �������
���������� ��� ���������. �� � ������ � ������������ �� ����� ������, �����
�������, ��� ����� ���� ���, ������, �����������, ����������� ����� ����� 3
���� � ���������� ������� �� ������ �� �������� ����<span style="mso-spacerun:yes">&nbsp; </span>����� ����� �������� ��� ����������� �� ����
����� ���������� ������������� �������������. � ���� ���� ���������� ������
������ �� ����, � ����� �� �������������� ����� �������. ���� �� ��� �� ��� ���.
����, ��� �������� ����� �������������� ���-�� �����������, � ������ ��������
��������, ���� �� � ������ ���� ���������� �� ����, ��� ����� ���. </p>

<p class="MsoNormal" style="text-align:justify;text-indent:35.4pt">�� ���, ���������
� ����, � ����� ���������. � ����� ���! ��� ��� ��� ��������� � ������ ������
��������� ������ � ������������. ������, ��������� ��������, ���� ��������� ���
���� � ���. </p>

<h2>���������� ���� ���� ������</h2>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">������, ���� ��������, ��� ���� ���-�� ������� � �������� ��
������� </p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">(�) ��������� �������� � �������� ����������� �����������
��� ���. ��������� �.�.</p>

<p class="MsoNormal" style="text-align:justify;text-indent:35.4pt"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;line-height:
115%;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;color:black;background:white">&nbsp;</span></i></p>

<p class="MsoNormal" style="text-align:justify;text-indent:35.4pt"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;line-height:
115%;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;color:black;background:white">��
����������� �� ���������� � �������. �� ���������� �����, �� ��������� ��
���������, �� ����������. ������ ��������� ���-�� ������. "�����
�����������", "����� �������", "�������, �� ������ ���
�������?" ��� ��������� � ��������� ������. ����� ���� ����� ���� ��, ���
����� �� �������, ������ ����� � ������. "��� ���, ������. ��� ����.
���...? ����...?", "��� ��� �����, �����?!" ��������� ������
����� ���� ����� ������: "��� �� ������?" �� �������� �� ���� ����.
���� � �����.</span></i><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;line-height:115%;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;
color:black"><br>
<span style="background:white">��� �������������� � ������ �����, ����
����������� �� �����, �������� �����. ����� � ���� ���������� ������ ������,
�������� � �������. �� ��������� ����� �����, ����� ���������� ��, �� �� ������
������ �������. ��� ���������� ��� ������� � �������. �� ���������� ������ ��
���. ����� ��������, ������ ������, ��������� ������� ���������� ���������. ��
���������� �����, �������� � ��������, ����� � �������. �� ������� ����� ��� �
��������� �������, � ��������� ������. ������ �������� ��������� ������-��
������������� �������� �����. ���-�� �� ���. ��� � ��� �� ���? �� ����� ��
�����, �� ��� ����������, ��� ���������. ��� �� ������, �� �������� ��� ����
����� ���������. ���������� �����.</span></span></i></p>

<h1 align="center">����� 4. � ������� �����.</h1>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>����� �
�������, �����, ���,</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">����� ��� ��� ����, �����, ���...</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">�� ������, ������ ������?</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">� � ��� - ����� �, �����, ���...</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right;line-height:normal" align="right">(�) ����� �.�. (�� �������� �����������
��������������� ��� ���. ��������� �.�. ��� 2.)</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right;line-height:normal" align="right">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>�����������
��� ���� ����������� ��� ������������� ������������. ����������� ����� � ����
������� ������ �� ������ � ������ � �������������, �������� ��� ��������
������� � ���� ���-������ �������, ���� ����� ���-�� � ������ �������.<span style="mso-spacerun:yes">&nbsp; </span>�����, ���� � ����� �����. ������� ��������
�� ������ ���, ���������, �� �������� ���������� ��������� ����������, �������
��������� ������, ����� ������������ � ������ �����, ������� ����� �� ������ �
���� ���������� ������ ����. </p>

<p class="MsoNormal" style="text-align:justify;text-indent:35.4pt">������, ���������,
��� ������ ������. �� ����� ��� �������, �� ��� ��� �� ������� �����녻,
����, ������� �����, ���� �� ���-���� ���셻 - ������, ��������� ����� ����
��������� ������ ������. �� ����� ������ ���, ��� ������, �� ���������� ����,
������� � ���������� ������ ����� ������� ������. � ����� ������, �� ������
����������� ���������� ������ ���������� ���� ���� ���������� ����� � ��
�����������, ����� ����������� ����-������ ������. ������� ������, �����, ��� �
��� ������������ ���� ���� �� ��� ������. ������ ������ ��� ����� ���������
��������� ��� �������, ��������� ����� �����������. ��� �� ������������ �����
�������, � ��� ��� ������������ �� ��������� ��������� ������, ����� ����� 3
������ ������� � ���� � ������. </p>

<p class="MsoNormal" style="text-align:justify">- ���-�� ����� ������ ����
����������� � ����� ��������� ���� ����������. � ���, �� �������� �����������
������� ���� ���������� ����� � �� ��� ������ ������ ���� �����������. </p>

<p class="MsoNormal" style="text-align:justify">- �����, ��� �� ������ �� ����
�������? � ������ ���� ���������. ���� �� �������������� �����������, �� �����
������� � ������ ������ ��������� � ���������� ���</p>

<p class="MsoNormal" style="text-align:justify">- � �������� ����� ����������
���� ��� �����. �� � �� ����, ��� ��������� �� ����������� ������. ������ ���
��������� �� ������� ����� � ���.</p>

<p class="MsoNormal" style="text-align:justify">- � ��� �� ����������? � ��� ���
�. ��� ������� ������� �� �����������, ������, ���������� �����. � ���� �����
����, ��� ������� �����, ������ �����, ����� ��������� ������, �� ������
���������������� ���������� � ������� ���������� ������� ���� ���������� ����
�������� ���������</p>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>�����
������, ����� � ���� �� ������ ��������� � ����������� ���������� � ��������
��������� ����� ����� � �������, ������������� ������ �������� ����� � �����.
������ ��������� ��� ��������� � ������ ����� ���� ������������ ����� � ����
��������. �� ��� � ��� ����� ��, ��� ������ �� ��� ����� ����� �������
��������, � ��������� � �� ���� ����������� ������ ���������� �� ���� ������
����� <span style="mso-spacerun:yes">&nbsp;</span>�� ������� ����, �����������
������������� ����� ��� �������, � �� �������� �� � ���� ���������� ��
������������ ������, � � ������ ������ �������� ���-�� �������������
�����-����� </p>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>���������
������, � �� ����������� �� ��������, � ��������, ����� ����� ��������
������������� ��������������� ���-�� ������������ ��������� ������ �������
�������. ������������, ��� ����� ���� � ���� ������ ��������� �������, �� ���
�, �������, � ������ ����������. ���������� ������ � ������ ����������
���������� ���� ������ ���������, �� �� ���� ��������� ��������� �������������
�� �������� � ���� ����� ����� ����������� �����, � ��������� �� ������.
��������� ���� ����� � ���� ������� ������ ����! ����� ���� �����䅻 </p>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>���������,
��� � ��� ����������� ������ ������������� � ����������. ���, � ��� �� ������
���������� �������� � ��������� �� �����, �� ��� ������� ��������� �����-�����
���������� ������� ����������� ����. ������ ����� � ������ �����������
������������� ���������� ������� � ����������, ������ ������� �������������� ��
��������� ������ ��������� �������, ���������� ������ ���� ������. �� ����
��������� ��� ���� ������, ��� ��� ��������� ������� ��������� ����������� �
������ ��������. ����� ����, ��� � ��� ��������, � ������ ������������ �����, ���,
�� ����� ��������� �� ���� � ��� �������� ����� ����������� ������ ���
���������� ������ ������ � ���������, ������� � �����, ������������� ���������
� ������������. ������ ��������������� ������� ������������ ��������������� ��
����������, ����������� �������������, ������ ���������������� ������������� �
������� �� ������������� � �����������. �� ��� ���� ��� ������ ���������, ���
�������� � ���� ����.</p>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>��
������ �����, ��� ��� ��� ������ ��������� �������� �� ������ ����� ������
����������� ��� ����� ��� ��� ������, ����������� ������ ��� ����� ����������
�����������. ����� � ��� � ���������-���������� ������ ������ ������ �
������<span style="mso-spacerun:yes">&nbsp; </span>� ������ ��� ������. </p>

<p class="MsoNormal" style="text-align:justify">- ��� ������ ���� ��������
�������� � ���� ������ �������������� ���� ��� ����������� ���� ������
�������. </p>

<p class="MsoNormal" style="text-align:justify">- ��� ��������� ����� ���� �
���������? </p>

<p class="MsoNormal" style="text-align:justify">� � ��� ��� ��� ����? � �����
������� � ����� ���������� ������������. </p>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>�������������
� ������������ ����� � ����� � ������� ���������� ���������. ��� ������ ����� ��
������������ ���������. ������ ������� ���� ��� ���������� ��������
������.<span style="mso-spacerun:yes">&nbsp; </span><span style="mso-spacerun:yes">&nbsp;</span>���������� ��������� ������ ������� ������-��
������������ ������������, ������� ���������, ������������ ������� � �������
���� ������ ������ ���������� ����� � ����������� ����һ. ����� � �����, ���
���� ����� ����һ, ������� ��������������� ��� �������������� ���������
������������ ���-��-��� � ���� �� ����� ��������, ������� ��������� �����
������������, ������ �� ��� �� ���� ������������� ����� ������� ������������ �
��������-��������� ����� ������. � ����� ������ �� ���� ������������� (�������
���������� ����� ������� ������ � ���� �����-�� �������� � ����� � ��������),
��������� � ������� ������������� ���� �������������, ����� ��������������
����� ������ ������ � ����� ����������, �� ��� ��� ����� �����. � �������
���� ����� ��������, ��� ��� � ����� ������� ��� ����� ������������ ���� ���
������, ��� ������. </p>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>����
�� � ����� ������� ���� ������, ��������������� ����������� �, ������ �������
���������� ��������� � ����� � �������, �� ��� ���� �� ������ ���������, ���
�������� ������ ������ � ����. � ������������. � ��� ��������. �� � ��� ���
���� ������? ������ ��������, ��� ���� �� ���������� ����� ����� �� ��������
�������� ������������� ����� ������ ��������������� ���������� ��������� �
������ ���� ��� � ����� ����� �� ���� �� ���. � ������ ����� �� ���� �� ������
����� ������ ��������� �������� � �����, ������� ��� �������� ������ ������� ��
������� ����������, ��������� ������� ���� �����. ��������. ��������� ������
�������� ���������������� ��������. ����� ����, � �� ���� ������ ������
��������. �� ���� � ������� ��������� �� ������� ���, �� ����� �����,
�����-������ ������� ������ ��� ��������. �� ��� ��� ��������, ���� ������ �
������ ���� �� ����������������, �� �� ���� �� ����� �������� ��������, � ��
��������, ���<span style="mso-spacerun:yes">&nbsp; </span>��������� ������, � ���
����� �������� ������, � ��� ������, ��� ������� ������� �����������. ������,
��� ���������� ��������� ����� ��������� ����� � ������� ���� �� ����������
������ �������������, � �������� ���� ������ �� ����� �������� ���������. �����
�� ��� ��� ��� �� ��� � ��������, �������, ������ ������� � ������ �����.
������ ���������� �����-�� ������������ ����, ���� �������� ��� ��������, ��
��� ��� ������ ��������� ������� ��������, ��� ����� ���� �������� �� ������� �
������������ � ��������. ���� ������� �������, �������� ��� ������ ����� ����
��� � �������, ��� � � �������� ���� � ��� �� ��� ������� �� �����
������������, ���� ������ � ����, ������� �� ��� �������� � �������� ���������
���� �������� � ������. �� �� �����������</p>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>���������
��������� � ��� ��� �������� ������ ������������ ����������� �����������, �
����� ��������� ����� � ������ ��� ������ ������ � �� ������ ��������� ������,
������ �����, ����������� ����������� ��������� � �� ����� � �����������
�������, ��� ��� �� ���� � ������ ���� ������� ��� �����, �� ���� � ������ ����
���������� ����� ������ �����. ���� � ���, ��� �� ����� �� ��� ������� �����
���������� ������������ �������� �����, ���������� �������� ������������������
����� ��� �����. ��� ����� ����� ����������� ����, ���� ��� ��������� �������,
�������� ���������, ��� �� ������������ �������� � ���������� �� ����� �������,
��� ����� ��� ������ ����������� �������� ��� �� ������ �� ���� ������, � � ��
��������� �����������.<span style="mso-spacerun:yes">&nbsp; </span>������ ��� ��� �
������� �������� ����, �� ����� ����� ������� � ��� �������� ����� � ����
�������� � ��� �������. </p>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>�������
���� �������� ������������� ����, ���-�� �� �� ������������? � �����, ��� ��
������, ��� ������� ���� ������ ���� � ��� ��� ��������� ����, �� �� ������� �
����� ������ � ������������, ��� � �������� ��� ������ ��� ����, �����
��������� ��������� ����. � ��� ����� ��� �� ��� �� �� ������ ��
��������������, � ��� ������ � ������ � ��� ���� ���������� �����������. ������-�������
������, ����� ��� ��� ����������� � ���� ����� ������ ��� ���� �������� ����,
���-�� �������, ���-�� ������������, ���-�� ������������ ������ ������ ����
��� ������ ���. � ������ �� ���� ���. ������, ����� � ���� �� � ��������������
��������, � ��������� �� � ��� ��? � �� ���� ���, ������ � �� ����� ����� ����,
�������, �������, ������ ����, ���� ��� �������<span style="mso-spacerun:yes">&nbsp;
</span>�� ���� �� ������ �� �������� ��� ��� ������� � ������������ ���������
��������� ��������. ����������� ��� �������� ����� � �����-�� �������� ������
�� ���� �����, ��� ���������� ��� ��������� ������ � ���� �������� ������ �
��������� ���� (���� �������� ��� ���) ����������� ������ ������������ � ������
�������������. � ������������� �� �� ����� �������, ���� ��� ��� �����. </p>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>�����
���� ������ �� ������, �� ������� ��� ����������� ���� ��� �������. ��-������
���� ���� ��������. ���� �������, �������� �� ����, ����������� ��� ��������,
������������ �� ���� � ����� ��������� � ���������, ��� ��� �����������
���������� ������� ���������, �������. ��� ���� ������!�. ������ �������������
������ � ��� ���������� ���-�� ���������� ���� �����. ����� �� ��� �� �����,
��� ���� ����� ����� ���� ������ ��� ����� �� ���������� �� ������ �������� �
�������, � ������ ��������� ��� ������� � ����������� �������� �������� �������
� ��� �������. � ����� � �� ������ ����� �������� ����� ������������, �� �� ��
� ���� ����� </p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<h2>������� �� �������� ����� 3.</h2>

<p class="MsoNormal">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><i style="mso-bidi-font-style:normal">���� ���� ���������� ������� ��� �� �����, �
�������� ����� ��� ������ � ��������� ��� ������� ������ ����������. ��������,
� �����, ��� ��������� ���� ����� �����. � ����� �� ��� �� ����������. � �����
�� �������� ��� ����, ������, ��� ����. ������ ���, �������� ������. ���� �����
�����, ��� � ���� �������� ���� � ����� �������� � � ��� ���� ����� ������. �
������ ���� ���������, ����� ����������� �� ������, ����� � �������, � ��� ���
������� � ������ ����� ���� � ������ � ��� � �����������. �� ���� ������������,
������ ���� ��� ��� ���� ��� �������� �� �������. </i></p>

<h1 align="center">����� 5. ����� �������� � ������ ������. </h1>

<p class="MsoNormal">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>����� �������,
����������,</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">��� ��������, ���� �������,</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">��� ����������� ����������,</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">��� �� ��� ��� ����������?</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">����� ����� ���, ��� ���������,</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">���� ������� ���, ��� ��������,</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">��� ����������� - �� ���������.</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">��� �� ��� ��� ����������?</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right;line-height:normal" align="right">(�) ����� �.�. (�� �������� �����������
��������������� ��� ���. ��������� �.�. ��� 1.)</p>

<p class="MsoNormal">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>�����
����������� ����, ����� ������� ����, ������ �����, �������� � �������� ������
� ���������� ������� �������� ��������, �������� ������������ � ��� ������
����������� �� ������, ��� ��� ���������� �������� ����� �������� ������� ����
�����. </p>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>�������.
�� ����� ����� �� �� �����������, �� �� ����������, �� �� ���-�� ��� ���� ���
��� ��� ��� �������� ������ ������ ������������� � ��� ��������: �� � ��� �
���� ��������, ���� �� ���� ��� �������� ���������� ��������� �� ������?!?�.
��� ���� ������� ����� ����� � ��� ���������������, ��� �� ������ �� �����
���������� � ������������,<span style="mso-spacerun:yes">&nbsp; </span>�������
������ � ��������� ���������� ���� � ������ � ������������ ������ ������� ��
��������, ���� ���� � ������ � ������ �� ����� �������� ���. �������, ��� �����
����� � �� �������� ���������� � ��� �� ����� � ��������, ������� ������������
������ �����.</p>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>�����
�� �������� ����, ��� ��� �������� ����������� ���� ������ ������, ���
����������� �� �������� �������, ��� ���� �� ������� �� ����� ��� �� ����. ����
�� ������� �� �� �����-�� ������, �� �� �����-�� ������������, �������� ��
�����. ���� ��� �� ������������, �����, ��� �� ������������ � ��� ������, ����,
��������, ��������� � ������ ��������� ���������, ������� ����������� ��
�������� ������ �������. <span style="mso-spacerun:yes">&nbsp;</span>�� ������������
��� ���� �����, ����� � ������ � �������� ����� � �������, �� ������������ �
�����, ����� �������� ������ ��������� ��� ���������������� ������ � ��������
������ �������. ���� ���� ������</p>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>�����
���� ������������ ����� � ������ � ��� ��������� �� ��������������� �
����������� ������ �������. ����� ���-�� ���������� �� �� ������ ���� �������
�� ������, ����� ������ ��������� � ���������� �������� � ��������
��������������� ��� ������ ���� � ������ ��� ���!�. ���� ����� �������� ������
������� ������� �� ������ ���� ���� ������� - ��� ������ �������������
����������; ���� ������ ����� �� �����. ������ � �� ������!� <span style="mso-spacerun:yes">&nbsp;</span>�����������, ��� �������� ��������� ��������
������������� ������� ����� ������ �������� �� ����������, ����� �������������
�� ������������ ������, ���� ������� ��������� � �������� �� � ���� ��?�. �����
������ ����� ���� �� ����, ��� ���� ������� ��! ��� �� �������!�. ����� ��
�������� ��������� ��, ��� �� ������ ���� ����� �� �������, ��� ������� ����,
��� ��������� � ������� ��������� ������, ������� � ������ ������ � ����� ���
�������� �� ������. </p>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>�����
������� ����, ������������� � ������ ������. ����� ������, ����������� �����.
��������� ������������� �� ������ � ���������� �������� �������� �� ���-���
��������. ���-�� ��� ������, ���-�� ���������� ���������, ���-�� ����������� ��
������ � ������. ������, ���� ������������, �� � ������ ��� �����-��
����������, �� ������ �� ������ ������ �����. �� � ���� ���� ������ ���
�������� �������� �, � ���������������� ������, ������� � ���. �� �� ����� ��
���� ������������ ������ � ��������� ����� ������� � ����, ����� ���� �������
����������� � ���������. ��� �������� ����������� � �� ������ � �� ������ ����
������ ����������. </p>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>���������
������ ����������� ��������� � ����� � ������������� ������������ ��������,
����������� ��� �� ������. ����� ��������� ���� ����� � ������� ������� �����,
�� � ����� ��������� ���� ����� ��� ������� �����. ������ ���� ����� � ������
�������� ���-�� �� ������ � ��, �� ��������� �������, ��������: ����, �����
���. � �� ����, ��� �����������. ����� ������ ������ � �������. </p>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>�
�����-�� ������ �������� � ��������� ������� ���� ����, � � ���� ������� �����.
������� � �� ����, ��� ���-�� ������ ���� �� �����. ������ ���� ������
���������� ���������� ��� ������� �, ��� ��� ����������, � �������� �����������
��� � �����. </p>

<p class="MsoNormal" style="text-align:justify">- ����? �����, ��� ��?</p>

<p class="MsoNormal" style="text-align:justify">- ��, ��� � � � ���� ������
��������� � �����������, � �� ����� �� �� ����, �� �� �����. </p>

<h2>���������� ������� ��������� ��� �����</h2>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;
font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-fareast-language:RU">��� ��� ���������� ������� �����. �� ���������
����������� � ����, ��� ����� ��������� ������ ������� �����. �� ���� �
�������� ����� � ������� � ������ �������.<br>
- ����, ��� ����� ���� ���������. � ����, ��� �� ���������� � ����... �� �����
����� ��������. � ������ ���� ���������� �� ���.<br>
- �� ��� � �� ������� ���, ��� ���������?<br>
- �������, �� ��... - ����� ���������.<br>
- ����� � ������ ����� ��� �����?<br>
- ���, �� ����. � ����, ����� �� ��� ���, �����, ������ �������� �� ��������,
�� ����������. �� �������?<br>
- �������, �����.<br>
<br>
�������, �������� �������. ����� �����, �������� �� ��������. ����� � ��� -
������� �������� ���������, ��������� ���������, � ������ �� ������� �� �����.
��������������� ��������, �� ��� ������� ������� �������. ��� �������
����������, ������ ���.<br>
- �����, ��� �� ����� �������?<br>
- ������, � ������ ���� �����, ��� ���...<br>
- ����� ���� ��� �������, �� ��� �� ��� �������� �� ����.<br>
- ��... �� �� ������, � ���������! � ������� ����.<br>
- ������ ����������, ����� ���� �������� ����! �����, ������ � ������!<br>
<br>
��� ��� �������. �� �� ������������ � ����� � ������.<br>
- ���� �� ������? ��� ����� ������ �������.<br>
������� ������� ����������:<br>
- ��, ��� ��� �������� ����, ��� �� ����!<br>
����� ��������� � ���� �� ���:<br>
- �� ����, ����, �� ���������� � ���!<br>
�� �������� �� ��������� �� ����� �����. ��� ��� ��� �� �����������.<br>
- � ��, �������? �������� ���������� ��, ��� ��-�� ��� ������� ������� ���
����?<br>
����� ������������ � ���� ���� �������� � ��������� ����.<br>
- � ������ ������ ������������ ����� ����� � ����� ��������, � �� ��������, ���
�������� �� ������ �����.<br>
- ���� ��� ��������. �������? �������� ���� ��� � ��������� �����! - ��������
���. �� ���� ����, ���� ������ ��� ��� �������.<br>
- �� ���������, ���, � ��������, ��� ������� ��� ����. �����, ��� ��� ����� -
������� �� ���� �����, � �� ���������. �� ����������� ��� ������.<br>
����� ����������� �� ������� �� ����� �����.<br style="mso-special-character:
line-break">
<br style="mso-special-character:line-break">
</span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;line-height:
normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;
font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:&quot;Times New Roman&quot;;
mso-fareast-language:RU">��� ���� ����������� ����������� � ����������������
����� ������. � ������ ��������� ����� ����� ������� � ���� ����� �������? </span></i></p>

<h2><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;
line-height:115%;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;color:windowtext;mso-fareast-language:RU;font-weight:normal"><br>
����� ��������� ���� ����� ����������� ��������� ������� ��������� � �����, �
��� ���� �������� �������, �������� ������. ���, ��-��������, ��������� ��. �
����� �������, ��� ��� �� ������ ������ ����� �����. ������ ����� ���������
����������� � ������ ��������, �� � ����� ����� ��������� ��������.</span></i></h2>

<p class="MsoNormal"><span style="mso-fareast-language:RU">&nbsp;</span></p>

<h1 align="center">����� 5. ����� �������� � ������ ������ (�����������)</h1>

<p class="MsoNormal" style="text-align:right" align="right"><span style="font-size:10.0pt;line-height:115%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
color:black;background:white;mso-ansi-language:EN-US;mso-bidi-font-weight:bold" lang="EN-US">Ab
exterioribus ad interiora� (</span><span style="font-size:10.0pt;line-height:
115%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:black;background:white;mso-bidi-font-weight:
bold">�</span><span style="font-size:10.0pt;line-height:115%;
font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:black;background:white;mso-ansi-language:
EN-US;mso-bidi-font-weight:bold" lang="EN-US">) </span><span style="font-size:10.0pt;
line-height:115%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:black;background:white;
mso-bidi-font-weight:bold">�������</span><span style="mso-ansi-language:
EN-US;mso-fareast-language:RU" lang="EN-US"></span></p>

<p class="MsoNormal" style="text-align:justify">- �����, ��� �� �! � � ������
������� ������ ����� ������� � ������. �� � ��������� ����� ������, ��� � ��
����� �� ������, �� ������� ����, �� ������� � ���, � �� ��� ����� �� �� ����
��������� ������, ����� �������� �� ���� � � ���� ���������� ������. </p>

<p class="MsoNormal" style="text-align:justify">- ������������ - ���������
����������� ���������������� ����. ���� ��, �� ������ ��� ����������. </p>

<p class="MsoNormal" style="text-align:justify">- ���, �� ��� �� �, ����� � �����
������, ���������. ��� ������� � ����������� ����� ���� ��������������. �������
�������� ������������� � ���������� �� �����, ������ �������������� ���������,
������� ������ �������� � ����</p>

<p class="MsoNormal" style="text-align:justify">- �����, ��������, ���-�� � ����
�������. �� �����������? � ���������, ����������. � ������������� �� �����
����������� ���� ���������, ������� ����, �������</p>

<p class="MsoNormal" style="text-align:justify">- ���, � ����� ��������! ���������,
�����, ���, ���� ������� ������ �� ��� ���, �� �� � ������� ������ - ��� ���
�������. ������� �� ���������� ���� ��������������</p>

<p class="MsoNormal" style="text-align:justify">- �����, ��������, ����� �����-��
������. � ��������� ���� ����� �������� � ������ �������. � ������, ��� �� �
��� ��� ������ �������. �� ��������� ����������, ����� ���� ��� ���������� �
���� �������� </p>

<p class="MsoNormal" style="text-align:justify">- �� ��� �! � ����� ��� ��
����������� ������ � ����, ��� �� ������� ���, ��� �� ������� � ����� �����
��� ������ �������� ��������, ������ ������, �����, ��� ���� ������� ����
�����������. ����, ��� ����������? ��� �������! � ���� �� � ���� � ����
��������� �����, ����������� ������� �� ����� ��������, ������� ������� �
�����, � ������� ����� ������ ��������� ������� ��� �������. </p>

<p class="MsoNormal" style="text-align:justify">- �����, � ������, �� �� ����
����������. ��� ����� ������! � �������, ��� ��� ������ ����������
������������</p>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>��������,
� ������ ���������� �����. ������� ���� � ���� �� �������������, �� ��������
��� �� �����. ����� ��� �������������. ������ �� ���, �������� � ������,
��������� ����� � ��� ����� ����� ������ ������. �� ������. � ���� ��� ������
������� ������ ������. � ���������, ����� ������ �� ������ ������� ��������� �
�� ��� ������������ ��� ���� �������.</p>

<p class="MsoNormal" style="text-align:justify">- ���, ������. � � ����� �
��������� ������, ��� �� ���� �� ������� �������� <span style="mso-ansi-language:EN-US" lang="EN-US">deja</span><span lang="EN-US"> </span><span style="mso-ansi-language:EN-US" lang="EN-US">vu</span>, �� ��� ������� �� ���
��� ������ ������� ��������� �� ��������.</p>

<p class="MsoNormal" style="text-align:justify">- �� ����� ��������, ���������? �
���� ��� � �������� ���������� �� ����, �� ����� �� ���������� ���� </p>

<p class="MsoNormal" style="text-align:justify">- ���</p>

<p class="MsoNormal" style="text-align:justify">- �������! ��� � ������ �����
���� �������. ������������ ���������� � � ��� ����� ������ - �������� ��� ����
������� ������, ��� ���� � ��� ����� ����������� ������� � ��������
������������ ����������. </p>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>�������
�������. ����� ��������� ����� � ����� ������� ������������� ������� ���� ����.
���������, � ��� �� ����� � ��������� ������? ���� �� �����������, �� �����,
����� � ���� ��� ������������ �������� �� ���������� �� ����� ����� ����. ���
�������� �� �����, ���� �� ��� �����-�� ������� �� ��� ���� � ���� ��� ��? ���
� ������� ������? � ��������� ��� ����������� ������ ������ � �� �������� �����
�� �������� ���, � � �������� ���� �������, ������� ������� ����� ���������� �
����� ������� � ���������� � ������ �����. </p>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>����
��� �� ������� ���� ���� ����. �� ���������� � ������<span style="mso-spacerun:yes">&nbsp; </span>� ������. ����, ������� ���������� ���
���������, �������������� ������������� ������ �� ��������� ��������,
������������� �� ���� �������. ������ ������������� �������� ������, ������
����� ����� �� �����, ������ �����, � ������ ����� ������ ��������������� ��
����� ������ �������� </p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<h1 align="center">����� 6. ������ ��� �������������</h1>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">������� ����� � ������� ������,</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">������ ����� �� ����� �� ����,</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">����� ������. ��� �� �����.</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">�������� ���-���� ����� - �����.</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">����� �� ������ ���, � ����� �� ������,</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">�� �� �������� ����� ������.</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">� �� ���� ������� � ���������,</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">� ���� ���� ������ �� ����.</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">������� ��� ��������� ��������,</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">������� �������� �������� ������...</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">���� ��������� ������� �������.</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">������. ���������. ������ ��������.</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">(�) ������� �.�. (�� �������������)</p>

<p class="MsoNormal"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></p>

<p class="MsoNormal" style="text-align:justify;text-indent:35.4pt">�� ���� ���
����. ��� �������� ���� ����� � �����, ��������� �� ����. ����� ���������������
�� ���������� ��������, �������� ���� �������� �����, ��� ����� ����� ���������
������� �� ���� ������������ ������� ���� �� ���������. ������ �������� �� ����
������ �������� �������� �������� � ����������� ������� ���������, ������ � ��
�������. �� ���� ������ ������. ���� ������, ��������� ���������, �������
�������, ������ ������ ���� </p>

<p class="MsoNormal" style="text-align:justify;text-indent:35.4pt">���� ����� ��
���� �������� ����� � ������ ������ �����, ���, ��������, �� �������� �����, ��
�������� ��� � �� �������� ��� ��� �����, ������ ������ �� ������, ��� ���
�����, ��� ���-�� ���� ������, � ������������ ����� ��, �� ��� ������ ����
���������, ������� ���������� ���� ������ ����-��. </p>

<p class="MsoNormal" style="text-align:justify;text-indent:35.4pt">�� ����
�������� ������ ������� �������� ��� �������, ��� �������� �� ��� ����� � ����
�� �� ��� �� ������������ ������ �� ��� �����, �� ������ � ������ �����,
������, ��� ��� ����. ���� ����, ��� �� �� ������ ������. ���� �����������
�������� - ������� ���, � �����. �� �������� ������ � �����.</p>

<p class="MsoNormal" style="text-align:justify;text-indent:35.4pt">����, ��� ���
����� �� ����. ��� ���� �� ������� -������ �� � ������. ���� � ����. ����, ���
���� ���� ��������. ��� ��� ������ ������, � � ������� ������ �����. � ������
������� � ����� ������ �����, �������� �� �������� � ������, ������ ��������
���� </p>

<p class="MsoNormal" style="text-align:justify;text-indent:35.4pt">������, �
������ ������� ����� ��������, ������ ����� ��� ������. ���������� ������ ��
�����, ���� ������ �� ��������������� �� �����, ������� � ������<span style="mso-spacerun:yes">&nbsp; </span>����. </p>

<h2>����������. ����� ���� ���� ����������</h2>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">�� ��� �� ������ ����� � ������� ��
������� ���������. ��������� ��� �������������� �� ����������� ����, ���� ��
�����, �� ����� �������� ����������. �� ���� ���� ����������� ��������. ���,
��� ����� ��� �� ����. ������� �������, ������� �� ���������� ��������� ����
������� �������. ����� ������ � �������� �������, ��� �� ������� �� ��� � ����
���� ����������. ������ �������� ���������� ����, ������� ������� �������, ��
�� �� ������� ����� ��������� �����, ������� ����������� ����, �� �� �� �����
����� ���� ������ �������� � ��� ��� ����� �� ������ �������. �� ��� ���
��������� ������ ������� �������� ������� � ����� ���� �������� ������� �
�����, ����� �� �� ���� ���� � �������� ������� � ������ ��������������
��������. �����, �� ������ �������� �� ������ ����� ������-������ ���������.
������ �� �� �������� �� � �� ���������?</span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">&nbsp;</span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">����� ���� �����, �������,
������������, ����� �� ��������.<span style="mso-spacerun:yes">&nbsp; </span>��
��������� �� ���������� (� ��� ����� �� ������ ���������) �� ����� ���������.
��������, �� ������ ���������� ��, �� ��� ���� � ��, ��� ����� ��. �����, ���
����� ��� �����, ����� ��� ��� ����� ���������� ��������, ����� ���� ����������
��� �� ������� �� ���� ����������� �������� � ��������� �������� �� ��� �
������ ����������� � ���. </span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">��� ������� �����, ��� ������ �
������������ �������� ��� �� ������, � ���-�� ���� ��������������. �� �����
���� �� �����, ���� ��� �� �������� - ������ �� ������ ����� �� ���� ���������?
�� ���, ��� ���������?� �� ���������� � ��������. �� ��� �� �������, ��� ������
�� ���� ������� ���� ������� ��������, �� �� ����. ������� �� ����� ����� �
������ ��������, ��� ��� ��������� ����� </span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">&nbsp;</span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">���� �� �����, ��� ���� ���-�� ������,
�������� �� �� ������ � ������, ��� ������� �� ���. ����� ���� ��������� ����
������� �������������, ������, �� �� ������ �����, ��� ��� ����� ���������� ���
����. </span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">&nbsp;</span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">������� ������� �������� ����
����������� � ��� ����� ��� ���� �����. ��������� ���������� �� ����� � ���,
������, � ����� �� ��������� �� ���� �� �����, ��� �������. ��������� ��� ��
������� � �������� ���������. ��� �, ��� ����� ����� �� � ���������� ������
������ �� ����</span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">&nbsp;</span></i></p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;text-align:
justify;line-height:normal"><i style="mso-bidi-font-style:normal"><span style="font-size:12.0pt;font-family:&quot;Times New Roman&quot;,&quot;serif&quot;;mso-fareast-font-family:
&quot;Times New Roman&quot;;mso-fareast-language:RU">�� ����� �� ����. ���, � ����� ����
�����, ����� � ����� �������� ������ ��������� �������� ����� � �����������
���������. �� ���������� ������ ������ �� ���� ���� � ������ ���� �����</span></i></p>

<h1 align="center">����� 6. ������ ��� ������������ (�����������)</h1>

<p class="MsoNormal">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>���
�� �� �������, ������ �������� ��������? ���, �� ��� ��� �� �� �������, ����
�� �������, � ������� �� ������� ��������, � �� ������� ������ �������� ��
������ � <s>��������</s> ������� ������� �� ����������� ����������� ���������,
���� ������� ����� � �������� � ������ ������ � �����-�� ������ ��������
��������, ������ ��� � ������, �������� ��� ������ ������� �������� ����� ����
����� ����������� ������ ��� ����������? � ����� ����� �� ���� ������ � � ��
���� � ������. ��������, ��� �� ����� ������ � �� ��� ����� �� �����
����������� ������� �� ������������, ��, ��� �������� ������� �������� �<span style="font-size:10.0pt;line-height:115%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;
color:black;background:white;mso-bidi-font-weight:bold">Agere sequitur esse</span>�.
</p>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>�����,
� ����� ��� � �������?� - ��� � �����, ������ ���������� ��� �� �������� �,
��������, ��� ���������� ����, ������� ������ ���� �����������. � ������ ������
���� ���� ������������� � ���, ��� � ��� ������ ���������� ����� � �����������
��������� � ��� �� �������� �������������� �� ���� ��� ������. ������� �
�������� � ���� �������� ��������� ������� ������ �, �������, ����������� ���
��� ������������� �������. �� �� � ������������� ������ ���������, �� �� �������
� ����� ������ ��������� ��� ������ �������� �� ��������������, �� ����
��������� ������ ���� �� �����. ������ ����. � ����� � ����� ��������� ��������
�� ��� ������� ������, �� ������� ����� � ����. ��������� ��� </p>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>��������,
��� ���� �� ����� ������������������ ������ ���������, � ��� �������� ��������,
��� �� ����� ����� ������ ��0�� ����� ������ ������ ���-�� �� ���������?
�����������, ���������� �� �� ������ � ��������� �����������, ��� � ��� ������,
� ��� �� �������� �� ������, ��� ����� ������� ���� ����, ������ ���� �� ���
��� ������ ���, � ������� �� ���������� � ���� �����-�� �������� ������, �
������� �� �� �� ��� �� �������, � � ���������� �� ������. � ��� ��
������������ � ���, ������ ��������� ����� ��������� � ���������������,
���������� � ���� ����������� �������� � ��������, ��� ��, �������, ����, ��
����� ������, ��� ��� �����, � ������ ��� �����������, � ��� ����� ����������
�����!</p>

<h2>������� �� �������� ����� 4. </h2>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><i style="mso-bidi-font-style:normal">�����, ����� ��� �����������, �� ����������
���������� ������������, ���������� � ��������, ���������� ����������� �������
�������������� ��� �������� �� �������. ��������, �� ����� ��������. ������
����, ������� ������� � ���, ������ ����� ���� ��� ������� ������� �������� �
�� ����? �� ��� ��� �������� � �����������, ����� ��� ����� ������ ��� �
���������, ��� ������ ��������� ������� �������� ��� ������� � ��������
�������� ���<span style="mso-spacerun:yes">&nbsp; </span></i></p>

<p class="MsoNormal" style="text-align:justify"><i style="mso-bidi-font-style:
normal"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></i>� ����������
��� ������ ����� ������� ����� ������� ����� �� ����� ����� � ��� � �� ����.,.�
� ������ ���������� ��� � ���, ��� ��� ������ � ��������, ��� ��� ������ ���
���� ����� �� ������ 14 �������. � ����� ����� ���� ������. ������, ���������
��� ������� � ������� (��� ����� ������������� ��� ������ �������� �������,
���������� � ����) � ��� �������� ��� �������� ��� ��� ����� ��������� ��
����� ����������<span style="mso-spacerun:yes">&nbsp; </span>����� ������� �����, ��
��� ������� ������. </p>

<p class="MsoNormal" style="text-align:justify">- �������, � ���� ��� � �������?
� ��� ����� ����� ����������� � �������������. �� ����������� ���������� ������
� ���, ��� ����� ��� �����</p>

<p class="MsoNormal" style="text-align:justify">- ��, �����, ��� ������. ������
����� - ��� ��������� �� ���� �����. �����? ����? �������? </p>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>��
��������� ����, ������ �� ��� ���� ��������, ��������� ������ ������ �����, �
����� �� �����. ��� ����� ������ � ������ ������. ��� ��� ����� �����? ������
�� ������, �������� ����� ������� ���� ������ ���. ������ ��� ���������� ���
����� ������ ������, ������� ������ ������������� ���� �������� ��������. ���
�� ��������? ��� ��� �����. � ���� ����, ������ ���������� ���� ����� ��
�������� � ������ �� �����, ��������������� ����� ����� ������, ��������� �����
����� ���� � ���� � ��� ������� ������. ����� ����� �� ����� � ������ �����
�����..,� ��, �����, ���� ��� ���� �� ��� ������ � ����� � ���� � ��� �����</p>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>�����
����� ����� ����������� � ������ � �� ����� ��������� �������� ���� � ������. ������
���� �������� ������� �����, �������� ������ ����������� ���������� ����� �
�����, �����, ���, �����, �����. � ���, ��� ����� ������, � �������� �� ������.
� ���, ��� ���� ����� � ��� �����. � ����������� � ������ ������ � � � ������,
����� ������, �� ����� ������, �� ����� �������� � ���������. </p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify">&nbsp;</p>

<h1 align="center">����� 7. ������� � ������ ����. </h1>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">��� ������ ������... </p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">��� ��� ��������... </p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">���� ������ �� �������....</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">������ ��� ����������...</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">���������� ������...</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">������� � ��� ����...</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">�� ��������� �����...</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">&nbsp;</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">������... �� ����... �� ���...</p>

<p class="MsoNormal" style="margin-bottom:0cm;margin-bottom:.0001pt;
text-align:right" align="right">(�) ������ ������� (������������ �)</p>

<p class="MsoNormal"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></p>

<p class="MsoNormal" style="text-align:justify;text-indent:35.4pt"><i style="mso-bidi-font-style:normal">����� ������ �� ������ �������. ������
�����������, � ���� � ���� ��� ������� ������, �� ������ ���� ������� ���������
����� � ����� ������� ������, ��� ���� �� ������ �� ������, � ������
����������� �� ����, �������� � ������� ������� ���������� ����.<span style="mso-spacerun:yes">&nbsp; </span>�������� �����, ���������� �������� �����
����, ������������ �� �� ������� ��������� ����, ������ ������� �����������
���������� ��������� ������� �����. �� � ����, �� ������ �������� ��������
��������, ���� �� ������� ����� ������� ������ ����� �, �������, �� ���������
�������. ���������� ���� �����, ������ �� ���� ��������������� ����-�������
������. </i></p>

<p class="MsoNormal" style="text-align:justify;text-indent:35.4pt">� �� �����,
������� � ������ �� ���� ������, ��� ���� ����� �������� ����� � ������, ���,
����, ��������? ��� ������, �� ��� �����, �������, �������. � ���� ����� �� ��
����, � �� ������ �� ��, ��� �� ���� �������, ��� ����� ���������� ��� ������ �
���� ��������, � �� ������� ����� ������. �� � ��� ����� �� �������. ����
�������� ������� ��. ������ ����� � ����� �� �� ������, ������ �� ��������. </p>

<p class="MsoNormal" style="text-align:justify">-����� � �������? � ����� ���
����� ����� ���� �� ������������. � ���������. ��� ������, ����� � ����� ����
����� � � ������ ��� �����. </p>

<p class="MsoNormal" style="text-align:justify">- ������. � ���� ���� ����. -
���������� �����. ��� ����������� ������ � ������ ������� ������. </p>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>�����.
������ ������������ ����������� ����� � ������. ��������� ������ ����� ����� �,
�������, ������, �� ���� ������ �� ���������, ��<span style="mso-spacerun:yes">&nbsp; </span>�� ���� ���-�� ������, ��� ����� �� � ���
�������� ������ � ���� �������, ����� ��� ���������� ������ ����� � ����� ���
���������� ���� �����. �������, � ����� ������ ������, ����� ��� ������, � ��</p>

<p class="MsoNormal" style="text-align:justify">- ��������, ������ �� ����������?
� ��� ����������� �� ������� ��� � �����. �������, ������ ��� ��������.</p>

<p class="MsoNormal" style="text-align:justify">- ���� ��� �����. ������, � ���
����� ����� �� ���� ��������� ���... �� �� ���� ��-���������� ��������� �����-��.<span style="mso-spacerun:yes">&nbsp; </span>� � ���� �� ����� ��������, ��� ���
����������. � ������� �������������� ������ �������� � �������, ������,
����������, ������������. �� ���� ���� � ������. �� ���� �� ����� ������
��������, ������ ���� ������� ������� � ��������� ���� ���� �� ������? �
��������, ��� ���� ������ ������� ���������, � �� ����� �������� ���������
�����! � ���� �� �����, ����� ��������� ����. � � ������� ����� � ��������� ��
���. </p>

<div style="mso-element:para-border-div;border:none;border-bottom:solid #4F81BD 1.0pt;
mso-border-bottom-themecolor:accent1;padding:0cm 0cm 4.0pt 0cm">

<p class="MsoTitle">����� ������ �����</p>

</div>

<h2>������� �� �������� ��� ������.</h2>

<p class="MsoNormal">&nbsp;</p>

<p class="MsoNormal" style="text-align:justify"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span><i style="mso-bidi-font-style:normal">������, ����� � ��� ������ ���� ����� �
������� ������� ��� ������ �����, ����� �� ������ ��� ������ �����. ��� ����, �
���������, ����� �� ������� ������, ������, �������� �, ������ �� ������, �
������� � ����� � 5-�� �������. ������, �������� � ��� ��������� (��� �� ��
������?) �� �������� ������.<span style="mso-spacerun:yes">&nbsp; </span>� �
�������, ��� ��������� ������ ������ ������ ���������� �� ����������� ��������.
� ����� ����, ����������� ���������� �, ��������, � �� �����, ��� � ��� ������,
�� ����� ��� �� ����� ��������. ������� � �������� ������� ������� ��� ������ �����
��� �������, ��������� � ������ �����, ����� ��� ����, ����� � �� �����, ��
����� ������, ��������������� � ���������������� �������� � ������� ������. �
�������, ��� �����-������ � ������ ������ 2� � ����� ��� ��� �����������������
� �����������. </i></p>

<p class="MsoNormal" style="text-align:justify"><i style="mso-bidi-font-style:
normal"><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span>���� �� � �������
���������� ����������� ����� (����������� � ����� ����, �� ��������?).</i></p>

<p class="MsoNormal" style="text-align:justify"><i style="mso-bidi-font-style:
normal">- ���� ������ ����� �����������. �������� �����, ������������,
������������� ������� � �������� ���. � ���������� ��������� � ������ ��������
����� �������� �������� �� ��� ������������ � ��� �������� ��� 14-�� �������.
��� �������� ���, � ��� ����� ��� �� ���� ������ ������) � ������ ���� ������.
��� �������� ��� ��������� �� ���������� �������� ������������ ������� ����� �
���������� � ����� ;) ��� ����� �� ������� ���� ��������, ������� ���������
�������� ����� ���������. ����������� ������ �� ������, � ������� �������, �
��� ��������. </i></p>

<p class="MsoNormal" style="text-align:justify"><i style="mso-bidi-font-style:
normal">- ����� ����� �������� �������� ������� ������ ���� (������ ��� �
�������� ��� ������)</i></p>

<p class="MsoNormal" style="text-align:justify"><i style="mso-bidi-font-style:
normal">- ����� ���� ������ ��������� ���� � ����� ������ � ����, ��� ���
������ ����, ��� ��� ������ ��� ������ � ����������� �������, ������ ��� ��
������ ������. ���� ����� ������ ������, �������� ���� ���� �� ��������� �
�������. (������ ���������, ��, ���� ������ � ������� � ��� ��� ����������)</i></p>

<p class="MsoNormal" style="text-align:justify"><i style="mso-bidi-font-style:
normal">- ����������� ������� ������ ����������, � ��� ������� ������ �
����������� �������� ������.<span style="mso-spacerun:yes">&nbsp; </span></i></p>

<p class="MsoNormal" style="text-align:justify"><i style="mso-bidi-font-style:
normal">- ����� ����� ������ (�������� ���), ������� ����������� � �� ��
����������� � ��������� ������. </i></p>

<p class="MsoNormal" style="text-align:justify"><i style="mso-bidi-font-style:
normal">- ��� ���� ������, �������, ����������� �� ��� ��� �������� ����, ���
��� � ������ ��� (��� ������, �?) � ����� ���� ������� � ������ ������ �
������� � ������� � ������ �����. ����� ���� � ��������� � ����� ������������. </i></p>

<p class="MsoNormal" style="text-align:justify"><i style="mso-bidi-font-style:
normal">- �������� ������ � ������ ������ (������ ��� � �������, � � ����
���������), �������� �� ����� ��������� �������-�� ���� ������-�� ���� (������
���-�� ;)</i></p>

<p class="MsoNormal" style="text-align:justify"><i style="mso-bidi-font-style:
normal">- ������ � ���� � ����, � ������� � ��������� �������. ������ �� ������
� ��������� �� ��� ������ �������� �������, ��� ��� ������ ����� ������, �����
�� ���� ��������� ������ �� �������. (� �� ����, ��� ��� � ����� ��� ����, �? �
������ �������� ���� ���������, ���� ���� �� ��� ����).</i></p>

<p class="MsoNormal" style="text-align:justify"><i style="mso-bidi-font-style:
normal">- ������� ��� ������������ ���, ��� ����<span style="mso-spacerun:yes">&nbsp; </span>� �������, ��� ��� ������� ����� ��. �������
� �� � �������.</i></p>

<p class="MsoNormal" style="text-align:justify"><i style="mso-bidi-font-style:
normal">- ������ �����, ������ ����. ���� ��� ���, ���� ���� ������. ���������
������ ������ � ��������, ��������� ������������ ��� �����. </i></p>

<p class="MsoNormal" style="text-align:justify"><i style="mso-bidi-font-style:
normal">- ������ ����� �� �������. </i></p>

<p class="MsoNormal" style="text-align:justify"><i style="mso-bidi-font-style:
normal">��, ���-�� ��� ������ � ��� ����� �� ������ ;)</i></p>

<!--[if gte mso 9]><xml>
 
  Normal
  0
  
  
  
  
  false
  false
  false
  
  RU
  X-NONE
  X-NONE
  
   
   
   
   
   
   
   
   
   
  
  
   
   
   
   
   
   
   
   
   
   
   
  
</xml><![endif]--><!--[if gte mso 9]><xml>
 
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
 
</xml><![endif]--><!--[if gte mso 10]>
<style>
 /* Style Definitions */
 table.MsoNormalTable
	{mso-style-name:"������� �������";
	mso-tstyle-rowband-size:0;
	mso-tstyle-colband-size:0;
	mso-style-noshow:yes;
	mso-style-priority:99;
	mso-style-parent:"";
	mso-padding-alt:0cm 5.4pt 0cm 5.4pt;
	mso-para-margin-top:0cm;
	mso-para-margin-right:0cm;
	mso-para-margin-bottom:10.0pt;
	mso-para-margin-left:0cm;
	line-height:115%;
	mso-pagination:widow-orphan;
	font-size:11.0pt;
	font-family:"Calibri","sans-serif";
	mso-ascii-font-family:Calibri;
	mso-ascii-theme-font:minor-latin;
	mso-hansi-font-family:Calibri;
	mso-hansi-theme-font:minor-latin;
	mso-fareast-language:EN-US;}
</style>
<![endif]-->






  <? right_block('lost'); ?>
  </tr>
  <? show_footer(); ?>
