<?
define("ROOT","../../../../../");
require_once(ROOT . 'funcs.php');
show_header("���� :: ����� ���� :: �������� :: ������� ���������",LOST);
show_menu("inc/main.menu"); ?>

	    <td class="box">
<div class="boxheader"><a href="http://lost.bastilia.ru/">����</a> :: <a href="http://lost.bastilia.ru/final/">����� ����</a> :: �������� :: ������� ���������</div>
<p>��&nbsp;������, ��&nbsp;�&nbsp;������� ������� ������ �&nbsp;��������� ����������. ������ ��� ��� ��&nbsp;��� ���, �&nbsp;���� �������, ��� ���� �&nbsp;���� ��&nbsp;���, ��� ������ ������ ������� ����� �����. 
<p><strong>����� 1</strong></p>
<p>������� ��&nbsp;������ ���� ������ ������� ��&nbsp;�����������. �&nbsp;����� �&nbsp;��&nbsp;����������� ����� �&nbsp;�������� ���� ������������� �&nbsp;�������� �����, ��� �&nbsp;�&nbsp;������, ���� �������. ���������� �������� ��� ����� ���������� ������, ����� ����������� �������, ��&nbsp;������� �&nbsp;��������� ��&nbsp;��������, ������������� �&nbsp;��������������� ��������, �&nbsp;��� ��� ��� ����.<br />
<br />
��&nbsp;�&nbsp;���-�� ��&nbsp;��������� ����� �&nbsp;����������� �������. �&nbsp;������� ����� �����, ����� �������� ������� ������� ��&nbsp;����� �������� ����� ��������. &laquo;��&nbsp;����� ��&nbsp;�&nbsp;�������, ������, ��������, ������ ��&nbsp;�����&raquo;&nbsp;&mdash; ��������&nbsp;�, ����� ����� �&nbsp;���������� ��� ������ ��� �&nbsp;���, ����� ���-�� ������� ��� ���� ��&nbsp;�����.<br />
<br />
&mdash;&nbsp;����� ��� ����? &mdash;&nbsp;������� ���� ������� �����.<br />
<br />
�&nbsp;������� ����� �&nbsp;������� �������� �&nbsp;������ ������ (���� ��������� ������� �&nbsp;���������� ����� ����� ������ ������ �&nbsp;�����������) �&nbsp;��������. <br />
<br />
&mdash;&nbsp;��� ��&nbsp;�������, ��� ��� ����� ��&nbsp;�����, ��&nbsp;��� ��&nbsp;���������?<br />
�&nbsp;������, ����������, �����, ���, ��������, ������&nbsp;�� ���������, ��� ��� ����� ����� �&nbsp;�&nbsp;��&nbsp;������ ��� ��&nbsp;�������, ��&nbsp;�������.<br />
<br />
&mdash;&nbsp;������� ����� ���, ����! &mdash;&nbsp;��������� �������. &mdash;&nbsp;������&nbsp;��, ��� �����, �&nbsp;��� ���� ����� ������� ����� ������! &mdash;&nbsp;��&nbsp;������ �&nbsp;��� ���� �����&nbsp;&mdash; ����, ��� ��� ����������&nbsp;&mdash; �&nbsp;�����, �&nbsp;���, ��� ��� �&nbsp;��������, ������� ���� ���������, ���������� ��� �����. ����� �&nbsp;���������� ������� ������, ���� �������� ���, ��� ��&nbsp;����� ����, ��� ��&nbsp;����, �&nbsp;�����. ����� ���� ������� ��&nbsp;������� �&nbsp;���������� ����� �&nbsp;����� ��������.</p>
<p><strong>����� 2</strong></p>
<p> &mdash;&nbsp;���, �&nbsp;��� ��� ��&nbsp;�������,&nbsp;&mdash; ����������� ����. &mdash;&nbsp;��&nbsp;������ �&nbsp;����� �&nbsp;����� ������, ��� ���� ���� �������������, �&nbsp;��������� ��� ���. </p>
<p>&mdash;&nbsp;���, ��&nbsp;���&nbsp;�� ������ �������!</p>
<p>&mdash;&nbsp;������ �������? ��&nbsp;��� ��� ��� ���� ������! ��� ��� �&nbsp;����� ��� ������! ��&nbsp;�� ��&nbsp;��������, ��&nbsp;�� �������. </p>
<p>&mdash;&nbsp;��&nbsp;������ ��&nbsp;������, ������ ������� �����. </p>
<p>&mdash;&nbsp;������� �����, ���. ���, �&nbsp;������, ��� ��&nbsp;������&nbsp;�� ������ �&nbsp;��������. </p>
<p>�&nbsp;��� ���� ����� �&nbsp;�������� ����� ��������, �������� ������, ��������������, �&nbsp;��&nbsp;������ �����, �������� ���� �&nbsp;�������� ��������� ���������, ��� ����� ���� ������� ���� ��&nbsp;�����, ������ ��� ���� ������ �������. �&nbsp;���� ����� ������. </p>
<p>���� ������������, ��� ��&nbsp;������� �������� �&nbsp;��������� ��� ��������� �����, �&nbsp;������� ��� ������ ��������� ��������� ����������� ������������, �&nbsp;����� ���� ������, ��� ��� ��&nbsp;������ ������������, �&nbsp;�����-�� ������������. ��&nbsp;��� ��&nbsp;������� ��&nbsp;��������� ��&nbsp;����� �&nbsp;������������� �&nbsp;�������, ������� ��� ���������, ����� ��� �&nbsp;��&nbsp;�������� &laquo;������������&raquo;. <br />
<br />
���� ���������� ��������� �&nbsp;���������� ���� �����. ��&nbsp;��������&nbsp;&mdash; �������� ������� �&nbsp;���� �������. ���� ������� ��������, ��� ������ ��&nbsp;��&nbsp;����, �&nbsp;����� &laquo;���� ������������&raquo; ���������. <br />
<br />
&mdash;&nbsp;�&nbsp;��� ������ ��� �������, ��� ���� ������ ������� ��&nbsp;��� ��������� ��&nbsp;������, ��� ��� ���� ����� ���������, �&nbsp;����������� �&nbsp;���������. �&nbsp;���������, �&nbsp;��� ��&nbsp;������, ��� ��� �����-��, �&nbsp;��� ����� ��&nbsp;����, ��� ��������������� ���������, ������ ��&nbsp;������� ��&nbsp;�����.</p>
<p><strong>����� 3</strong></p>
<p>��� ��� �������� ���. ��&nbsp;������ ��&nbsp;������� �&nbsp;�������� �������, ��&nbsp;����� ��&nbsp;����������. ����� ������� ������, ��� ���������� ����. ��&nbsp;����� ��� ������� ��&nbsp;��������. </p>
<p>�&nbsp;�����-�� ������ ��&nbsp;�������������, ��� ������� ��������. ��&nbsp;������ �������, �&nbsp;�����. ��� ��� ��� �������. ����� ��������, ��� ���� ������ �������� ��������. ��&nbsp;������ �������� �������� �������� ������� ����, ������� ������� �������. </p>
<p>&mdash;&nbsp;����,&nbsp;&mdash; ������ ��&nbsp;�&nbsp;����� ���������, ��� ���� ����� �&nbsp;���� ������� �&nbsp;������� ��&nbsp;����. </p>
<p>&mdash;&nbsp;���,&nbsp;&mdash; ���������� ����������&nbsp;��. </p>
<p>��� ���������� ������ �&nbsp;��������, ��&nbsp;������� ��� ����� ������ ����, ��&nbsp;����. </p>
<p>��&nbsp;��������� ������ ����� �&nbsp;������ ��&nbsp;���������� ����� ������, ��� ����� ��� ����� �������. �&nbsp;���� ������ ��������� ������ �������. ��&nbsp;��������� ������ �&nbsp;������ ���������� �������, ��&nbsp;������ ������� ��������� �������. ��� ������ ��&nbsp;����� �������. �&nbsp;��&nbsp;���������, ��� ���� �������&nbsp;&mdash; ���&nbsp;��. </p>
<p><strong>����� 4</strong></p>
<p>��&nbsp;����� ������ �&nbsp;��������� ������� ��&nbsp;������, ��&nbsp;������������. ���� �������� ����, ������� ���� ����� ��������. </p>
<p>��&nbsp;��&nbsp;�&nbsp;���� ������� ��&nbsp;����, ��&nbsp;��&nbsp;��&nbsp;������ ������, ��� �����. �����, �&nbsp;���� ������� ��&nbsp;����? ��&nbsp;�������� ���������, ��&nbsp;��� �&nbsp;�������. </p>
<p>��, ��������, ������&nbsp;�� ��� ������. �&nbsp;������ �������, ���� �������&nbsp;��, ��� ���� ������� ���� ��&nbsp;����� �&nbsp;����. </p>
������� ����������, �&nbsp;��&nbsp;���������� �&nbsp;���� �����. &laquo;���� ������� ��� �����, ��&nbsp;������ ��� ������ ���?&raquo;&nbsp;&mdash; ��&nbsp;��� ����� ��������� ���� ��������.
<p>
  <? right_block('lost'); ?>
  </tr>
  <? show_footer(); ?>
