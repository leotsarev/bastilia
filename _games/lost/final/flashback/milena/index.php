<?
define("ROOT","../../../../../");
require_once(ROOT . 'funcs.php');
show_header("���� :: ����� ���� :: �������� :: ������ �����������",LOST);
show_menu("inc/main.menu"); ?>

	    <td class="box">
<div class="boxheader"><a href="http://lost.bastilia.ru/">����</a> :: <a href="http://lost.bastilia.ru/final/">����� ����</a> :: �������� :: ������ �����������</div>
<p>�������� �&nbsp;������� �&nbsp;�������, �&nbsp;������������ ������������� �����. �&nbsp;���� ���� ������, �������. ����� ��&nbsp;�������� �����, ����� ���� ����&nbsp;15, ��� �������� �����. �&nbsp;��&nbsp;�����.<br />
<br />
����� ��&nbsp;������� ���� �&nbsp;������ �&nbsp;������ �������.<br />
<br />
��������� ��&nbsp;��������, ���������� ��&nbsp;������, ���� ������� �������.<br />
������ ��� ����� �&nbsp;������� ��&nbsp;��������� �&nbsp;������ ��&nbsp;������.<br />
�&nbsp;��� ������ ��� ����������� ���� ������� ��&nbsp;���� ��� ������. �����, ��� ��� ������ ��������� ����� ���������.
<p><strong>����� 1</strong></p>
<p>�&nbsp;����� ������ ���� ��&nbsp;����� ������� �������, �������. ��� ��� �������� ������ �����, �������������� �&nbsp;��������... ��� �&nbsp;��&nbsp;�������� ������������. ����� �&nbsp;��� ��� ������� ������� ��� ��������, �������. ��&nbsp;���������� ��&nbsp;��� ������, �&nbsp;���� ��� ��� ���� �������. ��� ����, �&nbsp;������ ����� �������� ��&nbsp;����� �&nbsp;�������� �&nbsp;����������. ����� �&nbsp;�����, ��� ���� ��� ������, ���� ��������������� ������� ������� �����. ��� ���� ��&nbsp;������� �&nbsp;��&nbsp;�������. ��, ������� ������� �&nbsp;�������. ������� ��&nbsp;��� ��&nbsp;�������, �&nbsp;��&nbsp;�������� ��� ���-�� �&nbsp;���� ����. ��&nbsp;�������� ��&nbsp;������... �&nbsp;����� ���� �&nbsp;������������ �����, �&nbsp;����� ��&nbsp;����� ���� ����. ��&nbsp;�������� ������ ����� �&nbsp;����� ������ ����� ����� �&nbsp;��� �&nbsp;����� �����. <br />
����� ��� ��&nbsp;�������� ���� ��������. ��&nbsp;������ �������� ��� ���� ��������... ��&nbsp;����� �&nbsp;���� ��� ������ �&nbsp;��&nbsp;���������. ��� ��&nbsp;������ ������� �&nbsp;������ �&nbsp;������� ������, �&nbsp;���� ������, ��� �&nbsp;���������� ��&nbsp;������&nbsp;&mdash; ������� ���. ����� ��&nbsp;����� �&nbsp;������ �������. �������. ����� ��������.... �&nbsp;���������� ���, ������� ���� ������� ��&nbsp;������ ���� �&nbsp;����, ������ �&nbsp;���� ������� �&nbsp;��� ���� ������. ��&nbsp;����� ��� ���� ����� ��&nbsp;������ �&nbsp;��������, �&nbsp;���� ��������� ���, ��� ���� ��� ���������� ��� �����������, ��� ���������� �����!! �&nbsp;��� ���� ��� ������� ��� ����� ����� ������ ���� �&nbsp;��������� ��� ���� ��� �����. ���� ������ ������� ��� ���� ��������, ���. ��� ��������� ���� ������ ������ ������� ������, ���� ����������, ���������. ������ ��&nbsp;������� ��&nbsp;������������.<br />
�&nbsp;���, ��� ��� ���� ������������� �����-�� ���� �&nbsp;����� ����� �&nbsp;��� ���� �&nbsp;���� ���� ��������� ������ ���� �������� �������: ���������� ���&nbsp;&mdash; �&nbsp;������ ���� ������ ���������� ����&nbsp;&mdash; ��, ��� �����-�� ���� ����� ����� �&nbsp;������. �&nbsp;��, �&nbsp;������ ���� ���� �������� ����� �&nbsp;���� ������������� ������!! ����� �&nbsp;��� ���� ������&nbsp;&mdash; �&nbsp;���� �&nbsp;����� ��&nbsp;����� ����. �&nbsp;������. �������!!<br />
��� ��� ���� �&nbsp;�����. <br />
<br />
�&nbsp;������� �&nbsp;�������� ������ ���� ��������� ��������� ��������� ��&nbsp;������� �&nbsp;�������� �����, ������� �����. �����, ����� ������������ ������� ������ ��������, ������� ��� ��� ������ �&nbsp;�������, �&nbsp;������ �����, ��� ����� ��&nbsp;�����. �&nbsp;������ ������, ���� ������, ������&nbsp;�� ��&nbsp;������ ���� �������. ������� �&nbsp;�������. ��&nbsp;��� ������ ��������� ���� ��������. ��&nbsp;��������� ������� ����, �&nbsp;������� ������ �&nbsp;��������� �&nbsp;��� ����������� ��������� ����������� �&nbsp;�������� �����. ������ �&nbsp;��� ��&nbsp;���������, ��� ��&nbsp;������ �������, ������� ��� ���� �&nbsp;�������. ��� �&nbsp;�������� ������ ����� �&nbsp;��������, ��� ������� �������. ��� ��� ��������&nbsp;&mdash; ������ ��&nbsp;��� ���� �&nbsp;��� ��� �����. ��� ����� ��&nbsp;����� �������, �&nbsp;�&nbsp;���� ���� ����-�������. �&nbsp;������&nbsp;&mdash; �&nbsp;��� �������. ���� ����� ���� ����� �&nbsp;����. ������ ������ &mdash;� ���������� ������. ��&nbsp;�����, ��� ���� ���-�� ���� �&nbsp;����������. ��&nbsp;���� ���, ���, ���, �&nbsp;������ ����� ������� ��� ���� �������� &laquo;��� �&nbsp;��, ���� ����, ��� �&nbsp;��&raquo;.</p>
<p><strong>����� 2</strong></p>
<p>������ �������, ������ ������� ������ ����. ��, ���������, ��� ������ ���������� �&nbsp;������ ��������� ������. ������ ����� �&nbsp;��������� ������., ��&nbsp;������ ������ ������ ������ ��&nbsp;�����, ������ ���� ��&nbsp;��&nbsp;����... ����-����, ����� ��� ���� ����, ���������� &laquo;��� �&nbsp;��, ���� ����, ��� �&nbsp;��&raquo; �������� �����&nbsp;� ������� ������ ����� �&nbsp;������, ��� ��������� �&nbsp;������, �&nbsp;���� ������� ���� �&nbsp;���������� ������, �&nbsp;���� �����, �&nbsp;�����, ����� ������� ��&nbsp;����� �����.<br />
... ����� ����� ��������� ����� ��&nbsp;�������� &laquo;�������!&raquo;<br />
����� ����... ������ ������ �&nbsp;��������, ��� ��� �����������. ����� ��������� ���, ������ ����������� ���� ����, ������ �&nbsp;���, ��&nbsp;������� ��&nbsp;������ ������ ������� ������ ����� ����.. ������� &laquo;���!&raquo;<br />
����� ����������� �&nbsp;������, ��������� �����, ��������� ����������� �����, ����� ��&nbsp;�������� ���� ���..</p>
<p><strong>����� 3</strong></p>
<p>������������� �������� ����. �&nbsp;��� �&nbsp;������ ��� ���������� ������� ����...��� ������? �����������. �&nbsp;��� ������� ���������� �����, ��� ��������� ����� �&nbsp;���������...��� ����� �����&nbsp;� (�����!) ����� �����, �&nbsp;��� ��� ����� �����, �&nbsp;���������� ������ �������, �������.. . �&nbsp;������� ��������� ����������� ����������, ������������ ������ ��&nbsp;��������. ��&nbsp;��� ������-�� ������� �&nbsp;���, ��������� ���� �������. ������ ������ ������� ��� ������, ��� ����� ��� ������ &laquo;����� ���&raquo;? ����� ����� �������� ��&nbsp;���.<br />
���������� ������ ��������� &laquo;������ ������&raquo;&nbsp;&mdash; ��&nbsp;���� ��� �������... ��&nbsp;��&nbsp;����������, ��� ������, ������ ��� ��&nbsp;���-�� ��� ������� ������������� ����������..<br />
��&nbsp;����� ��&nbsp;������, ��� ������� ������ ����, �&nbsp;��� ������� ��������� ������... ����� ������������ ������� �������� �����, ���� ��&nbsp;��� ��&nbsp;���������, ������, ��� ���������..</p>
<p><strong>����� 4</strong></p>
<p>������� ������ ����, ������� ������������� ������. �������� ����������, ��&nbsp;��� �������, ��� ������ ������. ������ �&nbsp;������, ������ ��������� ��������, ����������, ��� ��� �������� �������, ��� ��� ����� ���������... ��� �&nbsp;���� �������� ���� ������� �������, �����, ��&nbsp;����� ����... ��&nbsp;�������� �&nbsp;���� ��������. �&nbsp;�����-�� ������, ������������ ��&nbsp;����, ��&nbsp;��������� ��� ����������, ����� ��&nbsp;����� ������� ���� ��&nbsp;�����...<br />
<br />
�������� ������� ������� ����������� �&nbsp;�����. ��&nbsp;�������� �������� �&nbsp;������������� ������� ������� ����� �&nbsp;����. ���� ��&nbsp;������������� �&nbsp;������������, ��&nbsp;�������.</p>
<p>��&nbsp;��������� ���� ��&nbsp;����� ������� ��&nbsp;������, ���&nbsp;�� ����� �������� ��� ���������. ��&nbsp;��������, ��&nbsp;������ �&nbsp;������� ������, ���������..<br />
���������� ��������, ������, �����. ������ ���� ��� ����� ��&nbsp;����.</p>
<p>
  <? right_block('lost'); ?>
  </tr>
  <? show_footer(); ?>
