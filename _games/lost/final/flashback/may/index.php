<?
define("ROOT","../../../../../");
require_once(ROOT . 'funcs.php');
show_header("���� :: ����� ���� :: �������� :: ����� �������� (���� �������)",LOST);
show_menu("inc/main.menu"); ?>

	    <td class="box">
<div class="boxheader"><a href="http://lost.bastilia.ru/">����</a> :: <a href="http://lost.bastilia.ru/final/">����� ����</a> :: �������� :: ����� �������� (���� �������)</div>
<p><strong>����� 1</strong>
<p>���� �&nbsp;���� ��������� ��� �������� ��������. ��&nbsp;������� ��� ������� ���������� �������. ��� ������ ������ ����, ��&nbsp;��� ���� ����� ��� �&nbsp;�����. ���� �&nbsp;���� ������ ��������, ��&nbsp;������� �������. ������ ������, ����� ��� ��������� �����, ������� ��� �����. ���� �������, ��� ��&nbsp;������ �������. ��&nbsp;��&nbsp;�����&nbsp;&mdash; ��� ��� ��� �����. ���� ��� ����� �������, �&nbsp;����� ��� ��������� ������ ��������. ��&nbsp;������� �������� ������ �������� ������� ��������, ������� ��� ������������ ��� �������.
<p><strong>����� 2</strong></p>
<p>��&nbsp;��������� �&nbsp;����� �&nbsp;������������ ��&nbsp;����, �&nbsp;��&nbsp;������� �&nbsp;���� ����������. ���� �������� ��� ��� ������ �&nbsp;������� ��&nbsp;3&nbsp;����, �&nbsp;��&nbsp;���������� ��&nbsp;������� �&nbsp;������ ����� ������� �������. ��&nbsp;����� ��� ��������, ���� �&nbsp;��&nbsp;�����. �&nbsp;���&nbsp;�� ���� ������, ��&nbsp;����� ��� ���� ���������� ������ ��&nbsp;����� ������. �&nbsp;�������, ����� ��&nbsp;�&nbsp;��������� ��� ����������, ��, ������ �&nbsp;����, ����������� �������� �&nbsp;��������� ���. ��� ���� ��� ���� ��������, ��&nbsp;������ ��� ����� ���. ���� ������, �&nbsp;��� ������� ���. ����� ����&nbsp;&mdash; ���� �������, ��� ��&nbsp;������ ��&nbsp;������� ������ ����� �����. ��&nbsp;��&nbsp;��&nbsp;����� ��������� �&nbsp;����. �������� �&nbsp;������ ��&nbsp;������������, ��� ��� ��&nbsp;����-�� ��&nbsp;������ ��������. �&nbsp;������ ��&nbsp;����������. �&nbsp;�������� ��&nbsp;���� ��������� ��������, ��� ��� ��� ��&nbsp;��� ����� �������, �&nbsp;������&nbsp;&mdash; ���� �������.<br />
<br />
������ ����� ����� ����� ���������� ��&nbsp;���������. ��&nbsp;�������� ���� ������ �&nbsp;������� ������, ��&nbsp;��� ��������� ���������. �&nbsp;������� &laquo;������ ������&raquo; ���� ���������� ���� ���������� ����������� ���������, ��&nbsp;��&nbsp;����� ������ �������� ������������ ����. ���� ������� ��&nbsp;�����-�� ������, ������, ��� ��� ��������� ��&nbsp;������ �&nbsp;��� ����������� ������������ ��� ���� ��������. ����� ������� ��&nbsp;�������� �&nbsp;�������� ��������, ��� ����������, ������, ��� ���������� �&nbsp;�������� ��������, �&nbsp;�������������� ������ �����. ��&nbsp;���������� ��������� �&nbsp;&laquo;�������� ������&raquo;, ��&nbsp;��&nbsp;���� ������-�� ��&nbsp;����������. �&nbsp;�&nbsp;������ ��������� ���������� ���� �����-�� ���������. ��&nbsp;������ ��&nbsp;������, ��� ��� ��� ����� ������, ��� ������������ ������ &laquo;������� ������&raquo;, �&nbsp;��&nbsp;����� ������� ������� ������� ����, ���������� ��&nbsp;���������. <br />
<br />
<strong>����� 3</strong></p>
<p>�&nbsp;�������� ��&nbsp;������������� �&nbsp;����� ������� ����������, ������� ��&nbsp;����� ���������. ��&nbsp;����� ��������� ����. ������ ��� ������, ��� ��&nbsp;����������, ��&nbsp;��&nbsp;�������� ��&nbsp;��������, ��������, ��� ��� ����� ��&nbsp;�������������� �������� �&nbsp;������. ��� �������, ��� ���� ����� ��&nbsp;���� �&nbsp;������� �&nbsp;�&nbsp;�������� ��������&nbsp;�� ��&nbsp;��������� �&nbsp;�����. ��&nbsp;���� ���� ����� ���������, ��� ��� �&nbsp;��������� ����� �&nbsp;���� ������ ��&nbsp;�������� ������.<br />
<br />
<strong>����� 4</strong></p>
<p>��&nbsp;��� ��&nbsp;���������� �������� ���������, �&nbsp;�������� ����� ����� ��&nbsp;���������. ��&nbsp;�������� ����-�� �������� ��&nbsp;��������. ������, ��� ������... ���� ����� ���������� ������, �&nbsp;������ ���������� ����������, ��&nbsp;���� ��&nbsp;���� ��������. ��&nbsp;����������, ��� ������ �&nbsp;��&nbsp;������� ���������� �������. ��&nbsp;����� ���� ���� ��&nbsp;��&nbsp;�����, �&nbsp;��&nbsp;������ ��������� ����. ��&nbsp;�&nbsp;��� ��� ��� �&nbsp;����� �������.<br />
<br />
�������, �������� �������. ��&nbsp;������, ������� ������, ���&nbsp;�� ��&nbsp;������� ������ �������� ��� �����, ��&nbsp;��&nbsp;���� ��&nbsp;��� ��&nbsp;������ ��&nbsp;��, ������� �����&nbsp;�� �������� ����. ������-�� ���� �������, ��� ���� ��&nbsp;�������&nbsp;��, ��� ������ ����. �&nbsp;����� �����������. ��� ��� ������. ��&nbsp;���������� �&nbsp;��������� ���� �����.<br />
<br />
&mdash;&nbsp;�����, ��� ��&nbsp;����� �������? &mdash;&nbsp;���������� ������������ ������� �����.<br />
&mdash;&nbsp;������, �&nbsp;������ ���� �����, ��� ���...<br />
&mdash;&nbsp;����� ���� ��� �������, ��&nbsp;��� ��&nbsp;��� �������� ��&nbsp;����.<br />
&mdash;&nbsp;��... ��&nbsp;�� ������, �&nbsp;���������! �&nbsp;������� ����.<br />
&mdash;&nbsp;������ ����������, ����� ���� �������� ����! �����, ������ �&nbsp;������! &mdash;&nbsp;��&nbsp;���������� �&nbsp;���������� ��������� ����� �&nbsp;�����. ����� ������� �&nbsp;����������������. &laquo;���?!&raquo; <br />
&mdash;&nbsp;���� ��&nbsp;������? ��� ����� ������ �������. &mdash;&nbsp;��� ����. ��&nbsp;��� ���� ��&nbsp;�������� �&nbsp;�������. ���� ����� ���������. <br />
&mdash;&nbsp;��, ��� ��� �������� ����, ��� ��&nbsp;����! &mdash;&nbsp;���������� ������ ������� �����.<br />
��&nbsp;������, ��� ������� �&nbsp;�������� ������ ����. ��&nbsp;��� ��������!<br />
&mdash;&nbsp;��&nbsp;����, ����, ��&nbsp;���������� �&nbsp;���! ��&nbsp;���������� ��� ��&nbsp;���. ����� ��������� �����. <br />
&mdash;&nbsp;�&nbsp;��, �������? �������� ����������&nbsp;��, ��� ��-�� ��� ������� ������� ��� ����? ���������� ���� �&nbsp;���������.<br />
&mdash;&nbsp;�&nbsp;������ ������ ������������ ����� ����� �&nbsp;����� ��������, �&nbsp;��&nbsp;��������, ��� �������� ��&nbsp;������ �����. &mdash;&nbsp;�������� ��.<br />
&mdash;&nbsp;���� ��� ��������. �������? �������� ���� ��� �&nbsp;��������� �����! &mdash;&nbsp;������ ��� ���� ���� ����������&nbsp;��. <br />
��������� ���� �&nbsp;������, �&nbsp;��� ������. ��� �������, ��������� ��������&nbsp;��. ����� ���������� ���� �&nbsp;������, ����� ���� ����, �������, ����������.<br />
&mdash;&nbsp;��&nbsp;���������, ���, �&nbsp;��������, ��� ������� ��� ����. �����, ��� ��� �����&nbsp;&mdash; ������� ��&nbsp;���� �����, �&nbsp;��&nbsp;���������. ��&nbsp;����������� ��� ������.</p>
<p><strong>����� 5</strong></p>
<p> &mdash;&nbsp;������&nbsp;�� ��&nbsp;��������� ���� ���? �����, ���� ��&nbsp;��������� ���� �������, ���� ��&nbsp;�������? ������&nbsp;�� ��&nbsp;����� ����� ����� �&nbsp;���� ����? ������&nbsp;�� ��&nbsp;�������, ��� ��� ����� �������� ��&nbsp;����? </p>
<p>�������, ��� ��� �������� ��&nbsp;���. ��&nbsp;��� �&nbsp;��� ������������?</p>
<h3>����������� ������������ (��&nbsp;��������)</h3>
<p><b><i>����� ��&nbsp;�&nbsp;������ ��� ������� ������� ��������, ������ ������������ ����:</i></b></p>
<p>��&nbsp;������, ���������� �&nbsp;������� ������, ��&nbsp;�&nbsp;��� ��&nbsp;�������� �������� �����. ��� ��� �������, �&nbsp;���� ���� ������ ������, ��&nbsp;�&nbsp;���� ��� ����. ��&nbsp;������� ����, ��&nbsp;��&nbsp;����������� ������ �����, ������� ��������� ������ �&nbsp;������. ����� �������� ����, ��&nbsp;��&nbsp;������� ������������, ��� ��� ������ �������� ���������� ������ �����. �&nbsp;����� ���������� ��������� ������� �����.<br />
<br />
&mdash;&nbsp;�������� ��&nbsp;��&nbsp;�� ������, ��� ���� �������� ������ ��� ��� �������� ������ ������.- �������� ��� ������� �������� ��������� �����. ��&nbsp;������ �&nbsp;���� �&nbsp;�������� ��� �������.<br />
&mdash;&nbsp;����� ��&nbsp;���&nbsp;&mdash; ���������&nbsp;��.</p>
<p><i><b>����� �&nbsp;������� ���������� ���� ����:</b></i></p>
<p>����� �&nbsp;�������, ����� ������.<br />
<br />
&mdash;&nbsp;��&nbsp;��� ���������� ��������, ������� ���� ����?<br />
&mdash;&nbsp;��� ������ ����� ��� ����.<br />
&mdash;&nbsp;��� �&nbsp;�������. ������, ���� ���� �&nbsp;��������, ���� ��������.</p>

���� ����� ���������� ����. ��� ������� ���������� ����� ����� ��������. ������ ������ ��&nbsp;��� ����� ������� ��������.
<p>
  <? right_block('lost'); ?>
  </tr>
  <? show_footer(); ?>
