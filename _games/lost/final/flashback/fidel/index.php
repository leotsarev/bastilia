<?
define("ROOT","../../../../../");
require_once(ROOT . 'funcs.php');
show_header("���� :: ����� ���� :: �������� :: ������ �����",LOST);
show_menu("inc/main.menu"); ?>

	    <td class="box">
<div class="boxheader"><a href="http://lost.bastilia.ru/">����</a> :: <a href="http://lost.bastilia.ru/final/">����� ����</a> :: �������� :: ������ �����</div>
<p><strong>����� 1</strong>
<p>��� ���� 14&nbsp;���, ������� �����������, ����, ��� �������� �����, �&nbsp;��������������� ������� ����� ����� ��&nbsp;������� ���� �������� ���������� ������ ������, ��� ������� (��������) �������� ������ ��&nbsp;�����. ����� �&nbsp;���� ��� ���� &laquo;��� ��&nbsp;��� ������&raquo;, ������������&nbsp;&mdash; �����, ������� ���, ����� ����, ����, ��&nbsp;������� ��&nbsp;�������� �������� ������...<br />
<br />
����������� ������ �����, �&nbsp;������, �������� ���� ��&nbsp;������� �������� ������� �����, ������� ��&nbsp;��� ������� ����� �������� ���������� ��&nbsp;������ (��&nbsp;����� ���� ��&nbsp;��������, �&nbsp;�&nbsp;������� ������, ��&nbsp;�&nbsp;������ ��������&nbsp;&mdash; �������� �&nbsp;���������). ������������ �����, ������ ���, ��� ����� �&nbsp;��&nbsp;���� �����...
<p><strong>����� 2</strong></p>
<p>��&nbsp;��������� �����������, ����������, ������ ���� �&nbsp;�������� ������� �����. ������� ���� ������ ����������� �&nbsp;����� ����������� �&nbsp;������ ������. </p>
<p>�&nbsp;��� ��&nbsp;����� �&nbsp;������, �&nbsp;���, ��� ����� ������ ���� ������ �����. ��&nbsp;���� ��&nbsp;�������, �&nbsp;������ ��������� �&nbsp;����� ����� ��� ���� �������. ��&nbsp;�&nbsp;��� �������� ����� ��&nbsp;������. ��&nbsp;���� �������, ��� ��� ��������� ����. </p>
<p>�&nbsp;����������� ���� �������: &laquo;������, ��, ��� ��&nbsp;��������, ����� ��� ���������. �&nbsp;��� ���� ��� ��� ������&raquo;. �&nbsp;������. </p>
<p>����� �����������, ��&nbsp;���������, ��� ����������, ������� ��� ������. ������, �������� �&nbsp;����� ���� ��� ���� ��&nbsp;������ ����. ����� ��&nbsp;�� ������� ��&nbsp;��������� ������, ��&nbsp;�� ������� ���� ��&nbsp;������-�� ����, ��&nbsp;�� �&nbsp;���� ���� �����-�� ���� ��������� ��&nbsp;�����������. ����� ���������� ������������ ������ (��������, ������� ��� �&nbsp;����������� ��� &laquo;������ �����&raquo;), �&nbsp;������ ����� ����������, ����� ��&nbsp;������� ��� �����, ���&nbsp;��, ��� ����&nbsp;&mdash; ��� ����� ����, ��&nbsp;��&nbsp;����� ���������������, ����� ��� ����� ��������. ���&nbsp;��, ��� ����� ���� ��������� �&nbsp;������ ����� �&nbsp;�&nbsp;������ �����. ������������ ��� �����&nbsp;�� ��������, ��� ���� ���� �&nbsp;���-�� ����������. </p>
<p>��&nbsp;������ ��&nbsp;������ ��� ������. ��� ���� SMM-���������, ������ ����, ���� ��&nbsp;�����, ������������ ��� ����� ����. ����� ���� �������� ��������� �������� �&nbsp;������ �������� ��� �������� ��� �&nbsp;���������. </p>
<p>���������� ������ ���� �������: ��&nbsp;���� �������� �������������� �������, ��� ��������, ��������, ���������, ���������������. ��&nbsp;����� ��������� ��&nbsp;��&nbsp;�������. ����� ����, ��� ��� ��� ������ ����� ����, ��&nbsp;������ ����� ���� ���������� ��� ������ �����, ��������&nbsp;��, ����� ���� �&nbsp;������ ��&nbsp;����������. </p>
<p>��&nbsp;���� ��������� �������������� ���������� �����������. �&nbsp;�����&nbsp;��. ������������ ������������� ��������� ��&nbsp;���� �������� ���� ��� �������. ��&nbsp;���������� ��� ���� ��������. ��� ������� ��&nbsp;����������� �&nbsp;��������. ������ �������� ���� ������� �&nbsp;�����������. </p>
<p>�&nbsp;�����-�� ������ ���� ��� ��� ����������. �&nbsp;��&nbsp;������ ��� ������. </p>
<p><strong>����� 3</strong></p>
<p>��&nbsp;��� ����� ��&nbsp;���� ����� ������ ���������. ��&nbsp;���������, ��� �������� �&nbsp;����� ��� ����, ������� �������� ������������� ������������ ��&nbsp;�����, �&nbsp;�������� ���� �������� ������. ��&nbsp;���������� �&nbsp;��&nbsp;����� ������� ������� ������-���������� �&nbsp;���� ������� ��� ����� �������� ������ ���������. </p>
<p>���������, ������, ��������, �.�. ������ ��&nbsp;����� ��&nbsp;�������. </p>
<p>�&nbsp;���������� ������ ���� �������� ���������: ���� ������� �����, ����� �������� ��&nbsp;�������� �&nbsp;���������� ������ �����.</p>
<p><strong>����� 4</strong></p>
<p>�&nbsp;��&nbsp;���� �&nbsp;���� ���������� ��������� ��������, ����������� �������� ������������ ����� ������-6. ����� ���� ������� ����������� ������. �������� ��&nbsp;������, ������ �&nbsp;���, ��� ��&nbsp;������������ ���������. </p>
<p>������... �������, ��������� �������, ��� ���� ���������� ���� ������������ ��&nbsp;�������. </p>
<p><strong>����� 5</strong></p>
<p>��&nbsp;����� �&nbsp;���� ���� �&nbsp;��&nbsp;��� �������, ����� ������� �����. ��&nbsp;���������� ����� �&nbsp;������� �&nbsp;����. �����, ��&nbsp;����� ����� ������� �&nbsp;����� �����. </p>
<p>&mdash;&nbsp;��� �������,&nbsp;&mdash; ������� ������&nbsp;��, ��&nbsp;��&nbsp;������ ��� ����� ���������, ��� ����� ��&nbsp;����� ����� �&nbsp;�����. </p>
<p>&mdash;&nbsp;�������, ������ �&nbsp;�� ��� ����� ������������ �������������� ������? ������� ��� ������, ������� �������� ��&nbsp;�������� ������ ��&nbsp;������? �������������� �������� ������, ������� ��� ������� ���� ������? �&nbsp;������-�������������� ������ ��� ������ ������? ������� �������, ������,&nbsp;&mdash; ������� ���� ������� �&nbsp;����� �&nbsp;�������� ���������� �&nbsp;������ ��&nbsp;�����. </p>
<p>��&nbsp;��&nbsp;��������� ��������� ���. �&nbsp;���������. </p>
&mdash;&nbsp;��&nbsp;���� ��&nbsp;���������? &mdash;&nbsp;������� ��&nbsp;��� ����.
<p>
  <? right_block('lost'); ?>
  </tr>
  <? show_footer(); ?>
