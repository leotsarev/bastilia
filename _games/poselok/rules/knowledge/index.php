<?
define("ROOT","../../../../");
require_once(ROOT . 'funcs.php');
show_header("�������: �� �� ������� :: ������� :: ������",POSELOK);
show_menu("inc/main.menu"); ?>

	    <td class="box">
<div class="boxheader"><a href="http://poselok.bastilia.ru/">�������: �� �� �������</a> :: <a href="http://poselok.bastilia.ru/rules/">�������</a> :: ������</div>

<h4>������ ������ �������</h4>

<p>������ ��&nbsp;����� ���� ������������... ��������!

<p>������ ������������� ������, ������� �������� ���� ������� ������ (������� ������, ��� ��&nbsp;������� �������� ������, �&nbsp;��� ��������, ��� �������� ��� �&nbsp;�.�. &mdash;&nbsp;������ ������ ��� ��� ��&nbsp;������ �&nbsp;���������� �&nbsp;����), ���������� ������ <i>�����������</i>, ����������� �&nbsp;����� ��&nbsp;�������� ������/������/�������, ������� ������ ������� ������ ��������� �&nbsp;������. ��� ������� ��������� ������ �&nbsp;���.
<p>����� �������� ������&nbsp;&mdash; ��� ����� ������� (���������� ��&nbsp;����������� �&nbsp;������ ��������), ������� ��������� ��������, ��������� ������� ������� ��&nbsp;������� ���.

<p>��� ���������, ��� &laquo;�����&raquo; ��������, ��� ������ ���������� ��������.
��&nbsp;����� ����������� ������ �&nbsp;������� ������ ������ �����������.

<p>����� ������, ���� ��&nbsp;����� ������� ����� ��������, ������ ���� �&nbsp;������� �������� ��������������� �������. �&nbsp;��� �&nbsp;���������� ����� ������� ������ ��������� ���������� ������ �����, �������� �������� ������� ������� &laquo;��������&raquo;, ����� ���������� �&nbsp;��� �������.


<h4>������ ������� ������</h4>

<p>���������� ������ �������� ������� ����� ��������-��������� (��� �������� ��&nbsp;�������� ������������� �&nbsp;����&nbsp;&mdash; ��&nbsp;��������������, ��������� �&nbsp;������), ������ ��&nbsp;�����&nbsp;&mdash; ����� (�������� ��&nbsp;���� ��&nbsp;��������� ��������������).

<p>��� ����� ���� ������� ����� ������ (����� �������)?

<p>��� ������, ���� ����� ������� ����� ��������� ������ ������� ������, ��&nbsp;���� ����� ��������� ��������. �&nbsp;����������� ������� ����� �����&nbsp;�� ���������� ���, ������, �&nbsp;������� &laquo;������&raquo; ��� �����, �����������, �&nbsp;�������, &laquo;����� �&nbsp;���&raquo;. �������������� ��&nbsp;���.

<p>�������&nbsp;��, ������ ����� �������� ������������ �����, �������� ��&nbsp;�������� ��� ����� �����-���� �����. ��� �&nbsp;��������, �������� ��&nbsp;&laquo;������&raquo;, ����� &laquo;������&raquo; ����.

<p>������ ����� ��������� ��&nbsp;��������, �&nbsp;������ �������, ���&nbsp;��, �&nbsp;������� ���������� �������������� ������. ��� ����� ������������ &laquo;������&raquo; ��������� ������������ ���&nbsp;�� ���� ��� ���������� ������� ���������� ����.

<p>�������, ������ ����� ���� �������� ����������� ������� ��&nbsp;��������� ��������.

<p>�������� ����� �&nbsp;����� ��������, ������� ����� �&nbsp;����� ����� �����.


<h4>��� ��� �������� �&nbsp;��� ��� ��������</h4>

<p>��������� ������ ����� ���� ����������� �&nbsp;����� �����������:
<br><i>��� ������� �&nbsp;������ ������ ��������� �����.</i>
<br>��, �������������. ��� ���� ������� �������� ���.

<p>������ ����� ��������� &laquo;�������������&raquo;:
<br><i>������� ����� ����� ��������,
<br>�&nbsp;������� ����� �&nbsp;������� ��&nbsp;����.</i>
<br>��� ������������, ��� �������: ������� ����� �������, �������&nbsp;&mdash; ������, ��&nbsp;��� ��� ��&nbsp;����� �&nbsp;��� ��&nbsp;�����? ��� ��� ��� ���������� ������� �������� ��� ����� ������� ��������.

<p>������ ����� ��������, ��&nbsp;��������, ������:
<br><i>��&nbsp;����� ����������� ����� ����� ��&nbsp;����� ����� ��&nbsp;���������� ������� �������.</i>
<br>��� ��������� ���������:
<br><i>����&nbsp;&mdash; ��&nbsp;��� ��������, ��&nbsp;������ �������.</i>

<p>������ ����� ���� ��������:
<br><i>������� ����, ��� ����� ��� ����������,
<br>��������� ������, ���� ����������
<br>&lt;...&gt; ����� ����� ���.</i>
<br>�&nbsp;������ ������ ��&nbsp;��������� ������ ��������, ��������, ��������� ������.

<p>��&nbsp;���� ������� ���������!
<br>��� ������� ���� ������, ������� ����� ������ �����:
<br><i>���� ��&nbsp;�������, ���� ����� ��� ������� �������.</i>

<p>��, ����� ����� ��&nbsp;�����, ��� ��&nbsp;����������, ��� ����� ��� ��� �������� ��&nbsp;�����: ��� ������� (�������� ��&nbsp;�������), ��� �&nbsp;������. ��� ��� ��&nbsp;���� ������ ������� ��&nbsp;���������, �&nbsp;����� ��������� ����� ���� �&nbsp;�������� �������.


<h4>������������, ���������� �&nbsp;�������� ������</h4>

<p>������ ������ (������� ��&nbsp;��������) ��������� �&nbsp;������������ �������.

<p>������������ ��� ����� ������ &laquo;����������&raquo; �&nbsp;������ ������� ������ (���, �&nbsp;���� ��� ������� ������� �&nbsp;������).

<p>���� ��&nbsp;����� ����� ������ (����� �������� �&nbsp;��������), ��&nbsp;������������ ��� �������������� ����� ������ ���� ������ ��������� �&nbsp;��� �������, ������� ��&nbsp;&laquo;������&raquo;. �&nbsp;��������� ������ ��� ����� ����� �������� �������� &laquo;�������&raquo;&nbsp;&mdash; ���� ��&nbsp;������ ��� ������������. ����� �������� �������� ��&nbsp;�&nbsp;����� ������ �������� ��������� ��&nbsp;������ (������� ������ ��&nbsp;�������) .

<p>���� ��������, ��������� ������ �������, �������� ��� (��������� �������) �������, �&nbsp;�������� ��� ������� ����� ������� ����� ������, ������� �������� ��� ������ �������� �&nbsp;������ ������: ��&nbsp;����� ��&nbsp;������������ �&nbsp;���������� ������ (����� ������). ������, �����������, ��� ������ ��������.

<p>�������� ������ &laquo;��-�����������&raquo; ������! ����� ���� ���������� ���, ��� ������.

<p>��������, ������ &laquo;<i>��� ������� �&nbsp;������ ������ ��������� �����</i>&raquo; ��-������������ ������� ��&nbsp;�����&nbsp;&mdash; ��� ����� ��������� ��&nbsp;������� �&nbsp;������ �&nbsp;������, ��&nbsp;�&nbsp;��������� ��� �������� ������ ��&nbsp;�� �����.

<p>�������� ������ ����� ���������� (������), �����������, ����� ���� ���, ��� ������� ��������������� �������� ������. ��-���������� ������ ��&nbsp;���������� �&nbsp;���������.

<p>���� ��&nbsp;����������� ���������� ������ ��&nbsp;�������� ��� &laquo;��������&raquo; ������ ������� ����� (����������� �������, ��� �&nbsp;������ <i>������� �&nbsp;�����</i>), ��� ����� ������ ���� �������� �&nbsp;����� ��&nbsp;��������. ������ ��������� �� ��&nbsp;�� �������.

<p>(��� �������� ������ ������-������, ������� ������ ������ ��������� ��&nbsp;������, ��� ������� ��� &laquo;������&raquo;, ������� ����� ������ &laquo;������&raquo; �������� �&nbsp;��������� ������� ������).

<h4>������</h4>

<p><i>1. ����� �������� ������&nbsp;&mdash; ��� ����� �������, ���������� ��&nbsp;��������, ������� ��������� ��������, ��������� ������� ������� ��&nbsp;������� ���. ��� ���������&nbsp;&mdash; ���� ������ ������������� ��� ���� ������� ������� (�&nbsp;�������&nbsp;&mdash; ��������� ��&nbsp;������ �&nbsp;���������� �&nbsp;����), ���� ������ �����������, ��&nbsp;��&nbsp;��� ��&nbsp;��������.</i>
<p><i>2. ������ ������ (������� ��&nbsp;��������) ��������� �&nbsp;������������ �������. ������������ ��� ����� ������ &laquo;����������&raquo; �&nbsp;������ ������� ������ (���, �&nbsp;���� ��� ������� ������� �&nbsp;������). &laquo;��-����������&raquo;, �������� �������� ����� ������ (�������� �&nbsp;��������), ������������ ��� ��&nbsp;������ �&nbsp;��� ����� �������� ������ (��������) &laquo;�������&raquo;.</i>
<p><i>3. ��&nbsp;������ �&nbsp;������� ���� ����� ������ (�&nbsp;��������&nbsp;&mdash; 2 �������� ��&nbsp;�������� ������������� �&nbsp;1&nbsp;&mdash; ��&nbsp;��������������, ��������� ������� �&nbsp;������, �&nbsp;�����&nbsp;&mdash; 1 �������� ��&nbsp;���� ��&nbsp;��������� ��������������).</i>
<p><i>4. �������� ����� ������ �����: ������� &laquo;�������&raquo; ������ (���-�� ������� ����� �������� �&nbsp;��������), ��&nbsp;�������� (���������� �&nbsp;����� ������� ������� ����� ������� ��&nbsp;������������ �&nbsp;������ �������, �&nbsp;��� ������������, ��� ���������� ��&nbsp;����� ��������� �&nbsp;��� �������, �&nbsp;������� ����� �������), �&nbsp;���������� ���������� ��� ����� (���������� ����� ������� ��&nbsp;��&nbsp;������ ����� ������), �&nbsp;����� �&nbsp;���������� &laquo;��������&raquo; ������ ��&nbsp;������ �������� (��&nbsp;������ ��������� ������������ �&nbsp;����� ��� ���������� �������� ������ ������������ ��������� ����� ������ �&nbsp;����� �������).</i>
<p><i>5. �������� ����� �&nbsp;����� ��������, ������� ����� �&nbsp;����� ����� �����. �������� ������ &laquo;��-�����������&raquo; ������! ����� ���� ���������� ���, ��� ������ �&nbsp;���������� ������.</i>

  <? right_block('poselok'); ?>
  </tr>
  <? show_footer(); ?>
