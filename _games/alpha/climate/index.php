<?
define("ROOT","../../../");
require_once(ROOT . 'funcs.php');
show_header("����� ��������: ����������� :: ������� :: ������������",ALPHA);
show_menu("inc/main.menu"); ?>

	    <td class="box">
<div class="boxheader"><a href="http://alpha.bastilia.ru/">����� ��������: �����������</a> :: ������� :: ������������</div>
<p><i>������� ������ ������� �������� �����!</i></p>
<h4>����� �������� ��� ������</h4>
<p>�&nbsp;������ ������ ���������� �����������������, ��� ������� ��� ����� ��&nbsp;����� ������������� ������, ������� ��������� ������������� ������ ��� ��� ����. ��������� ��&nbsp;������� ���������, ����������� ��� ������� ��� �&nbsp;��������� ������� ��&nbsp;�������������� ���������, �&nbsp;��� ���������� �������� ���� ��������������, ��� ����� ������������ ������������ ��������� �&nbsp;������������ �&nbsp;���������� ��������.</p>
<p>�������� ��������, ������� ��������� ����������� ������������, �������� �����, ��&nbsp;����� ������������� ������� �������.</p>
<p>������������ ����� ����� ������������� ������ �������������� ��������� ����� (������� ������������ ���������� ��������).</p>
<p>��� ������� ������� �&nbsp;���� ������ ������ ���������� ����������: ����������� �����, ������� ����� �&nbsp;����� �&nbsp;�������, ������� ����������� �&nbsp;�.�.</p>
<p>��������, ��� ������� ����� ���� ��� ���������� ������� ��������� �����, �������� ��������� ���������. ��������, ������ ��&nbsp;�������, ����� �������� ��������� �&nbsp;��������� ������ �����, ������ �������������� ��� �������� �������� ����� �������� ��&nbsp;�������� ����� ���������� �����.</p>
<p>��������� �������� ��������, ����������� �������������� ������������������ ����� ��� �������� ����� ���������� �����. ������������� ������� �������� ��������� �����, ������� ���������� �������������� ��������� ����� �&nbsp;��������������� ������ �������.</p>
<p>��� ��������� ������� ���������� ����������� �����������, ����������� ������ ���������� ����� �&nbsp;�����. ������������ �������� ����� �������� ������� ������ ��&nbsp;������� ��������������� �������, �&nbsp;��&nbsp;������� ��&nbsp;���� ������� ����� ��&nbsp;����������� ���������� ��� ����. ������ ���������� ����� �������, ������� �������� �������� ����������������: ��&nbsp;����, ��&nbsp;����������� ������ ��� ����, ����� ������ ������������ �������� �&nbsp;���������� �����.</p>
<p>��� ���������� ������������� ������ ������� ��&nbsp;����������� ����������� ��&nbsp;������� ����������� �������, ���������� ��������. ��������� ������� (�&nbsp;���������� �����), ���� ��� ���������, ���������� ��&nbsp;���� ������� ������. ������ �&nbsp;������������ �������� ��&nbsp;�������� �&nbsp;������� ����������� �������.</p>
<p><br />
����������� �������, �������� ��&nbsp;������, ������������ ����� ����������������� �������. ������� ���� ������������� �������� 3&times;3, ��� �&nbsp;������ ��������� ������� �������. �&nbsp;�������� ��� �������� ����� ����������������� ����� ��������� �&nbsp;��������� ������ �����.</p>
<p>����������� �������� ��������� ������� ���������� ��&nbsp;���� ��� ��������� ��������.</p>
<p>�&nbsp;���������� ����� ���� ������� ��� ��������� ����� ���� �����:</p>
<ul>
<li>�����������&nbsp;&mdash; ������� ����� ����� ��� �&nbsp;���� �&nbsp;����� �������� ���������� ����� �&nbsp;������������ �����.</li>
<li>������������&nbsp;&mdash; ����������� ������ �������, �&nbsp;��� ����� �&nbsp;��&nbsp;������� ����������. ��� ������� ����������, ������������ ������ ����� ���������� �&nbsp;������� ���������� ���������� ��&nbsp;��������� �������, ���� ��� �������.</li>
</ul>
<p>��� ������������ ��������� ���������� ����� �&nbsp;������ �&nbsp;���������� ����� �&nbsp;�������� ������� �������� ��������� �&nbsp;��������� ���������, ��������� �&nbsp;��������� ������ ��������, ������ �&nbsp;���������� �&nbsp;�.�. ����� ���������� ����� �&nbsp;������ ������ ��&nbsp;��� �&nbsp;���������� ������� ����� �&nbsp;�����.</p>
<h4>��������� �������</h4>
<p>��� �&nbsp;��� ���� ������ �&nbsp;������ ������ �������� ������ ��&nbsp;������������ �������.</p>
<p>��������� ��������:</p>
<ol>
<li> ������ �������� ���������� ����� �&nbsp;������, ��� ����� ����������:
<ol type="a">
<li>������ �&nbsp;�������� �&nbsp;������ �����</li>
<li>�������� ������� ������� �����</li>
<li> ����� ����� �&nbsp;������� ������ �������� ������������, �������� ������, ��������������� �������� ���������� ����������, �&nbsp;���������� ��� ��������� ����� ��� ��������. ���� ������ ��� �������, ��&nbsp;������ ���������� ��� ��������� �����.</li>
<li>������ �&nbsp;�������� ������� ����� �&nbsp;������ ��&nbsp;����, �&nbsp;������ �&nbsp;������� ���������� �����</li>
</ol>
</li>
<li>����������������� ������ ������� ���� ��&nbsp;������������ ������������ �&nbsp;������������ ����� (��������, ���� ��&nbsp;������� ��������), ����� ������ ����� ��&nbsp;����� (���� �������� ����������) ���������� ������� �������� ����� ��� ��������� �������.</li>
<li>������������� ���������� ��������������� �������� ����� �&nbsp;���������� ������ ��&nbsp;�������. ��� ����� ��&nbsp;�������� ��&nbsp;������ �&nbsp;������� ������ �&nbsp;������� �����, ��&nbsp;��������� ������� ��&nbsp;��������� ����� ���������� ��&nbsp;�������, ��� ��� ���� ���������� ����� �����������+�����������. ����� �������, ����� ���������, ����� �&nbsp;���������� ��������� ��������� ������ �������.</li>
<li>���������� ����������������� ����� �&nbsp;������������� ���������� �����. ���� ������� ��� ����������, ��&nbsp;���� ����� ����� �����������. ������������� ������� ��&nbsp;��������.</li>
<li>������ ��� ��������� ����������� ������� ����� (��������, ��� ��������� ���������������) �&nbsp;������� ����� ������ ��&nbsp;������� �����.</li>
</ol>
<p>���������� ����� ����������� ������ ��&nbsp;����.</p>
<p>
  <? right_block('alpha'); ?>
  </tr>
  <? show_footer(); ?>
