<?
define("ROOT","../../../");
require_once(ROOT . 'funcs.php');
show_header("����� ��������: ����������� :: ������� :: ���������",ALPHA);
show_menu("inc/main.menu"); ?>

	    <td class="box">
<div class="boxheader"><a href="http://alpha.bastilia.ru/">����� ��������: �����������</a> :: ������� :: ���������</div>
<p><span id="internal-source-marker_0.5180344206497263">���� ��� ������, ����������� ���������� �������� ������� �����&nbsp;&mdash; �&nbsp;����� �������� ��� �������. ����� ���������� ���������� �������&nbsp;&mdash; ������ ����� ��&nbsp;����� ������ ���������� ��������. ��&nbsp;������ ���� ������� ������� �&nbsp;��&nbsp;����������� ��������, ��&nbsp;��� ������ ������ ���� ���� �&nbsp;���������� ��������.</span><p>������ �������� ����� ����:
<ul>
<li>���������� ���������, ��������� ���� �����. ����� �������� ��&nbsp;��&nbsp;��� ��&nbsp;���������� ��&nbsp;����� ������� �&nbsp;��&nbsp;������������ �&nbsp;������</li>
<li>������������ �&nbsp;�����-�� �������, ����������� ��&nbsp;������� �&nbsp;��� ��� ���� ����</li>
<li>���������������� ������ ��������, ���� ��&nbsp;����������� ��&nbsp;�&nbsp;����� �������. ��&nbsp;������ ���� ����������� ��� ��������� ��������� �&nbsp;���� ���������.</li>
</ul>
<h4>��������</h4>
<p>
�&nbsp;�������� ���� ��������� ��������� ������������ ������������ ��&nbsp;��&nbsp;���� <strong>����������</strong>. �������� ������� ������ ����� ���������� �&nbsp;������������� ��� ��� ���� ����, ��&nbsp;������ �������� ������������� ����� ��������. 
<p>��� �������� �������� �������� �&nbsp;������� ��������� ������������ ���� ��&nbsp;������ <strong>����</strong> (����������� ��&nbsp;������ ���� ��&nbsp;��� ��������������� ����).
<table width="90%" border="2" align="center" cellpadding="1" cellspacing="1">
<tr>
<td><div align="center"><strong>�����</strong></div></td>
<td><div align="center"><strong>�������</strong></div></td>
</tr>
<tr>
<td><ul>
<li><span id="internal-source-marker_0.5180344206497263">��� ������������� ���������: �������� ������� ��� ������������� ������� �������, ��������� ������� ��� �������� ��� ���� ��&nbsp;�����.</span></li>
<li>��� ����������: ���������� ����������� ����������, �����&nbsp;&mdash; �������, �&nbsp;���������&nbsp;&mdash; ���� ��������� ������������ ���������. </li>
<li>���������� �������� ����&nbsp;&mdash; ������� �����: ����������, ������, ���������, ������� �&nbsp;��������.</li>
</ul></td>
<td><ul>
<li><span id="internal-source-marker_0.5180344206497263">��� ����������: ���������� �������, ������������� ��������� ��&nbsp;������� ������������� ���������&nbsp;&mdash; ����, ���, ���������.</span></li>
<li>��������� �&nbsp;���� ������ �������������: ���� �������� ��&nbsp;��������� �&nbsp;����, ����&nbsp;&mdash; �&nbsp;������, ������&nbsp;&mdash; �&nbsp;���������.</li>
<li>���������� �������� ����&nbsp;&mdash; ����. ������ ������� ���� ��������, ��&nbsp;��������� ������� ���, ������ ����������� ����, ����� ������� �������� ���������.</li>
</ul></td>
</tr>
<tr>
<td><div align="center"><strong>��������</strong></div></td>
<td><div align="center"><strong>��������</strong></div></td>
</tr>
<tr>
<td><ul>
<li><span id="internal-source-marker_0.5180344206497263">�������������� �������, ����� ��������, �������������.</span></li>
<li>������ �������� (��&nbsp;������ ������� �������).</li>
<li>��&nbsp;�������� ������������, ������ ����������� ������, ���������� ������������ ������.</li>
<li>������� ������ ���������� ������������� ��&nbsp;������ ����.</li>
</ul></td>
<td><ul>
<li><span id="internal-source-marker_0.5180344206497263">����� ������ �����, ����� ����� ������ ������ �����������.</span></li>
<li>������� ������ ��������� ���� &laquo;����&raquo; ������������� &laquo;����&raquo;.</li>
<li>������ �������� ������, ������, ��&nbsp;����� ��������� ����� ���������� ����������� ���� ������ �&nbsp;������� �&nbsp;�����������.</li>
<li>������� ����� �&nbsp;������ ���������� ��&nbsp;��������� ������.</li>
</ul></td>
</tr>
<tr>
<td><div align="center"><strong>��������� ������� </strong></div></td>
<td><div align="center"><strong>������� ������� </strong></div></td>
</tr>
<tr>
<td><ul>
<li><span id="internal-source-marker_0.5180344206497263">������� ������ ����� �&nbsp;����� ���� �&nbsp;������ �������������� �&nbsp;�����.</span></li>
<li>��������� ������� ������ ���� ����������� ������������ ��������� �&nbsp;�����.</li>
<li>��&nbsp;����� �&nbsp;������ ����� ���� �&nbsp;��������� �&nbsp;��������.</li>
<li>��&nbsp;��� �������� ����� �&nbsp;��&nbsp;������ �������� �����.</li>
</ul></td>
<td><ul>
<li><span id="internal-source-marker_0.5180344206497263">��&nbsp;������ ������ ����� ������������, �&nbsp;������ �������� ���������� ��� ��� �����.</span></li>
<li>��&nbsp;������ ������� ��������� ������� ������ ���� �������� ������ ��������.</li>
<li>��&nbsp;����� ������� ������, �&nbsp;����� �&nbsp;����� �������� ��������� �&nbsp;����� �����������.</li>
<li>����� ������ ��� ��� ����� ������.</li>
</ul></td>
</tr>
<tr>
<td><div align="center"><strong>����������</strong></div></td>
<td><div align="center"><strong>���������</strong></div></td>
</tr>
<tr>
<td><ul>
<li><span id="internal-source-marker_0.5180344206497263">����� ������� ����� ���������� �&nbsp;�������� ���� ������.</span></li>
<li>��� ����� �������� �������� �������, ���� ����� ������.</li>
<li>������ ������, ��&nbsp;���� ����������� ��������.</li>
<li>��������� ��������.</li>
</ul></td>
<td><ul>
<li><span id="internal-source-marker_0.5180344206497263">������ ������ ��������� ��&nbsp;���� ��������������� ��&nbsp;���� ��������.</span>
</li>
<li>
������ �������� ��&nbsp;��������, �&nbsp;���� ��� ������� ������, ����� ��&nbsp;��������. </li>
<li>��&nbsp;������ ��&nbsp;�������&nbsp;��: ������ ������ ��&nbsp;���� ������ �����. </li>
<li>��������� ���������.</li>
</ul></td>
</tr>
</table>
<p><span id="internal-source-marker_0.5180344206497263">��&nbsp;��������� ��������� ��������� ���������, ������������ ������ ������, ����������: </span><strong>��������� ��������� ������ �������� ��������</strong> (�������, ��&nbsp;������ ����������&nbsp;&mdash; ������, �����������, ������� ����� ����� ���� ���������).<p>
�������� ��������, �������� ���������� �&nbsp;������ ��&nbsp;��������� �������� ��������.</p>

<p><b>����� ���������� ��������, ��������������� ���� ��&nbsp;����, ����� �������� ���� �������� � ��������, ������ ��������������� ����� � ���� ���������. ����������� ������� �������.</b></p>
    <? right_block('alpha'); ?>
    </tr>
  <? show_footer(); ?>
</p>
	    