<!DOCTYPE html>
<html>

<head>
    <title>Бастилия и друзья. «Чёрный город»</title>
    <meta name="description" content="ПРИ «Чёрный город»">
    <meta name="keywords" content="">
    <meta charset="utf-8">
    <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />

    <!-- Favicons -->
   
	<link rel="shortcut icon" href="http://blackcity.bastilia.ru/wp-content/uploads/2014/09/favicon.png">
	

    <!-- CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/style-responsive.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/vertical-rhythm.min.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/magnific-popup.css">


</head>

<body class="appear-animate">

    <!-- Page Loader -->
    <div class="page-loader">
        <div class="loader">Загрузка...</div>
    </div>
    <!-- End Page Loader -->

    <!-- Page Wrap -->
    <div class="page" id="top">

        <!-- Fullwidth Slider -->
        <div class="home-section fullwidth-slider bg-dark" id="home">


            <!-- Slide Item -->
            <section class="home-section bg-scroll bg-dark-alfa-30" data-background="images/full-width-images/section-bg-6.jpg">
                <div class="js-height-full container">

                    <!-- Hero Content -->
                    <div class="home-content">
                        <div class="home-text">

                            <h2 class="hs-line-14 font-alt mb-50 mb-xs-30">
							<span class="t1">Ролевая игра</span>
							<span class="t2">"Черный город"</span>
							</h2>
                            <h1 class="hs-line-8 no-transp font-alt mb-50 mb-xs-30">10-12 июля, Ленобласть, 90 игроков </h1>	
                            <div class="local-scroll">
                                <a target="_blank" href="http://blackcity.bastilia.ru/" class="btn btn-mod btn-border-w btn-medium btn-round">На сайт</a>
                                <span class="hidden-xs">&nbsp;</span>
                                <a target="_blank" href="https://vk.com/blackcity1914" class="btn btn-mod btn-border-w btn-medium btn-round">В группу</a>
								<span class="hidden-xs">&nbsp;</span>
								<a target="_blank" href="http://blackcity.bastilia.ru/roles/" class="btn btn-mod btn-border-w btn-medium btn-round white">Подать заявку</a>  

                            </div>

                        </div>
                    </div>
                    <!-- End Hero Content -->

                </div>
            </section>
            <!-- End Slide Item -->

            <!-- Slide Item -->
            <section class="home-section bg-scroll bg-dark-alfa-30" data-background="images/full-width-images/section-bg-6-2.jpg">
                <div class="js-height-full container">

                    <!-- Hero Content -->
                    <div class="home-content">
                        <div class="home-text">

                             <h2 class="hs-line-14 font-alt mb-50 mb-xs-30">
							<span class="t1">Ролевая игра</span>
							<span class="t2">"Черный город"</span>
							</h2>
                            <h1 class="hs-line-8 no-transp font-alt mb-50 mb-xs-30">10-12 июля, Ленобласть, 90 игроков </h1>
                            <div class="local-scroll">
                              <a target="_blank" href="http://blackcity.bastilia.ru/" class="btn btn-mod btn-border-w btn-medium btn-round">На сайт</a>
                                <span class="hidden-xs">&nbsp;</span>
                                <a target="_blank" href="https://vk.com/blackcity1914" class="btn btn-mod btn-border-w btn-medium btn-round">В группу</a>
								<span class="hidden-xs">&nbsp;</span>
								<a target="_blank" href="http://blackcity.bastilia.ru/roles/" class="btn btn-mod btn-border-w btn-medium btn-round white">Подать заявку</a>  

                            </div>

                        </div>
                    </div>
                    <!-- End Hero Content -->

                </div>
            </section>
            <!-- End Slide Item -->
			
           <!-- Slide Item -->
            <section class="home-section bg-scroll bg-dark-alfa-30" data-background="images/full-width-images/section-bg-6-3.jpg">
                <div class="js-height-full container">

                    <!-- Hero Content -->
                    <div class="home-content">
                        <div class="home-text">

                             <h2 class="hs-line-14 font-alt mb-50 mb-xs-30">
							<span class="t1">Ролевая игра</span>
							<span class="t2">"Черный город"</span>
							</h2> 
                            <h1 class="hs-line-8 no-transp font-alt mb-50 mb-xs-30">10-12 июля, Ленобласть, 90 игроков </h1>
                            <div class="local-scroll">
                               <a target="_blank" href="http://blackcity.bastilia.ru/" class="btn btn-mod btn-border-w btn-medium btn-round">На сайт</a>
                                <span class="hidden-xs">&nbsp;</span>
                                <a target="_blank" href="https://vk.com/blackcity1914" class="btn btn-mod btn-border-w btn-medium btn-round">В группу</a>
								<span class="hidden-xs">&nbsp;</span>
								<a target="_blank" href="http://blackcity.bastilia.ru/roles/" class="btn btn-mod btn-border-w btn-medium btn-round white">Подать заявку</a>  

                            </div>

                        </div>
                    </div>
                    <!-- End Hero Content -->

                </div>
            </section>
            <!-- End Slide Item -->

        </div>
        <!-- End Fullwidth Slider -->

        <!-- Navigation panel -->
        <nav class="main-nav dark transparent stick-fixed">
            <div class="full-wrapper relative clearfix">
                <!-- Logo ( * your text or image into link tag *) -->
                <div class="nav-logo-wrap local-scroll">
				
                <!--     <a href="#top" class="logo">
                        <img src="images/logo-white.png" width="118" height="27" alt="" />
                    </a>
				-->	
                </div>
                <div class="mobile-nav">
                    <i class="fa fa-bars"></i>
                </div>
                <!-- Main Menu -->
                <div class="inner-nav desktop-nav">
                    <ul class="clearlist scroll-nav local-scroll">
                        <li class="active"><a target="_blank" href="http://blackcity.bastilia.ru/">На сайт</a>
                        </li>
                        <li><a target="_blank" href="https://vk.com/blackcity1914">В группу</a>
                        </li>
                        <li><a target="_blank" href="http://blackcity.bastilia.ru/roles/">Подать заявку</a>
                        </li>
                        </ul>
                </div>
            </div>
        </nav>
        <!-- End Navigation panel -->

		
<!-- Features Section
        <section id="crules" class="page-section interest">
            <div class="container relative">
			<h2 class="section-title font-alt align-left mb-70 mb-sm-40">
                        #Бастилия и друзья приглашает:
             </h2>

             
                <div class="row multi-columns-row alt-features-grid">

               
                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="alt-features-item align-center">
                            <div class="alt-features-icon">
                                <span class="icon-circle-compass"></span>
                            </div>
                            <h3 class="alt-features-title font-alt">1 шаг</h3>
                            <div class="alt-features-descr align-left">
                                Купи свадебный костюм и два любых аксессуара к нему в магазинах Albione!
                            </div>
                        </div>
                    </div>
                  
                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="alt-features-item align-center">
                            <div class="alt-features-icon">
                                <span class="icon-camera"></span>
                            </div>
                            <h3 class="alt-features-title font-alt">2-й шаг</h3>
                            <div class="alt-features-descr align-left">
                                Весело отпразднуй свадьбу и не забудь сфотографироваться со своей избраницей!
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="alt-features-item align-center">
                            <div class="alt-features-icon">
                                <span class="icon-presentation"></span>
                            </div>
                            <h3 class="alt-features-title font-alt">3-й шаг</h3>
                            <div class="alt-features-descr align-left">
                                Загрузи фото на сайт и приглашай друзей проголосовать за вашу пару!
                            </div>
                        </div>
                    </div>
        
                    <div class="col-sm-6 col-md-4 col-lg-4">
                        <div class="alt-features-item align-center">
                            <div class="alt-features-icon">
                                <span class="icon-browser"></span>
                            </div>
                            <h3 class="alt-features-title font-alt">4-й шаг</h3>
                            <div class="alt-features-descr align-left">
                                Выиграй поездку на Мальдивы, Total look от Albione, ужин в шикарном ресторане или 10 других супер-призов!
                            </div>
                        </div>
                    </div>




                </div>
              

            </div>
        </section>
        <!-- End Features Section -->
		
		       <!-- Section 
            <section class="home_slider1 page-section members bg-dark-alfa-70" data-background="">
			-->
			<section class="home_slider1 page-section members bg-dark-alfa-70" data-background="">
                <div class="container relative">
       <h2 class="section-title center font-alt align-left mb-70 mb-sm-40">
        #Бастилия и друзья приглашают
       </h2>
             
                    <!-- Features Grid -->
                    <div class="grid cl"> 
                        
                   <!-- Features Item -->
                        <div class="features-item">
                            <div class="icon21">
								<img src="images/memb/memb1.jpg" alt="">
                            </div>
                            <div class="features-title font-alt">
                                Скракан
                            </div>
                            <div class="features-descr">
                               Борис Земсков
                            </div>
							<!-- <div class="features-descr">
                              Главный мастер / АХЧ
                            </div>  -->
                        </div>
                        <!-- End Features Item -->
                        
                         <!-- Features Item -->
                        <div class="features-item">
                            <div class="icon21">
								<img src="images/memb/memb2.jpg" alt="">
                            </div>
                            <div class="features-title font-alt">
                                Митяй
                            </div>
                            <div class="features-descr">
                               Дмитрий Иоффе
                            </div>
							
                        </div>
                        <!-- End Features Item -->  

                     <!-- Features Item -->
                        <div class="features-item">
                            <div class="icon21">
								<img src="images/memb/memb3.jpg" alt="">
                            </div>
                            <div class="features-title font-alt">
                                Ван
                            </div>
                            <div class="features-descr">
                               Иван Котомин 
                            </div>
							
                        </div>
                        <!-- End Features Item --> 
						
                      <!-- Features Item -->
                        <div class="features-item">
                            <div class="icon21">
								<img src="images/memb/memb4.jpg" alt="">
                            </div>
                            <div class="features-title font-alt">
                                Антон ХЗ
                            </div>
                            <div class="features-descr">
                               Антон Зайцев
                            </div>
						
                        </div>
                        <!-- End Features Item -->   
                    
                
					
					     <!-- Features Item -->
                        <div class="features-item">
                            <div class="icon21">
								<img src="images/memb/memb5.jpg" alt="">
                            </div>
                            <div class="features-title font-alt">
                                Фрикси
                            </div>
                            <div class="features-descr">
                               Екатерина Годнева
                            </div>
							
                        </div>
                        <!-- End Features Item -->   
                    
                 
					
					<!-- Features Item -->
                        <div class="features-item">
                            <div class="icon21">
								<img src="images/memb/memb6.jpg" alt="">
                            </div>
                            <div class="features-title font-alt">
                               Борисыч
                            </div>
                            <div class="features-descr">
                               Алексей Царев
                            </div>
						
                        </div>
                        <!-- End Features Item -->  
						
 					<!-- Features Item -->
                        <div class="features-item">
                            <div class="icon21">
								<img src="images/memb/memb7.jpg" alt="">
                            </div>
                            <div class="features-title font-alt">
                            Алисия
                            </div>
                            <div class="features-descr">
                             Ольга Канальцева
                            </div>
						
                        </div>
                        <!-- End Features Item --> 
						
   					     <!-- Features Item -->
                        <div class="features-item">
                            <div class="icon21">
								<img src="images/memb/memb8.jpg" alt="">
                            </div>
                            <div class="features-title font-alt">
                            Ксионтес
                            </div>
                            <div class="features-descr">
                            Юлия Земскова
                            </div>
						
                        </div>
                        <!-- End Features Item --> 
						
						<!-- Features Item -->
                        <div class="features-item">
                            <div class="icon21">
								<img src="images/memb/memb9.jpg" alt="">
                            </div>
                            <div class="features-title font-alt">
                            Лео
                            </div>
                            <div class="features-descr">
                            Леонид Царев
                            </div>
						
                        </div>
                        <!-- End Features Item --> 
						
						
						<!-- Features Item -->
                        <div class="features-item">
                            <div class="icon21">
								<img src="images/memb/memb10.jpg" alt="">
                            </div>
                            <div class="features-title font-alt">
                            Энно
                            </div>
                            <div class="features-descr">
                            Семён Сетраков
                            </div>						
                        </div>
                        <!-- End Features Item --> 
			
 
						
                    </div>
                    <!-- Features Grid -->
                
                </div>
            </section>
            <!-- End Section -->


        <!-- About Section -->
        <section class="page-section" id="about">
            <div class="container relative">

                <h2 class="section-title center font-alt align-left mb-70 mb-sm-40">
                        Твои кореша хотят поехать, а ты?
                    </h2>

                <div class="section-text mb-50 mb-sm-20">
				<script type="text/javascript" src="//vk.com/js/api/openapi.js?116"></script>

<!-- VK Widget -->
<div id="vk_groups"></div>
<script type="text/javascript">
VK.Widgets.Group("vk_groups", {mode: 0, width: "1200", height: "400", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, 76667484);
</script>
               
                </div>


            </div>
        </section>
        <!-- End About Section -->

		            <!-- Section -->
            <section class="page-section must_play">
                <div class="container relative">
<h2 class="section-title center font-alt align-left mb-70 mb-sm-40">Почему надо ехать?</h2>             
                    <!-- Features Grid -->
                    <div class="row multi-columns-row alt-features-grid">
                        
                        <!-- Features Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="alt-features-item align-center">
                              <div class="icon"><img src="images/wyp/wyp1.jpg" alt="wyp"></div>
                                <h3 class="alt-features-title font-alt"></h3>
                                <div class="alt-features-descr align-left">
                                 Как всегда на нашей игре &#8212; отличные игроки и мастера.   
                                </div>
                            </div>
                        </div>
                        <!-- End Features Item -->
                        
                        <!-- Features Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="alt-features-item align-center">
                               <div class="icon"><img src="images/wyp/wyp2.jpg" alt="wyp"></div>
                                <h3 class="alt-features-title font-alt"></h3>
                                <div class="alt-features-descr align-left">
                                Наш взнос по сегодняшним меркам &#8212; совсем не зверский, и за каждый потраченный рубль мы традиционно отчитаемся.   
                                </div>
                            </div>
                        </div>
                        <!-- End Features Item -->
                        
                        <!-- Features Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="alt-features-item align-center">
                                <div class="icon"><img src="images/wyp/wyp3.jpg" alt="wyp"></div>
                                <h3 class="alt-features-title font-alt"></h3>
                                <div class="alt-features-descr align-left">
                                 Мы делаем жанровую игру, которая формирует пространство для сотворчества и создания смыслов, а не транслирует идею.  
                                </div>
                            </div>
                        </div>
                        <!-- End Features Item -->
                        
                        <!-- Features Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="alt-features-item align-center">
                                <div class="icon"><img src="images/wyp/wyp4.jpg" alt="wyp"></div>
                                <h3 class="alt-features-title font-alt"></h3>
                                <div class="alt-features-descr align-left">
                                 Наша игра начинается в пятницу вечером, т.е. при определенных условиях вам даже не потребуется брать отгул на работе.   
                                </div>
                            </div>
                        </div>
                        <!-- End Features Item -->
                        
                        <!-- Features Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="alt-features-item align-center">
                                <div class="icon"><img src="images/wyp/wyp5.jpg" alt="wyp"></div>
                                <h3 class="alt-features-title font-alt"></h3>
                                <div class="alt-features-descr align-left">
                                На нашу игру не стрёмно заявляться булочником или дворником, ведь каждый персонаж шпионского боевика &#8212; не тот, за кого себя выдает.
                                </div>
                            </div>
                        </div>
                        <!-- End Features Item -->
                        
                        <!-- Features Item -->
                        <div class="col-sm-6 col-md-4 col-lg-4">
                            <div class="alt-features-item align-center">
                                <div class="icon"><img src="images/wyp/wyp6.jpg" alt="wyp"></div> 
                                <h3 class="alt-features-title font-alt"> </h3>
                                <div class="alt-features-descr align-left">
                                Игры по началу XX века &#8212; не редкость, но часто ли вам доводилось играть в добычу нефти на задворках Империи?   
                                </div>
                            </div>
                        </div>
                        <!-- End Features Item -->
                        
                    </div>
                    <!-- End Features Grid -->
                    
                </div>
            </section>
            <!-- End Section -->

		
	     <!-- Section -->
            <section class="page-section bottom_nav">
                <div class="container relative">
<div class="local-scroll">

								<a target="_blank" href="http://blackcity.bastilia.ru/" class="btn btn-mod btn-border-w btn-medium btn-round">На сайт</a>
                                <span class="hidden-xs">&nbsp;</span>
                                <a target="_blank" href="https://vk.com/blackcity1914" class="btn btn-mod btn-border-w btn-medium btn-round">В группу</a>
								<span class="hidden-xs">&nbsp;</span>
								<a target="_blank" href="http://blackcity.bastilia.ru/roles/" class="btn btn-mod btn-border-w btn-medium btn-round black">Подать заявку</a>

</div>
                </div>
            </section>
            <!-- End Section -->

			
        <!-- Foter -->
        <footer class="page-section bg-gray-lighter footer pb-60">
            <div class="container">

                <!-- Footer Logo 
                <div class="sitename local-scroll mb-30 wow fadeInUp" data-wow-duration="1.5s">
                    <a href="#top">
                   
					 <span class="t1">Ролевая игра</span>  <span class="t2">"Черный город"</span>
                    </a>
                </div>
                <!-- End Footer Logo -->


                <!-- Footer Text -->
                <div class="footer-text">

                    <!-- Copyright -->
                    <div class="footer-copy font-alt">
                        <a href="/" target="_blank">&copy; 2015</a>
                    </div>
                    <!-- End Copyright -->

                    <div class="footer-made">

                    </div>

                </div>
                <!-- End Footer Text -->

            </div>


            <!-- Top Link -->
            <div class="local-scroll">
                <a href="#top" class="link-to-top"><i class="fa fa-caret-up"></i></a>
            </div>
            <!-- End Top Link -->

        </footer>
        <!-- End Foter -->


    </div>
    <!-- End Page Wrap -->


    <!-- JS -->
    <script type="text/javascript" src="js/jquery-1.11.1.min.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/SmoothScroll.js"></script>
    <script type="text/javascript" src="js/jquery.scrollTo.min.js"></script>
    <script type="text/javascript" src="js/jquery.localScroll.min.js"></script>
    <script type="text/javascript" src="js/jquery.viewport.mini.js"></script>
    <script type="text/javascript" src="js/jquery.countTo.js"></script>
    <script type="text/javascript" src="js/jquery.appear.js"></script>
    <script type="text/javascript" src="js/jquery.sticky.js"></script>
    <script type="text/javascript" src="js/jquery.parallax-1.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.fitvids.js"></script>
    <script type="text/javascript" src="js/owl.carousel.min.js"></script>
    <script type="text/javascript" src="js/isotope.pkgd.min.js"></script>
    <script type="text/javascript" src="js/imagesloaded.pkgd.min.js"></script>
    <script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>
    <script type="text/javascript" src="js/gmap3.min.js"></script>
    <script type="text/javascript" src="js/wow.min.js"></script>
    <script type="text/javascript" src="js/masonry.pkgd.min.js"></script>
    <script type="text/javascript" src="js/jquery.simple-text-rotator.min.js"></script>
    <script type="text/javascript" src="js/all.js"></script>
    <script type="text/javascript" src="js/contact-form.js"></script>
	
    <script type="text/javascript" src="js/jquery.mask.js"></script>
    <script type="text/javascript" src="js/form_validator.js"></script>	
    <script type="text/javascript" src="js/photo_upload.js"></script>		

	

    <!--[if lt IE 10]><script type="text/javascript" src="js/placeholder.js"></script><![endif]-->
<script type="text/javascript">


$(window).resize(function(){
window.cwidth = parseInt($(".page-section:first .container").width());
console.log("width - "+window.cwidth+"");
VK.Widgets.Group("vk_groups", {mode: 0, width: ""+window.cwidth+"", height: "auto", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, 76667484);
});
</script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter26167932 = new Ya.Metrika({id:26167932,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/26167932" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
</body>

</html>
