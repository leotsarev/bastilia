<?
header("Location: http://ice.bastilia.ru/roles");
die();
//**********************
define("ROOT","../../../");
require_once(ROOT . 'funcs.php');
show_header("˸� :: �������� ���� �����������",ICE);
show_menu("inc/main.menu"); ?>

	    <td class="box">
<div class="boxheader"><a href="http://ice.bastilia.ru/">˸�</a> :: �������� ���� ����������� </div>
<h2 align="center">��������� ��������. �������� � <a href="http://ice.bastilia.ru/roles/">����� �����</a> </h2>
<p><strong>������� ����������</strong> ���������� �&nbsp;����������� ���-�� ������ �����. ��� ��������� ����������� ���� ��&nbsp;������ ��&nbsp;���� �������������, ��&nbsp;�, ��������, �&nbsp;���������� �������� �����. �������������, ��� ��������� ����� ����� ������� �������� �������� ����������� �����������. ������� �&nbsp;���� ��� ��������� ��������. ���� ������� �������� ��� ���� �&nbsp;���� �����&nbsp;&mdash; ��������� ������� �������, ������ ������� ������ ��&nbsp;�������. <br />
    <em>(��&nbsp;���� ������� ����� ����������� ������� ��&nbsp;5&nbsp;�������) </em></p>
<p><strong>�������� �����</strong>&nbsp;&mdash; ��������� ���������� ��&nbsp;������ �����������. ������� ������ ����� ���� ���������, ��&nbsp;��&nbsp;������ ������� ������������&nbsp;&mdash; ����� ���������� ������� ������������� ����� �����������. ����� ����� ��������� ����� ���������� ����-������� ��&nbsp;����� �&nbsp;����������� ����� �����������. ������ ������ ������ ��� ����������� ��������� ��������� ������, �&nbsp;��, �&nbsp;���� �������, �������� �&nbsp;��� ������� �&nbsp;�������� ������ �������������. <em><br />
(��&nbsp;����&nbsp;&mdash; 5&nbsp;�������) </em></p>
<p>����� ������� ���������� ����������� �������� <strong>����</strong>, ��������� �������������� ������� ������ ������������ ������, ������ �&nbsp;c�����. ������ ��������� ��������� �����, �&nbsp;������� ������ ������������� ���������� �����������&nbsp;&mdash; �������, ��������, ����, �������� �&nbsp;��������� �����. ����� ����� ����� ��������� ������� ���������������� ������������ ����.<br />
<em>��&nbsp;����� ���� ��� ���� ��&nbsp;������������, ��&nbsp;������� ������������� ������������� �����: </em></p>
<p><strong>�������</strong>&nbsp;&mdash; ������ ������������� �������, �������������� ������� �&nbsp;������������ �����. ����� ��&nbsp;���� ��������� ������� ��&nbsp;����������� ���������� �������� �������, ���������� �&nbsp;������. ����������� ������ ������� ���������� ���������� ������� ������ ���������, �&nbsp;��� ������������� ��������� �&nbsp;�&nbsp;������� ������ ��&nbsp;���������� ������&nbsp;&mdash; ���������, ���������������� ��� ������. ��� �������, ���������� ��������� ���������� ��������, ������� ������� ������ ������� ��������������� ������ �����������. ����� ���������� �������� ������ ��������������� ������������ �����.<br />
<em>(��&nbsp;���� ������� ����� ����������� ������� ��&nbsp;5&nbsp;�������) </em></p>
<p><strong>����</strong> (������������� ��������&nbsp;&mdash; ������ ������)&nbsp;&mdash; ����������� ������������ �����������, �������� ������� ����� ������� �������� ������. �������������� ������� ������ ������ �������� ����������������� �&nbsp;������ �&nbsp;����� ����, ��&nbsp;������� ��&nbsp;�������� �����������. ������ ������� �������� ��&nbsp;������� ����������, ���� ��� �������� ������ �������, �������������� ����� ����� ������������ �&nbsp;����������� ���������� ����.<br />
<em>(��&nbsp;����&nbsp;&mdash; 2&nbsp;��������) </em></p>
<p><strong>��������</strong>&nbsp;&mdash; ����������� ���������� �������� ����� �&nbsp;������������ ���������������� ����� ����� �&nbsp;�����������. �������� ����� ��&nbsp;�������� ����������� ��� �&nbsp;�����. ������� �������� ��������� ����� ������������ �������, ��������� �&nbsp;���������� ��������� �����������. ������ ��������� ��&nbsp;��������� ��������� ������� ���������� ���������. <br />
<em>(��&nbsp;����&nbsp;&mdash; 2&nbsp;��������) </em></p>
<p><strong>��������</strong> (������������� ��������&nbsp;&mdash; �����)&nbsp;&mdash; ���� ��&nbsp;��������� �����������, ��������� ��&nbsp;������� ����� �����. �������� ���������&nbsp;&mdash; ����� ��&nbsp;�������������� ������ �&nbsp;����������� �������. ������ ����� ����� �������� ���������� ������������ �������� ������ �&nbsp;�������, �&nbsp;����� �������, ����� �&nbsp;������ ���������� ����. �&nbsp;������ �������� ������ ������ �������. <br />
<em>(��&nbsp;����&nbsp;&mdash; 4&nbsp;��������) </em></p>
<p><strong>���������</strong>&nbsp;&mdash; �������������-���������������� ������������, �������� ������ ���� ������&nbsp;&mdash; �������� ��������� �&nbsp;���-��������� ����� �����������, ���������� ��&nbsp;���������� ������ ������� ����. ������� ���������� ��&nbsp;������ ����� �&nbsp;�������� ����������, �&nbsp;����� ���� ������� ���������� ��������� ��������� ���������. 
������� ���������� ��� �&nbsp;������� ������ ������� ��������� ������������ ����������� ��&nbsp;��������� �&nbsp;��������� ��������.<br />
<em>(��&nbsp;����&nbsp;&mdash; 6&nbsp;�������) </em></p>
<p><strong>����������</strong>&nbsp;&mdash; ��������, ������� ��������� �������������� �������� �&nbsp;�����������. ��� �������, ��� �����, ��&nbsp;������� ��&nbsp;�����, ��&nbsp;������. ������ ���������� ������������ �&nbsp;�����-���� ��&nbsp;�����������, ��&nbsp;������ ��&nbsp;��� ��� ����� ���������� ������ ���� ����. ���-�� ������ ����� ������, ���-�� ��������� �����������, �&nbsp;���-�� ������ ����� ��� ���������� ����������. <em><br />
(��&nbsp;����&nbsp;&mdash; 6&nbsp;�������)</em></p>
</td>
<? right_block('ice'); ?>
	     </tr>
<? show_footer(); ?>

