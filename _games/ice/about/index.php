 <?
define("ROOT","../../../");
require_once(ROOT . 'funcs.php');
show_header("˸� :: �� ��� ����� ������",ICE);
show_menu("inc/main.menu"); ?>

<td class="box">
<div class="boxheader"><a href="http://ice.bastilia.ru/">˸�</a> :: �� ��� ����� ������ </div>

<h4>�������������</h4>
<p>�����������&nbsp;&mdash; ������ ������������� ���. ������������� ����������� ��� ���� ����������� ������� �&nbsp;��������� ���������. ���������� ������ �&nbsp;������ ��������� �������� ��������� ������ ����� ���: ���������������, ����������, &laquo;�����������&raquo;, ������ �����... ������ �&nbsp;������� �&nbsp;�&nbsp;&laquo;��������&raquo; ����������� ��� �&nbsp;&laquo;��������&raquo; �&nbsp;��������, �&nbsp;��� ������ ��������� �&nbsp;������������� ��������...
<p>��&nbsp;������ ������ ������ ��&nbsp;�����-�� �������� ��������. ������ ���� �&nbsp;���� ����������� ������ ����; �, ��� ����� �������, ��&nbsp;������� �������� ��� ���������� ������ ����. ��������� ���������� ���������� �&nbsp;������������ ����������� ��������� ������� ���������������� �&nbsp;���������� ����� �����. ��&nbsp;�&nbsp;��&nbsp;���� ������� ��&nbsp;��������. ��&nbsp;����������� ����������, ��&nbsp;��� ����� ����� ������.
<h4>���������</h4>
<p>������ ������� ����������� ������ ���������� ������, ���������� �&nbsp;������ �������, ��� ��&nbsp;����� ��&nbsp;���������� ��&nbsp;���������. ��&nbsp;������ ���� ������� ��������� �&nbsp;������� ��������. ��� �������� ���� �&nbsp;�����, ��&nbsp;�&nbsp;�����-������ �������� ��&nbsp;������� ������������, ��� ���� ������� ����.
<p>��� ����������� ����������, ��������� ��&nbsp;����� ����������� ���������� ���������� ���� ��&nbsp;�������� ���������, ��� ��� ����������� ��� �������� ����� �&nbsp;����� ���������. ����������� ������� �����, ������ �������� �&nbsp;���������� ����� ��&nbsp;���� ����� ������� ��� ������ �������� �����������.
<h4>������</h4>
<p>��� ��&nbsp;��������� ������ ���� �&nbsp;���� ������ ���������. ������� ������������ ��������� ����� ������, ������������� �&nbsp;������� �����&nbsp;&mdash; ���, �������, ������������, ��&nbsp;������ �������� ��� �������� �����. ������� ��&nbsp;������� ���, ��� ����������� ��&nbsp;������ ���� ���������� �����-������ ������. ������ ��&nbsp;������ �&nbsp;������ ����������� ��&nbsp;����� ����������. ������ ������������ ����� ������ ��&nbsp;��������. ������ ��&nbsp;���������: ���� ���� ����� ��&nbsp;�&nbsp;������������� ������. �������� ���� �&nbsp;����� ��&nbsp;������ ��&nbsp;�����, ���������� ��&nbsp;������������ ����������. ����� �������, ���������� ����� �&nbsp;������ �������� �&nbsp;��������, ��� ���������� ������ ���� ������ ��&nbsp;������.
<h4>������������ ��������� �����������</h4>
<p>��������, ���������� ����� ��&nbsp;����� ��� �������&nbsp;&mdash; ��&nbsp;��&nbsp;����� ������ �&nbsp;&laquo;�������&raquo;. �������, ��&nbsp;����� ���� ����������� �&nbsp;������� ������� (�����, ��������, ������), ������ �� ���&nbsp;&mdash; ���� ��������� �������� ����� ������� �������������. ������������� ������ �����, ���� ������� ���� ������� ������� ������&nbsp;&mdash; ��&nbsp;������ ������� ���� �&nbsp;��������� ���������. ��&nbsp;���������, ��� ��&nbsp;������ ��������� ������������ ����� ������� ������ ��&nbsp;������ ����������, ��������������� �&nbsp;������������� ������. ��������� ����� �&nbsp;����� ��&nbsp;������ ����� ����������� �������� ������ ��� �&nbsp;�����������. ������� ��������� ������� ���� ���������� ��������� �����, �&nbsp;��&nbsp;���������� ����� �������� ��&nbsp;������� ��&nbsp;������ ��������, ������� ��&nbsp;��������, ���������� �&nbsp;������ ������������ �����.
<h4>�����</h4>
<p>����� �&nbsp;����������� �����. ��� �&nbsp;�������, ���������� ��&nbsp;����, �&nbsp;������������ ������, �&nbsp;������������ �������, �&nbsp;���� ����� ������� �����������. ��������� ���� ���������������� ��&nbsp;���-�� �����, ������&nbsp;&mdash; ����� ��, ��&nbsp;������������. �&nbsp;��� ���� ��������� ���� �&nbsp;���, ��� ��� ��������� ������������. ����� ����� ����� ����������� �&nbsp;������� ���&nbsp;�� ������������ ����������, ��� �&nbsp;�����, �����&nbsp;&mdash; ����� ���������� ���������.
<h4>��������� �&nbsp;��������������</h4>
<p>���������&nbsp;&mdash; �����&nbsp;�� ��������� ������ ��� ���������� �����������, ��� �&nbsp;��� ��� �&nbsp;����. ����������� ������� �������� ��������������� �����, ��� ����� ������ ������������ �������� ����&nbsp;&mdash; �&nbsp;�������� ���������. ������ �����, ������������� ������� ����� �&nbsp;����� ���� �������� ���������� ��������. ��&nbsp;��� ����� ������ ��������� ��������� �&nbsp;��������&nbsp;&mdash; �&nbsp;���� ����� ����� ����������� ������ ������.
<p>������ ���������� ������ ��� �&nbsp;����������, ��� ������ ���������. ��&nbsp;�&nbsp;��������� &laquo;�����&raquo; ������ ��&nbsp;����� ��������� ����������� ������������ �&nbsp;����� ����������. �����, ��&nbsp;������ ���������� �����-������� ������� ���������.
<h4>���� �&nbsp;��������</h4>
<p>��&nbsp;������ ������ �������������: ��&nbsp;������������� ��&nbsp;���������������� ������, �������������� ��� ��������� ������ ����������� ����. �������� ��������, ��� ��&nbsp;������ ����� �&nbsp;���������� ����������� ��� ������� ��&nbsp;�������� �����������, ������� ������ ������ ����� ������� �������� ����������������. �������, ��� ��&nbsp;������, ��� ��&nbsp;��&nbsp;������� ������� ������� ���������. ������ ��&nbsp;������ ����� �&nbsp;���� ������� ���� ������� ������&nbsp;&mdash; ����� ����������� ���������� �&nbsp;���������� ������������ �����.</td>
<? right_block('ice'); ?>
	     </tr>
<? show_footer(); ?>

