<?
define("ROOT","../../../");
require_once(ROOT . 'funcs.php');
show_header("˸� :: �������",ICE);
show_menu("inc/main.menu"); ?><td class="box">

           <div class="boxheader"><a href="http://ice.bastilia.ru/">˸�</a> :: ������� </div>
               
           <p><table border="1" align="center" cellpadding="1" cellspacing="1" bgcolor="#999999">
<tr>
<td><h2 align="center"><a href="ice-dobiralovo.doc">������� ������ ��� ������</a></h2></td>
</tr>
</table>
<h4>���������� ���������� </h4>
<p>������� ���� ��������� �&nbsp;���� ������ ��&nbsp;������� ����������� (�&nbsp;�����������, �&nbsp;���� �������, �&nbsp;�������� ����� ���� ��&nbsp;������). ������������ �&nbsp;����������� ����������: </p>
<table border="2" align="center" cellpadding="1" cellspacing="1">
<tr height="17">
<td height="17" rel="tooltip"><div align="center"><strong>����������� ������</strong></div></td>
<td><div align="center"><strong>��������</strong></div></td>
<td><div align="center"><strong>�����������</strong></div></td>
<td><div align="center"><strong>����� ��������</strong></div></td>
</tr>
<tr height="17">
<td height="17" align="right"><div align="center">6:51</div></td>
<td align="right" rel="tooltip"><div align="center">7:03</div></td>
<td align="right" rel="tooltip"><div align="center">8:17</div></td>
<td rel="tooltip"><div align="center">���������</div></td>
</tr>
<tr height="17">
<td height="17" align="right" rel="tooltip"><div align="center">8:23</div></td>
<td align="right" rel="tooltip"><div align="center">8:36</div></td>
<td align="right" rel="tooltip"><div align="center">9:50</div></td>
<td rel="tooltip"><div align="center">���������</div></td>
</tr>
<tr height="17">
<td height="17" align="right"><div align="center">8:48</div></td>
<td align="right" rel="tooltip"><div align="center">9:00</div></td>
<td align="right" rel="tooltip"><div align="center">10:15</div></td>
<td rel="tooltip"><div align="center">��&nbsp;��������</div></td>
</tr>
<tr height="17">
<td height="17" align="right"><div align="center">9:29</div></td>
<td align="right" rel="tooltip"><div align="center">9:43</div></td>
<td align="right" rel="tooltip"><div align="center">10:57</div></td>
<td rel="tooltip"><div align="center">���������</div></td>
</tr>
<tr height="17">
<td height="17" align="right" rel="tooltip"><div align="center">11:46</div></td>
<td align="right" rel="tooltip"><div align="center">11:59</div></td>
<td align="right" rel="tooltip"><div align="center">13:14</div></td>
<td rel="tooltip"><div align="center">��&nbsp;��������</div></td>
</tr>
<tr height="17">
<td height="17" align="right" rel="tooltip"><div align="center">12:50</div></td>
<td align="right" rel="tooltip"><div align="center">13:02</div></td>
<td align="right" rel="tooltip"><div align="center">14:18</div></td>
<td rel="tooltip"><div align="center">��&nbsp;��������</div></td>
</tr>
<tr height="17">
<td height="17" align="right"><div align="center">15:30</div></td>
<td align="right" rel="tooltip"><div align="center">15:43</div></td>
<td align="right" rel="tooltip"><div align="center">16:59</div></td>
<td rel="tooltip"><div align="center">���������</div></td>
</tr>
<tr height="17">
<td height="17" align="right" rel="tooltip"><div align="center">16:32</div></td>
<td align="right" rel="tooltip"><div align="center">16:44</div></td>
<td align="right" rel="tooltip"><div align="center">17:59</div></td>
<td rel="tooltip"><div align="center">��&nbsp;��������</div></td>
</tr>
<tr height="17">
<td height="17" align="right" rel="tooltip"><div align="center">17:45</div></td>
<td align="right" rel="tooltip"><div align="center">17:57</div></td>
<td align="right" rel="tooltip"><div align="center">19:11</div></td>
<td rel="tooltip"><div align="center">���������</div></td>
</tr>
<tr height="17">
<td height="17" align="right" rel="tooltip"><div align="center">18:52</div></td>
<td align="right" rel="tooltip"><div align="center">19:03</div></td>
<td align="right" rel="tooltip"><div align="center">20:21</div></td>
<td rel="tooltip"><div align="center">���������</div></td>
</tr>
<tr height="17">
<td height="17" align="right" rel="tooltip"><div align="center">20:40</div></td>
<td align="right" rel="tooltip"><div align="center">20:53</div></td>
<td align="right" rel="tooltip"><div align="center">22:08</div></td>
<td rel="tooltip"><div align="center">���������</div></td>
</tr>
<tr height="17">
<td height="17" align="right" rel="tooltip"><div align="center">22:10</div></td>
<td align="right" rel="tooltip"><div align="center">22:22</div></td>
<td align="right" rel="tooltip"><div align="center">23:32</div></td>
<td rel="tooltip"><div align="center">���������</div></td>
</tr>
</table>
<h4>�������</h4>
<p>&laquo;˸�&raquo; ������� �&nbsp;������������ ����� ����� (���&nbsp;��, ��� �&nbsp;&laquo;��������&raquo; �&nbsp;2008&nbsp;�.). ���� ������������ ����� �������� �&nbsp;����� ������ ��&nbsp;����.</p>
<p align="center"><a href="polygon.jpg"><img src="polygon-mini.jpg" width="549" height="365" border="0" /></a></p>
<p>��&nbsp;������ �������� ���� ��&nbsp;�����&nbsp;�� �����, ��&nbsp;�&nbsp;��� ���� ������. ��&nbsp;������� ����������� ����� 354 ��������&nbsp;&mdash; ��� 1&nbsp;��, �.�. ��&nbsp;��������������� ������� ��&nbsp;�����&nbsp;&mdash; ���-�� 3&nbsp;��������� ��&nbsp;������.</p>
<p>�������� �����������: &laquo;<strong>�</strong>&raquo;&nbsp;&mdash; ������� ����������, &laquo;<strong>�</strong>&raquo;&nbsp;&mdash; ����� (�&nbsp;����������� ��������) , &laquo;<strong>�</strong>&raquo;&nbsp;&mdash; ���� (�������, ���� �&nbsp;��������), &laquo;<strong>�</strong>&raquo;&nbsp;&mdash; ���������, &laquo;<strong>�</strong>&raquo;&nbsp;&mdash; �������� .</p>
<h4>��� ���������� ������ </h4>
<p>����� �&nbsp;���������� <strong>(0)</strong>, ���������� ����� ����� �&nbsp;������� ������. ������ ������ ��&nbsp;����������� �������� ������ �&nbsp;������������� <strong>(1)</strong>. ��������� �������, �������� ����� ������������� ������� �&nbsp;���������� ��&nbsp;����������� ������. ��&nbsp;������ ���� ����� ������ ������ <strong>(2)</strong>. ����� ����� ��&nbsp;���������� ���������������� �������� <strong>(3)</strong>. ������������ ��&nbsp;��������� �������� ����� �������� ������� <strong>(4)</strong>. ��&nbsp;������� �&nbsp;���.</p>
<p>��������� ������, ����� ������������ ��&nbsp;������� ���� ����������� <strong>(5)</strong>&nbsp;&mdash; �����-�� ��� ���� ���������� ����� ����������. ������ ��� (�����) �������� ������. �&nbsp;�����, ��������� ����� 1,5 ����� ����������� ������ ��&nbsp;��������� ��&nbsp;����������� <strong>(6)</strong>. ������ ������ ����� �������� �&nbsp;�����, ��&nbsp;��� ��� ��� ��&nbsp;������. ����������� �������� �����.</p>
<p>������ ��������� ��&nbsp;�������� <strong>(7)</strong>. ������ ������ ����� ��� ��&nbsp;��������� �������, ��&nbsp;��� ���� ������. ����� ����� <nobr>7-8 ��</nobr> ���� ���������� ��� ���� �������� <strong>(8)</strong>. <strong>������������� ������ ��� ��� ����� �������������!</strong> �������&nbsp;��, ������ �����, ������� �������������� ����������, �&nbsp;����� �������������� ���. ��� ��� ���������� &laquo;�������&raquo; ��������. ������ ���� ��&nbsp;���� ���������!</p>
<p>�&nbsp;�����, ����� ����� �&nbsp;����� ����� ��&nbsp;��������� ��&nbsp;������� ����������� <strong>(9)</strong>. ��&nbsp;����� ��������������. ���� ���� �����&nbsp;&mdash; �������� �������� �&nbsp;������� �����. ��&nbsp;��� ����� ����� ��������� ��������������� �&nbsp;���� �&nbsp;������ ����������� ���� ��&nbsp;�����. ������� ������������� ������� �&nbsp;��&nbsp;���������� ���� �����.</p>
<p>����� ����� 10&nbsp;�� ���� ���������� �������� <strong>(10)</strong>. ����� ������&nbsp;&mdash; �&nbsp;������ ��������� ��&nbsp;������� ������� (�������� ��&nbsp;����� ������). ��&nbsp;������ ��&nbsp;���� ��� �������� ����� �&nbsp;�������� <strong>(11)</strong>.</p>
<p>�&nbsp;�����, ������ ������ ��������������� ������ (������� �&nbsp;���� �������, ������ �����, ��� ����� ����������) �&nbsp;�&nbsp;����� ���������� ��&nbsp;������ <strong>(�)</strong>. ���&nbsp;�� ����������� &laquo;�����������&raquo; ��������. ����������� ��������� ������� �&nbsp;����� ���������! ����� ��� ���������, ��� ��������� ��&nbsp;����� �������.</p>
<h4>��� ���������� ��&nbsp;���������� </h4>
<p>�&nbsp;������ &laquo;�����&nbsp;&mdash; ������&raquo; ������� �������� ��&nbsp;������� ������ (�������� �&nbsp;������ <nobr>85-��</nobr> ���������). ������ ��������� ����� ��&nbsp;������, ������ ����������� ����� ������� �&nbsp;���������� ������ ��&nbsp;�����������. �&nbsp;�����, �������������� ��&nbsp;����������. ���������� ������� �������� �&nbsp;����� ��������� ���������� ��������� �&nbsp;���������������� �������� �&nbsp;����������� <strong>(1)</strong>. ������ ������� ��&nbsp;�������� ������ ��������. </p>
<h4><strong>����� ��&nbsp;������ ����</strong></h4>
<p>����������, ��� ���� ���������� <nobr>6-��</nobr> ����� ����. ������� ����� ��&nbsp;�������� ��� �&nbsp;<nobr>4-��</nobr> �����. ������� ���� �&nbsp;��� ���� ������� (�&nbsp;�����������)&nbsp;&mdash; ���������� �������, ����� ������ ���� ��� �����.</p>
<h4>���������� �������� ����������</h4>
<table border="2" align="center" cellpadding="1" cellspacing="1">
<tr height="51">
<td height="51" rel="tooltip"><div align="center"><strong>�����������</strong></div></td>
<td><div align="center"><strong>��������</strong></div></td>
<td><div align="center"><strong>����������� ������</strong></div></td>
<td><div align="center"><strong>����� ��������</strong></div></td>
</tr>
<tr height="17" rel="">
<td height="17" align="right"><div align="center">5:35</div></td>
<td align="right" rel="tooltip"><div align="center">6:51</div></td>
<td align="right"><div align="center">7:05</div></td>
<td rel="tooltip"><div align="center">���������</div></td>
</tr>
<tr height="17" rel="">
<td height="17" align="right"><div align="center">6:29</div></td>
<td align="right" rel="tooltip"><div align="center">7:49</div></td>
<td align="right"><div align="center">8:03</div></td>
<td rel="tooltip"><div align="center">���������</div></td>
</tr>
<tr height="17" rel="">
<td height="17" align="right"><div align="center">7:10</div></td>
<td align="right" rel="tooltip"><div align="center">8:28</div></td>
<td align="right"><div align="center">8:42</div></td>
<td rel="tooltip"><div align="center">���������</div></td>
</tr>
<tr height="17" rel="">
<td height="17" align="right"><div align="center">9:41</div></td>
<td align="right" rel="tooltip"><div align="center">10:57</div></td>
<td align="right"><div align="center">11:11</div></td>
<td rel="tooltip"><div align="center">���������</div></td>
</tr>
<tr height="17" rel="">
<td height="17" align="right"><div align="center">11:03</div></td>
<td align="right" rel="tooltip"><div align="center">12:23</div></td>
<td align="right"><div align="center">12:37</div></td>
<td rel="tooltip"><div align="center">��&nbsp;��������</div></td>
</tr>
<tr height="17" rel="">
<td height="17" align="right"><div align="center">12:50</div></td>
<td align="right" rel="tooltip"><div align="center">14:08</div></td>
<td align="right"><div align="center">14:22</div></td>
<td rel="tooltip"><div align="center">���������</div></td>
</tr>
<tr height="17" rel="">
<td height="17" align="right"><div align="center">15:10</div></td>
<td align="right" rel="tooltip"><div align="center">16:29</div></td>
<td align="right"><div align="center">16:43</div></td>
<td rel="tooltip"><div align="center">���������</div></td>
</tr>
<tr height="17" rel="">
<td height="17" align="right"><div align="center">16:14</div></td>
<td align="right" rel="tooltip"><div align="center">17:35</div></td>
<td align="right"><div align="center">17:49</div></td>
<td rel="tooltip"><div align="center">��&nbsp;��������</div></td>
</tr>
<tr height="17" rel="">
<td height="17" align="right"><div align="center">16:47</div></td>
<td align="right" rel="tooltip"><div align="center">18:08</div></td>
<td align="right"><div align="center">18:22</div></td>
<td rel="tooltip"><div align="center">���������</div></td>
</tr>
<tr height="17" rel="">
<td height="17" align="right"><div align="center">17:49</div></td>
<td align="right" rel="tooltip"><div align="center">19:08</div></td>
<td align="right"><div align="center">19:22</div></td>
<td rel="tooltip"><div align="center">���������</div></td>
</tr>
<tr height="17" rel="">
<td height="17" align="right"><div align="center">19:15</div></td>
<td align="right" rel="tooltip"><div align="center">20:36</div></td>
<td align="right"><div align="center">20:50</div></td>
<td rel="tooltip"><div align="center">��&nbsp;��������</div></td>
</tr>
<tr height="17" rel="">
<td height="17" align="right"><div align="center">20:15</div></td>
<td align="right" rel="tooltip"><div align="center">21:33</div></td>
<td align="right"><div align="center">21:47</div></td>
<td rel="tooltip"><div align="center">���������</div></td>
</tr>
<tr height="17" rel="">
<td height="17" align="right"><div align="center">21:46</div></td>
<td align="right" rel="tooltip"><div align="center">23:03</div></td>
<td align="right"><div align="center">23:16</div></td>
<td rel="tooltip"><div align="center">���������</div></td>
</tr>
</table>
   
             <? right_block('ice'); ?>
             </tr>
             <? show_footer(); ?>

  