<?php

if (!defined('ROOT'))
	die ('Error!');

define ('FREE_ROLE', 0);
define ('HIDDEN_ROLE', 1);
define ('CASTING_ROLE', 2);
define ('MAX_ROLE', 3);

function begin_role_table()
{
	echo "\n<table class=\"roletable\" border=\"2\" cellpadding=\"1\" cellspacing=\"1\">\n";
}

function character ($name, $desc, $player, $photo = FALSE)
{
	$name = $name ? $name : '&nbsp;';
	$desc = $desc ? $desc : '&nbsp;';

	$special_roles = array (FREE_ROLE => '&nbsp;', HIDDEN_ROLE => '���� ������', CASTING_ROLE => '������ �� �����������');
	if ( is_numeric ($player) && ($player < MAX_ROLE))
	{
		$player = $special_roles[$player];
	}
	$player = $photo ? "<a href=\"photo/$photo\">$player</a>" : $player;
	echo "\n<tr><td>$name</td>\n<td>$desc</td>\n<td>$player</td></tr>\n";
}

function character_lj ($name, $desc, $player, $lj = FALSE)
{
  $name = $name ? $name : '&nbsp;';
	$desc = $desc ? $desc : '&nbsp;';

	$special_roles = array (FREE_ROLE => '&nbsp;', HIDDEN_ROLE => '���� ������', CASTING_ROLE => '������ �� �����������');
	if ( is_numeric ($player) && ($player < MAX_ROLE))
	{
		$player = $special_roles[$player];
	}
	echo "\n<tr><td>$name</td>\n<td>$desc</td>\n<td>$player";
	if ($lj)
	{
    echo " (";
    lj_user($lj);
    echo ")";
	}
	echo "</td></tr>\n";
}

function location ($location, $colspan = 3)
{
	echo "\n<tr><td colspan=\"$colspan\"><strong>$location</strong></td></tr>";
}

function end_role_table()
{
	echo "</table>\n";
}

?>