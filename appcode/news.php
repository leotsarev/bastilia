<?php
if (!defined('ROOT'))
	die ('Error!');

require_once ROOT . 'funcs.php';
require_once ROOT . 'appcode/users.php';
require_once ROOT . 'appcode/database.php';
require_once ROOT . 'appcode/remotetypograf.php';

define ('NEWS_TABLE', SQL_SITE . '.news');
define ('DEFAULT_COUNT', 10);
define ('TAGS_TABLE', SQL_SITE . '.tags');
define ('NEWSTAGS_TABLE', SQL_SITE . '.news_tags');

function format_date ($date)
{
	return date('j/m/Y', strtotime ($date));
}

function get_all_tags()
{
	return get_array ('SELECT * FROM ' . TAGS_TABLE . ' ORDER BY `tag_name`');
}

function do_typograf ($text)
{
	$remoteTypograf = new RemoteTypograf ('Windows-1251');
	$remoteTypograf->htmlEntities();
	$remoteTypograf->br (false);
	$remoteTypograf->p (false);
	$remoteTypograf->nobr (0);
	$text =  $remoteTypograf->processText ($text);
	$text = str_replace ('<nobr>', '', $text);
	$text = str_replace ('</nobr>', '', $text);
	return $text;
}

function add_news_item ($header, $text)

{
	$author = get_username();

	if (!is_admin($author)) die ('HACK ATTEMPT!');
	$date = date ('Y-m-j-H-i-s');
	$header = quote_smart ($header);

	$text = quote_smart ($text);
	$author = quote_smart ($author);

	$sql =
	  'INSERT INTO ' . NEWS_TABLE . " SET `news_date` = '$date', `news_author` = $author, `news_header` = $header, `news_text` = $text";

	do_query_or_die ($sql);

	return last_insert_id();
}

function get_news_item_ex ($id = 0)
{
	if ($id)
	{
		$sql = 'SELECT news_id, news_date, news_author, news_header, news_text FROM ' . NEWS_TABLE . ' WHERE `news_id` = ' . quote_smart ($id);
	}
	else
	{
		$sql = 'SELECT news_id, news_date, news_author, news_header, news_text FROM ' . NEWS_TABLE . ' ORDER BY `news_date` DESC LIMIT 1';
	}
	return get_scalar ($sql);
}

function set_tags_post ($id, $post)
{
	delete_tags ($id);
	foreach ($post as $key => $item)
	{
		if (substr($key, 0, 3) == 'tag')
		{
			$tag = substr ($key, 3);
    	do_query_or_die ('INSERT INTO ' . NEWSTAGS_TABLE . " SET `tid` = '$tag', `nid` = '$id'");
    }
	}
}

function delete_tags ($id)
{
	do_query_or_die ('DELETE FROM ' . NEWSTAGS_TABLE . " WHERE `nid` = '$id'");
}

function modify_news_item ($id, $header, $text) {

	$header = quote_smart ($header);

	$text = quote_smart ($text);

	$sql =
	  'UPDATE ' . NEWS_TABLE . " SET `news_header` = $header, `news_text` = $text WHERE `news_id` = '$id' LIMIT 1";

	do_query_or_die ($sql);
}

function delete_news_item ($id)
{
	$date = quote_smart ($id);

	$sql = 'DELETE FROM ' . NEWS_TABLE . " WHERE `news_id`  = $id LIMIT 1";

	do_query_or_die ($sql);

	delete_tags ($id);
}

function get_all_news ()
{
	return get_array ('SELECT * FROM ' . NEWS_TABLE);
}

function find_news_by_year($year)
{
	$year = intval($year);

	$sql = 'SELECT * FROM ' . NEWS_TABLE . " WHERE YEAR(news_date) = $year ORDER BY news_date DESC";

	return get_array ($sql);

}

function find_news_by_tag($tag)
{
	$tag = quote_smart($tag);

	$sql =
		'SELECT * FROM '
			. NEWS_TABLE . ', ' . TAGS_TABLE . ', ' . NEWSTAGS_TABLE
			  . " WHERE tags.`uri` = $tag"
				. ' AND tags.`id` = news_tags.`tid`'
				. ' AND news_tags.`nid` = news.`news_id`'
				. ' ORDER BY `news_date` DESC';

	return get_array ($sql);
}

function find_last_news($count)
{
	$count = quote_smart ($count);
	$sql = 'SELECT * FROM ' . NEWS_TABLE .' ORDER BY `news_date` DESC LIMIT ' . $count;

	return get_array ($sql);
}

function write_tag_list($id)
{
	$tags_array = get_news_tags($id);
	$tags_set = array();
	if (is_array($tags_array))
	{
		foreach ($tags_array as $tag)
		{
			$tags_set[] = $tag['id'];
		}
	}
	foreach (get_all_tags() as $tag)
	{
		if (array_search($tag['id'], $tags_set)===FALSE)
			echo '<input type="checkbox" name="tag' . $tag['id'] . '"><a href="/news/tag/' . $tag['uri'] . '/">' . $tag['tag_name'] . "</a></input><br />\n";
			else echo '<input type="checkbox" name="tag' . $tag['id'] . '" checked="checked"><a href="/news/tag/' . $tag['uri'] . '/">' . $tag['tag_name'] . "</a></input><br />\n";
	}
}

function get_news_tags ($id)
{
	$sql =  'SELECT *'
		. ' FROM ' . SQL_SITE . '.`tags` , ' . SQL_SITE . '.`news_tags`'
		. " WHERE tags.id = news_tags.tid AND news_tags.`nid` = '$id'";

	return get_array ($sql);
}



function show_news_item($row) {
	$tags = get_news_tags ($row['news_id']);
	$date = date('j/m/Y',strtotime($row['news_date']));
	$id = $row['news_id'];
	$uid = date('YmjHis', strtotime($row['news_date']));
	$header = htmlspecialchars ($row['news_header']);
	echo("<div class=\"newsheader\" id=\"$uid\">$header - $date </div>\n");
	echo($row['news_text']);
	echo '<div class="newsauthor">';
	if (is_admin(get_username()))
	{
		echo "<strong><a href=\"/news/edit/?id=$id\">�������������</a></strong> :: ";
	}
	echo format_tags ($tags);
	lj_user($row['news_author']);
	echo "</div>\n";
	echo(RULER);
}

function show_promobox () {
	?>

	<div class="promobox">
<a href="http://www.livejournal.com/friends/add.bml?user=bastilia_news">
	<img src="http://bastilia.ru/images/lj/addfriend-bw.png" style="border:0" width="22" height="22" alt="�������� � ������" title="�������� � ������ (��)" />
</a>

<a href="http://lenta.yandex.ru/settings.xml?name=feed&amp;url=http%3A//feeds.feedburner.com/bastilia/">
	<img src="http://bastilia.ru/images/yandex-add-bw.png" style="border:0" alt="������ � ������.�����" width="100" height="22" title="������ � ������.�����" />
</a>

<a href="http://fusion.google.com/add?feedurl=http%3A//feeds.feedburner.com/bastilia/">
	<img src="http://bastilia.ru/images/google-add-bw.png" width="104" height="17" style="border:0" alt="Add to Google" title="�������� � Google" />
</a>

<a href="http://feeds.feedburner.com/bastilia">
	<img src="http://bastilia.ru/images/rss-bw.png" width="16" height="16" style="border:0" alt="RSS" title="RSS �����" />
</a>
</div>
<strong>������� ��: <a href="/news/2004/">2004</a>, <a href="/news/2005/">2005</a>, <a href="/news/2006/">2006</a>, <a href="/news/2007/">2007</a>, <a href="/news/2008/">2008</a>, <a href="/news/2009/">2009</a>, <a href="/news/2010/">2010</a>, <a href="/news/2011/">2011</a></strong>, <b><a href="/news/2012/">2012</a></b>, <b><a href="/news/2013/">2013</a></b>, <b><a href="/news/2014/">2014</a></b> ��.:: <a href="/news/tag/"><strong>��������� ��������</strong></a>
<table class="ruler"><tr><td></td></tr></table>
	<?php

}

function format_tags ($tags)
{
	if (!is_array ($tags))
	{
		return '��� ��������� <strong>::</strong> ';
	}
	$result = '';
	foreach ($tags as $tag)
	{
		$uri = 'http://bastilia.ru/news/tag/' . $tag['uri'];
		$name = $tag['tag_name'];
		$result .= "<a href=\"$uri\"><strong>$name</strong></a>, ";
	}
	$result = substr($result, 0, strlen($result) - 2);
	return "���������: $result <strong>::</strong> ";
}

function get_tag_name ($tag)
{
	$tag = quote_smart ($tag);

	$sql = 'SELECT tag_name FROM ' . TAGS_TABLE . " WHERE `uri` = $tag LIMIT 1";

	$result = get_scalar ($sql);
	//var_dump ($result);

	return $result['tag_name'];
}

function get_news_tag_count()
{
	$sql = 'SELECT `tid`, COUNT(`nid`) AS `count` FROM ' . NEWSTAGS_TABLE . ' GROUP BY `tid`';

	$array =  get_array ($sql);
	foreach ($array as $item)
	{
		$key = $item['tid'];
		$value = $item['count'];
		$result[$key] = $value;
	}
	return $result;
}

?>