<?
define("ROOT","../../../");
require_once ROOT . 'funcs.php';
show_header('BSG :: ������� :: ������', BSG);
show_menu("inc/main.menu"); ?>

<td class="box">

<div class="boxheader"><a href="http://bsg.bastilia.ru/">Battlestar Galactica</a> :: <a href="http://bsg.bastilia.ru/rules/">�������</a> :: ������</div>
<div>
<h2>������</h2>
<img src="/images/fight.jpg" class="bsgpic" style=" float:right">
<h3>���������� ���</h3>
<p><b>����</b> ������� ������������ ������� ������� ��&nbsp;������� (�����, �����, �����) �&nbsp;����������� &laquo;����&raquo;. 
���� ��&nbsp;�����&nbsp;&mdash; ��&nbsp;���������. ������������ ��� ����� ������� �&nbsp;���������� <b>��&nbsp;10&nbsp;������ ��&nbsp;10&nbsp;�����</b> 
���� �&nbsp;��� �&nbsp;������ ���������� ������ � ������ �� ������, ��� ����� 10 ������ (������� ������ ����������� ���).<br />
����������&nbsp;�� ��������� ����� ������, ����� ����� ������ ������ �&nbsp;��� ���������� ������, ��&nbsp;��������� ������ ������� (�&nbsp;��&nbsp;����, ���� ����&nbsp;&mdash; ���� ��� �������, ������, ������).<br />
<i>������� ��������, ������������ ��-������, ������.</i></p>
<p>������ ������� ����������� ��� �������� <b>������</b>.<br />
���� �������� ��������� �������� �&nbsp;����������&nbsp;&mdash; ����� ����������, ����� ��������� �������������� ��� ���������. ��������, �������� ���������� ��&nbsp;�����, ��&nbsp;����� ������ ������, ����� ��� ��������.<br />
����� ���������� ����������������� ���������, ��� ���������� ���������, ������� �����, ����������, ����� ��� ���������� ��� ������� ����. ��������, �������� ��� ��������, ��&nbsp;����� ������ ������, ����� ��� ������ (��� ������) �&nbsp;��������. ��� ����� ��������� ������� �&nbsp;������ �����, �����, ��� ������ �&nbsp;�.&nbsp;�., ��&nbsp;������ ���������, �&nbsp;�������, ������ ��&nbsp;������, ������� ���, ���������� ��&nbsp;��������� �&nbsp;�.&nbsp;�.</p>
<p>�������, ������� ���������� �������, ��&nbsp;����� ����� �&nbsp;��� ����� ��������, ���������� ����-�� ���, ���� ����-�� ����� ��� ����� �&nbsp;�.&nbsp;�. ����� ������ ����� ��������� ����� ��� ������� �����-�� ������� (�&nbsp;�.&nbsp;�. ������).</p>
<H3>���</H3>
<p><b>���</b> ������������ ����������, ���������, ������������� ��� ������������� ������� �������-������ ��&nbsp;�������������� ����. 
��� ��������������� ����� ����� �&nbsp;������ �&nbsp;���������� <b>��&nbsp;10&nbsp;������ ��&nbsp;10&nbsp;�����</b> �������� �&nbsp;<b>�������</b> (��. <a href="../survival/">������� ��&nbsp;���������</a>)<br />
��&nbsp;���� ���&nbsp;&mdash; ������� ������. ����� ����:</p>
<ul>
	<li>�&nbsp;��������</li>
	<li>�&nbsp;���, ���� ��������� ��&nbsp;�������.</li>
</ul>
<p><i>������� ����� ��������, ������������ ��-������, ������.</i></p>
<H3>���������, ����������, ��������� ������</H3>
<p>�������� ��&nbsp;���� ������.<br />
������� ��������&nbsp;&mdash; ����.<br />
����� ����, ����� ��&nbsp;���� ��&nbsp;����������. ����������: ����� (���������� ��&nbsp;����, ��&nbsp;�������� �������� ��� ������) �&nbsp;���������� (���������� ��&nbsp;����� �������) ��&nbsp;&laquo;���������&raquo;.</p>
<H3>�����</H3>
<p>����� �� ���� �������������� �� �����. �������, �� ������� ����� ������� ���� � �������������� ���������� ���� ������ ������� �����. ������ �������������.</p>
<H3>������������ ���������</H3>
<p>��� �������� ���������, ����� �������� ��������� (�������, �������, ���������, ������, ���������� ���������, ��������, ����������� ������ � �.�.). 
����� �� ��������� �������� � ���������������� ���������� ������������ (��. ������� �� �������).</p>
<H3>������ ������</H3>
<p>�������� ������������ ����� ������� ������� (������� �������� ������������� ���������, ��&nbsp;����� ������ ��������� ��������) �&nbsp;������������ ��������: ����� ��������� ���������� ������� (������ �&nbsp;�.&nbsp;�.) ���������. ����������, ����� ��� �������� �������� ������� ���� (��, �&nbsp;���� �������� ���� ��&nbsp;������, ����� ��������� �������� &laquo;�������&raquo; ��� &laquo;���&raquo;, ��� �������� ���������� �����).<br />
�������������: ����������� ������������� �������� (������� ��������, ������ ����) ��� ��������� ������� ������� (������ ����).<br />
��&nbsp;������ ���� ������ ������ ����� ���� �&nbsp;������ �������� ����� (�.&nbsp;�. ������� ��������� �&nbsp;������) �&nbsp;��������. ���������� ����� �����.</p>
<p>��� ������ ������� ������ ���� �������� ����� <b>����������</b> �������. ��� ����� ����������: ������� �������� ��&nbsp;����, �������, �&nbsp;���� ��������� (���� ��� ��&nbsp;��������) �&nbsp;������ ��&nbsp;�����.<br />
������� ���������� ��������. ����������&nbsp;&mdash; ���� ������ ���������� �&nbsp;������ ��������� (�.&nbsp;�. �������� ����� ������� �����) ��� ������ ���������� ��&nbsp;���������� ����� 4 ������.</p>
<p>����� ��������� ������ ���������� <b>�������</b> (��. <a href="../survival/">������� ��&nbsp;���������</a>). ���� ����� ���� ���-�� ���, ����� ������, ���������� ������ ������������ ��&nbsp;&laquo;<b>�������� ���</b>&raquo;&nbsp;&mdash; ����������� ���-�� ������������� ��&nbsp;��� ��������&nbsp;&mdash; ���������� ���, ��������, ������� �&nbsp;�.&nbsp;�. �������� ������ ��� ����� ��&nbsp;����� ��� ����� ���������.</p>
<p><i>������ ������ ��&nbsp;��������� ��� �������� ��&nbsp;�������.</i></p>
<H3>��-�����</H3>
<p>������� ���������� ���������� <b>��-������</b> �&nbsp;������������ ��&nbsp;������������ ���������� ������� (������ ��������). �������� ���������.<br />
�������� �������� �&nbsp;���� ������, �������� �������� (���������� ��&nbsp;������) �&nbsp;������ (����������� �����). ����� ������ ��������, �&nbsp;��-������ ����� ����� ��� ���� ���������� ��&nbsp;��-�����. ��������� ����������� ������������� (������� ������). ����� ���� ���������, ������� ����� ������ ������, �&nbsp;��-����� ������������� ����������� (��&nbsp;��������). �������� ��-����� ��� ���� ���������� �������. ��-����� ����� ������������ �&nbsp;��� ������ ������ (����� ���������� ����-�� ��� ��-�����).<br />
������������ ��-����� ��� ������� ������.</p>
<p>������� �&nbsp;��-������ �&nbsp;�&nbsp;�������:</p>
<ul>
	<li>�������� ��� ������ ����� (����� ������ ��������) �&nbsp;�����;</li>
	<li>����� ���� ������� ��������� ��&nbsp;������� ������ ������ �&nbsp;���� � ������ � ������.</li>
</ul>

<H3>������� ��&nbsp;�������</H3>
<p>��� ������� ��&nbsp;������� ��������� ��������� �����������:</p>
<ul>
	<li>������ ������ ��&nbsp;���������;</li>
	<li>��� �������������� ����� �������� ��������, ������� ��������� ���&nbsp;��, ��� �&nbsp;�������, ������������� �&nbsp;��-������ (��� ��������� ������, ����� ���� ���������&nbsp;&mdash; ����� = �����).</li>
</ul>

<h3>�������</h3>
<p>
������ �������������� �������� (��� �����������, ��� �&nbsp;�������������� ���������) �������� ������ ��&nbsp;�����. ��������� �����, �������, �����. �&nbsp;���� ������� �����������, ���&nbsp;�� �������������� ����������� ����������� ��������, ����&nbsp;�� ��� �&nbsp;��� ������������� ����:</p>
<ul>
	<li>������ �������� ��� ��������� ��&nbsp;������� ������ (����������&nbsp;��);</li>
	<li>������ �������� ��� ������ ����� ��� ����� (����������&nbsp;��);</li>
	<li>������� ���������� �������� (��������� ������ &laquo;��������&raquo;);</li>
	<li>������ ����� �������� �������� ����� �����, �&nbsp;������ ������ ���-�� ���;</li>
	<li>������ ����� ������� �������� �����, ���� ���� �� � ��-������ � � �������� ������� �&nbsp;������&nbsp;�� ���� �&nbsp;������ ����� (��������� ������ &laquo;�����&raquo;);</li>
	<li>������ ����� �������� ��������� (������������ ����� ������������);</li>
	<li>������� ������ ����� �����������, ��� ����� ��&nbsp;��&nbsp;����� (���������� ������� ���������);</li>
	<li>������� ������ ����� �������������� (������� ������ ���������� �������, �&nbsp;����� ��������� ����� ����� ������).</li>
</ul>
<p><i>��� ���: � �������� �������� ����� ���� ��� ��� �����������, �� ����� ��� �����-�� �����. ��� �������� ������� ����������. �������������� ����������� � �������������� ��������� ����� ����������.</i></p>

</div>
 </td>
<? right_block("bsg");?>
	     </tr>
<? show_footer(); ?>

