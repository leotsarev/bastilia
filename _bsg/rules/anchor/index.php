<?
define("ROOT","../../../");
require_once ROOT . 'funcs.php';
show_header("BSG :: ������� :: ����� � �������������", BSG);
show_menu("inc/main.menu"); ?>

<td class="box">

<div class="boxheader"><a href="http://bsg.bastilia.ru/">Battlestar Galactica</a> :: <a href="http://bsg.bastilia.ru/rules/">�������</a> :: ����� � �������������</div>
<div>
<h2>����� � �������������</h2>
<h3>�����</h3>
<p><strong>�&nbsp;������� ��������� ���� ���� ��� ����� ������&nbsp;&mdash; ���������, ������� �������� ������������� ������� �&nbsp;��� ��������</strong>. ������ ����� ���� ���������� ������� (�������/�������, ���/����, ����/������, ���, ����, ����, ����, ��������), ������ ��� (������� ���������, ������, ��� �������� ������������, �������� ���������� �������), ��������� (�������� ����������, �����, �����, ������, ������� ������, ����� �&nbsp;���������), ���� ������ (��� ����� ��� ���-�� ����������), ������ ���, �������� (������ ���� ������ ������), �������� �&nbsp;�.&nbsp;�.</p>
<p>����� ���������� ������� ��������� �&nbsp;�������� (��. <a href="/rules/anchor/choose/">����� ��� ����� ������</a>).</p>
<p>����� ��������� ������ ���������������, �&nbsp;��������� ����� ������ ����� ���-�� ������ ������������ ������� ��������� ����� ����. ����� ���������� �������� �&nbsp;����� ��� ���������� ������������� ���� ��� �������� ����������, ��&nbsp;��� ������ ���� ��������. ��������� ������� &laquo;�&nbsp;���� ����� ��&nbsp;����, ���� �&nbsp;����&raquo;&nbsp;&mdash; ������. ������������� ��������� ��������� ���� ����� ��&nbsp;����.</p>
<img src="../../images/saul-ellen-alcohol.png" class="bsgpic">
<h3>��������</h3>
<p><strong>�������� ����� ������ �����</strong>. ��� ��� ������, ������� ��&nbsp;����������� �����. �������:<br />
<em>��� ���������� ����� ������ �������� ��� ����� ���� �����, �&nbsp;������� ��&nbsp;��������� ��� �&nbsp;������. ��� ������, ��� ��&nbsp;��&nbsp;���������� ��������, ��������� ��������, �&nbsp;���� ��� ������ �&nbsp;������� ��������, ����� �������� ���� ������, ����� ��&nbsp;������.<br />
��� ���������� ���� ��� ������ �������� ��������. �&nbsp;����� ������� ��������� �������� ��&nbsp;���������� �&nbsp;�������, ����� ����� �&nbsp;��� ��������.</em></p>
<p><strong>�������� ��������� ��&nbsp;�������� ���� ����� �&nbsp;�������, ��� ����� ��&nbsp;�������� ���.</strong> ���������� ��&nbsp;�������� ���� ����� ���������. ���������� ��&nbsp;����, ����� ������� ��� ����� ��������, ����� ������ ������ ��� (��. ����). ���� ��� ����� ������������ ���� �����, �&nbsp;��������� ��� ����� ������, ����� ��� ������� ���� ��&nbsp;������.</p>
<br style="clear:both">

<h3>������</h3>
<p>����� ����������� ������������ �����-���� ���������� ��������� �&nbsp;���� ����. �&nbsp;�������, �&nbsp;��������� ����� ���� ��������� ����� (�����: ���� ������), ����������� ����� ���� (�����: ���������� �&nbsp;�����), ������ �&nbsp;������������� ��&nbsp;������������ (�����: ����). ����� ��������� �������� ����� ������� ��������� ����� ���� ��� ����������, ��&nbsp;��&nbsp;����������� �������� ��������.</p>

<h3>�������������</h3>
<p>�&nbsp;�������� ���� ����� ��������� ����� ���� ���������.</p>
	<img src="../../images/starbuck-with-girl.jpg" class="bsgpic" style="float:right">
<p>������, <strong>�������� ����� ������� ���� �����</strong>. ��� ����������, ����� �������� ����������� ������������ ����� ���-�� ������ ��� ��������� �&nbsp;����� ��������.<br />
<em>������: ������, ��� �������� ���������� ������, ��� �������� ������������� �����, �� ����� ���������� �������� �� ���������� ���, ����������� ��� ���������� ���������� ������.</em></p>

<p>������, <strong>����� ����� ������� ���������</strong>. ��� ����������, ����� ������� ��������� �&nbsp;����� ��������� ��������.<br />
<em>������: ���� ����� ������, ��� ������ ����� �� ����� ���� �� �����, ��� ��������� �����. ������ ���� ������� � ���� ���������, ��� ����������� ������ ���� ������ � ������ ������ ��������.</em>
</p>
<p>�&nbsp;��������� ����� <strong>����������� �������� ��� ������� �����</strong>. ���� �������� �������, ��� ��&nbsp;������ ���� ����� ��� ����� ������ ���&nbsp;&mdash; ��&nbsp;����, ���� ����� ��� ���� �����������&nbsp;&mdash; ��&nbsp;<strong>��������� ��������</strong>. ��� ������ �������� ���������, ����� ��������� ������������ �&nbsp;���������� �����������. ����������� �������� �������� ��� ������ ����������� ������� �&nbsp;����������� ����. ���� ����� ����������� ��� (������, �����, ��&nbsp;�������), ������ ������ �&nbsp;����.</p>

<h3>�������</h3>
<p>�&nbsp;������� ��������� ����, ������ �������� ������, ���� �������� �������.<br />
���� �&nbsp;����� ������ �������� �������, ��� ������ �������� ����������������� �������� ����-�� �&nbsp;��� ������, ��&nbsp;��&nbsp;��������� ��� �������� (��������� ��� ��� ������� ��������� �&nbsp;���� ��&nbsp;������� ��������) �&nbsp;������� ��&nbsp;�&nbsp;����������� ����.<br />
����� ��� �������� ����� ���� ������������ ��������&nbsp;&mdash; ���� ������ �������� ������� ���������� ����-�� �&nbsp;���, �&nbsp;����� ������ ���������, ������������ ��������, �����.<br />
��� ������������ �������� ������ �������������� �������� ������� ��&nbsp;������ �������� �����.</p>
<h3>����� �����</h3>
<p>�&nbsp;�������� ���� ��� ����� ���� ���������� ����� ����� �������� �����. �������� ����� ��� ��������, ���� ������������� ������������ ���� ����� ������ �����. ���������� ������ ������ ����� �������� ���� �������� ���.</p>

</div>
 </td>
<? right_block("bsg");?>
	     </tr>
<? show_footer(); ?>

