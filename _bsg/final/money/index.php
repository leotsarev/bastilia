<?
define("ROOT","../../../");
require_once ROOT . "funcs.php";
show_header("BSG :: ����� ���� :: ���������� �����", BSG);
show_menu("inc/main.menu"); ?>

	    <td class="box">

<div class="boxheader"><a href="http://bsg.bastilia.ru/">Battlestar Galactica</a> :: <a href="http://bsg.bastilia.ru/final/">����� ����</a> :: ���������� ����� </div>
           
<p>��&nbsp;������� �&nbsp;������� �&nbsp;����� ��������� <strong>275000</strong>&nbsp;������, ��������� <strong>267075</strong>&nbsp;������. �������� �������&nbsp;&mdash; <strong>7925</strong>&nbsp;������.</p>
<p>������ (275000&nbsp;������) �������������� ��&nbsp;��������� �������:</p>
<ul>
<li><strong>1500</strong> ������&nbsp;&mdash; <strong>1</strong>&nbsp;����� (��&nbsp;���� ��&nbsp;������);</li>
<li><strong>2500</strong> ������&nbsp;&mdash; <strong>50</strong>&nbsp;�������;</li>
<li><strong>3000</strong> ������&nbsp;&mdash; <strong>46</strong>&nbsp;�������;</li>
<li><strong>3500</strong> ������&nbsp;&mdash; <strong>3</strong>&nbsp;������.<br />
</li>
</ul>
<p>���� ������ ��������� ��������, ��&nbsp;��� ����� ��&nbsp;������, ���� ����������� ��������� ����� �&nbsp;��������� ��������. </p>
<table width="95%" border="1" align="center" cellpadding="1" cellspacing="1">
<tr>
<td><strong>������ ��������</strong></td>
<td align="right"><div align="center"><strong>���-��</strong></div></td>
<td align="right"><div align="center"><strong>����</strong></div></td>
<td align="right"><div align="center"><strong>�����</strong></div></td>
</tr>
<tr>
<td colspan="4"><strong>������� �&nbsp;PR ���� (7778&nbsp;�.) </strong></td>
</tr>
<tr>
<td><div align="left">DVD&nbsp;c �������� ��� ������� ������������� ������� (��������, ������������� �&nbsp;������), ��</div></td>
<td align="right"><div align="center">100</div></td>
<td align="right"><div align="center">30,5</div></td>
<td align="right"><div align="center">3050</div></td>
</tr>
<tr>

<td><div align="left"><nobr>DVD-��������,</nobr> ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">30</div></td>
<td align="right"><div align="center">30</div></td>
</tr>
<tr>
<td><div align="left">������ �������������, ��.</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">210</div></td>
<td align="right"><div align="center">210</div></td>
</tr>
<tr>
<td><div align="left">���� ��������� ��&nbsp;����������, 24&nbsp;�����</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">660</div></td>
<td align="right"><div align="center">660</div></td>
</tr>
<tr>
<td><div align="left">�������</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">1412</div></td>
<td align="right"><div align="center">1412</div></td>
</tr>
<tr>
<td><div align="left">���������� ���� ��� �����</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">1800</div></td>
<td align="right"><div align="center">1800</div></td>
</tr>
<tr>
<td><div align="left">����� ��������, ��</div></td>
<td align="right"><div align="center">4</div></td>
<td align="right"><div align="center">29</div></td>
<td align="right"><div align="center">116</div></td>
</tr>
<tr>
<td><div align="left">����� (���� ��� ������� ��� ��&nbsp;��������)</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">500</div></td>
<td align="right"><div align="center">500</div></td>
</tr>
<tr>
<td colspan="4"><div align="left"><strong>���������� �&nbsp;������� ��&nbsp;���� (152440&nbsp;�.)</strong> </div></td>
</tr>
<tr>
<td><div align="left">���������� �&nbsp;������� �������, �������</div></td>
<td align="right"><div align="center">99</div></td>
<td align="right"><div align="center">1380</div></td>
<td align="right"><div align="center">136620</div></td>
</tr>
<tr>
<td><div align="left">������� �������� �&nbsp;����������, �������</div></td>
<td align="right"><div align="center">19</div></td>
<td align="right"><div align="center">580</div></td>
<td align="right"><div align="center">11020</div></td>
</tr>
<tr>
<td><div align="left">���������� ����������, �������</div></td>
<td align="right"><div align="center">6</div></td>
<td align="right"><div align="center">800</div></td>
<td align="right"><div align="center">4800</div></td>
</tr>
<tr>
<td colspan="4"><div align="left"><strong>������� �&nbsp;����������� (35753&nbsp;�.) </strong></div></td>
</tr>
<tr>
<td><div align="left">FM-��������� ��� ������������� ����� &laquo;������&raquo;, ��</div></td>
<td align="right"><div align="center">4</div></td>
<td align="right"><div align="center">240</div></td>
<td align="right"><div align="center">960</div></td>
</tr>
<tr>
<td><div align="left">�����������, �</div></td>
<td align="right"><div align="center">100</div></td>
<td align="right"><div align="center">11</div></td>
<td align="right"><div align="center">1100</div></td>
</tr>
<tr>
<td><div align="left">��������� AA&nbsp;Duracell, 4&nbsp;��</div></td>
<td align="right"><div align="center">9</div></td>
<td align="right"><div align="center">76</div></td>
<td align="right"><div align="center">684</div></td>
</tr>
<tr>
<td><div align="left">��������� AA&nbsp;Energizer, 4��</div></td>
<td align="right"><div align="center">12</div></td>
<td align="right"><div align="center">80</div></td>
<td align="right"><div align="center">960</div></td>
</tr>
<tr>
<td><div align="left">��������� ��&nbsp;Duracell, 4&nbsp;�� (���������)</div></td>
<td align="right"><div align="center">3</div></td>
<td align="right"><div align="center">90</div></td>
<td align="right"><div align="center">270</div></td>
</tr>
<tr>
<td><div align="left">��������� ��&nbsp;Duracell, 8&nbsp;��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">109</div></td>
<td align="right"><div align="center">109</div></td>
</tr>
<tr>
<td><div align="left">��������� ��� (��� ���������������), 2&nbsp;��</div></td>
<td align="right"><div align="center">4</div></td>
<td align="right"><div align="center">35</div></td>
<td align="right"><div align="center">140</div></td>
</tr>
<tr>
<td><div align="left">������� Jetbalance JB-112, �����.</div></td>
<td align="right"><div align="center">4</div></td>
<td align="right"><div align="center">317</div></td>
<td align="right"><div align="center">1268</div></td>
</tr>
<tr>
<td><div align="left">���������� RJ-45 8p8c, 100&nbsp;��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">325</div></td>
<td align="right"><div align="center">325</div></td>
</tr>
<tr>
<td><div align="left">�������� <nobr>X-storm</nobr> TN-18&nbsp;Black, ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">108</div></td>
<td align="right"><div align="center">108</div></td>
</tr>
<tr>
<td><div align="left">�������� c&nbsp;���������� Cosonic CD721MV, ��</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">228</div></td>
<td align="right"><div align="center">456</div></td>
</tr>
<tr>
<td><div align="left">�������� c&nbsp;���������� Gembird ETG MHS-103, ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">158</div></td>
<td align="right"><div align="center">158</div></td>
</tr>
<tr>
<td><div align="left">�������� Soger HY-0018, ��</div></td>
<td align="right"><div align="center">4</div></td>
<td align="right"><div align="center">61</div></td>
<td align="right"><div align="center">244</div></td>
</tr>
<tr>
<td><div align="left">�������� �&nbsp;���������� Sven ��-520, ��</div></td>
<td align="right"><div align="center">3</div></td>
<td align="right"><div align="center">240</div></td>
<td align="right"><div align="center">720</div></td>
</tr>
<tr>
<td><div align="left">����-���� 5E&nbsp;��������� (3�), ��</div></td>
<td align="right"><div align="center">3</div></td>
<td align="right"><div align="center">58</div></td>
<td align="right"><div align="center">174</div></td>
</tr>
<tr>
<td><div align="left">����-���� 5E&nbsp;��������� (5�), ��</div></td>
<td align="right"><div align="center">3</div></td>
<td align="right"><div align="center">69</div></td>
<td align="right"><div align="center">207</div></td>
</tr>
<tr>
<td><div align="left">���������� <nobr>DVI-VGA,</nobr> ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">280</div></td>
<td align="right"><div align="center">280</div></td>
</tr>
<tr>
<td><div align="left">������� ������ ��&nbsp;6&nbsp;�������, ��</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">297</div></td>
<td align="right"><div align="center">594</div></td>
</tr>
<tr>
<td><div align="left">������� ������, 5&nbsp;�������, 3&nbsp;�����, ��</div></td>
<td align="right"><div align="center">3</div></td>
<td align="right"><div align="center">148</div></td>
<td align="right"><div align="center">444</div></td>
</tr>
<tr>
<td><div align="left">������ 150��, 100��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">95</div></td>
<td align="right"><div align="center">95</div></td>
</tr>
<tr>
<td><div align="left">���������� �����-3, 3&nbsp;������, �����</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">55</div></td>
<td align="right"><div align="center">110</div></td>
</tr>
<tr>
<td><div align="left">������ ��-����� �&nbsp;��������, �����.</div></td>
<td align="right"><div align="center">10</div></td>
<td align="right"><div align="center">500</div></td>
<td align="right"><div align="center">5000</div></td>
</tr>
<tr>
<td><div align="left">������ ������ ������� (������� ��� �����), �����.</div></td>
<td align="right"><div align="center">6</div></td>
<td align="right"><div align="center">50</div></td>
<td align="right"><div align="center">300</div></td>
</tr>
<tr>
<td><div align="left">��������� ��� ������ �������&nbsp;��, ��</div></td>
<td align="right"><div align="center">16</div></td>
<td align="right"><div align="center">5</div></td>
<td align="right"><div align="center">80</div></td>
</tr>
<tr>
<td><div align="left">����������� ��&nbsp;���������� ��-�������</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">720</div></td>
<td align="right"><div align="center">720</div></td>
</tr>
<tr>
<td><div align="left">��������� (10&nbsp;��������� �&nbsp;6&nbsp;�������). ������ (��������� �������� ����������� ��&nbsp;����������)</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">18085</div></td>
<td align="right"><div align="center">18085</div></td>
</tr>
<tr>
<td><div align="left">������ �&nbsp;���������� (��� ��������� �������� ���������), �����.</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">360</div></td>
<td align="right"><div align="center">360</div></td>
</tr>
<tr>
<td><div align="left">����� ���������, ��</div></td>
<td align="right"><div align="center">4</div></td>
<td align="right"><div align="center">300</div></td>
<td align="right"><div align="center">1200</div></td>
</tr>
<tr>
<td><div align="left">���������� COM-USB (��� ����������� ���������), ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">602</div></td>
<td align="right"><div align="center">602</div></td>
</tr>
<tr>
<td colspan="4"><div align="left"><strong>������� (18359&nbsp;�.) </strong></div></td>
</tr>
<tr>
<td><div align="left">����� (��� ����������� �������� ��������), �</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">140</div></td>
<td align="right"><div align="center">280</div></td>
</tr>
<tr>
<td><div align="left">��������� <nobr>3-�������</nobr> 2060&times;1500 (��� �������� ����), ��</div></td>
<td align="right"><div align="center">70</div></td>
<td align="right"><div align="center">52,6</div></td>
<td align="right"><div align="center">3682</div></td>
</tr>
<tr>
<td><div align="left">��������� <nobr>3-�������</nobr> 2060&times;1600 (��� �������� ����), ��</div></td>
<td align="right"><div align="center">10</div></td>
<td align="right"><div align="center">56</div></td>
<td align="right"><div align="center">560</div></td>
</tr>
<tr>
<td><div align="left">���� &laquo;��������� ������&raquo;, ��</div></td>
<td align="right"><div align="center">36</div></td>
<td align="right"><div align="center">10</div></td>
<td align="right"><div align="center">360</div></td>
</tr>
<tr>
<td><div align="left">����� �������� ��&nbsp;������� (��� �������), ��</div></td>
<td align="right"><div align="center">132</div></td>
<td align="right"><div align="center">10</div></td>
<td align="right"><div align="center">1320</div></td>
</tr>
<tr>
<td><div align="left">���������� (��� ����������� �������� ��������), ��</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">7</div></td>
<td align="right"><div align="center">14</div></td>
</tr>
<tr>
<td><div align="left">����� ��� �����, ����������</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">225</div></td>
<td align="right"><div align="center">450</div></td>
</tr>
<tr>
<td><div align="left">������� ����� ������������ 12��/50� (��� ����� �����), ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">93</div></td>
<td align="right"><div align="center">93</div></td>
</tr>
<tr>
<td><div align="left">�������� �������� �������� CR2032 (�������), ��</div></td>
<td align="right"><div align="center">20</div></td>
<td align="right"><div align="center">11,6</div></td>
<td align="right"><div align="center">232</div></td>
</tr>
<tr>
<td><div align="left">�������� �������� �������� CR2450 (�������), ��</div></td>
<td align="right"><div align="center">10</div></td>
<td align="right"><div align="center">22,2</div></td>
<td align="right"><div align="center">222</div></td>
</tr>
<tr>
<td><div align="left">������ Edding E-8280 ���������� (������� ������ �&nbsp;��-�����), ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">83,5</div></td>
<td align="right"><div align="center">83,5</div></td>
</tr>
<tr>
<td><div align="left">���������������� ���� (��� ������� �������), �</div></td>
<td align="right"><div align="center">40</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">80</div></td>
</tr>
<tr>
<td><div align="left">������� �&nbsp;��������� &laquo;���������&raquo;, ��</div></td>
<td align="right"><div align="center">52</div></td>
<td align="right"><div align="center">80</div></td>
<td align="right"><div align="center">4160</div></td>
</tr>
<tr>
<td><div align="left">���� ���������, ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">110</div></td>
<td align="right"><div align="center">110</div></td>
</tr>
<tr>
<td><div align="left">������ ���������� �����������, ��</div></td>
<td align="right"><div align="center">248</div></td>
<td align="right"><div align="center">2,8</div></td>
<td align="right"><div align="center">694,4</div></td>
</tr>
<tr>
<td><div align="left">����������� ����������� �����, ��</div></td>
<td align="right"><div align="center">3</div></td>
<td align="right"><div align="center">110</div></td>
<td align="right"><div align="center">330</div></td>
</tr>
<tr>
<td><div align="left">���������� 5&nbsp;�� (�������), ��</div></td>
<td align="right"><div align="center">30</div></td>
<td align="right"><div align="center">4</div></td>
<td align="right"><div align="center">120</div></td>
</tr>
<tr>
<td><div align="left">�������� �������� ���������� (���������� ������), ��</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">30</div></td>
<td align="right"><div align="center">60</div></td>
</tr>
<tr>
<td><div align="left">��-��������, ��</div></td>
<td align="right"><div align="center">3</div></td>
<td align="right"><div align="center">250</div></td>
<td align="right"><div align="center">750</div></td>
</tr>
<tr>
<td><div align="left">���� 12&nbsp;�������, ��</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">1000</div></td>
<td align="right"><div align="center">2000</div></td>
</tr>
<tr>
<td><div align="left">���� ������ �������, ��</div></td>
<td align="right"><div align="center">9</div></td>
<td align="right"><div align="center">200</div></td>
<td align="right"><div align="center">1800</div></td>
</tr>
<tr>
<td><div align="left">������ ������� 1,5&nbsp;� (��� ��������������� ��������), ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">200</div></td>
<td align="right"><div align="center">200</div></td>
</tr>
<tr>
<td><div align="left">������� (��� ������), 1,5&nbsp;�</div></td>
<td align="right"><div align="center">4</div></td>
<td align="right"><div align="center">16</div></td>
<td align="right"><div align="center">64</div></td>
</tr>
<tr>
<td><div align="left">������ �&nbsp;������ (���������, 2&times;1,8&nbsp;�), ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">242</div></td>
<td align="right"><div align="center">242</div></td>
</tr>
<tr>
<td><div align="left">����� Tic-Tac (��� ����������� ��������), 16&nbsp;�</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">18,3</div></td>
<td align="right"><div align="center">36,6</div></td>
</tr>
<tr>
<td><div align="left">����� Tic-Tac (��� ����������� ��������), 47&nbsp;�</div></td>
<td align="right"><div align="center">3</div></td>
<td align="right"><div align="center">58,5</div></td>
<td align="right"><div align="center">175,5</div></td>
</tr>
<tr>
<td><div align="left">������ (��� ����������� ��������), ��</div></td>
<td align="right"><div align="center">60</div></td>
<td align="right"><div align="center">2,8</div></td>
<td align="right"><div align="center">168</div></td>
</tr>
<tr>
<td><div align="left">������ 2&nbsp;�� (��� ����������� ��������), ��</div></td>
<td align="right"><div align="center">40</div></td>
<td align="right"><div align="center">1,8</div></td>
<td align="right"><div align="center">72</div></td>
</tr>
<tr>
<td colspan="4"><div align="left"><strong>���������� (7489,8&nbsp;�.) </strong></div></td>
</tr>
<tr>
<td><div align="left">&laquo;����������� ���������&raquo;, 1,5&nbsp;�</div></td>
<td align="right"><div align="center">3</div></td>
<td align="right"><div align="center">30</div></td>
<td align="right"><div align="center">90</div></td>
</tr>
<tr>
<td><div align="left">��������� &laquo;����-������&raquo;, 400&nbsp;��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">399</div></td>
<td align="right"><div align="center">399</div></td>
</tr>
<tr>
<td><div align="left">����������� 12�, ��</div></td>
<td align="right"><div align="center">3</div></td>
<td align="right"><div align="center">300</div></td>
<td align="right"><div align="center">900</div></td>
</tr>
<tr>
<td><div align="left">������� (��� ������������ ������ �����), ����</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">10</div></td>
<td align="right"><div align="center">20</div></td>
</tr>
<tr>
<td><div align="left">������ 4&nbsp;�� (��������� �����), �������</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">469</div></td>
<td align="right"><div align="center">469</div></td>
</tr>
<tr>
<td><div align="left">������ 2&times;0,5, �</div></td>
<td align="right"><div align="center">5</div></td>
<td align="right"><div align="center">12</div></td>
<td align="right"><div align="center">60</div></td>
</tr>
<tr>
<td><div align="left">���� ������� �&nbsp;����������� (��� ������������ ������ �����), ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">65</div></td>
<td align="right"><div align="center">65</div></td>
</tr>
<tr>
<td><div align="left">��������� ��� ������������, ��</div></td>
<td align="right"><div align="center">10</div></td>
<td align="right"><div align="center">3</div></td>
<td align="right"><div align="center">30</div></td>
</tr>
<tr>
<td><div align="left">������ ��� ������������, ��</div></td>
<td align="right"><div align="center">3</div></td>
<td align="right"><div align="center">20</div></td>
<td align="right"><div align="center">60</div></td>
</tr>
<tr>
<td><div align="left">������� ������������ (������ ����� �&nbsp;�����), ����������</div></td>
<td align="right"><div align="center">3</div></td>
<td align="right"><div align="center">740</div></td>
<td align="right"><div align="center">2220</div></td>
</tr>
<tr>
<td><div align="left">������ ����������������, ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">102</div></td>
<td align="right"><div align="center">102</div></td>
</tr>
<tr>
<td><div align="left">����� ������������ �������, 20&nbsp;��</div></td>
<td align="right"><div align="center">3</div></td>
<td align="right"><div align="center">81</div></td>
<td align="right"><div align="center">243</div></td>
</tr>
<tr>
<td><div align="left">����������� ����������� (���� �����������), ��</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">480</div></td>
<td align="right"><div align="center">960</div></td>
</tr>
<tr>
<td><div align="left">����������� ����������� (���� �����������), ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">550</div></td>
<td align="right"><div align="center">550</div></td>
</tr>
<tr>
<td><div align="left">��������� ������������ (��� ������������ ������ �����), ��</div></td>
<td align="right"><div align="center">3</div></td>
<td align="right"><div align="center">195</div></td>
<td align="right"><div align="center">585</div></td>
</tr>
<tr>
<td><div align="left">�������, �</div></td>
<td align="right"><div align="center">5</div></td>
<td align="right"><div align="center">9</div></td>
<td align="right"><div align="center">45</div></td>
</tr>
<tr>
<td><div align="left">���������� �����, ��</div></td>
<td align="right"><div align="center">4</div></td>
<td align="right"><div align="center">30</div></td>
<td align="right"><div align="center">120</div></td>
</tr>
<tr>
<td><div align="left">����� ������� (��� ������������), ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">200</div></td>
<td align="right"><div align="center">200</div></td>
</tr>
<tr>
<td><div align="left">����� ������� (��� ������������), ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">150</div></td>
<td align="right"><div align="center">150</div></td>
</tr>
<tr>
<td><div align="left">�����-����, �����</div></td>
<td align="right"><div align="center">3</div></td>
<td align="right"><div align="center">50,6</div></td>
<td align="right"><div align="center">151,8</div></td>
</tr>
<tr>
<td><div align="left">������ �����, �</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">70</div></td>
<td align="right"><div align="center">70</div></td>
</tr>
<tr>
<td colspan="4"><div align="left"><strong>��� (3567,66&nbsp;�.) </strong></div></td>
</tr>
<tr>
<td><div align="left">�����, �</div></td>
<td align="right"><div align="center">3</div></td>
<td align="right"><div align="center">140</div></td>
<td align="right"><div align="center">420</div></td>
</tr>
<tr>
<td><div align="left">���� �����������, 200&nbsp;�</div></td>
<td align="right"><div align="center">3</div></td>
<td align="right"><div align="center">126,7</div></td>
<td align="right"><div align="center">380,1</div></td>
</tr>
<tr>
<td><div align="left">����� �����������, 100&nbsp;��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">57</div></td>
<td align="right"><div align="center">57</div></td>
</tr>
<tr>
<td><div align="left">������� &laquo;���������&raquo;, �����</div></td>
<td align="right"><div align="center">32</div></td>
<td align="right"><div align="center">13,33</div></td>
<td align="right"><div align="center">426,56</div></td>
</tr>
<tr>
<td><div align="left">���� &laquo;365&nbsp;����&raquo;, 2,5&nbsp;�</div></td>
<td align="right"><div align="center">12</div></td>
<td align="right"><div align="center">75,8</div></td>
<td align="right"><div align="center">909,6</div></td>
</tr>
<tr>
<td><div align="left">����&nbsp;2,5&nbsp;�����, ���. (��������� �&nbsp;�����������)</div></td>
<td align="right"><div align="center">4</div></td>
<td align="right"><div align="center">78,5</div></td>
<td align="right"><div align="center">314</div></td>
</tr>
<tr>
<td><div align="left">�����-�������, 1&nbsp;��</div></td>
<td align="right"><div align="center">5</div></td>
<td align="right"><div align="center">40,8</div></td>
<td align="right"><div align="center">204</div></td>
</tr>
<tr>
<td><div align="left">������ �����������, 100&nbsp;��</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">41,7</div></td>
<td align="right"><div align="center">83,4</div></td>
</tr>
<tr>
<td><div align="left">�������� &laquo;��������&raquo;, �����</div></td>
<td align="right"><div align="center">20</div></td>
<td align="right"><div align="center">7,2</div></td>
<td align="right"><div align="center">144</div></td>
</tr>
<tr>
<td><div align="left">�������� ������, ����� (��������� �&nbsp;�����������)</div></td>
<td align="right"><div align="center">15</div></td>
<td align="right"><div align="center">11</div></td>
<td align="right"><div align="center">165</div></td>
</tr>
<tr>
<td><div align="left">������� ����������� ������, 100&nbsp;��</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">61</div></td>
<td align="right"><div align="center">122</div></td>
</tr>
<tr>
<td><div align="left">��� Ceylon, 100&nbsp;�</div></td>
<td align="right"><div align="center">3</div></td>
<td align="right"><div align="center">114</div></td>
<td align="right"><div align="center">342</div></td>
</tr>
<tr>
<td colspan="4"><div align="left"><strong>������ ����� (5539&nbsp;�.) </strong></div></td>
</tr>
<tr>
<td><div align="left">�������&nbsp;0,25&nbsp;�, ��</div></td>
<td align="right"><div align="center">25</div></td>
<td align="right"><div align="center">5</div></td>
<td align="right"><div align="center">125</div></td>
</tr>
<tr>
<td><div align="left">������� &laquo;����-����&raquo;, 1,5&nbsp;�</div></td>
<td align="right"><div align="center">24</div></td>
<td align="right"><div align="center">11</div></td>
<td align="right"><div align="center">264</div></td>
</tr>
<tr>
<td><div align="left">������� &laquo;������&raquo;, 1,5&nbsp;�</div></td>
<td align="right"><div align="center">12</div></td>
<td align="right"><div align="center">14,3</div></td>
<td align="right"><div align="center">171,6</div></td>
</tr>
<tr>
<td><div align="left">���� &laquo;Altstein&raquo;, 0,5&nbsp;�</div></td>
<td align="right"><div align="center">36</div></td>
<td align="right"><div align="center">25,4</div></td>
<td align="right"><div align="center">914,4</div></td>
</tr>
<tr>
<td><div align="left">���� &laquo;Zlaty Bazant&raquo;, 0,5&nbsp;�</div></td>
<td align="right"><div align="center">18</div></td>
<td align="right"><div align="center">28</div></td>
<td align="right"><div align="center">504</div></td>
</tr>
<tr>
<td><div align="left">���, �</div></td>
<td align="right"><div align="center">3</div></td>
<td align="right"><div align="center">300</div></td>
<td align="right"><div align="center">900</div></td>
</tr>
<tr>
<td><div align="left">�������� �����, 250&nbsp;�</div></td>
<td align="right"><div align="center">4</div></td>
<td align="right"><div align="center">21</div></td>
<td align="right"><div align="center">84</div></td>
</tr>
<tr>
<td><div align="left">�������� Pall Mall, ������</div></td>
<td align="right"><div align="center">4</div></td>
<td align="right"><div align="center">254</div></td>
<td align="right"><div align="center">1016</div></td>
</tr>
<tr>
<td><div align="left">������, �</div></td>
<td align="right"><div align="center">3</div></td>
<td align="right"><div align="center">400</div></td>
<td align="right"><div align="center">1200</div></td>
</tr>
<tr>
<td><div align="left">�������, ������</div></td>
<td align="right"><div align="center">20</div></td>
<td align="right"><div align="center">18</div></td>
<td align="right"><div align="center">360</div></td>
</tr>
<tr>
<td colspan="4"><div align="left"><strong>����������� �������� (17563,78&nbsp;�.) </strong></div></td>
</tr>
<tr>
<td><div align="left">�����, ��</div></td>
<td align="right"><div align="center">5</div></td>
<td align="right"><div align="center">19</div></td>
<td align="right"><div align="center">95</div></td>
</tr>
<tr>
<td><div align="left">���� ��������, 6&nbsp;�</div></td>
<td align="right"><div align="center">8</div></td>
<td align="right"><div align="center">25</div></td>
<td align="right"><div align="center">200</div></td>
</tr>
<tr>
<td><div align="left">����� ����� 45&times;60&nbsp;������, ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">402</div></td>
<td align="right"><div align="center">402</div></td>
</tr>
<tr>
<td><div align="left">����-���� ����, 2&nbsp;�</div></td>
<td align="right"><div align="center">6</div></td>
<td align="right"><div align="center">45,6</div></td>
<td align="right"><div align="center">273,6</div></td>
</tr>
<tr>
<td><div align="left">����-����, 2&nbsp;�</div></td>
<td align="right"><div align="center">24</div></td>
<td align="right"><div align="center">41,4</div></td>
<td align="right"><div align="center">993,6</div></td>
</tr>
<tr>
<td><div align="left">���� 3�1, 50&nbsp;�</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">119</div></td>
<td align="right"><div align="center">238</div></td>
</tr>
<tr>
<td><div align="left">������� ������� �&nbsp;�������� ��� �������� �&nbsp;����������, ��</div></td>
<td align="right"><div align="center">31</div></td>
<td align="right"><div align="center">130</div></td>
<td align="right"><div align="center">4030</div></td>
</tr>
<tr>
<td><div align="left">��������� Fellowes Mars&nbsp;A4, ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">1990</div></td>
<td align="right"><div align="center">1990</div></td>
</tr>
<tr>
<td><div align="left">����� &laquo;�������&raquo;, ��</div></td>
<td align="right"><div align="center">30</div></td>
<td align="right"><div align="center">16</div></td>
<td align="right"><div align="center">480</div></td>
</tr>
<tr>
<td><div align="left">����� &laquo;�������&raquo;, ��&nbsp;(��������� �&nbsp;�����������)</div></td>
<td align="right"><div align="center">15</div></td>
<td align="right"><div align="center">22</div></td>
<td align="right"><div align="center">330</div></td>
</tr>
<tr>
<td><div align="left">������, ��</div></td>
<td align="right"><div align="center">1,564</div></td>
<td align="right"><div align="center">100</div></td>
<td align="right"><div align="center">156,4</div></td>
</tr>
<tr>
<td><div align="left">������ &laquo;������&raquo;, 2&nbsp;�</div></td>
<td align="right"><div align="center">6</div></td>
<td align="right"><div align="center">46</div></td>
<td align="right"><div align="center">276</div></td>
</tr>
<tr>
<td><div align="left">������, �����</div></td>
<td align="right"><div align="center">5</div></td>
<td align="right"><div align="center">25,4</div></td>
<td align="right"><div align="center">127</div></td>
</tr>
<tr>
<td><div align="left">������� &laquo;���������&raquo;, �����</div></td>
<td align="right"><div align="center">30</div></td>
<td align="right"><div align="center">13,33</div></td>
<td align="right"><div align="center">399,9</div></td>
</tr>
<tr>
<td><div align="left">���� &laquo;Zlaty Bazant&raquo;, 0,5&nbsp;�</div></td>
<td align="right"><div align="center">6</div></td>
<td align="right"><div align="center">27,9</div></td>
<td align="right"><div align="center">167,4</div></td>
</tr>
<tr>
<td><div align="left">���� &laquo;������� &#8470;&nbsp;0&raquo;, 0,5&nbsp;�</div></td>
<td align="right"><div align="center">24</div></td>
<td align="right"><div align="center">25,8</div></td>
<td align="right"><div align="center">619,2</div></td>
</tr>
<tr>
<td><div align="left">���� &laquo;������� &#8470;&nbsp;7&raquo;, 0,5&nbsp;�</div></td>
<td align="right"><div align="center">24</div></td>
<td align="right"><div align="center">25</div></td>
<td align="right"><div align="center">600</div></td>
</tr>
<tr>
<td><div align="left">���� ������������ �/�, 450&nbsp;�</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">84,4</div></td>
<td align="right"><div align="center">168,8</div></td>
</tr>
<tr>
<td><div align="left">���, �</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">300</div></td>
<td align="right"><div align="center">600</div></td>
</tr>
<tr>
<td><div align="left">��� &laquo;�����&raquo;, 1,5&nbsp;�</div></td>
<td align="right"><div align="center">16</div></td>
<td align="right"><div align="center">55</div></td>
<td align="right"><div align="center">880</div></td>
</tr>
<tr>
<td><div align="left">������ �����������, 100&nbsp;��</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">41,7</div></td>
<td align="right"><div align="center">83,4</div></td>
</tr>
<tr>
<td><div align="left">�������� &laquo;�����&raquo;, �����</div></td>
<td align="right"><div align="center">5</div></td>
<td align="right"><div align="center">10,3</div></td>
<td align="right"><div align="center">51,5</div></td>
</tr>
<tr>
<td><div align="left">�������� &laquo;��������&raquo;, �����</div></td>
<td align="right"><div align="center">12</div></td>
<td align="right"><div align="center">7,2</div></td>
<td align="right"><div align="center">86,4</div></td>
</tr>
<tr>
<td><div align="left">�������� ������, ����� (��������� �&nbsp;�����������)</div></td>
<td align="right"><div align="center">15</div></td>
<td align="right"><div align="center">11</div></td>
<td align="right"><div align="center">165</div></td>
</tr>
<tr>
<td><div align="left">��� �&nbsp;�������, 400&nbsp;�</div></td>
<td align="right"><div align="center">3</div></td>
<td align="right"><div align="center">119</div></td>
<td align="right"><div align="center">357</div></td>
</tr>
<tr>
<td><div align="left">������� ����������� ��������, 100&nbsp;��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">86</div></td>
<td align="right"><div align="center">86</div></td>
</tr>
<tr>
<td><div align="left">������, �</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">400</div></td>
<td align="right"><div align="center">800</div></td>
</tr>
<tr>
<td><div align="left">��������� ������ <nobr>2-�������,</nobr> 4&nbsp;������</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">31</div></td>
<td align="right"><div align="center">62</div></td>
</tr>
<tr>
<td><div align="left">����� ��� �������, �������</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">49</div></td>
<td align="right"><div align="center">49</div></td>
</tr>
<tr>
<td><div align="left">����� &laquo;365&nbsp;����&raquo;, �����</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">21</div></td>
<td align="right"><div align="center">42</div></td>
</tr>
<tr>
<td><div align="left">����� Lay&rsquo;s, �����</div></td>
<td align="right"><div align="center">3</div></td>
<td align="right"><div align="center">51</div></td>
<td align="right"><div align="center">153</div></td>
</tr>
<tr>
<td><div align="left">������ ������, ��</div></td>
<td align="right"><div align="center">7,69</div></td>
<td align="right"><div align="center">158</div></td>
<td align="right"><div align="center">1215,18</div></td>
</tr>
<tr>
<td><div align="left">���������� �������� &laquo;�������&raquo;, ��</div></td>
<td align="right"><div align="center">48</div></td>
<td align="right"><div align="center">16,3</div></td>
<td align="right"><div align="center">782,4</div></td>
</tr>
<tr>
<td><div align="left">���������� �������� &laquo;�������&raquo;, ��&nbsp;(��������� �&nbsp;�����������)</div></td>
<td align="right"><div align="center">15</div></td>
<td align="right"><div align="center">21</div></td>
<td align="right"><div align="center">315</div></td>
</tr>
<tr>
<td><div align="left">�������������, ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">289</div></td>
<td align="right"><div align="center">289</div></td>
</tr>
<tr>
<td colspan="4"><div align="left"><strong>���������� (3699,96&nbsp;�.) </strong></div></td>
</tr>
<tr>
<td><div align="left">���� &laquo;El&nbsp;Paso&raquo; ������� �/��, 3&nbsp;�</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">285</div></td>
<td align="right"><div align="center">285</div></td>
</tr>
<tr>
<td><div align="left">���� &laquo;El&nbsp;Paso&raquo; ������� ���, 3&nbsp;�</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">278</div></td>
<td align="right"><div align="center">556</div></td>
</tr>
<tr>
<td><div align="left">������� �������� ��� ���, ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">38</div></td>
<td align="right"><div align="center">38</div></td>
</tr>
<tr>
<td><div align="left">�������� ��� �������, 1&nbsp;�</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">52</div></td>
<td align="right"><div align="center">52</div></td>
</tr>
<tr>
<td><div align="left">���������� &laquo;�������&raquo;, ����� �&nbsp;�������-�������</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">1450</div></td>
<td align="right"><div align="center">1450</div></td>
</tr>
<tr>
<td><div align="left">����� ��� �������, �������</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">49</div></td>
<td align="right"><div align="center">98</div></td>
</tr>
<tr>
<td><div align="left">����, �������</div></td>
<td align="right"><div align="center">4</div></td>
<td align="right"><div align="center">24</div></td>
<td align="right"><div align="center">96</div></td>
</tr>
<tr>
<td><div align="left">������ ������, ��</div></td>
<td align="right"><div align="center">7,12</div></td>
<td align="right"><div align="center">158</div></td>
<td align="right"><div align="center">1124,96</div></td>
</tr>
<tr>
<td colspan="4"><div align="left"><strong>���������� (2462&nbsp;�.) </strong></div></td>
</tr>
<tr>
<td><div align="left">������ �4&nbsp;������� (280&nbsp;�/�), 150&nbsp;�</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">343</div></td>
<td align="right"><div align="center">343</div></td>
</tr>
<tr>
<td><div align="left">������&nbsp;�4, �����</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">108</div></td>
<td align="right"><div align="center">216</div></td>
</tr>
<tr>
<td><div align="left">������ ������� (�����) 160&nbsp;�/�2, 250&nbsp;�</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">442</div></td>
<td align="right"><div align="center">442</div></td>
</tr>
<tr>
<td><div align="left">������&nbsp;�1, ��</div></td>
<td align="right"><div align="center">10</div></td>
<td align="right"><div align="center">8,6</div></td>
<td align="right"><div align="center">86</div></td>
</tr>
<tr>
<td><div align="left">�������, ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">60</div></td>
<td align="right"><div align="center">60</div></td>
</tr>
<tr>
<td><div align="left">��������� ��� ������������� �4, 100&nbsp;��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">297</div></td>
<td align="right"><div align="center">297</div></td>
</tr>
<tr>
<td><div align="left">��������� ��� ������������� �4, 100&nbsp;�� (���������)</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">527</div></td>
<td align="right"><div align="center">527</div></td>
</tr>
<tr>
<td><div align="left">������� �5&nbsp;�����, ��</div></td>
<td align="right"><div align="center">120</div></td>
<td align="right"><div align="center">1,3</div></td>
<td align="right"><div align="center">156</div></td>
</tr>
<tr>
<td><div align="left">�������&nbsp;�6, ��</div></td>
<td align="right"><div align="center">200</div></td>
<td align="right"><div align="center">0,55</div></td>
<td align="right"><div align="center">110</div></td>
</tr>
<tr>
<td><div align="left">������� ��� ����� �����, 8&nbsp;��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">28</div></td>
<td align="right"><div align="center">28</div></td>
</tr>
<tr>
<td><div align="left">����� �������� ��� ����� �����, 4&nbsp;�����</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">55</div></td>
<td align="right"><div align="center">55</div></td>
</tr>
<tr>
<td><div align="left">������������� �������� 30&times;19, 1500&nbsp;��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">54</div></td>
<td align="right"><div align="center">54</div></td>
</tr>
<tr>
<td><div align="left">����� ��� ����� �����, ��</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">44</div></td>
<td align="right"><div align="center">88</div></td>
</tr>
<tr>
<td colspan="4"><div align="left"><strong>��������� (2191&nbsp;�.) </strong></div></td>
</tr>
<tr>
<td><div align="left">���������� �&nbsp;�������, ��</div></td>
<td align="right"><div align="center">4</div></td>
<td align="right"><div align="center">94</div></td>
<td align="right"><div align="center">376</div></td>
</tr>
<tr>
<td><div align="left">��������, ��</div></td>
<td align="right"><div align="center">15</div></td>
<td align="right"><div align="center">7</div></td>
<td align="right"><div align="center">105</div></td>
</tr>
<tr>
<td><div align="left">������� ��&nbsp;����������, ��</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">37</div></td>
<td align="right"><div align="center">74</div></td>
</tr>
<tr>
<td><div align="left">����� ������������� ������-����� (��� �������� ������), 100&nbsp;�</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">100</div></td>
<td align="right"><div align="center">100</div></td>
</tr>
<tr>
<td><div align="left">����� ������������� ������-����� (��� �������� ������), 200&nbsp;�</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">138</div></td>
<td align="right"><div align="center">276</div></td>
</tr>
<tr>
<td><div align="left">����� ��� ������ 120&nbsp;�</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">90</div></td>
<td align="right"><div align="center">90</div></td>
</tr>
<tr>
<td><div align="left">��� ������������ �������, ��</div></td>
<td align="right"><div align="center">5</div></td>
<td align="right"><div align="center">11</div></td>
<td align="right"><div align="center">55</div></td>
</tr>
<tr>
<td><div align="left">������ ��� ������ 60&nbsp;�</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">34</div></td>
<td align="right"><div align="center">68</div></td>
</tr>
<tr>
<td><div align="left">�������� ���, 5&nbsp;��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">45</div></td>
<td align="right"><div align="center">45</div></td>
</tr>
<tr>
<td><div align="left">����� ��� �������� 14&nbsp;��, ��.</div></td>
<td align="right"><div align="center">3</div></td>
<td align="right"><div align="center">40</div></td>
<td align="right"><div align="center">120</div></td>
</tr>
<tr>
<td><div align="left">����� <nobr>2-���������,</nobr> ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">90</div></td>
<td align="right"><div align="center">90</div></td>
</tr>
<tr>
<td><div align="left">����� <nobr>2-���������,</nobr> ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">93</div></td>
<td align="right"><div align="center">93</div></td>
</tr>
<tr>
<td><div align="left">����� ��������, ��</div></td>
<td align="right"><div align="center">12</div></td>
<td align="right"><div align="center">20</div></td>
<td align="right"><div align="center">240</div></td>
</tr>
<tr>
<td><div align="left">������������ �������, ��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">305</div></td>
<td align="right"><div align="center">305</div></td>
</tr>
<tr>
<td><div align="left">���������� ������, 100&nbsp;��</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">15</div></td>
<td align="right"><div align="center">15</div></td>
</tr>
<tr>
<td><div align="left">������, 300&nbsp;�</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">139</div></td>
<td align="right"><div align="center">139</div></td>
</tr>
<tr>
<td colspan="4"><div align="left"><strong>���������� (1940&nbsp;�.) </strong></div></td>
</tr>
<tr>
<td><div align="left">������ &laquo;��� ������� ����� ����&raquo;, ��</div></td>
<td align="right"><div align="center">130</div></td>
<td align="right"><div align="center">8,5</div></td>
<td align="right"><div align="center">1105</div></td>
</tr>
<tr>
<td><div align="left">��������� ������� �&nbsp;������</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">700</div></td>
<td align="right"><div align="center">700</div></td>
</tr>
<tr>
<td><div align="left">���������� (����������� ������ ��� �������)</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">135</div></td>
<td align="right"><div align="center">135</div></td>
</tr>
<tr>
<td colspan="4"><div align="left"><strong>������ (8292&nbsp;�.) </strong></div></td>
</tr>
<tr>
<td><div align="left">��������������� ����� ��&nbsp;������� �&nbsp;�������, �������</div></td>
<td align="right"><div align="center">2</div></td>
<td align="right"><div align="center">3500</div></td>
<td align="right"><div align="center">7000</div></td>
</tr>
<tr>
<td><div align="left">�������� ��������� ��� ML-1520P Samsung</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">400</div></td>
<td align="right"><div align="center">400</div></td>
</tr>
<tr>
<td><div align="left">�������� ��������� Canon MF-4018</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">600</div></td>
<td align="right"><div align="center">600</div></td>
</tr>
<tr>
<td><div align="left">��������� �����</div></td>
<td align="right"><div align="center">1</div></td>
<td align="right"><div align="center">100</div></td>
<td align="right"><div align="center">100</div></td>
</tr>
<tr>
<td><div align="left">���������� ��� ������ (��&nbsp;�����������), ��</div></td>
<td align="right"><div align="center">1,28</div></td>
<td align="right"><div align="center">150</div></td>
<td align="right"><div align="center">192</div></td>
</tr>
<tr>
<td><div align="right"><strong>�����:</strong></div></td>
<td align="right"><div align="center"></div></td>
<td align="right"><div align="center"></div></td>
<td align="right"><div align="center"><strong>267075</strong></div></td>
</tr>
</table>
</td>
<? right_block("bsg");?>
	     </tr>
<? show_footer(); ?>

