<?
define("ROOT","../../../");
require_once ROOT . 'funcs.php';
show_header("BSG :: ������� :: �����", BSG);
show_menu("inc/main.menu"); ?>

<td class="box">

<div class="boxheader">
<a href="http://bsg.bastilia.ru/">Battlestar Galactica</a> 
:: <a href="http://bsg.bastilia.ru/orginfo/">�������</a> 
:: �����</div>
    
<div>
  <h2>��������</h2>
  <p>
  ����� ��� ������� ������� �Colonial One� � �Cloud 9� ���������� <b>2500 </b>�. ��� ���������� � <b>3000</b> �.
  ����� ����������� ����� <b>�� 1 �������</b>.
  </p>
  <h4>�� ��� ���� ������</h4>
  <p>
  ����� �������� � ���� ���������� �� ���� (� ������� �� �����������) � �������, � ����� ��������������� �������, � ��� �����:</p>
  <ul>
    <li>
      ������� ��������� � ������������ ���������;
    </li>
    <li>
      ����������� ������ ��� ����������� ���������������� ���������;
    </li>
    <li>
      ������� � ������� � ��� �� �Cloud 9�;
    </li>
    <li>
      ���������� ����� �������� � ���� ���������� ������� (���, ���� ��� ����������)
    </li>
    <li>
      ������������ � ������������� ������ (� �.�. ��������� ��� ���������)
    </li>
    <li>
      ��������� ���������� (������, ������� ���������)
    </li>
    <li>
      ������� � ����������� (��������� ����; �����-, �����- � ���������������)
    </li>
    <li>
      ������ ��-����� � ��������<br>
    </li>
    <li>
      ��������������� ����� ����� �� ������� � �������<br>
    </li>
  </ul>
  <p>
  ������ "���������" ������ �� 500 ������ ������, ��������� ����� �������� ��������� ������ � ���� ������� (������������ ���� �������, �������, ����� ��������, ��-�����).
  <br>
  ����� ���� ����� ������������ ������ ���������� ����� (��� �� ������ ����� ��� <a href=http://bastilia.ru/akunin/post/money/ id=gxyg title='"��������� ������"'>���������� �������</a>, <a href=http://bastilia.ru/hellas/final/money/ id=rzvh title='"���� ���������"'>����� ���������</a>, <a href=http://bastilia.ru/lynch/final/money/ id=mzyb title='"��������"'>���������</a>, <a href=http://bastilia.ru/alaska/final/money/ id=m60b title='"��������: ������� ���������"'>���������: ������� ���������</a>, <a href=http://bastilia.ru/luna/final/money/ id=yphu title='"�������� �� ����"'>��������� �� ����</a>).</p>
  <h4>����� �������</h4>
  <p>
  <b>������� ���� ����� ������� � �� 1 �������</b>, ��� ������� � ����������� �� ���� � ������� ���������.
  ������� � 1 ������� �� ����� ��������� ������ ���������� ���� �������, �� ������� �����, ������ �����.
  <br>
  � �������� ���������� �� ������ ������� (�� 1 �������) ������������ � ���������� �������� � ���, ��� �� �������� ����� �� 16 �������, �� �� 500 ������ ������ (�.�., ��������������, <b>3000</b> �. � <b>3500</b> �.). <br>
  � �������� ��������� ������ �� ������� ����� ��� ����� ����� ���������� ������.</p>
<p>
� �����-���������� ����� ����� ����� <b>��������</b> ��� ������ ������� �������; � ������ ������ ��������� <b>�����</b>. (��. <a href="/masters/">�������� ���� ��������</a>)</p>

<a name="remote" id="remote"></a><h4>��� �������� ����� ��������</h4><p>�������� �����, ��&nbsp;���������� � 
���������, ����� ����������� ���������. ��������� ������ ��&nbsp;��� ����� 
���� ����������� �&nbsp;����������, ��&nbsp;����������� �������� ��� ��������� 
��������.<p><b>�����-����</b><p>������� ����� �������� �� 
����� &laquo;�����-�����&raquo;. �����: ������������, ���������� ��������. ������: 
���������� ��������� ��&nbsp;��������� &laquo;�����-�����&raquo; (�������, �&nbsp;���������� 
��, ��� �������, 32&nbsp;��, �&nbsp;������&nbsp;&mdash; 83).<p>������� ��������:<br />
<ol>
<li>��������
��� ����� �&nbsp;������� <a target="" href="http://alfabank.ru/atm/">&laquo;���� ���������� �&nbsp;������&raquo;</a></li>
<li>�������� ��� �������� &laquo;���������� 
����� ��&nbsp;������ �����&raquo;</li>
<li>��&nbsp;��������� ����������� ������ 
���������� ���������� (�&nbsp;�.�. �&nbsp;��������� �&nbsp;�������� �����)</li>
<li>� 
��������� ��������� &laquo;�������� ��������&raquo; �&nbsp;������� ����� ����� ����������
<b>40817.810.7.0401.0194529</b>. ���� �� ������� ���������, ��&nbsp;������ 
���������� &laquo;������� ����� ���������&raquo;</li>
<li>����� ����� �������� ����� 
���������, ����������, SMS �������� ��&nbsp;����� <nobr class="phone">+7(911)278-22-69 &mdash;</nobr> <i>�, 
�����-�� �����-��, ������ ��� ������� �������-��</i>. ��� �����, 
��-������, ��� ����, ����� ��&nbsp;������, ��&nbsp;���� ������ ������ ������; 
��-������, ����� ������� ����� ��������, ��� �� ��������� �����������.</li></ol>
<p>��,
��� ��&nbsp;�������� ������� �&nbsp;����� �����, ����� ����� ����� ������� ����� -
��������� ������ �&nbsp;����� &laquo;�����-�����&raquo;, ����������� �&nbsp;�������� 
����������. �����: ������������, ���������� ��������, ������������ 
����������. ������: ����� �������; ������ �&nbsp;&laquo;�����-�����&raquo; ������, ��� 
���������� (14&nbsp;�� �&nbsp;����������, 35&nbsp;&mdash; �&nbsp;������).<p>������� ��������:<br />
<ol>
<li>��������
��� ����� �&nbsp;������� <a target="" href="http://alfabank.ru/russia/">&laquo;���������� ���� �&nbsp;������&raquo;</a> <br /></li>
<li>�������� &laquo;��������� ��� ������� 
���&raquo;</li>
<li>��&nbsp;��������� ����������� ������ ���������� ������ (�&nbsp;�.�.&nbsp;�
��������� �&nbsp;�������� �����)</li>
<li>������� �&nbsp;��������� ���� <b>�� 
����� ���������</b>.</li>
<li>������� ����������, ��� ��� ���� ��������� 
������ ��&nbsp;���� <b>40817.810.7.0401.0194529</b>. ��� ����������: <b>�������
����� ���������</b>.</li>
<li>����� ����� �������� ����� ���������, 
����������, SMS �������� ��&nbsp;����� <nobr class="phone">+7(911)278-22-69 &mdash;</nobr> <i>�, �����-�� 
�����-��, ������ ��� ������� �������-��</i> ����� �����-����. ��� �����,
��-������, ��� ����, ����� ��&nbsp;������, ��&nbsp;���� ������ ������ ������; 
��-������, ����� ������� ����� ��������, ��� �� ��������� �����������.</li></ol>

<p>
<b>��������</b><p>�������
����� �������� ��&nbsp;����� &laquo;���������&raquo;. �����: ������� ����������� 
���������� ���������. ������: �&nbsp;������, ��&nbsp;����������� � 
������-��������� ������� (�����-���������, �������������, ����������, 
���������������, ���������, ������������ �������, ���������� �������), 
������� 1,5% ��������, �&nbsp;������ ����� ���� ��&nbsp;5&nbsp;����. �&nbsp;������-�������� 
������� �������� ��&nbsp;�������, ��&nbsp;������ ������ ������ ��&nbsp;��������� ����.<p>�������
��������:
<p>
<ol>
<li>������� ���������� ��� ���� &laquo;���������&raquo;. ��� 
������� ������� �&nbsp;������� <a target="" href="http://maps.yandex.ru/?text=%D1%81%D0%B1%D0%B5%D1%80%D0%B1%D0%B0%D0%BD%D0%BA">������-����</a>, ��� ����� ������ ������ 
&laquo;���������&raquo;. �������� ��������: ��� ����� ������ ��������� �����, �&nbsp;�� 
�������� ����� ��� ��������!</li>
<li style="color: rgb(0, 0, 0);">�������
�&nbsp;��������� ���� <b>��&nbsp;����� ���������</b>.</li>
<li>������� ����������,
��� ��� ���� ��������� ������ ��&nbsp;����� <b>4276 5500 1134 0919</b>. <font color="#000000">��� ����������: ������� ����� ���������</font><font color="#000000">.</font></li>
<li>����� ����� �������� ����� ���������, 
����������, SMS �������� ��&nbsp;����� <nobr class="phone">+7(911)278-22-69 &mdash;</nobr> <i>�, �����-�� 
�����-��, ������ ��� ������� �������-�� ����� ��������</i>. ��� ����� 
��� ����, ����� ��&nbsp;�������� ������������, ����� ����� ������ �&nbsp;��&nbsp;���� 
�������� ������.</li></ol>
<p>
<b>������.������</b><p>������� �� 
���� <a target="" href="http://money.yandex.ru/">������.�����</a>. �����: ������������, 
������������ �����������. ������: ���������� ������� �������� (�&nbsp;����� 
���������&nbsp;&mdash; ��&nbsp;����� 3,6% ��&nbsp;����� ������).<p>������ ���������� 
��������� ��&nbsp;���� <b>4100153363354</b>. ��� ����������� �&nbsp;���, ��� ��� 
�������, ���� ��&nbsp;<a target="" href="http://money.yandex.ru/">�������</a>.<p>��������� 
�������� ��&nbsp;����� ������.����� ����������&nbsp;3%, ������� ��������� ��&nbsp;���� 
����������:<br />
<ul>
<li>������ 2500&nbsp;�. &mdash;&nbsp;<b>2578</b></li>
<li>������ 3000&nbsp;�. &mdash;&nbsp;<b>3093</b></li>
<li>������
3500&nbsp;�. &mdash;&nbsp;<b>3609</b></li></ul>


<form style="margin: 0pt; padding: 0pt; margin-left: 10pt;" action="https://money.yandex.ru/charity.xml" method="post"><input name="to" value="4100153363354" type="hidden" /><input name="CompanyName" value="Battlestar Galactica: � ������� ����" type="hidden" /><input name="CompanyLink" value="http://bsg.bastilia.ru/" type="hidden" />
<p><table border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td>
<div style="background: url(&quot;https://img.yandex.net/i/li-uncolorer-rt.gif&quot;) no-repeat scroll right top rgb(255, 255, 255);">
<div style="background: url(&quot;https://img.yandex.net/i/li-uncolorer-rb.gif&quot;) no-repeat scroll right bottom transparent;">
<div style="background: url(&quot;https://img.yandex.net/i/li-uncolorer-lb.gif&quot;) no-repeat scroll left bottom transparent;">
<div style="background: url(&quot;https://img.yandex.net/i/li-uncolorer-lt.gif&quot;) no-repeat scroll left top transparent; margin-right: 10px; padding: 10px 0pt 0pt 10px;">
<table border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td>
<table border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td><input value="���������" style="margin-right: 5px;" type="submit" /></td>
<td><input id="CompanySum" name="CompanySum" value="2578" size="4" style="margin-right: 5px;" type="text" /></td>
<td style="font: 70% Verdana,Arial,Geneva CY,Sans-Serif;" valign="bottom" nowrap="nowrap"><strong>������ ������.��������</strong></td></tr></tbody></table></td>
<td rowspan="3" valign="bottom" width="90"><a href="http://money.yandex.ru/"><img src="https://img.yandex.net/i/ym-logo.gif" style="margin-left: 5px;" width="90" border="0" height="39" /></a></td></tr>
<tr>
<td style="font: 70% Verdana,Arial,Geneva CY,Sans-Serif;" nowrap="nowrap">��&nbsp;���� <span style="color: rgb(0, 102, 0); font-weight: bold;">4100153363354</span> (<a href="http://bsg.bastilia.ru/"><span style="color: rgb(102, 102, 102); text-decoration: underline;">Battlestar Galactica: �&nbsp;������� ����</span></a>)</td></tr>
<tr>
<td><img src="https://img.yandex.net/i/x.gif" width="1" height="10" /></td></tr></tbody></table></div></div></div></div></td></tr></tbody></table></form>


<form style="margin: 0pt; padding: 0pt; margin-left: 10pt;" action="https://money.yandex.ru/charity.xml" method="post"><input name="to" value="4100153363354" type="hidden" /><input name="CompanyName" value="Battlestar Galactica: � ������� ����" type="hidden" /><input name="CompanyLink" value="http://bsg.bastilia.ru/" type="hidden" />
<p><table border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td>
<div style="background: url(&quot;https://img.yandex.net/i/li-uncolorer-rt.gif&quot;) no-repeat scroll right top rgb(255, 255, 255);">
<div style="background: url(&quot;https://img.yandex.net/i/li-uncolorer-rb.gif&quot;) no-repeat scroll right bottom transparent;">
<div style="background: url(&quot;https://img.yandex.net/i/li-uncolorer-lb.gif&quot;) no-repeat scroll left bottom transparent;">
<div style="background: url(&quot;https://img.yandex.net/i/li-uncolorer-lt.gif&quot;) no-repeat scroll left top transparent; margin-right: 10px; padding: 10px 0pt 0pt 10px;">
<table border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td>
<table border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td><input value="���������" style="margin-right: 5px;" type="submit" /></td>
<td><input id="CompanySum" name="CompanySum" value="3093" size="4" style="margin-right: 5px;" type="text" /></td>
<td style="font: 70% Verdana,Arial,Geneva CY,Sans-Serif;" valign="bottom" nowrap="nowrap"><strong>������ ������.��������</strong></td></tr></tbody></table></td>
<td rowspan="3" valign="bottom" width="90"><a href="http://money.yandex.ru/"><img src="https://img.yandex.net/i/ym-logo.gif" style="margin-left: 5px;" width="90" border="0" height="39" /></a></td></tr>
<tr>
<td style="font: 70% Verdana,Arial,Geneva CY,Sans-Serif;" nowrap="nowrap">��&nbsp;���� <span style="color: rgb(0, 102, 0); font-weight: bold;">4100153363354</span> (<a href="http://bsg.bastilia.ru/"><span style="color: rgb(102, 102, 102); text-decoration: underline;">Battlestar Galactica: �&nbsp;������� ����</span></a>)</td></tr>
<tr>
<td><img src="https://img.yandex.net/i/x.gif" width="1" height="10" /></td></tr></tbody></table></div></div></div></div></td></tr></tbody></table></form>


<form style="margin: 0pt; padding: 0pt; margin-left: 10pt;" action="https://money.yandex.ru/charity.xml" method="post"><input name="to" value="4100153363354" type="hidden" /><input name="CompanyName" value="Battlestar Galactica: � ������� ����" type="hidden" /><input name="CompanyLink" value="http://bsg.bastilia.ru/" type="hidden" />
<p><table border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td>
<div style="background: url(&quot;https://img.yandex.net/i/li-uncolorer-rt.gif&quot;) no-repeat scroll right top rgb(255, 255, 255);">
<div style="background: url(&quot;https://img.yandex.net/i/li-uncolorer-rb.gif&quot;) no-repeat scroll right bottom transparent;">
<div style="background: url(&quot;https://img.yandex.net/i/li-uncolorer-lb.gif&quot;) no-repeat scroll left bottom transparent;">
<div style="background: url(&quot;https://img.yandex.net/i/li-uncolorer-lt.gif&quot;) no-repeat scroll left top transparent; margin-right: 10px; padding: 10px 0pt 0pt 10px;">
<table border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td>
<table border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td><input value="���������" style="margin-right: 5px;" type="submit" /></td>
<td><input id="CompanySum" name="CompanySum" value="3609" size="4" style="margin-right: 5px;" type="text" /></td>
<td style="font: 70% Verdana,Arial,Geneva CY,Sans-Serif;" valign="bottom" nowrap="nowrap"><strong>������ ������.��������</strong></td></tr></tbody></table></td>
<td rowspan="3" valign="bottom" width="90"><a href="http://money.yandex.ru/"><img src="https://img.yandex.net/i/ym-logo.gif" style="margin-left: 5px;" width="90" border="0" height="39" /></a></td></tr>
<tr>
<td style="font: 70% Verdana,Arial,Geneva CY,Sans-Serif;" nowrap="nowrap">��&nbsp;���� <span style="color: rgb(0, 102, 0); font-weight: bold;">4100153363354</span> (<a href="http://bsg.bastilia.ru/"><span style="color: rgb(102, 102, 102); text-decoration: underline;">Battlestar Galactica: �&nbsp;������� ����</span></a>)</td></tr>
<tr>
<td><img src="https://img.yandex.net/i/x.gif" width="1" height="10" /></td></tr></tbody></table></div></div></div></div></td></tr></tbody></table></form>

<p>������ �&nbsp;����, ��� �&nbsp;��� ������� ��� � 
�������� ��&nbsp;�������&nbsp;&mdash; 0,5% ��&nbsp;����� �������� (�.�. ��������� ��������, 
��������������, 2590,95, 3108,54 ��� 3627,14 ������-������).<p>��� 
��������� ���������� ����� ����� �������� ����� ���������, ����������, 
SMS �������� ��&nbsp;����� <nobr class="phone">+7(911)278-22-69 &mdash;</nobr> <i>�, �����-�� �����-��, ������
��� ������� �������-�� ������-��������</i>. <p><b>��� ����������� 
�������������</b><p>���� ������� &laquo;��������-����&raquo; ��� ��&nbsp;�����, 
��������������� ���������� (������ ������ �&nbsp;��������) ����� ���������� 
��� ������������ ����� ����� ��������. ��&nbsp;��&nbsp;������ ������ ��&nbsp;�� 
�������������� ���������� ��� ��������� ��&nbsp;���������.<p><span style="font-weight: bold;">���� ����������� ���������!</span><p><img src="http://bastilia.ru/images/article.gif" border="0" /><a target="" href="http://bsg.bastilia.ru/vznos.rtf">��� ��������� �&nbsp;RTF-�����</a>. ������������ �&nbsp;�������� �&nbsp;����� �&nbsp;����.
</div>
 </td>
<? right_block("bsg");?>
	     </tr>
<? show_footer(); ?>