<?
define("ROOT","../../../");
require_once ROOT . "funcs.php";
show_header("BSG :: ���������� :: ���������� ���������", BSG);
show_menu("inc/main.menu"); ?>

	    <td class="box">

<div class="boxheader"><a href="http://bsg.bastilia.ru/">Battlestar Galactica</a> :: <a href="http://bsg.bastilia.ru/appendix/">����������</a> :: ���������� ���������</div>
           
<ul>
<li>������ &#8470;&nbsp;5&nbsp;&mdash; &laquo;Galactica&raquo;.</li>
<li>������ &#8470;&nbsp;7, <nobr>1-�</nobr> ����&nbsp;&mdash; &laquo;Colonial One&raquo; �&nbsp;���� ���������� ������.</li>
<li>������ &#8470;&nbsp;7, <nobr>2-�</nobr> ����&nbsp;&mdash; &laquo;Cloud&nbsp;9&raquo;.
</li>
</ul>
<h3>
&laquo;Galactica&raquo;. <nobr>1-�</nobr> ����</h3>
<p>
������� �&nbsp;�����-������� ����� ��&nbsp;6&nbsp;������� �&nbsp;�������. �������&nbsp;&mdash; ��&nbsp;5&nbsp;�������. ��������� �&nbsp;���������&nbsp;&mdash; �&nbsp;��������� ��������.<p>
��&nbsp;������ �������� ������, ������ ��� ������� �&nbsp;�������� ����� ������������.<p>
��������&nbsp;&mdash; ����� ������� ��� (������� ��&nbsp;����� ���� �&nbsp;�������� �&nbsp;�������). �&nbsp;����� ����� ��� �������� ��&nbsp;����������� �������<p>
�������� ������ �&nbsp;&mdash; ��� ������ ��&nbsp;�������, �������, ������ (����� ����). �����&nbsp;�� ����������� ����� �����.<p>
��������� ������� ����� ������������ ��&nbsp;����� (������� �&nbsp;�������� ������������� ���������).
<p align="center"><img src="5-1.gif" width="700" height="419" />

<h3>
&laquo;Galactica&raquo;. <nobr>2-�</nobr> ���� </h3>
<p>
���� ������� �� &nbsp;<nobr>2-�</nobr> ����� �������� ��� ���� ���������� ������. ��� ����� �������� ��&nbsp;��������� ��������� ������������� ������ �&nbsp;����� ����������� ����� ��&nbsp;�����.
<p>
������� �&nbsp;�������� &laquo;������&raquo;&nbsp;&mdash; &laquo;������������������&raquo; ����� ������� (������ ���������� ���������).<p>
����������&nbsp;&mdash; ����� ������ ��������.
<p align="center"><img src="5-2.gif" width="700" height="419" />

<h3>
&laquo;Colonial One&raquo; </h3>
<p>
&laquo;Colonial One&raquo;&nbsp;&mdash; ����� ��������� �������. ��&nbsp;����� ����� �&nbsp;��� ���������� ���� ���������� ������ (������� ������ � ������ �&nbsp;����� ����������� ����� ��&nbsp;�����).
<p>
��������� ����� �&nbsp;��������� �������. ��� ���������&nbsp;&mdash; �&nbsp;��������.<p>
���������� ����� �&nbsp;����� ���������.
<p>
�&nbsp;����� ����� ���� ��&nbsp;���� (�����).<p>
����������� ����������, ��&nbsp;����� ������ ����������� ��&nbsp;&laquo;Colonial One&raquo;, ����� (������) ��&nbsp;&laquo;Cloud&nbsp;9&raquo;.
<p>
��������� ������� ����� ������������ ��&nbsp;����� (������� �&nbsp;�������� ������������� ���������). ��������� &laquo;Cloud&nbsp;9&raquo; ����� ���&nbsp;��. 
<p align="center"><img src="7-1.gif" width="700" height="419" />
<h4>
&laquo;Cloud&nbsp;9&raquo; </h4>
<p>
17&nbsp;���������� ��&nbsp;������� &laquo;Colonial One&raquo; (������������� �������, �������� ��������, �&nbsp;����� ��������� ������ �&nbsp;���� ������) ��&nbsp;����� ���� ����� ��&nbsp;&laquo;Cloud&nbsp;9&raquo;. ������ �&nbsp;�������, ������� �&nbsp;������� ����������� &laquo;Cloud&nbsp;9&raquo; ��� �������� 7&nbsp;����� ������&nbsp;&mdash; �&nbsp;������� ��&nbsp;5&nbsp;������� ��&nbsp;�������.<p>
�&nbsp;&laquo;���������&raquo; �&nbsp;&laquo;����������&raquo;&nbsp;&mdash; ��&nbsp;2&nbsp;������� ��&nbsp;�����������. ��� ��� �&nbsp;�����, �&nbsp;����� ����.<p>
���&nbsp;&mdash; ��� ��� �&nbsp;����� ������� ���, ������� ������� ��&nbsp;����� ���� �&nbsp;�������� �&nbsp;�������. <p>
����������� ���� ��� ������&nbsp;&mdash; ��� �������� ��������, ������� ��&nbsp;�����. ���������� ����� ���� ������ ��&nbsp;����, ��&nbsp;����� ����������� ����� �&nbsp;������� ����.</p>
<p align="center"><img src="7-2.gif" width="700" height="419" /></p></td>
<? right_block("bsg");?>
	     </tr>
<? show_footer(); ?>

