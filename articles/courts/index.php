<? 
define("ROOT","../../");
require_once("../../funcs.php");
show_header("�������� :: ��� ��� � ������� ����������� ���� �� �����",BEFREE);
show_menu("inc/main.menu"); ?>

	    <td class="box">

            <div class="boxheader"><a href="http://bastilia.ru/articles/">��������</a> :: ��� ��� � ������� ����������� ���� �� �����</div>
<p><em><a href="mailto:leo@bastilia.ru">������ ����� (���)</a></em></a>
              <h3>Complete Idiot�s Guide to Creating 
              Courts on the LARP games,
              ���
            ��� ��� � ������� ����������� ���� �� �����</h3>
<p class="epigraph">&laquo;�� ��� ���.<br>
                    ���� ��� ���.<br>
                    � �� �������� 12 ������� � ����� ��������.&raquo;<br />
                    ������ ����� ����� �������
                    </p>
<p class="epigraph">&laquo;�� ��� ���.<br>
                    ���� ��� ���.<br>
                    � �� ��������� � ��� ����������!&raquo;<br />
                    �� ���������
                    </p>
 <p class="syltext">���� ��&nbsp;����� �������� ��������, ���&nbsp;�� ��������� ��&nbsp;���� ���,
����� ��� �&nbsp;������, �&nbsp;������������, �&nbsp;��������, �&nbsp;�������, ��� ���
�������� ����� �&nbsp;���� ��� �&nbsp;���� �������. ��� ������ ������������,
��� ���� ����� ������ �&nbsp;����� �����, ��� ������� �������� �����-�����
������������� ��������� ��������������. ��&nbsp;�������� ��&nbsp;&laquo;��������&raquo;
�������� ����� ��� ������ &laquo;Planescape: Sigil&raquo;, �&nbsp;&laquo;������������ ������&raquo;,
&laquo;������������ ����&raquo; �&nbsp;&laquo;���������� �����&raquo; (���������� �&nbsp;��� ��&nbsp;&laquo;��&raquo;!)
&#151; �������� ����. ��&nbsp;��� ������� ������������� ���-�� ��&nbsp;��������������.
��&nbsp;���&nbsp;�� &laquo;������������ �������&raquo; ��� ���, ��&nbsp;��&nbsp;��� �������������
�&nbsp;������� ����� ������� ����. ���� �&nbsp;������ ������ ������ �&nbsp;���������,
��� ��� ����� ��������������� �������&nbsp;&#151; �.�. ��������� �����, �
������� ��&nbsp;���� �������� ��&nbsp;����.</p>
<p> ������ ������, ������� ������ ������&nbsp;&#151; �������� ������ �������.
�������� ��&nbsp;�&nbsp;������. �������� ������. ������? ��-������, ������
������������� ��������. ��-������, ����� ��&nbsp;������ ������, ��&nbsp;���������
���� �����. ���� ��������� ������� ������������ ��������� ��������
������������. ����, ������, ���������, �&nbsp;������ (��&nbsp;���� ���������)
��� ������, ����� ��� ������� ��&nbsp;��������. ������, ���� ���� �������
��������� ��&nbsp;�����������. �&nbsp;������, ����� ��� ������� ��&nbsp;���������
�������� (����������� ��������, �����&nbsp;&#151; �������������, �&nbsp;�.�.).</p>
<p>��� ����� ��������? ��-������, ��� ������ ���� ����� (��� ������),
��������������� ���������� �������. (&laquo;��&nbsp;��� ������&raquo;). ��-������,
&laquo;��� ������&raquo;, ��&nbsp;���� ������, ��������������� ��������-���������������
�&nbsp;����������-��������������� �������.</p>
<p>����� ����-�� ���� ��&nbsp;�������: ������������ �������. �&nbsp;�������
��� ���������. ��&nbsp;��&nbsp;�����������. ��&nbsp;����� �&nbsp;������� �������� ����������
(�&nbsp;��&nbsp;������&nbsp;&#151; �����&nbsp;&#151; �������) ��,&nbsp;�����, �&nbsp;�����, ��&nbsp;������ ������
��&nbsp;�������. �&nbsp;����&nbsp;�� ��&nbsp;����� ������. �&nbsp;������� ��������� ������,
������������ ���������� �������, ������� �&nbsp;����� ���� ������������
������� ��&nbsp;����� ���������� ����������������. �&nbsp;�������, ���� �
��&nbsp;��&nbsp;����� 42&nbsp;(&laquo;���� �&nbsp;������&raquo;) �������� �&nbsp;���� 3&nbsp;��������� (&laquo;����&raquo;,
&laquo;������&raquo; �&nbsp;&laquo;�������� �&nbsp;������������ ������&raquo;), �&nbsp;17&nbsp;������ ��&nbsp;5&nbsp;�������,
��&nbsp;��� ��&nbsp;���� ��� ���� ��&nbsp;�����. ���� ���� ��&nbsp;���� ������������
������ ����������� �������� ���������� 10%&nbsp;��� ��������-����� ��
����� ����� ��������� �������� ����� ��&nbsp;����, �������&nbsp;�� ���������
��&nbsp;���� �����. ������ ���������, ���� ��&nbsp;��������� �������� ��&nbsp;������
���������&nbsp;&#151; ��� �������� ������� ���������� ������ ������ �&nbsp;���������.</p>
<p>�����������, ��� �&nbsp;��� ���� ��&nbsp;����������. ����� ������� ����&nbsp;��
��������� ������ &laquo;�&nbsp;������������ ��������&raquo;, &laquo;��&nbsp;����������� ��������������
��������&raquo;, &laquo;�&nbsp;����� ��&nbsp;������� ������&raquo;, &laquo;�&nbsp;�����&raquo; (��������� �&nbsp;����������
������ �&nbsp;����� ������ ����������� ������� �&nbsp;���������, ��� ��������).
��&nbsp;���� ��&nbsp;���� ���� ��� ������� ���������, �����, ������������
���������� ���������, ������� ��������� �������, ����� ����������,
����� CHOAM �&nbsp;����� &laquo;��&nbsp;������� ��������&raquo;. ��&nbsp;���� ��&nbsp;������� ���������
����� ��&nbsp;�������� �&nbsp;����� ������ (�&nbsp;����&nbsp;�� �&nbsp;��� ���� �����������
��������������� �������� ������������� 19-� �������� �&nbsp;�����������
���). ������, ��&nbsp;������ ������������� ��&nbsp;�������� �������&nbsp;&#151; ����������
������������ �������������. ���� ��,&nbsp;��&nbsp;��� ���, ����������� ���
����� ����� ��� ��,&nbsp;����� ��������� �&nbsp;���, ��� ��&nbsp;�������� 250&nbsp;�������,
�&nbsp;�&nbsp;���� ����������� ����� 100&nbsp;������������� �������, �&nbsp;��� ����������
���� ��������������� ������������� ���� ���� ���� ��&nbsp;������.</p>
<p><strong>��������������.</strong> �&nbsp;����� ������ �������, ������
�&nbsp;�������� ������. ������� ������������ ��� ������� �������������.
������&nbsp;&#151; ��� <em>���������� �������������</em> �������. ������������
������ ���� ������� ���������. ��� ��������� ����� ������� �����
������� ������������� ���������. ������ ��� ��������� ���� ����������.
��!&nbsp;��������� ���������� ��&nbsp;�����������. ��������� ������� �����������
�&nbsp;1/2, ��������� �����������&nbsp;&#151; ����� �&nbsp;1/2, ��������������, ���������
���������� �&nbsp;&#151; 1/4&nbsp;�������������� �&nbsp;�.�. ������� ������������ �������,
�&nbsp;������� ��������� ��������� ����� ����������� �&nbsp;1/8 �&nbsp;���� 1/16
��������������. ����� �������, ��������� ���������&nbsp;&#151; ��� ��&nbsp;��������
�������������� ������. ������������ ����� ��������, ��� �&nbsp;���� �������
���������� ����������� ��&nbsp;������ ������������ ������������ ���������.
�&nbsp;�������, ���������� ������������� (�.�., ��&nbsp;������� 1860-� �����)
��������������� ������� &laquo;�������� �&nbsp;����������&raquo; �&nbsp;&laquo;�������� �&nbsp;�������
����������&raquo;.</p>
<p>������ �������&nbsp;&#151; ��� �.�. <em>�����-������������ ���������</em>.
���������� ��� �����-������������, ��������� ���������� �&nbsp;�����-����������
�������� ����� (�&nbsp;������� ���� ��������� ������, ���, ���������,
������&#133;), �&nbsp;���� ����� ��������� �&nbsp;��������������� �������� �������
(�������, ��������, ������ ������ ��,&nbsp;������&#133;). ������������� ���
���������� ������� ���: &laquo;1.&nbsp;�����, ��������� ����������, �&nbsp;�����
��������, �����������, ����������� ��������� �������������� ��&nbsp;������
����������� ���������, ����������� ��&nbsp;������������ ��������� �&nbsp;���������
���� �������������, �������������� ��� ���� ������� �&nbsp;��������.
2.&nbsp;������� �������������� ��&nbsp;����� ������� ������������� ����&raquo;.
(������ 17&nbsp;��� ��).</p>
<p><strong>���������������� ��� ���������������� ��������?</strong>
��������� <em>��������������</em> ������� ���������� ����������
������������ �������: ���� ������������ ������������� ��&nbsp;����������
������� ��������� (������, ������ ������� ��� ��� �������� �������������),
���������� ���������� ������ ������������ ��������������, �����������
��� ����� ������, ��� �������������� ������������� ��&nbsp;��������,
�&nbsp;���� ����� �������� �������� �&nbsp;����. ��������� <em>��������������</em>
������� ���������� ���, ��� �&nbsp;����� ���� ��������������� ����������
��&nbsp;���������, ������ �&nbsp;���������� ����; ���������� �������� �&nbsp;��������
���� �������� ��� ������������, ��� �������������� ���������� �����
��&nbsp;����������� ����������. ������, �������������� ������� ����������
��&nbsp;����� &laquo;����������&raquo;, ��� ���, ��� �&nbsp;�����. ��&nbsp;������ ������� �������
���������.</p>
<p>� ������ ��������������� (��� ����������) �������� ��������� ������
&#151; ���� ����������� ������� ���������? ������ ��� ����������������
����������� ��������������� �������&nbsp;&#151; ������������ (������� �����
������������ ������� ������ �������&nbsp;&#151; ��&nbsp;���� ������� ��&nbsp;�����������
��������� ����������� �&nbsp;������ ������, �&nbsp;��&nbsp;������ ���������, ����������
��������� �������). ��� ���������� ��� ��������������� �����. ������
�������&nbsp;&#151; ����� ����������� ����� ���� ����� ���������������� ����.
������ �&nbsp;���� ���� ��� ������� (��� ��������&nbsp;&#151; ���������� �������
�&nbsp;������ ��������� �&nbsp;XIX ����). ���� ��� ����, ��� ����� ���������
����������� ������� ��&nbsp;����� �������, �&nbsp;�������� ��� ����� ���������
(����� ���� ������������ ������&nbsp;&#151; ������, �����&nbsp;�� ������� ��&nbsp;�����������
����������� ��&nbsp;&laquo;������&raquo;) ��� ������������ �������� ������������������
��������������� ����������&nbsp;&#151; ���������. ������, ����� &laquo;attorney&raquo;
����� ������������ ��&nbsp;������� ���� ��� ��������, ��&nbsp;��� �������.
����������� �&nbsp;��� ���.</p>
<p>������ ������� ��������������� ��������? ��� ���������������� ������
���������� ����� ������� (�&nbsp;�������������� ��������) ��������������
������������� �����. �&nbsp;�������, ����� ���������� ��&nbsp;����� ��&nbsp;���������
(����� ��&nbsp;������������, �&nbsp;�������, �&nbsp;��������� ��������) ��� �����
����������� �������� ���������. ��� ����������, �&nbsp;�����, ����� ����������
������� ������� ���������������, �&nbsp;��� ���� ������ ��������� II
&#151; ������.</p>
<p>����&nbsp;�� ��������� �&nbsp;����� �������� ������ ��&nbsp;������� ����. �����������
��� ����� ���� ������ �������� ��������� ��� ����� ����� ����� ������������
���� �&nbsp;������ ������ ���������� ��&nbsp;��� �������. �&nbsp;����� �&nbsp;�����
����� �������������� ������������ ����, ��� ������ �������, �&nbsp;�������
������� ������������ ������������ ����: ����� ����� ����� ������������
��&nbsp;�������������� ������ �&nbsp;������ ������ ������������� �����������,
�&nbsp;��&nbsp;����������� ����� ������� ������ �&nbsp;2003&nbsp;����.</p>
<p>����� ������� ��������� ���� ��&nbsp;������ ������������� ���������
��&nbsp;���� ����������� ���. ����� ������� ������������ ��&nbsp;�����, ��������
��&nbsp;����� ��������� ��������. �&nbsp;���������, ����� ��&nbsp;��������� ������������
������ ��&nbsp;��������� ����������� �������� ������� ��&nbsp;&laquo;������&raquo; �����
��,&nbsp;��� ����� ���� ������ ����� ���� �&nbsp;������ (��������� ��� ��
�����). ��� &laquo;��&nbsp;������ ������&raquo; ��������� �������� ����������� ��������
�������, �&nbsp;��������� (��� ������� ���������) ����������� ������
&laquo;��&nbsp;���������� ����������&raquo;. ��������������, ��� ���� ����������
����� �������������, ������ ���� ��� ������, ��� ������, ��� ����
������� ����������� ��&nbsp;��������� ���� (�&nbsp;���� ��������).</p>
<p>������ ������� ���������� ��������� ���� &laquo;������-1925&raquo;&nbsp;&#151; �������
�������������� ���� ��&nbsp;���� ��������� �������. ��� ����� ���� ����������,
���� ������� �������������� ��������� ���������� �������� ��� �����.
��&nbsp;�&nbsp;���� ������ ������� ����������, ����� �&nbsp;��������� &laquo;����� ����������
��������&raquo; ��&nbsp;���� �������� ��&nbsp;��������� �������������� ���, ��&nbsp;���
���, ���������. ��������, �&nbsp;���� ������ ������� ����� ��&nbsp;����, �����
����������� ��� �������������&nbsp;�� ������� ������� ������������ ����
�&nbsp;��������� ������ ������ �&nbsp;���������&nbsp;�� ���� ��&nbsp;����� ������������
�&nbsp;���&nbsp;�� ���.</p>
<p>������������ �������������� ������� ����� ����� ����� ������ ���
������� ���, �&nbsp;������� �������������� ������������� ���������� �������,
������ ��&nbsp;������� ����� ����������� ����� 50-100&nbsp;�������. �&nbsp;����
������ ������� ������������ ����� �������� (������� �����&nbsp;&#151; ���������
�����). ������ �&nbsp;���� ������ ��������� �������� �&nbsp;��������� �������
�����&nbsp;&#151; ������ ��� ����, �&nbsp;������� ����� ����� �������. �&nbsp;���� ������
����� ����� ����������� ���� �������. ��������� �&nbsp;����� �������
���� ������. ��&nbsp;������ ����������� ������������������ ����� ��&nbsp;�����,
�&nbsp;�������, ������� �����, ��� ��� ����� ���� ���������� ��� �����������
��������. ��&nbsp;��������� ������ ���� �&nbsp;���������� ��� ��������� ��������
�����. ��&nbsp;������ ���������� ��&nbsp;���� ������������� �&nbsp;���������� �����������.
���������� ���� (��������, �&nbsp;�������� ������� ���������) ����� ������������
���������������� �����, ��������� ��&nbsp;������� ��&nbsp;��������� ������
����. ��&nbsp;����&nbsp;�� ������ ��&nbsp;�������� ������ ��&nbsp;������� ��&nbsp;������
�����. ���� ���������� ���������������� ����� ����� ����� ����,
�������� �������� ���������� ����, ������� ����� ������������� ������
��&nbsp;������� ����� ������ (��� ��&nbsp;�������).</p>
<p>� ������ ������������� ����������� ���� ���� ��������� ����������
��������� ������� (�&nbsp;���� ������� ��� ����������� ��&nbsp;���) �&nbsp;�����������.
�����&nbsp;�� ������ ������������� ������ ��&nbsp;������� �������? ������
���, ���� ������ ��������� �&nbsp;�������� ����� ����������. �&nbsp;XII ����
����������� ���� ������ ������������ ���� ����������� ����������
������� ����� ��������� �&nbsp;������� ��������������. &laquo;�������� �������
��� ��&nbsp;���������� ������ ������&raquo;.</p>
<p><strong>���������� ������������.</strong> ����&nbsp;�� ��� �&nbsp;����� ����?
�&nbsp;����������� ����� ��,&nbsp;��&nbsp;�&nbsp;���� ��&nbsp;30-� ����� ��� ��&nbsp;����������
&#151; ���.</p>
<p><strong>������&nbsp;�� ���������� ������� ��������� ������������� ��
��&nbsp;����������������?</strong> �&nbsp;������������ ���������� ��������,
��� ��&nbsp;�����������, ��&nbsp;������. ��&nbsp;��� ��� �&nbsp;���������� ������� ����������������
��������&nbsp;&#151; ���������� �������������� ���������������.</p>
<h4>��������, ������� ��������� ��&nbsp;����� �&nbsp;�����������</h4>
<p>��� �&nbsp;������� ��������� �&nbsp;���������� ����&nbsp;&#151; &laquo;�����&raquo;. ������������
��������������� ��������&nbsp;&#151; ��� ������� ��� ���������? ������� �����
������� ��&nbsp;&laquo;������&raquo; ���� ���� ���������. ��&nbsp;��&nbsp;��������������� �������
������ ��� ���-�� ��������: ������ ������ ��&nbsp;������ �&nbsp;����������
���� ������������. ������������������ ���-�� 2/3&nbsp;��������&nbsp;&#151; ���
����� (������). ����� ��&nbsp;����������� ��&nbsp;���� ����������� ���� �������.
���������, ����������, ������������� ��������������, ��&nbsp;��� ����
������ �������������.</p>
<p>�������, ��� ����������� ������ �&nbsp;������������� ���� ����&nbsp;&#151; �����
������� �������. ���� �&nbsp;��� ��&nbsp;����� �������&nbsp;&#151; ��&nbsp;����� �&nbsp;������������
����������. ��&nbsp;������ (����� ��&nbsp;��� ������!), ������� �������� ������
������� �������: 1)&nbsp;��� ���� �������������; �&nbsp;2)&nbsp;��� ��������� ����������
����������. �������, ��� ��� ����� ����������� �������. ������ ����������
��������� ������ ������� ��� ������������ �-�� ���������� ��&nbsp;��������
�������� ����� ��&nbsp;���� �����, ������� ���� �������� ��&nbsp;���� ������������,
������ ������, ������ �������������� �&nbsp;������ ���� �&nbsp;���� �������������
��������. ��� ��� ��������� ���� ��&nbsp;������������, �-� ����. ��&nbsp;���������
�������������� ���� ��&nbsp;���� ��&nbsp;���� ��&nbsp;�������������, ����������
��� ������ ������� �������� ��&nbsp;��������. ��� ��������� ���� ����
�����, ������� ������� ������� ���������&nbsp;&#151; ���������� ������, ������,
�&nbsp;�.�.</p>
<p>�������� �������, ��&nbsp;������ ������, ��� �������. �������� �����
��������� �&nbsp;������. ������� �&nbsp;������ ������� ���� ��������� �������
�����. ��&nbsp;������� ����� ��������� ����� �������������� ����� ������������
��&nbsp;������.</p>
<p>����� �������� �������� �&nbsp;����������� ��������������� ��������
�&nbsp;��� ����������� &laquo;�������&raquo; (���� ��� ����� �&nbsp;������� ��-�� ������������
������). �&nbsp;�������, ������� ������, ��&nbsp;������� ������, ���� ����
��&nbsp;���������� ����������. ��������� ������� �������� (����� ��&nbsp;������
�������� ������, ��������� �������!), ����� �������� ������ ���
��������. �&nbsp;������ ����� �&nbsp;�&nbsp;�������� ���� �����: &laquo;��������!&raquo;. ������
������ ����� &laquo;��������!&raquo; ��&nbsp;�-�� ������.</p>
<p>������� ������, ��� ����������� ����� ��&nbsp;���� ������ ���������:
������ �&nbsp;�&nbsp;��������� ����� ���������� ������� ��&nbsp;�����. �������,
��� ������ ������ ��&nbsp;�����, ��&nbsp;��������� ��&nbsp;������� ����, ��������.
��&nbsp;����� ������ ���� ������ ��������: ����� ������, ������� �����
�,&nbsp;������� ������ �������, �������� ������, ����� �&nbsp;������ (���
���������� ������ ������������� ��&nbsp;������) �&nbsp;�������� �������. �������
����� �������� ������� �����, ��&nbsp;������ ��������, ��� �����������
&laquo;����� �����&raquo;.</p>


            </td>
<?php right_block('archive'); show_footer(); ?>

