<?
define("ROOT","../../../");
require_once ROOT . 'funcs.php';
show_header("��������: ������� ��������� :: ���������� ���� ������� :: ����� � ����",ALASKA);
show_menu("inc/main.menu"); ?>

<td class="box">

            <div class="boxheader"><a href="http://bastilia.ru/alaska/">��������: ������� ���������</a> :: <a href="../">���������� ���� �������</a> :: ����� � ����</div>
    <p>����&nbsp;&mdash; ������� ������ �&nbsp;������ <br />
<img src="01_alaska_-_yukon_boundary.jpg" width="600" height="514" /></p>
<p>���������<br />
<img src="02_alaskan_men.jpg" width="600" height="368" /></p>
<p>����� ���������<br />
<img src="03_alaskan_miners.jpg" width="600" height="373" /></p>
<p>����������� ���������<br />
<img src="04_alaskan_roadhouse.jpg" width="600" height="495" /></p>
<p>�������� ����� ��&nbsp;�����<br />
<img src="05_animal_tracks_in_the_snow.jpg" width="600" height="789" /></p>
<p>����� ��&nbsp;�������� �������� . �&nbsp;�������� ������<br />
<img src="06_awaiting_the_finish_at_the_sixth_all_alaska_sweepstakes.jpg" width="600" height="521" /></p>
<p>��� �&nbsp;������ �������� ������<br />
<img src="07_bird's_eye_view_of_nome_with_snow.jpg" width="600" height="464" /></p>
<p>������ ������������� ��&nbsp;������<br />
<img src="08_camp_of_alaskan_explorer.jpg" width="600" height="854" /></p>
<p>��������� ������ ������� (��&nbsp;��������� ��� ���������� ��� �������)<br />
<img src="09_canadian_mounted_police.jpg" width="584" height="447" /></p>
<p>���� �&nbsp;����������� ������� ������<br />
<img src="10_children_in_eskimo_fur_clothing.jpg" width="520" height="804" /></p>
<p>����������� ��&nbsp;������� ���-�����<br />
<img src="11_climbing_mt._mckinley.jpg" width="600" height="933" /></p>
<p>������������� �������� �������� �������� ����� �&nbsp;�������� ������ <br />
<img src="12_company_trader_buying_white_fox_furs_and_other_skins.jpg" width="600" height="871" /></p>
<p>������������� ������� ��&nbsp;���� ������ (������ ������ �����)&nbsp;&mdash; ����� �������� ��&nbsp;������<br />
<img src="13_episcopal_station_on_the_koyukuk_river,_the_farthest_north_mission_in_alaska.jpg" width="600" height="428" /></p>
<p>����������� ������� ���������<br />
<img src="14_fairbanks_general_store.jpg" width="600" height="901" /></p>
<p>������� �&nbsp;�����<br />
<img src="15_girl_in_a_parka.jpg" width="600" height="981" /></p>
<p>���������� ������ �&nbsp;�������� ������<br />
<img src="16_governor_george_parks_in_camp.jpg" width="600" height="320" /></p>
<p>����������� ��������� &laquo;Haly&rsquo;s&raquo;, ����<br />
<img src="17_haly's_roadhouse_-_yukon_river,_alaska.jpg" width="600" height="362" /></p>
<p>��������������� ��������������<br />
<img src="18_hauling_lumber_by_dog_sleds.jpg" width="600" height="383" /></p>
<p>������� �&nbsp;�������, �������� ��&nbsp;����<br />
<img src="19_hunter_with_bear_which_he_killed.jpg" width="600" height="386" /></p>
<p>������ �������� ������<br />
<img src="20_interior_alaska_costume.jpg" width="600" height="965" /></p>
<p>������� ������ �&nbsp;���������<br />
<img src="21_man_in_front_of_anchorage_clothing_store.jpg" width="600" height="375" /></p>
<p>���� ���-�����<br />
<img src="22_mt._mckinley.jpg" width="600" height="737" /></p>
<p>��� ���� ����������� ���������<br />
<img src="23_old_woman_road_house.jpg" width="600" height="361" /></p>
<p>�����, ������, ������� ��&nbsp;���� ���-����� ������<br />
<img src="24_prince,_an_alaskan_dog,_carrying_utensils_on_his_back.jpg" width="600" height="365" /></p>
<p>�������������� �&nbsp;��� ������ ������ �&nbsp;������� ������ <br />
<img src="25_prospector_and_dog_ready_for_the_summer_trail.jpg" width="600" height="965" /></p>
<p>�������������� �&nbsp;��� ������ ������ �&nbsp;������� ������ <br />
<img src="26_prospector_and_pack_dog_ready_for_the_summer_trail.jpg" width="600" height="965" /></p>
<p>������� �������� �������� �������<br />
<img src="27_prospector_with_dog_team.jpg" width="600" height="420" /></p>
<p>�������� �����<br />
<img src="28_reindeer.jpg" width="600" height="359" /></p>
<p>�������� ����� ��&nbsp;������ �������<br />
<img src="29_reindeer_on_street_dawson,_yukon_territory,_canada.jpg" width="600" height="424" /></p>
<p>������ �������<br />
<img src="30_reindeer_sleds.jpg" width="600" height="399" /></p>
<p>����������� ��������� ��&nbsp;���� ��&nbsp;��������<br />
<img src="31_road_house_on_valdez_trail.jpg" width="600" height="446" /></p>
<p>�������� �����<br />
<img src="32_skins_of_bears_killed_on_show_river_i.e._snow_river_on_line_of_a.c.r.r..jpg" width="600" height="452" /></p>
<p>��������� ������������������ �����<br />
<img src="33_taking_a_government_load_to_copper_center.jpg" width="600" height="428" /></p>
<p>�������<br />
<img src="34_tent_in_alaska.jpg" width="600" height="372" /></p>
<p>��������� ���������<br />
<img src="35_the_alaska_citizen_building.jpg" width="600" height="404" /></p>
<p>����������� ��&nbsp;������� �������<br />
<img src="36_traveling_with_reindeer.jpg" width="600" height="375" /></p>
<p>����������� ���<br />
<img src="37_view_of_nome_with_snow_on_ground.jpg" width="600" height="381" /></p>
<p>���� ��&nbsp;&laquo;������&raquo; �������� &laquo;Wells Fargo&raquo;<br />
<img src="38_wells_fargo_express_company.jpg" width="600" height="378" /></p>
<p>���� ��������<br />
<img src="39_wrangell_alaska.jpg" width="600" height="444" /></p>
<p>������<br />
<img src="40_yukon_gold_company_dawson,_1914.jpg" width="600" height="460" /></p>

</td>
	    <td width="150" class="side">

<? show_menu("inc/alaska.menu");?>
	     </tr>
<? show_footer(); ?>

