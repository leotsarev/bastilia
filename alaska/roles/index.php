<?
define("ROOT","../../");
require_once("../../funcs.php");
show_header("��������: ������� ��������� :: ������ ����������",ALASKA);
show_menu("inc/main.menu"); ?><td class="box">

            <div class="boxheader"><a href="http://bastilia.ru/alaska/">��������: ������� ���������</a> :: ������ ���������� </div>
            <p>���������� ������ ������������� ������ ��&nbsp;����, ��������� �&nbsp;������������� �������. ������� ��&nbsp;������ ��� �������� �������� �������� ���������� ���������� ��&nbsp;500&nbsp;���� ������, ������������� �&nbsp;<a href="../source/">&laquo;��������&raquo; ��������� ����� �������</a>. ������-�� ���� ���������� ���-�� ��&nbsp;30-35&nbsp;�������, ��&nbsp;���� ������������ �&nbsp;������� &mdash; ����� ����, ��&nbsp;���� ��������.</p>
<h4>��� ������ ������ </h4>
<p>������ ������ ��&nbsp;���� ���� �������������� <strong>������</strong> ����� ����������� ���� ������ <a href="http://rpgdb.ru/">rpgdb.ru</a>. ��� ��&nbsp;������. ��� ������ ����� <a href="http://rpgdb.ru/auth/users.php?new">������� ��� ���� �������</a> &mdash; �.�. ������ �&nbsp;����, ���������� ���� ���������� ����������. ����� �&nbsp;����, ����� <a href="http://rpgdb.ru/addcharacter.php?gid=19">�������� ������ ��&nbsp;������������ ��� ����</a> (�&nbsp;������ ������ ��� &laquo;��������: ������� ���������&raquo;). ���� ��&nbsp;��� ���������������� ��&nbsp;<a href="http://rpgdb.ru/">rpgdb.ru</a> (��������, ��� ������ ������ ��&nbsp;<a href="http://www.comcon.su/comcon">������</a>, ��&nbsp;���� ������� <a href="http://bastilia.ru/hellas/">&laquo;���� ���������&raquo;</a> �&nbsp;<a href="http://bastilia.ru/hobbit/">&laquo;������&raquo;</a>, ��&nbsp;������ ����), �������� ��� ������ ��&nbsp;����! ������ ������������ �&nbsp;<a href="http://rpgdb.ru/addcharacter.php?gid=19">��������� ������ ��&nbsp;&laquo;��������&raquo;</a>.</p>
<p>�������� ��������, ��� ��������� ���� ������ (&laquo;������&raquo;, &laquo;����&raquo;, &laquo;������&raquo; �&nbsp;��.) ��� ����������� ���������� ��� �������������� (�.�. ��������) &mdash; ������� ��&nbsp;������ ����������. ������������� ��� ��� ��������� ���� ��&nbsp;�������������� �&nbsp;������ ���������� ���� ����� (�&nbsp;�������, ������� ����� ����� ����� &mdash; ������ ��&nbsp;������������, ��� �������� ��� ����). ���� �����-�� �������� ��&nbsp;������� ���������� ��&nbsp;����������� ������ ��� ��������� ��������� &mdash; ��� ������� ��&nbsp;����� ����� ��������. ���� �&nbsp;���-�� ��&nbsp;��� ��&nbsp;������������ &mdash; ������� �&nbsp;��������������� ������ &laquo;��&nbsp;����&raquo;. ���� ������ ������ ����� ��������� �&nbsp;�������.</p>
<p>����� ����� ��������� ���������� (�������� ������ �������� �&nbsp;��������, ������������ ����� ��&nbsp;���������� ���� �&nbsp;�.�.), ������� ����������� ������ ��������� ������ ��������� �������� �&nbsp;����������� �������.</p>
<h4>���������</h4>
<p>����, ��� ��������� ������� ��&nbsp;4&nbsp;�������� ������:</p>
<ul>
<li><a href="#dawson">������</a></li>
<li><a href="#chechako">������. �������</a> (<strong>NB:</strong>&nbsp;��� ������� ��&nbsp;��������� ���������� �&nbsp;�������!) </li>
<li><a href="#forty_mile">��������� ����</a> (������ ������� ����������) </li>
<li><a href="#styx">����� �������</a> </li>
</ul>
<p><strong>������</strong> ������� �������� ������������ ���� (���� ������ �&nbsp;�&nbsp;�� ����� ������ &laquo;����������&raquo; ��� &laquo;�������������� �������&raquo;). <em>������</em> &mdash; ������ ���, ��&nbsp;���� ��������������� �������������� �&nbsp;��������� (���� �&nbsp;��������� ����� ������ ��&nbsp;����� &mdash; ���� �����������). ���� ������� &mdash; ������ ����, ��&nbsp;��� ��� �����������.
<p><table border="1" cellpadding="2" cellspacing="2">
<tr>
  <td><div align="center"><strong>�</strong></div></td>
<td><div align="center"><strong>��������</strong></div></td>
<td><div align="center"><strong>������� ��������</strong></div></td>
<td><div align="center"><strong>��������, �&nbsp;������� �����������</strong></div></td>
<td><div align="center"><strong>�����</strong></div></td>
</tr>
<tr>
<td colspan="5"><div align="left"><strong><a name="dawson" id="dawson"></a>������</strong></div></td>
</tr>
<tr>
  <td>1.</td>
<td><div align="left">������� ����������</div></td>
<td><div align="left">�������� ��&nbsp;����������� ������ �������. ����������� ������, �������� ��&nbsp;���� ���������.</div></td>
<td><div align="left">&laquo;���� ��������&raquo;, &laquo;���� ������&raquo;</div></td>
<td><div align="left"><strong>���</strong></div></td>
</tr>
<tr>
  <td>2.</td>
<td><div align="left">������� ���� �������</div></td>
<td><div align="left">���������� ��������. ������ �&nbsp;���� ���������� �������������� ������ (������������� �������).</div></td>
<td><div align="left">&laquo;���� ������&raquo;</div></td>
<td><div align="left"><strong>������ ������� </strong></div></td>
</tr>
<tr>
  <td>3.</td>
<td><div align="left">������ ���������</div></td>
<td><div align="left">���� ����������� �������. ������ ������ �������� �&nbsp;������ �������� �������. �&nbsp;����� ���� ����������� ��������������� �&nbsp;�������������� ������. ������� �&nbsp;�������� �����, ���������, ������. ��������. ����� ���� �������������. ������ ����� ������������ �������. ��������� ������ ��� �&nbsp;�����, ������ ������ ��&nbsp;����� ����� ������. ������� ������ �������� �&nbsp;����� �&nbsp;��&nbsp;������� �������.</div></td>
<td><div align="left">&laquo;���� ������&raquo;, &laquo;�������� ������� �����&raquo;, &laquo;������� ���������&raquo;</div></td>
<td><div align="left"><strong>���� (����� �������) </strong></div></td>
</tr>
<tr>
  <td>4.</td>
<td><div align="left">�����</div></td>
<td><div align="left">��������� �������. ������ ���������� ������������� (��������� ����� �&nbsp;������ ������). ������������ �&nbsp;��������� �������. ������� ��&nbsp;������� ���������� (����� ����������� ���������).</div></td>
<td><div align="left">&laquo;��� �������&raquo;. �&nbsp;����� ��.&nbsp;&laquo;���, ��� ��������� ������&raquo; �&nbsp;&laquo;������� ���������&raquo; (������)</div></td>
<td><div align="left"><strong>��� (���� �������)</strong></div></td>
</tr>
<tr>
  <td>5.</td>
<td><div align="left">���� �������</div></td>
<td><div align="left">���� ���������, ������. ���� ���� ������� �����, ���� &mdash; ����� ���� ���� ��� ������ ���������. �������, ����� ��������� �����������. �������� ������� ����������. �������� ������� ����� �&nbsp;������ ����������. ��������� ������.</div></td>
<td><div align="left">&laquo;���� ������&raquo;. �&nbsp;����� ��.&nbsp;&laquo;���� ��������� ������&raquo; (���� ������)</div></td>
<td><div align="left"><strong>��������</strong></div></td>
</tr>
<tr>
  <td>6.</td>
<td><div align="left">����� ������</div></td>
<td><div align="left">�����-�������. ���������. �� ��������� ������������ �� ���������� ��� �����. ������ �� �� ���������� � �� �������� �����! ����� ���� ����� � ���� ��� ��������, ����� ������� � ���������-����������, �������� ������ ����� ������ �� ������ �����, ��� ���������� �� ��������...<br />
</div></td>
<td><div align="left">&laquo;�������� �������&raquo;, &laquo;��&nbsp;����� ����������&raquo;, &laquo;��&nbsp;���, ��� �&nbsp;����!&raquo;, &laquo;������� ���������&raquo;</div></td>
<td><div align="left"><strong>������</strong></div></td>
</tr>
<tr>
  <td>7.</td>
<td><div align="left">��� �����</div></td>
<td><div align="left">������� ������� ��&nbsp;������. ���������������� ���������.</div></td>
<td><div align="left">&laquo;��&nbsp;���, ��� �&nbsp;����!&raquo;, &laquo;���� ������&raquo;, &laquo;���� ��������� ������&raquo;, &laquo;�������� �������&raquo;, &laquo;�����-��-����&raquo;. �&nbsp;�����: &laquo;�&nbsp;������� ����&raquo; (���-������), &laquo;���� ������&raquo; (���� ��-�����)</div></td>
<td><div align="left"><strong> �������</strong></div></td>
</tr>
<tr>
  <td>8.</td>
<td><div align="left">��������</div></td>
<td><div align="left">������ ��������. ������������ �&nbsp;�����������.</div></td>
<td><div align="left">&laquo;���� ������&raquo;. �&nbsp;����� ��.&nbsp;&laquo;Finis&raquo; (����������)</div></td>
<td><div align="left"><strong>�������</strong></div></td>
</tr>
<tr>
  <td>9.</td>
<td><div align="left">���� ��������� ��&nbsp;�������� &laquo;������� ����&raquo;</div></td>
<td><div align="left">���������. ��������� ����. ��������, ������ ��� ������ ������� ��&nbsp;�����. ������ �������� ��������� ������ �&nbsp;�������� ���������.</div></td>
<td><div align="left">&laquo;���� ������&raquo;</div></td>
<td><div align="left"><strong>���</strong></div></td>
</tr>
<tr>
  <td>10.</td>
<td><div align="left">����� �������</div></td>
<td><div align="left">�������� ������. ������� ���� ����� ��������. ����. ������������ ������.</div></td>
<td><div align="left">&laquo;���� ������&raquo;, &laquo;�����-��-����&raquo;</div></td>
<td><div align="left"><strong>�����</strong></div></td>
</tr>
<tr>
<td colspan="5"><div align="left"><strong><a name="chechako" id="chechako"></a>������. ������� </strong></div></td>
</tr>
<tr>
  <td>11.</td>
<td><div align="left">���� ������</div></td>
<td><div align="left">������� ������� 27&nbsp;���. ��&nbsp;����, ��� ����� ��&nbsp;������ �������� �&nbsp;������ ���-����������� ������, ��� &laquo;��������� ����������&raquo;, ����� �����, �����, �������. ��&nbsp;�������� ������ ��&nbsp;����������� ������, ��&nbsp;��� ����� �&nbsp;������� &mdash; �����, ��� ��� ��� ���������. ������������ ������������ ���������� �&nbsp;���� �&nbsp;���� ��������� �������. ����� �&nbsp;�������, ��� ���, ��� ��&nbsp;��� �������� ������ �������, ������ ����� ��������� �&nbsp;���. ����� ������� ��&nbsp;���� ������ ������, ��� ��� �������. �������, ��&nbsp;����� ����������� �&nbsp;��������� �&nbsp;����������������.</div></td>
<td><div align="left">&laquo;���� ������&raquo;</div></td>
<td><div align="left"><strong>�����</strong></div></td>
</tr>
<tr>
  <td>12.</td>
<td>���� ����� </td>
<td><div align="left">��&nbsp;�� ������-��-���-����. ��������� ����������� ���������. ���������� ���������. ��������� ����� ������. <br />
</div></td>
<td>&laquo;���� ������&raquo;</td>
<td><strong>�������</strong></td>
</tr>
<tr>
  <td>13.</td>
<td><div align="left">��� ���-����</div></td>
<td><div align="left">��������� ��������. �����������, ��� �&nbsp;��� ������. ������ ����������.</div></td>
<td><div align="left">&laquo;���� ��������� ������&raquo;, &laquo;�������� �������&raquo;, &laquo;������� ������&raquo;, &laquo;��&nbsp;��������� ����&raquo;</div></td>
<td><div align="left"><strong>������</strong></div></td>
</tr>
<tr>
  <td>14.</td>
<td><div align="left">����� ���-����</div></td>
<td><div align="left">���� ���� ���-�����. ��&nbsp;������ ������ ����� ��&nbsp;�������������� �������. ������ ��������� �����������, ��� ��&nbsp;����� ���� ��� ��&nbsp;���-�� ������.</div></td>
<td><div align="left">&laquo;��&nbsp;����� ����������&raquo; (����� ������)</div></td>
<td><div align="left"><strong>����</strong></div></td>
</tr>
<tr>
  <td>15.</td>
<td><div align="left">������� ����-�������</div></td>
<td><div align="left">������ �&nbsp;���������. �������������� �&nbsp;�������������. ������� ���������� ���������������� �����, ������� ��������� ��� ����� �����. ����� ������������ �&nbsp;����� ������������, ������� ���������� ������� �&nbsp;������.</div></td>
<td><div align="left">&laquo;���� ������&raquo;</div></td>
<td><div align="left"><strong>���� (������ ������)</strong></div></td>
</tr>
<tr>
  <td>16.</td>
<td><div align="left">���� �������� ��&nbsp;�������� &laquo;��� �����&raquo;</div></td>
<td><div align="left">������, �������� ������������, ���������� ��&nbsp;������� �����, ������� �������� ��&nbsp;���. ������ ��&nbsp;��������, ��&nbsp;���� ��&nbsp;�����, ��&nbsp;����������, ����� ������ 300.000&nbsp;�������� �&nbsp;�������� �����, ���������� ������������� ���.</div></td>
<td><div align="left">&laquo;��� ��������� �&nbsp;�������&raquo;</div></td>
<td><div align="left"><strong>�����</strong></div></td>
</tr>
<tr>
  <td>17.</td>
<td><div align="left">����� ���������</div></td>
<td><div align="left">�������������� �������. ���������� ��&nbsp;��������, ����� ������������ ��&nbsp;���������������. ���� ���������� ����� ���� ��&nbsp;����������� ��&nbsp;����� ���.</div></td>
<td><div align="left">&laquo;������ �����&raquo;. �&nbsp;����� ��.&nbsp;&laquo;���� ������&raquo; (���� �����)</div></td>
<td><div align="left"><strong>������</strong></div></td>
</tr>
<tr>
  <td>18.</td>
<td><div align="left">����� ������</div></td>
<td><div align="left">������, �������, ������, ���� �&nbsp;�������������. ������� �������, �������� ��&nbsp;��������. ������ ������� �&nbsp;����������: ��� ����� ������� ������ �&nbsp;���� �����?</div></td>
<td><div align="left">&laquo;������� �������&raquo;</div></td>
<td><div align="left"><strong>������</strong></div></td>
</tr>
<tr>
  <td>19.</td>
<td><div align="left">����� ������</div></td>
<td><div align="left">����� ���� �&nbsp;������, ���-���������. �������������� ��������. ����� ������������� �����������, �����-�� �������� ��������� �&nbsp;������������ �������. ������ ��� ����������� ��� ��������� ������������. ��� ��� ��&nbsp;��� ������ �&nbsp;��&nbsp;�������� ���� ����������. �&nbsp;���� ����� ��� ������ ������ ������, �&nbsp;������ �������� ��&nbsp;�����, ����� ��������� ���� ����������� ����.</div></td>
<td><div align="left">&laquo;������� ���������&raquo;</div></td>
<td><div align="left"><strong>�����</strong></div></td>
</tr>
<tr>
  <td>20.</td>
<td><div align="left">������ �����</div></td>
<td><div align="left">������� ������ �������. ����������. ������� ��&nbsp;�������� ��������� ���������, ������� ��� ����� ������� �&nbsp;���������� ��������� �������� ������.</div></td>
<td><div align="left">&laquo;�������� �������&raquo;, &laquo;������� ���������&raquo;, &laquo;�������� �������&raquo;, &laquo;��&nbsp;���, ��� �&nbsp;����!&raquo;, &laquo;���� ������&raquo;. �&nbsp;����� ��.&nbsp;&laquo;���� ������&raquo; (���� �������), &laquo;���� ������&raquo; (����� ������ �����)</div></td>
<td><div align="left"><strong>������</strong></div></td>
</tr>
<tr>
  <td>21.</td>
<td><div align="left">��������� ����</div></td>
<td><div align="left">������ ������ �������, ������� ����������� ����, ������� ������������ ����� ������������ ��������. ������������������ ����������, ������������� �������� ��������� ���� ��������������. ����� �&nbsp;������ ������ �������. ���������� ������.</div></td>
<td><div align="left">&laquo;���� ������&raquo;. �&nbsp;����� ��.&nbsp;&laquo;���� ������&raquo; (��������� �������), &laquo;�����-��-����&raquo; (��������� �������)</div></td>
<td><div align="left"><strong>�������</strong></div></td>
</tr>
<tr>
  <td>22.</td>
<td><div align="left">������ ����</div></td>
<td><div align="left">������� ���� ���������� ����. �������� ���������� ���������. ��������� ������������ ��&nbsp;������ ���������, ����������� �&nbsp;��������� ��������, �&nbsp;&laquo;������������ �&nbsp;�����&raquo;, ��&nbsp;������� ���������� ��&nbsp;������. �&nbsp;��&nbsp;������������� ��� �������� �&nbsp;�������� �&nbsp;������ &laquo;��������������� �����&raquo;, �&nbsp;������� ��� ����� ���������.</div></td>
<td><div align="left">&laquo;����������� ��&nbsp;���&raquo; (������ ���-���)</div></td>
<td><div align="left"><strong>���������</strong></div></td>
</tr>
<tr>
  <td>23.</td>
  <td><div align="left">����� ���� </div></td>
  <td><div align="left">���� ���������� ���� �� ������� �����. ���� �� ���� ��� ������ ����� ������. ������� � �������� ����� �� ���. </div></td>
  <td><div align="left">&laquo;���� ������&raquo; (����� ����) </div></td>
  <td><div align="left"><strong>������</strong></div></td>
</tr>
<tr>
  <td>24.</td>
  <td><div align="left">���� ����� </div></td>
  <td><div align="left">��������� ��������� � ����������� �� ���-���������, ��������� ���������� �� ������.</div></td>
  <td><div align="left">&laquo;���� ������&raquo;</div></td>
  <td><strong>������</strong></td>
</tr>
<tr>
  <td>25.</td>
  <td>����� ������</td>
  <td><div align="left">��������. ������ ������, 
    �������� ��������, �������� � ��������� �������� ����� �� ������ 
    ���������� ������, �� ������� �� �������� ����������� ���� � ������� 
    ������.</div></td>
  <td>&laquo;�����������&raquo;</td>
  <td><strong>�����</strong></td>
</tr>

<tr>
<td colspan="5"><a name="forty_mile" id="forty_mile"></a><strong>��������� ���� </strong></td>
</tr>
<tr>
  <td>26.</td>
<td><div align="left">���� ���� (���� ����)</div></td>
<td><div align="left">������������ ���������-���������. ����� ��&nbsp;�������. ������� ���� �������� ����.</div></td>
<td><div align="left">&laquo;��&nbsp;��������� ����&raquo;, &laquo;�������� �������&raquo;, &laquo;��&nbsp;���, ��� �&nbsp;����!&raquo;, &laquo;��&nbsp;����� ����������&raquo;</div></td>
<td><div align="left"><strong>������ �������� </strong></div></td>
</tr>
<tr>
  <td>27.</td>
<td><div align="left">����� �����</div></td>
<td><div align="left">���� ��&nbsp;����� ������������ ����� ��&nbsp;���������. ������, ������� ��� �&nbsp;������ ���� ��&nbsp;������������� �&nbsp;������ ���� ������ �&nbsp;������. ��&nbsp;��������� ������ �&nbsp;������ ����� �����, ��� �������� �&nbsp;��� ��������� ��������� �&nbsp;������������ ������. ������ ��&nbsp;��������� �������� ���. ������ ���� ����� �����, ��&nbsp;��&nbsp;���� ��&nbsp;�����������, ��&nbsp;������������� ��&nbsp;�&nbsp;���� �������, ����� ������������. ������������, ������ �������� ��� �������� ������ ����������� �&nbsp;�������, �&nbsp;����.</div></td>
<td><div align="left">&laquo;��&nbsp;��������� ����&raquo;, &laquo;��&nbsp;����� ����������&raquo;, &laquo;�������� ������� �����&raquo;, &laquo;���, ��� ��������� ������&raquo;, &laquo;������� �������&raquo;, &laquo;�������� �������&raquo;, &laquo;������� ���������&raquo;, &laquo;������ ������ �����&raquo;, &laquo;���� ������&raquo;</div></td>
<td><div align="left"><strong>����� (���� ������)</strong></div></td>
</tr>
<tr>
  <td>28.</td>
<td><div align="left">������� ���</div></td>
<td><div align="left">��������. ������� �&nbsp;������� �������. �&nbsp;��&nbsp;�� ����� �&nbsp;��� ���� ���� ���-�� ������, �����������, ��������� ���� �&nbsp;���� ��������� ������� ����. �������, ��� ��� ���� ����, ���������, ������������� ���, �&nbsp;��&nbsp;������� ���� �&nbsp;������� ��� ����� ��������� �&nbsp;��� �����, ������� ������� ������� ��&nbsp;�����������.</div></td>
<td><div align="left">&laquo;����� ���������&raquo;, &laquo;��&nbsp;��������� ����&raquo;, &laquo;�������� �������&raquo;, &laquo;��&nbsp;����� ����������&raquo;, &laquo;��� �����&raquo;, &laquo;��&nbsp;���, ��� �&nbsp;����!&raquo;, &laquo;���� ������&raquo;</div></td>
<td><div align="left"><strong>������� ������� </strong></div></td>
</tr>
<tr>
  <td>29.</td>
  <td><div align="left">����� �����</div></td>
  <td><div align="left">��������� ����������. 23-24&nbsp;����. ����� �������, ���������� ��&nbsp;�������� �&nbsp;����������� ���� ������. ��&nbsp;�������� ����� �&nbsp;��������� ��������, ������ ��� ������ ��&nbsp;����� ��&nbsp;����-�����, ��� ��&nbsp;������&nbsp;�� �&nbsp;���. ���������� �����, ��&nbsp;����� ���������.</div></td>
  <td><div align="left">&laquo;������� ���������&raquo;, &laquo;�����-��-����&raquo;, &laquo;���� ������&raquo;</div></td>
  <td><div align="left"><strong>����� �������</strong></div></td>
</tr>
<tr>
  <td>30.</td>
<td><div align="left">����� ��������</div></td>
<td><div align="left">��������� ������, ����������� ������. �������� ���� ������ ��&nbsp;��������� ���� ��� ��&nbsp;25&nbsp;��&nbsp;������ ������� ���������. ����� ���������� �������.</div></td>
<td><div align="left">&laquo;��&nbsp;��������� ����&raquo;, &laquo;��� �����&raquo;</div></td>
<td><div align="left"><strong>����</strong></div></td>
</tr>
<tr>
  <td>31.</td>
<td><div align="left">�����</div></td>
<td><div align="left">��������, ���������� ������ ����� �����. ���� ������ ��������, ������������ ��� ��&nbsp;���� ������������.</div></td>
<td><div align="left">&laquo;���� ������&raquo;</div></td>
<td><div align="left"><strong>����� �����</strong></div></td>
</tr>
<tr>
  <td>32.</td>
<td><div align="left">����� ������</div></td>
<td><div align="left">����. ���� ��&nbsp;����� ��������� �&nbsp;����������� ���������� �����.</div></td>
<td><div align="left">&laquo;���, ��� ��������� ������&raquo;, &laquo;�������� �������&raquo;, &laquo;��&nbsp;��������� ����&raquo;, &laquo;��&nbsp;���, ��� �&nbsp;����!&raquo;, &laquo;���� ������&raquo;, &laquo;�����-��-����&raquo;</div></td>
<td><div align="left"><strong>������</strong></div></td>
</tr>
<tr>
  <td>33.</td>
<td><div align="left">����</div></td>
<td><div align="left">���� ������ �������. ���������� (���� &mdash; ��������, ���� &mdash; ������� �������� ��������). ���� �&nbsp;��&nbsp;������� �&nbsp;��������, ��&nbsp;��&nbsp;������� ����� ���� ������� ��������� ���.</div></td>
<td><div align="left">&laquo;��&nbsp;��������� ����&raquo;, &laquo;����� ���������&raquo;, &laquo;��&nbsp;���, ��� �&nbsp;����!&raquo; (����), &laquo;������� �������&raquo; (������), &laquo;���� ������&raquo; (���������)</div></td>
<td><div align="left"><strong>����</strong></div></td>
</tr>
<tr>
  <td>34.</td>
<td><div align="left">���� ���� ��&nbsp;�������� &laquo;������� ������&raquo;</div></td>
<td><div align="left">��������. ������������� �������� �������. �����-�� ������� ������ ���� ��������, ���� ���� ������������� �&nbsp;�����������. ������ ���������� ����������, ��������� �&nbsp;����� �&nbsp;���������. ���� ����������� ������ ���� ����� �����, �&nbsp;������ �������� ��&nbsp;�����.</div></td>
<td><div align="left">&laquo;������� ������&raquo;</div></td>
<td><div align="left"><strong>������� ������� </strong></div></td>
</tr>
<tr>
  <td>35.</td>
<td>�������</td>
<td><div align="left">��������������. ��&nbsp;����� ��� ������ ���� ��������� �&nbsp;���������. ����, ��&nbsp;���, ��� ���� ������������ �&nbsp;��&nbsp;��� ����� ������ �&nbsp;������ �&nbsp;���. ������� ������ ������ �&nbsp;������ ��������� ��&nbsp;����� ��������. ��������� ���� �������� �������� �����. ������������ �&nbsp;������������ ��&nbsp;������, ��&nbsp;������ ��������� �����������, ��&nbsp;�������� �&nbsp;����������� �&nbsp;��&nbsp;����� �&nbsp;������������.</div></td>
<td>&laquo;���, ��� ���������� ����&raquo;</td>
<td><strong>�������</strong></td>
</tr>
<tr>
  <td>36.</td>
<td><div align="left">������ (����) �������</div></td>
<td><div align="left">��������������. ��������. ������� �&nbsp;�����. ������������ ��������� �&nbsp;������, ��������, �������� �&nbsp;�.�. ��������� ��������. ���������.</div></td>
<td>&laquo;������� ���&raquo; </td>
<td><strong>������</strong></td>
</tr>
<tr>
<td colspan="5"><div align="left"><strong><a name="styx" id="styx"></a>����� ������� </strong></div></td>
</tr>
<tr>
  <td>37.</td>
<td><div align="left">�����-������</div></td>
<td><div align="left">����� ������� �������. ������.</div></td>
<td><div align="left">&laquo;��� �����&raquo;</div></td>
<td><div align="left"><strong>�������</strong></div></td>
</tr>
<tr>
  <td>38.</td>
<td><div align="left">����-��-���</div></td>
<td><div align="left">����� ������� ����� �&nbsp;������. ��� ������ ��-�� �������� ����� �&nbsp;���������� ����������� ���</div></td>
<td><div align="left">&laquo;������� ��������&raquo;. �&nbsp;����� ��.&nbsp;��.&nbsp;�������: &laquo;�&nbsp;������ ������&raquo;, &laquo;��� �����&raquo;, &laquo;�������&raquo;, &laquo;���, ��� ���������� ����&raquo;.</div></td>
<td><div align="left"><strong>�����</strong></div></td>
</tr>
<tr>
  <td>39.</td>
<td><div align="left">�������������</div></td>
<td><div align="left">����� �������. ������� ��������, ������� �&nbsp;���� ��� ��������� ���. ���������� ������ �������� �������� ��������: �������� ������ �������, ����� ����� �&nbsp;�������� ����������, ��� ������ ��&nbsp;����� �&nbsp;��������� ���������. ��� ��&nbsp;�&nbsp;��� ��&nbsp;����������, ��&nbsp;�&nbsp;��&nbsp;���������.</div></td>
<td><div align="left">&laquo;���� ������&raquo;</div></td>
<td><div align="left"><strong>������ �������</strong></div></td>
</tr>
<tr>
  <td>40.</td>
<td><div align="left">������</div></td>
<td><div align="left">�������� ������, ���� �����. ��� ��������� ������� �������� (������� ��� �����), ��� ������� ������� �&nbsp;�����������. ����������� ������ �������� (��� ��&nbsp;�������� ������� �����, ��,&nbsp;��� ��������� ����� �&nbsp;������� �� ���� ������ �&nbsp;��� �&nbsp;�.�.), ������ ��� ������ ��&nbsp;����� ������� &mdash; �&nbsp;��������, ������ �&nbsp;���������� ����� �������.</div></td>
<td><div align="left">&laquo;����� �����&raquo;</div></td>
<td><div align="left"><strong>������</strong></div></td>
</tr>
<tr>
  <td>41.</td>
<td><div align="left">�����</div></td>
<td><div align="left">������, ���� ������� �������. ��� ���� ����������� ������ ��������, ������ ���� ��� ��&nbsp;��&nbsp;����� �������� ���. �&nbsp;��&nbsp;��&nbsp;������ ������ ��&nbsp;����� �������, ��&nbsp;�&nbsp;�������� ����������� ���� ���-�� �������.</div></td>
<td><div align="left">&laquo;���� ��������&raquo;</div></td>
<td><div align="left"><strong>������� �������</strong></div></td>
</tr>
<tr>
  <td>42.</td>
<td><div align="left">�����</div></td>
<td><div align="left">���� ������. ��&nbsp;������� ����� ����� ����� ��&nbsp;�������������� &mdash; ����� ��� ������� ������� ��� �&nbsp;�������� �&nbsp;������ �&nbsp;���� ������� �������.</div></td>
<td><div align="left">&laquo;������� ��������&raquo;</div></td>
<td><div align="left"><strong>�����</strong></div></td>
</tr>
<tr>
  <td>43.</td>
<td><div align="left">��������</div></td>
<td><div align="left">������� ���� �����. ������� �&nbsp;������������ �������, ��&nbsp;������� �������� ����� ������. ����� ��� �&nbsp;�����������. ����� ����� ������� �������� �������������� (������� ��������) �&nbsp;����� ����� �����, �&nbsp;�������� �&nbsp;�����. ����������� �&nbsp;����������. �������� ��������� ������ �����.</div></td>
<td><div align="left">&laquo;���� ������&raquo;. �&nbsp;����� ��.&nbsp;&laquo;�������� �������&raquo; (������)</div></td>
<td><div align="left"><strong>�������� �������</strong></div></td>
</tr>
<tr>
  <td>44.</td>
<td><div align="left">���</div></td>
<td><div align="left">��� �����. ��� ������ ������, ��������� ����� ������� ����� ��&nbsp;�������� ����, ������ ������ ����� ������� ��� ���� ����� ��� ����� �������. �&nbsp;��� ��� ��&nbsp;������� ����� ���������, ������ �&nbsp;��������� ����� �������� &mdash; ��&nbsp;������, ���� ����� ������� ����� ��&nbsp;������� �����, ������� �������� ��� ��&nbsp;�������������� ���������.</div></td>
<td><div align="left">&laquo;���� ������&raquo;.</div></td>
<td><div align="left"><strong>��������</strong></div></td>
</tr>
<tr>
  <td>45.</td>
<td><div align="left">�� ���</div></td>
<td><div align="left">���� ������. �������� �������� ��������� ��������, ��� ��&nbsp;���� ��&nbsp;����� ���� &mdash; �������, ������ ��&nbsp;����� ��� �������, �&nbsp;������ &mdash; ����� ������, ��� �&nbsp;�����. ������ �&nbsp;������� ��&nbsp;����� ��&nbsp;�����, ��������� ���� ������� �����������.</div></td>
<td><div align="left">&laquo;����������� ��&nbsp;���&raquo;</div></td>
<td><div align="left"><strong>������ �������� </strong></div></td>
</tr>
<tr>
  <td>46.</td>
<td><div align="left">�����</div></td>
<td><div align="left">�������� �����. ������� ������� ���. </div></td>
<td><div align="left">&laquo;������� ��������&raquo;</div></td>
<td><div align="left"><strong>�����</strong></div></td>
</tr>
</table>



            
</td>
	    <td width="150" class="side">

<? show_menu("inc/alaska.menu");?>
	     </tr>
<? show_footer(); ?>

