<?
define("ROOT","../../../");
require_once("../../../funcs.php");
show_header("��������: ������� ��������� :: ����� ���� :: ���������� �����",ALASKA);
show_menu("inc/main.menu"); ?>

	    <td class="box">

            <div class="boxheader"><a href="http://bastilia.ru/alaska/">��������: ������� ���������</a> :: <a href="../">����� ����</a> :: ���������� ����� </div>
                        
             <p>�� ������� �&nbsp;49&nbsp;������� <strong>23800</strong>&nbsp;�., ��������� <strong>23785&nbsp;</strong>�.&nbsp;������ �������� �&nbsp;15&nbsp;������ ������ �������. ��&nbsp;����� ���� ��&nbsp;���������� ����������� �&nbsp;������ &laquo;�����&raquo;, ������� ������ ����� (����� ������� �������) ��� ���������� ��&nbsp;������ ���� �������� (�<strong>�</strong>����� ����� ��� ��������� ������� �������� ��&nbsp;������ �������).</p>
<p>����� 23800&nbsp;������ �������������� ��&nbsp;��������� ����:</p>
<ul>
<li>����� ��&nbsp;<strong>300</strong>&nbsp;������ (��������� �����)&nbsp;&mdash; <strong>7</strong>&nbsp;������� (��&nbsp;��� <strong>3</strong>&nbsp;��&nbsp;�������);</li>
<li>����� ��&nbsp;<strong>400</strong>&nbsp;������ (��������� ����� �&nbsp;�������)&nbsp;&mdash; <strong>5</strong>&nbsp;�������;</li>
<li>����� ��&nbsp;<strong>500</strong>&nbsp;������ (����� �����)&nbsp;&mdash; <strong>27</strong>&nbsp;������� (��&nbsp;��� <strong>4</strong>&nbsp;��&nbsp;�������);</li>
<li>����� ��&nbsp;<strong>600</strong>&nbsp;������ (����� ����� �&nbsp;�������)&nbsp;&mdash; <strong>9</strong>&nbsp;������� (��&nbsp;��� <strong>1</strong>&nbsp;��&nbsp;������);</li>
<li>���� <strong>800</strong>&nbsp;������ (����� ����� ��&nbsp;��������)&nbsp;&mdash; <strong>1</strong>&nbsp;�������.</li>
</ul>
<p>����� ������� ����������� 8&nbsp;������� �������� ��� 3500&nbsp;�.&nbsp;&mdash; ������� ��&nbsp;��������. ����� ��������: 41&nbsp;����� + 5&nbsp;��������.</p>
<p>���� �������������� ��������, ��&nbsp;��� ����� �&nbsp;������, ���� ����������� ��������� ����� �&nbsp;��������� ��������.</p>
<p><table border="1" cellpadding="1" cellspacing="1">
<col width="503" />
<col width="49" />
<col width="46" />
<col width="60" />
<col width="96" />
<col width="60" />
<tr height="17">
<td height="17"><strong>������ ��������</strong></td>
<td align="right"><div align="right"><strong>���-��</strong></div></td>
<td align="right"><div align="right"><strong>����</strong></div></td>
<td align="right"><div align="right"><strong>�����</strong></div></td>
</tr>
<tr height="17">
<td height="17" colspan="3"><strong>����������� ���� ��&nbsp;�������� </strong></td>
<td align="right"><div align="right"><strong>975</strong></div></td>
</tr>
<tr height="17">
<td height="17" colspan="4"><strong>� ��� �����: </strong></td>
</tr>
<tr height="17">
<td height="17" width="503">������ �1&nbsp;(����� ������)</td>
<td align="right" width="49"><div align="right">1</div></td>
<td align="right" width="46"><div align="right">340</div></td>
<td align="right" width="60"><div align="right">340</div></td>
</tr>
<tr height="17">
<td height="17" width="503">������� ����� 0,7&nbsp;�</td>
<td align="right" width="49"><div align="right">1</div></td>
<td align="right" width="46"><div align="right">360</div></td>
<td align="right" width="60"><div align="right">360</div></td>
</tr>
<tr height="17">
<td height="17" width="503">��������� ���������� �&nbsp;������</td>
<td align="right" width="49"><div align="right">55</div></td>
<td align="right" width="46"><div align="right">5</div></td>
<td align="right" width="60"><div align="right">275</div></td>
</tr>
<tr height="17">
<td height="17" colspan="3"><strong>������� ������ </strong></td>
<td align="right"><div align="right"><strong>1531,95</strong></div></td>
</tr>
<tr height="17">
<td height="17" colspan="4"><strong>� ��� �����: </strong></td>
</tr>
<tr height="17">
<td height="17" width="503">������ ���������� ����� ������������ ������, ��.&nbsp;500&nbsp;�.</td>
<td align="right" width="49"><div align="right">23</div></td>
<td align="right" width="46"><div align="right">26</div></td>
<td align="right" width="60"><div align="right">598</div></td>
</tr>
<tr height="17">
<td height="17" width="503">������ ����� ������������, 1&nbsp;��</td>
<td align="right" width="49"><div align="right">1</div></td>
<td align="right" width="46"><div align="right">129</div></td>
<td align="right" width="60"><div align="right">129</div></td>
</tr>
<tr height="17">
<td height="17" width="503">������ ������������, ��.</td>
<td align="right" width="49"><div align="right">5</div></td>
<td align="right" width="46"><div align="right">16,99</div></td>
<td align="right" width="60"><div align="right">84,95</div></td>
</tr>
<tr height="17">
<td height="17" width="503">���� ��� ����������� ������</td>
<td align="right" width="49"><div align="right">1</div></td>
<td align="right" width="46"><div align="right">720</div></td>
<td align="right" width="60"><div align="right">720</div></td>
</tr>
<tr height="17">
<td height="17" colspan="3"><strong>����������</strong></td>
<td align="right"><div align="right"><strong>760</strong></div></td>
</tr>
<tr height="17">
<td height="17" colspan="4"><strong>� ��� �����:</strong></div></td>
</tr>
<tr height="17">
<td height="17" width="503">���������� (������� �������)</td>
<td align="right" width="49"><div align="right">1</div></td>
<td align="right" width="46"><div align="right">110</div></td>
<td align="right" width="60"><div align="right">110</div></td>
</tr>
<tr height="17">
<td height="17" width="503">������</td>
<td align="right" width="49"><div align="right">50</div></td>
<td align="right" width="46"><div align="right">10</div></td>
<td align="right" width="60"><div align="right">500</div></td>
</tr>
<tr height="17">
<td height="17" width="503">���������� (������� �������, �����, ������� ��&nbsp;�������)</td>
<td width="49"><div align="right"> </div></td>
<td width="46"><div align="right"> </div></td>
<td align="right" width="60"><div align="right">150</div></td>
</tr>
<tr height="17">
<td height="17" colspan="3"><strong>����������� ������� ������ </strong></td>
<td align="right"><div align="right"><strong>9377,94</strong></div></td>
</tr>
<tr height="17">
<td height="17" colspan="4"><strong>� ��� �����: </strong></td>
</tr>
<tr height="17">
<td height="17" width="503">����� ��������� ������� ����� (������ ���������&nbsp;&mdash; 11900&nbsp;�.)</td>
<td width="49"><div align="right"> </div></td>
<td width="46"><div align="right"> </div></td>
<td align="right" width="60"><div align="right">3771</div></td>
</tr>
<tr height="17">
<td height="17" width="503">�����-��������</td>
<td align="right" width="49"><div align="right">2</div></td>
<td align="right" width="46"><div align="right">2300</div></td>
<td align="right" width="60"><div align="right">4600</div></td>
</tr>
<tr height="17">
<td height="17" width="503">������� ����� FIT 1400&nbsp;�.</td>
<td align="right" width="49"><div align="right">1</div></td>
<td align="right" width="46"><div align="right">260</div></td>
<td align="right" width="60"><div align="right">260</div></td>
</tr>
<tr height="17">
<td height="17" width="503">�������� ��� ������� &laquo;�����&raquo; 0,5&nbsp;��</td>
<td align="right" width="49"><div align="right">6</div></td>
<td align="right" width="46"><div align="right">27,99</div></td>
<td align="right" width="60"><div align="right">167,94</div></td>
</tr>
<tr height="17">
<td height="17" width="503">���� CHAMPION ��� ���������</td>
<td align="right" width="49"><div align="right">1</div></td>
<td align="right" width="46"><div align="right">189</div></td>
<td align="right" width="60"><div align="right">189</div></td>
</tr>
<tr height="17">
<td height="17" width="503">���� STIHL ��� ���������</td>
<td align="right" width="49"><div align="right">1</div></td>
<td align="right" width="46"><div align="right">281</div></td>
<td align="right" width="60"><div align="right">281</div></td>
</tr>
<tr height="17">
<td height="17" width="503">����� ������� ��� ���������</td>
<td align="right" width="49"><div align="right">1</div></td>
<td align="right" width="46"><div align="right">49</div></td>
<td align="right" width="60"><div align="right">49</div></td>
</tr>
<tr height="17">
<td height="17" width="503">����� ��� ���������</td>
<td align="right" width="49"><div align="right">1</div></td>
<td align="right" width="46"><div align="right">60</div></td>
<td align="right" width="60"><div align="right">60</div></td>
</tr>
<tr height="17">
<td height="17" colspan="3"><strong>�������</strong></td>
<td align="right"><div align="right"><strong>5161,69</strong></div></td>
</tr>
<tr height="17">
<td height="17" colspan="4"><strong>� ��� �����: </strong></td>
</tr>
<tr height="17">
<td height="17" width="503">����� &laquo;����������������� �������&raquo;, �</td>
<td align="right" width="49"><div align="right">5</div></td>
<td align="right" width="46"><div align="right">100</div></td>
<td align="right" width="60"><div align="right">500</div></td>
</tr>
<tr height="17">
<td height="17" width="503">������, �</td>
<td align="right" width="49"><div align="right">5</div></td>
<td align="right" width="46"><div align="right">100</div></td>
<td align="right" width="60"><div align="right">500</div></td>
</tr>
<tr height="17">
<td height="17" width="503">������� &laquo;�����&raquo; 525&nbsp;�,&nbsp;�����</td>
<td align="right" width="49"><div align="right">16</div></td>
<td align="right" width="46"><div align="right">55,87</div></td>
<td align="right" width="60"><div align="right">893,92</div></td>
</tr>
<tr height="17">
<td height="17" width="503">������ &laquo;�����������&raquo;, �����</td>
<td align="right" width="49"><div align="right">8</div></td>
<td align="right" width="46"><div align="right">19,69</div></td>
<td align="right" width="60"><div align="right">157,52</div></td>
</tr>
<tr height="17">
<td height="17" width="503">���� &laquo;365&nbsp;����&raquo; �����������, ��.&nbsp;200&nbsp;�</td>
<td align="right" width="49"><div align="right">2</div></td>
<td align="right" width="46"><div align="right">94,99</div></td>
<td align="right" width="60"><div align="right">189,98</div></td>
</tr>
<tr height="17">
<td height="17" width="503">����� &laquo;365&nbsp;����&raquo;, ��.&nbsp;1&nbsp;��</td>
<td align="right" width="49"><div align="right">2</div></td>
<td align="right" width="46"><div align="right">26,98</div></td>
<td align="right" width="60"><div align="right">53,96</div></td>
</tr>
<tr height="17">
<td height="17" width="503">������ �������� &laquo;365&nbsp;����&raquo;, �����</td>
<td align="right" width="49"><div align="right">8</div></td>
<td align="right" width="46"><div align="right">18,42</div></td>
<td align="right" width="60"><div align="right">147,36</div></td>
</tr>
<tr height="17">
<td height="17" width="503">��� &laquo;365&nbsp;����&raquo;, ��.&nbsp;1&nbsp;��</td>
<td align="right" width="49"><div align="right">2</div></td>
<td align="right" width="46"><div align="right">25,6</div></td>
<td align="right" width="60"><div align="right">51,2</div></td>
</tr>
<tr height="17">
<td height="17" width="503">����� &laquo;Scottish Prince&raquo;, ���. 1&nbsp;�</td>
<td align="right" width="49"><div align="right">2</div></td>
<td align="right" width="46"><div align="right">365</div></td>
<td align="right" width="60"><div align="right">729,98</div></td>
</tr>
<tr height="17">
<td height="17" width="503">����� ��� ������, ��</td>
<td align="right" width="49"><div align="right">1</div></td>
<td align="right" width="46"><div align="right">9,29</div></td>
<td align="right" width="60"><div align="right">9,29</div></td>
</tr>
<tr height="17">
<td height="17" width="503">���������� �������� &laquo;Nuts&raquo;, ��</td>
<td align="right" width="49"><div align="right">36</div></td>
<td align="right" width="46"><div align="right">9,4</div></td>
<td align="right" width="60"><div align="right">338,4</div></td>
</tr>
<tr height="17">
<td height="17" width="503">�������� &laquo;�����&raquo;, ��.&nbsp;700&nbsp;�</td>
<td align="right" width="49"><div align="right">3</div></td>
<td align="right" width="46"><div align="right">19,99</div></td>
<td align="right" width="60"><div align="right">59,97</div></td>
</tr>
<tr height="17">
<td height="17" width="503">����� ��������� &laquo;365&nbsp;����&raquo;, ��.</td>
<td align="right" width="49"><div align="right">4</div></td>
<td align="right" width="46"><div align="right">20,4</div></td>
<td align="right" width="60"><div align="right">81,6</div></td>
</tr>

<tr height="17">
<td height="17" width="503">������, ��</td>
<td align="right" width="49"><div align="right">0,769</div></td>
<td align="right" width="46"><div align="right">110</div></td>
<td align="right" width="60"><div align="right">84,58</div></td>
</tr>
<tr height="17">
<td height="17" width="503">���������, ��</td>
<td align="right" width="49"><div align="right">1,114</div></td>
<td align="right" width="46"><div align="right">110</div></td>
<td align="right" width="60"><div align="right">122,53</div></td>
</tr>
<tr height="17">
<td height="17" width="503">��������� ������ &laquo;����� ����&raquo;, �������</td>
<td align="right" width="49"><div align="right">10</div></td>
<td align="right" width="46"><div align="right">5,75</div></td>
<td align="right" width="60"><div align="right">57,5</div></td>
</tr>
<tr height="17">
<td height="17" width="503">���� &laquo;������&raquo;, ��.</td>
<td align="right" width="49"><div align="right">1</div></td>
<td align="right" width="46"><div align="right">11,69</div></td>
<td align="right" width="60"><div align="right">11,69</div></td>
</tr>
<tr height="17">
<td height="17" width="503">����� �����������, ��������</td>
<td align="right" width="49"><div align="right">2</div></td>
<td align="right" width="46"><div align="right">20,69</div></td>
<td align="right" width="60"><div align="right">41,38</div></td>
</tr>
<tr height="17">
<td height="17" width="503">����� �����������, ��������</td>
<td align="right" width="49"><div align="right">1</div></td>
<td align="right" width="46"><div align="right">56,99</div></td>
<td align="right" width="60"><div align="right">56,99</div></td>
</tr>
<tr height="18">
<td height="18" width="503">������� �����������, ��������</td>
<td align="right" width="49"><div align="right">1</div></td>
<td align="right" width="46"><div align="right">49,49</div></td>
<td align="right" width="60"><div align="right">49,49</div></td>
</tr>
<tr height="17">
<td height="17" width="503">������� �����������, ��������</td>
<td align="right" width="49"><div align="right">2</div></td>
<td align="right" width="46"><div align="right">28,99</div></td>
<td align="right" width="60"><div align="right">57,98</div></td>
</tr>
<tr height="17">
<td height="17" width="503">�������� ��� ����� ������ &laquo;���&raquo;, ��</td>
<td align="right" width="49"><div align="right">1</div></td>
<td align="right" width="46"><div align="right">18,99</div></td>
<td align="right" width="60"><div align="right">18,99</div></td>
</tr>
<tr height="17">
<td height="17" width="503">����������, ��������</td>
<td align="right" width="49"><div align="right">1</div></td>
<td align="right" width="46"><div align="right">17,39</div></td>
<td align="right" width="60"><div align="right">17,39</div></td>
</tr>
<tr height="17">
<td height="17" width="503">������� ������ ��� �������, 0,2&nbsp;��</td>
<td align="right" width="49"><div align="right">16</div></td>
<td align="right" width="46"><div align="right">37,2</div></td>
<td align="right" width="60"><div align="right">595,2</div></td>
</tr>
<tr height="17">
<td height="17" width="503">����� ��� ���������� �����, ��</td>
<td align="right" width="49"><div align="right">1</div></td>
<td align="right" width="46"><div align="right">40,69</div></td>
<td align="right" width="60"><div align="right">40,69</div></td>
</tr>
<tr height="17">
<td height="17" width="503">����� ��������, ��</td>
<td align="right" width="49"><div align="right">9</div></td>
<td align="right" width="46"><div align="right">10,6</div></td>
<td align="right" width="60"><div align="right">95,4</div></td>
</tr>
<tr height="17">
<td height="17" width="503">��������� ������, ��</td>
<td align="right" width="49"><div align="right">4</div></td>
<td align="right" width="46"><div align="right">13,05</div></td>
<td align="right" width="60"><div align="right">52,2</div></td>
</tr>
<tr height="17">
<td height="17" width="503">������������ �����, ���</td>
<td align="right" width="49"><div align="right">1</div></td>
<td align="right" width="46"><div align="right">41,5</div></td>
<td align="right" width="60"><div align="right">41,5</div></td>
</tr>
<tr height="17">
<td height="17" width="503">����� ���������, �����</td>
<td align="right" width="49"><div align="right">5</div></td>
<td align="right" width="46"><div align="right">21</div></td>
<td align="right" width="60"><div align="right">105</div></td>
</tr>
<tr height="17">
<td height="17" colspan="3"><strong>���������</strong></td>
<td align="right"><div align="right"><strong>5110</strong></div></td>
</tr>
<tr height="17">
<td height="17" colspan="4"><strong>� ��� �����: </strong></td>
</tr>
<tr height="17">
<td height="17" width="503">��������� ��&nbsp;&laquo;�����&raquo; ��&nbsp;����������� ������</td>
<td align="right" width="49"><div align="right"> </div></td>
<td align="right" width="46"><div align="right"> </div></td>
<td align="right" width="60"><div align="right">100</div></td>
</tr>
<tr height="17">
<td height="17" width="503">������ ����������, ����</td>
<td align="right" width="49"><div align="right">4</div></td>
<td align="right" width="46"><div align="right">950</div></td>
<td align="right" width="60"><div align="right">3800</div></td>
</tr>
<tr height="17">
<td height="17" width="503">������, �</td>
<td align="right" width="49"><div align="right">36</div></td>
<td width="46"><div align="right">22,2</div></td>
<td align="right" width="60"><div align="right">800</div></td>
</tr>
<tr height="17">
<td height="17" width="503">����� ������</td>
<td width="49"><div align="right"> </div></td>
<td width="46"><div align="right"> </div></td>
<td align="right" width="60"><div align="right">410</div></td>
</tr>
<tr height="17">
<td height="17" colspan="3"><strong>������</strong></td>
<td align="right"><div align="right"><strong>869,26</strong></div></td>
</tr>
<tr height="17">
<td height="17" colspan="4"><strong>� ��� �����: </strong> </td>
</tr>
<tr height="17">
<td height="17" width="503">�������� C6,&nbsp;��</td>
<td align="right" width="49"><div align="right">150</div></td>
<td align="right" width="46"><div align="right">0,34</div></td>
<td align="right" width="60"><div align="right">51</div></td>
</tr>
<tr height="17">
<td height="17" width="503">����� ������� �&nbsp;�����</td>
<td align="right" width="49"><div align="right">2</div></td>
<td align="right" width="46"><div align="right">12</div></td>
<td align="right" width="60"><div align="right">24</div></td>
</tr>
<tr height="17">
<td height="17" width="503">������� ��� ����������</td>
<td align="right" width="49"><div align="right">1</div></td>
<td align="right" width="46"><div align="right">75</div></td>
<td align="right" width="60"><div align="right">75</div></td>
</tr>
<tr height="17">
<td height="17" width="503">������� ��� �����</td>
<td align="right" width="49"><div align="right">1</div></td>
<td align="right" width="46"><div align="right">15</div></td>
<td align="right" width="60"><div align="right">15</div></td>
</tr>
<tr height="17">
<td height="17" width="503">������ ��� ������ 120&nbsp;�,&nbsp;��������</td>
<td align="right" width="49"><div align="right">2</div></td>
<td align="right" width="46"><div align="right">24,99</div></td>
<td align="right" width="60"><div align="right">49,98</div></td>
</tr>
<tr height="17">
<td height="17" width="503">������ (���� ��� �����)</td>
<td align="right" width="49"><div align="right">1,904</div></td>
<td align="right" width="46"><div align="right">195</div></td>
<td align="right" width="60"><div align="right">371,28</div></td>
</tr>
<tr height="17">
<td height="17" width="503">�������� ������������� FIT, ��</td>
<td align="right" width="49"><div align="right">1</div></td>
<td align="right" width="46"><div align="right">215</div></td>
<td align="right" width="60"><div align="right">215</div></td>
</tr>
<tr height="17">
<td height="17" width="503">����� 14&nbsp;��,&nbsp;��</td>
<td align="right" width="49"><div align="right">1</div></td>
<td align="right" width="46"><div align="right">41</div></td>
<td align="right" width="60"><div align="right">41</div></td>
</tr>
<tr height="17">
<td height="17" width="503">������ ��������������</td>
<td align="right" width="49"><div align="right">9</div></td>
<td align="right" width="46"><div align="right">3</div></td>
<td align="right" width="60"><div align="right">27</div></td>
</tr>
<tr height="17">
<td height="17" width="503"><div align="right"><strong>�����:</strong></div></td>
<td width="49"><div align="right"> </div></td>
<td width="46"><div align="right"> </div></td>
<td align="right" width="60"><div align="right"><strong>23785,8</strong></div></td>
</tr>
</table>
</td>
	 <td width="150" class="side">



<? show_menu("inc/alaska.menu");?>
	     </tr>
<? show_footer(); ?>

