<?php
define ('ROOT', '../../../');
require_once ROOT . 'funcs.php';
show_header("���� ��������� :: ������� � ��������� :: ������� �� ����������",HELLAS);
show_menu("inc/main.menu"); ?>
<td class="box">
 <div class="boxheader"><a href="http://bastilia.ru/hellas/">���� ���������</a> :: <a href="http://bastilia.ru/hellas/rules/">������� � ���������</a> :: ������� �� ����������</div>
 <h3>� �����, ������� ��������� ������ �&nbsp;���������� ��� ��������</h3>

<p>�� ������� ����� ��&nbsp;����� ������ ����� &laquo;����������&raquo;. ��&nbsp;���� ��� �����, ��� ��� ����� �&nbsp;��� ��� �������������. ��&nbsp;������ ��� ���: ���������� &#151; ��� ������ �������� ��������, ������� ��&nbsp;��&nbsp;�������� �&nbsp;������������ ��� ���� ����� ������������. ��&nbsp;��� ������ &#151; ��&nbsp;������ �������, �&nbsp;������ �����, �&nbsp;��� ������� ��&nbsp;��������. ������� �&nbsp;���������� ����� ��&nbsp;�������� ��&nbsp;������� (��������), �&nbsp;����������� (���-���������), ���, �������������� ����� ��������, ������� ��������� �&nbsp;������������ ��������� ��-������. �&nbsp;������ ����������� ���� ����������, ���� ��� �&nbsp;�&nbsp;������� �������.</p>

<img src="brain.jpg" width="80" height="97" style="float:right;margin: 1em" alt="���� ������" title="���� ������">
<p>������� ���� ��&nbsp;�������, ������� ������������ ��������, ������ ��� ��&nbsp;���������� �&nbsp;�������.

C����� �� ������� ��������� ���� ���������� ���������������� ����� ������. ���, ��� ���� �&nbsp;���� �&nbsp;������, �.&nbsp;�.&nbsp;������� ��������� �&nbsp;��������� ��������, ���������� �&nbsp;��&nbsp;������� ���� �&nbsp;���� ����� �&nbsp;�������, �������������� �������� ����� ���������. ������ ����������� ���������� ���������������� ��������:</p>
	
<ul>
<li><strong>����</strong>, �.&nbsp;�.&nbsp;������������� �&nbsp;���������� ��� ��&nbsp;���������� ������, ������� ������ ��� ����� �&nbsp;������� ��� �&nbsp;�������;</li>
<li><strong>��������</strong>, �.&nbsp;�.&nbsp;������������� ��&nbsp;������, �������, ������� ���������� �&nbsp;����� ����� ������������ ������� ��������;</li>
<li><strong>����������</strong>, �.&nbsp;�.&nbsp;���������. ��������� ������, ��������������� ������� �������� �,&nbsp;��������������, ����������� ���������� ����.</li>
</ul>
</p>

<p>�������� ��&nbsp;����������� ���������������� �������� �������� �����, �������, �������, ������������, ��,&nbsp;�&nbsp;�����, ������������ �&nbsp;�������� ���������. ���:
<ul>
<li><strong>����</strong>, �.&nbsp;�.&nbsp;������� ����������������, ������� ��������. ��� ������������ ����� ����� (�&nbsp;�������������� ����� �&nbsp;������ ����������� ���) &#151; ��� ������������ �&nbsp;������������ ��&nbsp;���� ��������, ���������� ���������� ��������;</li>
<li><strong>���</strong>, �.&nbsp;�.&nbsp;��� &#151; ��� ��&nbsp;������ �������� �&nbsp;��������� ��� �������� ��&nbsp;�������� �&nbsp;������, ����-����� ����������� ����������, ��� &#151; ������ �&nbsp;������ �&nbsp;��������� ���������. ���, ��� ���������� ����� &#151; ������ ���������� ����� ���������� ����;</li>
<li><strong>������</strong>, �.&nbsp;�.&nbsp;�������� &#151; ������� ������� ���� ���������������, ������� ���� �&nbsp;����� ���������� ��&nbsp;����� ������ ���������, ��� ������� ��&nbsp;����� ������ ����������, ����� ��&nbsp;���� ������.</li></ul></p>

<p>��� ��� ������� ���������� �&nbsp;�������� �������� ����� ��&nbsp;������ ��� ����������� �������� (��� ���������� �&nbsp;�������� ������������ �������� ������� ����, ����� �&nbsp;�������), ��&nbsp;����� ������������ ���������.</p>

<blockquote>��������, ���� ������� �������, ��� �������� �����-�� ������, ��&nbsp;������ ��������� �,&nbsp;��&nbsp;����� �������, ������� ������� ��� �������, ��������� ��������� ��� ���� ��������� ���������������. ����������, ����� ��������� ��� ��&nbsp;������ ��������, �&nbsp;��&nbsp;��������� ������, ���&nbsp;�� ��&nbsp;������ �,&nbsp;��������������, ��������� ���������.</blockquote>

<p>����������, ��� �������� ������� �&nbsp;����� �������� ����� ��������� ���&nbsp;�� ��&nbsp;������� �������� �&nbsp;�������� ���� �&nbsp;�����������, �&nbsp;����, �&nbsp;�������. ��� ��&nbsp;����� ��������, ��� ������� �&nbsp;������������ &laquo;����&raquo; ������� ��&nbsp;��������� ��&nbsp;�&nbsp;����� �&nbsp;����� &laquo;�������� ����������, ��� ��������� ���������� ����� ���� �&nbsp;�������&raquo;. ������ �&nbsp;����� �������� ����� ������ ����������� �&nbsp;���� ���� ��� ��������������, �������� ���������. ������ ������� ��,&nbsp;��� ��&nbsp;������� ���������, ��� �������� ����� �������� ����������� �����������, �&nbsp;�������� ������� ������� ���������� ��&nbsp;���� ����������������. �������� �������� ����� ����������� ������� ������, ��� �������� ����������� �������������, ���� ������� ����� ������ �&nbsp;�����.</p>

<p>�� ����&nbsp;�� ������� ����� ������� ������ ����&nbsp;�� ��&nbsp;�&nbsp;������ ���� �&nbsp;����&nbsp;�� ��&nbsp;�&nbsp;������ ������� <strong>����� ������������ ����, �����������, ��������������, �������� �&nbsp;��������.</strong></p>

<blockquote>��������, ����������� ����, ��� �&nbsp;����� �������� ���������� ����� �����. ��� ����� �������� ��&nbsp;������ ��,&nbsp;��� ���� ���� ������ �&nbsp;����, ��&nbsp;����� �&nbsp;��,&nbsp;��� ��� ������, �������� �������� ����������� ������� (������ ��� ������� ��������) ����� �������� ��������, ��� ��� ������ ��� ������� �����, �&nbsp;��&nbsp;������ ���� ����� ����������, ����� ��&nbsp;���������� �.</blockquote>

<p>��� ������������� ������� ������ �&nbsp;�������� �������� �������� �&nbsp;�������� ����, ��&nbsp;������ ��� �������� ��,&nbsp;������� ������� ��&nbsp;����� ����� ������ ��� ���� ��������� ��&nbsp;��������������.</p>

<p><h4>�� ��������� �&nbsp;���������</h4></p>

<p>�� ����� ���������� �������� ��� ���� ������ ���������: ������ ���������� ������� ������ ��������, �.&nbsp;�.&nbsp;�������, �����, �����, �����-�� ���� ��������, �&nbsp;������� ����� ����� �������, ��� ��� ��&nbsp;�������, ��� ��&nbsp;������ �������, ��� ����� �������� �������, ���� ��&nbsp;�������� ���&nbsp;�� ������-�� �������� ��������� �������������� (���������), ��� ��� ����� ����� ���. ���� ������������ ��� ��� ��������: ��,&nbsp;��� ������������� ������, �&nbsp;��,&nbsp;��� ��� ������ �&nbsp;������� &#151; ����������, �&nbsp;��&nbsp;&laquo;���� �����������&raquo;.</p>

<p>�������, <strong>������ ��� ������ ��������</strong>, ��������, ��������� �&nbsp;���� ������� �����, ������� ��&nbsp;���� ��&nbsp;���������, ���, ������� �&nbsp;�����, ��� ����������� �������� ����� ����������, ���, ������, ��� ���-�� ��������� ����� �&nbsp;����, ���� ����� ���� �������, ��&nbsp;������� ������ �&nbsp;������� �&nbsp;�������, ��� ������ ������ ����� &#151; ��� ��&nbsp;��&nbsp;��������, ��� ��������� ��&nbsp;������ �&nbsp;������ ��� ���������, ������� �&nbsp;���� �&nbsp;������ ������. <strong>���� ��������� �&nbsp;������, �&nbsp;��� ��� ����.</strong></p>

<p>�������, ������� ����, ������&nbsp;�� ������������ ��� ��&nbsp;����, ������ ����� ������ ��&nbsp;�����, �&nbsp;�&nbsp;������ ��&nbsp;�������� ��&nbsp;�������� �&nbsp;����� ��������� ����� ������� &#151; �&nbsp;��&nbsp;���� ��� ��&nbsp;�������. ��&nbsp;�������, ��� ��� ��������� ��� ����� �&nbsp;����������� ����������� ������� ���� ������ ���������, �&nbsp;��&nbsp;���-�� �����, ��� ������� �����������, ��&nbsp;���� ���������� ��������� ������-�� ���������� ���������, ���������: &laquo;����� �&nbsp;��� �����? &#151; ��� �����-�� ����� ��&nbsp;������ ������� ���� ���� ���������. ��� �&nbsp;��&nbsp;��&nbsp;��� ���, �&nbsp;��� �������&raquo;. &#151; ��� ������ ������������� ���� ������������������� ����� ��&nbsp;������, �&nbsp;��� ����� �&nbsp;������ ������� ���������� ��&nbsp;���.</p>

<h4>��� �&nbsp;���� ������?</h4>

<p>����������, �������� ��������� �������� ���� ������, ������ �&nbsp;&laquo;��������&raquo; �������, �&nbsp;���������� ���������� �&nbsp;���, ��� <strong>����� �����, ��&nbsp;���� �&nbsp;�����.</strong></p>

<p>����� ����� <strong>�������� �����, ��������</strong>, ���������� �&nbsp;���������, ����� ��������, ��� ������ <strong>����� ��&nbsp;����</strong>. �&nbsp;���������� ��� �&nbsp;���� ����� ��� �&nbsp;����� �&nbsp;�����, ������ ��� �����, ��� ��&nbsp;������������� ��&nbsp;����� ������� ��� ��&nbsp;��������, ���������� �&nbsp;���� �&nbsp;������������� �&nbsp;���� �&nbsp;����������� ������� �������, ��������� ������������ �������� (�����), ����� ������� �������� ������ ��� ������� ��������, �.&nbsp;�.&nbsp;��� ���������.</p>

<p>����������, ����� �������� �&nbsp;��������� ���� ���������, ��&nbsp;�&nbsp;����������� ������� ��������� �&nbsp;������� �&nbsp;������� ����������� �������������� ����������� ������ �������: ��������, �������� ���� �&nbsp;������ ����� ��� ��� �&nbsp;����, ������ ������� ����������� ������ �&nbsp;����������� �����, ����������� ����������� ���������� ��������, ��������� �����, �&nbsp;�.&nbsp;�.</p>

<p>����� ��&nbsp;������� ���� ������� ���������� �&nbsp;��������� �&nbsp;����, ��&nbsp;���������� �������� ��&nbsp;���� ����������� ����������, ����� �������, �&nbsp;������� ��� ����� ��� ���� ��� �����: ��� ��&nbsp;����� �&nbsp;������������ ����� ��������, ���������� ��&nbsp;��� ����� �������� �&nbsp;��� ������������ ��� ��� ��� ������ �������� ���� �&nbsp;������ ��� �������� ��&nbsp;����� ������.</p>

<p><p class="nb">����� �������, ���� ����� �&nbsp;����� ����� ��� ������� ����� �&nbsp;�������� ������������� ����������� ��������, ��� �&nbsp;��� ��������� ������, �&nbsp;������ ������ ���, ��� ��� �&nbsp;����, ��&nbsp;��&nbsp;�������� �������, ���� ��� ����� �������� ������ ����, ����� ���������� ��&nbsp;����, �&nbsp;��� ����� �������������� ����������, ����� ��&nbsp;���������� �&nbsp;����� ���� ����������, ��&nbsp;��������� ���������������� �&nbsp;�������.</p></p>

<p><h4>����������� �&nbsp;�������� ��� ��� ����?</h4></p>

<p>��� ��� ���� �����. ��&nbsp;��� ����, ����� �� ������������� ��&nbsp;�������� ����� ���� �&nbsp;����������� �������� �����, �&nbsp;�������&nbsp;�� ��&nbsp;������ ����� ������� ����, ��&nbsp;�� ������ ���������� �&nbsp;���� ������ ����������� �&nbsp;�������� ��&nbsp;��������� ��������, ���������� �������� �������. ����:</p>

<p><p><ul>
<li> �&nbsp;����� ������ ������� ������ ����� ��������, ���������� ��� �&nbsp;���������� �&nbsp;���� ��&nbsp;���� ������������.</li>
<li> ���� ���� �������� ��&nbsp;������-������ ������, ����������, ����� �������������� �&nbsp;����� �������� ������, ��&nbsp;������� ������� ����������� �������� ������ �&nbsp;��������. �&nbsp;��� ������ ����� ���������� ����&nbsp;�� ��&nbsp;��&nbsp;����� �������.</li>
<li> ���� ��&nbsp;������� ������� (������) ����������������, ��� �������� ������� (���������) �������, ���������� ��������� �,&nbsp;������ �����, ���-�� �������� ���, ��� ����� ����������� �����, �&nbsp;���� ��������� ����� �������� (������ ��&nbsp;����, ��� �&nbsp;��� ������� ��&nbsp;��� ����� ����� �&nbsp;���������) �&nbsp;�.&nbsp;�., ���������� ��� ����� �������, �&nbsp;����� ���� �&nbsp;��&nbsp;����� ������� �����, ��� ������ &#151; ��������� �������� ����� ������ ������ ����.</li>
<li>��������� ��������, ���������� �&nbsp;����� ��� �&nbsp;��������� �&nbsp;����������: ���� ����� ���� ������� �������, ����������� ������������� ����������, ��� ������ ���� ���������� ����</li>
<li><strong>����</strong> (����������������) &#151; ��&nbsp;��������� ���������� ��&nbsp;�&nbsp;����� ����, ��������, ���������� �&nbsp;����, ���� ������ ����������� �����������, �&nbsp;���� ������������ ����� �����������</li>
<li><strong>���</strong> &#151; ���� ����� �������� ��� ������ �������� ��������� &#151; ������ �������� ����, �&nbsp;������ �������� ���� �������� ����, ������ ����� �&nbsp;��&nbsp;���. �&nbsp;��� ���� ���� ��������� ��� ����, ������ ������� ������ �&nbsp;���������� �����. ������� ��&nbsp;���������: ������� ����������� �&nbsp;����� ������� ����������</li>
<li><strong>������</strong> (��������) &#151; ������ (�&nbsp;���������) ����������� ������������� �&nbsp;���������. ������� �&nbsp;�����, ����������� ����������� �&nbsp;����������� �������, ������� ���� �����, �������� ������� ����� �&nbsp;��&nbsp;������ �&nbsp;����� ������������ ��� ��������� ������. �&nbsp;����� ������ �������� �������� &#151; ��&nbsp;��������� ����� �&nbsp;������ ��&nbsp;������.</li></ul></p>

<p>������������� ��&nbsp;�������������� ������� ���� �������� �����. �&nbsp;������ ������ ��������� �����, ������ ��-��������, ��� ���������� ������� ��������� �&nbsp;�������� ��������. ��� ��&nbsp;������������� ��&nbsp;�&nbsp;���������, ��&nbsp;�&nbsp;��,&nbsp;� ���� ������� ��&nbsp;������&nbsp;�� ��� ��������, ��&nbsp;��� �&nbsp;��&nbsp;�����: ��������� ��������� ����, ���� ������������� ������������ ����. ������� ��&nbsp;���������� ������ ��������� ��&nbsp;���.</p>

<p><h3>���������� ����������</h3></p>

<h4>�������� (������)</h4>

<p>������ ����� <strong>���������� �������� ����&nbsp;�� ����� �������</strong> (����� �&nbsp;����� �����), ���������, ������� ������� ��� ������, �&nbsp;��������� ��� �������� ��&nbsp;����.</p>

<p>��������:</p>

<p><ol>
<li> ������� ������ �&nbsp;����� ������;</li>
<li> �������, ��� ����� �&nbsp;������� ������ ������� ������;</li>
<li> �������� ���� ������� ������ ���. </li></p>

<p></ol></p>

<p>�������, ��������������� �&nbsp;�������� ��&nbsp;�������� �������. ������ ���������� ����� ������� ������� ������ ������� �&nbsp;�������. ����� ����� �������� ��&nbsp;�����, �&nbsp;���� ����� ������� ��������, ��&nbsp;�&nbsp;������� ����� ��&nbsp;���������� �&nbsp;��&nbsp;������.</p>

<p>������ �������:</p>

<p class="nb">���� ����� ����� ��&nbsp;���� �������, ������� ��� ������, ��&nbsp;(���� ������� ������), ��&nbsp;��������, ��� ������� ��&nbsp;�������� �����-�� ��������� ������, ������� ������ ��� �����. ����� ������ &#151; ��&nbsp;��&nbsp;�����. �&nbsp;���� ������� �������, ��&nbsp;����� ���� ������������, ����� ��&nbsp;�������� ������ ��� ��������� ����� �&nbsp;�������. ����� �������� ��� ���-�� ������: ����� �&nbsp;�������, ���������� ����� ����, ���� ������� ��������, ��&nbsp;��� ������ ���-�� ������, ����� �������� ����������� ����� ��������� ��&nbsp;����������� ���� ��� ��������� ��������� ��������, ������� ����� �������� ��� �����, ���� ��������� ������� �������. </p>

<p>������ ����� ����� ������� �&nbsp;�������� ������� ��� ����. �&nbsp;��� ��������, ������� ��&nbsp;��������, �� ����. �&nbsp;��� �&nbsp;��� ��������, ��� ��&nbsp;�����, ����� ���������� ��������. ����� ����� ��� ��&nbsp;���� ���������� ����������� �������, ������� ��&nbsp;���&nbsp;�� �&nbsp;�������. �.&nbsp;�.&nbsp;���� ��&nbsp;������� ������� �&nbsp;����: &laquo;������� �&nbsp;����� ����� ���� ���� ��&nbsp;�������&raquo;, ��&nbsp;��&nbsp;����� ������� ������ ��&nbsp;������� ������� ��&nbsp;����� ���� �&nbsp;���� ���� &#151; ��� ��������� ����-������ ������� ���. ���� ������� �����������, ��������, ���� �������� ���������� �&nbsp;��������� �����, �&nbsp;������� ����� �&nbsp;��������� �������� �����, ��� ��� ������ ���, �&nbsp;���� ��� ������� ������, �&nbsp;��� ����� ����� �������� ��� �����.</p>

<p>������ ������� ������������ ��� ��������� ������ &#151; <strong>��&nbsp;���� ��� ������ ���� ��� ������� ����������� ����������� �&nbsp;�����</strong> ��� ������, ��� ����� ����� ��������� ���� �����.</p>

<h4>���� (������, ����� ��� ����)</h4>

<p>� ���� (�������, ������) ������� �������� ��&nbsp;��,&nbsp;��� ������ �����&nbsp;�� ������ ��&nbsp;����������/������ ����. �����, �&nbsp;����� ������, ��&nbsp;�����, �&nbsp;������ ������ ����� �������� ��������� �&nbsp;���������� ��� ��������� �������, ����� �&nbsp;������ ������, ��� ������������ ������ ��������� ��������. ��� ����� ����� �������������� ����-�� ������� ��&nbsp;��� ���� ����, �.&nbsp;�.&nbsp;����������� ������ �������.</p>

<p>����� ������ <strong>�������� �������, ��&nbsp;��������&nbsp;�� ��� ��� ����� ����</strong>. �������� �&nbsp;��� ����� ������ ����� �������� ��&nbsp;�������, ���������, ��������� �&nbsp;�.&nbsp;�., �&nbsp;�.&nbsp;�.&nbsp;&laquo;���� ����� ��&nbsp;������ ������&raquo;.</p>

<p>��������� ��������� ������ ����� ���� �����:</p>

<p><ol>
<li>����� ��� ����������� ��&nbsp;&#151; �������, ������� �����-�� &#151; �&nbsp;��������� �������, ������ �&nbsp;�������;</li>
<li>������� ������� ������� ���� ����������� �&nbsp;������� ������� ��������� ������� ��&nbsp;����;</li>
<li>������� ����� �������� ����� ��&nbsp;����. ���� ��&nbsp;��������� ����������: ������� ������������ ��&nbsp;��������������� �����������/����������� ������, ����� �&nbsp;����, ��� ������� ��� �����/���� ����� ��� ����� �������� ������ �&nbsp;����������.</li>
</ol></p>

<blockquote>��������, �������� ��������� ���� �������� �������� ��������, ���, ���� ����� ������� �������������� ������ �&nbsp;������������ �������� �&nbsp;����, ��� ��������� �&nbsp;���������� ��&nbsp;�������, ��&nbsp;���� ����. �&nbsp;��� ������, �&nbsp;������. &#151; ��� ����� ��� ���������� ���� �����. ��,&nbsp;�&nbsp;��&nbsp;�� �����, ��&nbsp;���� ������� ��������, ��� ���� ��� �&nbsp;������� ������ ��&nbsp;����� ������, ��� ��&nbsp;����� ��� ������� (����� �������� ��������� �&nbsp;�����������, ���� �������), ��&nbsp;����� ��&nbsp;�����. �&nbsp;��� ��� �������. &#151; ��� ����� ������� �����-�� ������� �����, ������� ������� ��������. ���-�� ������ ������ ������������ �&nbsp;�������, �&nbsp;���-�� &#151; �&nbsp;�������.</blockquote>

<p>�����, ���� ������� ��&nbsp;�������� ���, ��� ����� ����� ������� �����, �&nbsp;����� ���, ��� ������� ��&nbsp;�����, ��� �������. ������ �������: </p>

<p class="nb">���� ����� ������ ����, ��&nbsp;��&nbsp;������ ���� ������ ���������� ������ �������, ���������, ��� ��&nbsp;��� ������ ��� ��� ��������� �������, ���� ����������� �&nbsp;���, ���&nbsp;�� ������������� ���������. ����������/������������ ���������������� ������ ������ ���� ������ ��&nbsp;�����, ���������� �&nbsp;�����, �&nbsp;������� ��&nbsp;������, ������� ����� ������ ����������������, �.&nbsp;�., ������������� ������� ���� ��������� �����������, ��� �&nbsp;��������, ������ ��� ��������� ���� ��&nbsp;�������, ��� �������� ���������� ��&nbsp;������ ���������� ��������, ��&nbsp;���� �����.</p>

<h4>���������� (���������)</h4>

<p>���� ��&nbsp;���� �&nbsp;����������� ���������, ������� �&nbsp;������� �&nbsp;����������, ���� <strong>���������, ��&nbsp;���� ��������� ����� ������</strong>. ��� ��������� ��&nbsp;���� ����� ���� �������. ��� &#151; ���������� ����������� ��������������, ��������� ��� ���� �&nbsp;�����, �������������� �&nbsp;����������. ��&nbsp;����, ������� �&nbsp;���� ������� ������ ����� ��������, �&nbsp;������ ��&nbsp;���������� ������������. ��� �������� ������ ��������, ���� ���������� �����������, ����������� ������ ��&nbsp;�������� ��� ������������� ����, ��� ��&nbsp;�������� ����� ���������� ��&nbsp;�������, ��� ����� ������� ������� ������, ������� �������� �&nbsp;������. ������� ����� ����� ������������� �&nbsp;���� ������� ������, �&nbsp;�&nbsp;������������ ������ ������� ����� ���� �����, ��� �������&nbsp;�� ������������ ������ ��&nbsp;��������� &laquo;������� ���������� �������&raquo; ���, ��������, ��������&nbsp;�� ������������ �&nbsp;��&nbsp;�����, ����� ��� ������� ������������ (����, ��������, ����� ���� ���������� �&nbsp;����� ��&nbsp;����� ������� ��&nbsp;��� ���, ���� ��&nbsp;���������� ���� �&nbsp;�����, �&nbsp;��&nbsp;���������� �������� ������������).</p>

<p>��� ���� ��� ������, ��� ������ ���� ����� ���������� �&nbsp;����, ��� <strong>�&nbsp;��� �������</strong>, ��� �&nbsp;��� �����, ����� ��� ������ �&nbsp;������. ���� ��&nbsp;���������� �����-�� ��������� �&nbsp;������� �&nbsp;���� �����, ��&nbsp;���&nbsp;�� ��� ������ ��������� ��������� &#151; ����������, ��� ������ ��&nbsp;������ ������ �������. ����� �������� ��� ��� ����������� �&nbsp;���������, ����� ��������� ���-���� ���� ����� �&nbsp;����� ��������, �&nbsp;����� �&nbsp;����������.</p>

<p>������ �������:</p>

<p class="nb">������� ����������� ��&nbsp;������������ �������, ��&nbsp;���������� �&nbsp;����, ��&nbsp;���, ��� ��� �������� ����������� �&nbsp;������������ �������. ���� ������� ���������� ��������� &#151; ���������� ��&nbsp;�������������. ������� ���������� &#151; ���� ��&nbsp;�������. ���� ��� ���� ������, ������ ��&nbsp;�&nbsp;�������� �&nbsp;������, ������ ���� ��������.</p>

<p><strong>��, �&nbsp;����������. ���������� ������ ������� ��� ����� ������ ����� ������ ��&nbsp;���� ��������� ������� ������. ������ ��� �&nbsp;���� ���������� ����� ������� ���� ����� ����. ���, �&nbsp;��&nbsp;������, ��������� ��� �������. ��&nbsp;������ ��������� �&nbsp;����� ��&nbsp;���, ��� �&nbsp;���� ����� ��������� �&nbsp;��������, �&nbsp;��� ��� �&nbsp;�������� ��������� ������� ����, �.&nbsp;�.&nbsp;��&nbsp;���� ������������. </strong></p>





      </td>
<? show_menu_ex("inc/hellas.menu");?>
	     </tr>
<? show_footer(); ?>

