<?
define("ROOT","../../../");
require_once("../../../funcs.php");
show_header("�������� �� ���� :: ����� ���� :: ���������� �����",LUNA);
show_menu("inc/main.menu"); ?>

	    <td class="box">

            <div class="boxheader"><a href="http://bastilia.ru/luna/">�������� �� ����</a> :: <a href="../">����� ����</a> :: ���������� ����� </div>
            <p>�� ������� �&nbsp;������� �&nbsp;����� ��������� <strong>31978</strong>&nbsp;������, ��������� <strong>42457</strong>&nbsp;������. ������� ������� &mdash; <strong>10479</strong>&nbsp;������.</p>
<p>����� 31978&nbsp;������ �������������� ��&nbsp;��������� ����:</p>
<ul>
<li>���� <strong>10</strong>&nbsp;������&nbsp;&#8212; <strong>1</strong>&nbsp;�����-�������� (��&nbsp;���� ��&nbsp;�������);</li>
<li>����� ��&nbsp;<strong>798</strong>&nbsp;������ (����� ��&nbsp;����)&nbsp;&#8212; <strong>24</strong>&nbsp;������;</li>
<li>����� ��&nbsp;<strong>998</strong>&nbsp;������ (����� ��&nbsp;��������)&nbsp;&#8212; <strong>12</strong>&nbsp;�������;</li>
<li>����� �&nbsp;����� <strong>840</strong>&nbsp;������ �&nbsp;�������� ������������� ������������� ��&nbsp;����&nbsp;&#8212; <strong>2</strong>&nbsp;��������. ������� ������ ����� ��&nbsp;�����: ��� ����� �&nbsp;�����. </li>
</ul>
<p>���� ������ ��������� ��������, ��&nbsp;��� ����� ��&nbsp;�&nbsp;���� ������, ���� ����������� ��������� ����� �&nbsp;��������� ��������. </p>
<table width="95%" border="2" cellpadding="2" cellspacing="2">

<tr>
<td><div align="left"><strong>������ �������� </strong></div></td>
<td><div align="center"><strong>���-��</strong></div></td>
<td><div align="center"><strong>����</strong></div></td>
<td><div align="center"><strong>�����</strong></div></td>
</tr>
<tr>
<td colspan="4"><strong>��� ��� ������� (19608&nbsp;�.) </strong></td>
</tr>
<tr>
<td><div align="left">���� ������, 2&nbsp;�&nbsp;(����������� ����)</div></td>
<td align="right">30</td>
<td align="right">50,39</td>
<td align="right">1512</td>
</tr>
<tr>
<td><div align="left">�������� ������, 2&nbsp;�&nbsp;(����������� ����)</div></td>
<td align="right">24</td>
<td align="right">31,02</td>
<td align="right">744,5</td>
</tr>
<tr>
<td><div align="left">���� &laquo;3&nbsp;�&nbsp;1&raquo;, 50&nbsp;���������</div></td>
<td align="right">3</td>
<td align="right">115,3</td>
<td align="right">346</td>
</tr>
<tr>
<td><div align="left">����, �������</div></td>
<td align="right">25</td>
<td align="right">17,49</td>
<td align="right">437,3</td>
</tr>
<tr>
<td><div align="left">�����, ����</div></td>
<td align="right">28</td>
<td align="right">10,45</td>
<td align="right">292,6</td>
</tr>
<tr>
<td><div align="left">�����-�������, 1&nbsp;��</div></td>
<td align="right">4</td>
<td align="right">36,59</td>
<td align="right">146,4</td>
</tr>
<tr>
<td><div align="left">������� �&nbsp;���������� ��������, �����</div></td>
<td align="right">10</td>
<td align="right">37,9</td>
<td align="right">379</td>
</tr>
<tr>
<td><div align="left">������� &laquo;���������� ����&raquo; ����������, �����</div></td>
<td align="right">48</td>
<td align="right">7,99</td>
<td align="right">383,5</td>
</tr>
<tr>
<td><div align="left">������� &laquo;���������&raquo;, �����</div></td>
<td align="right">40</td>
<td align="right">11,49</td>
<td align="right">459,6</td>
</tr>
<tr>
<td><div align="left">������ ������, �����</div></td>
<td align="right">20</td>
<td align="right">21,09</td>
<td align="right">421,8</td>
</tr>
<tr>
<td><div align="left">�������, �����</div></td>
<td align="right">35</td>
<td align="right">16,79</td>
<td align="right">587,7</td>
</tr>
<tr>
<td><div align="left">������, �����</div></td>
<td align="right">16</td>
<td align="right">24,99</td>
<td align="right">399,8</td>
</tr>
<tr>
<td><div align="left">����, 3&nbsp;�</div></td>
<td align="right">30</td>
<td align="right">57,29</td>
<td align="right">1719</td>
</tr>
<tr>
<td><div align="left">��������, �����</div></td>
<td align="right">78</td>
<td align="right">6,59</td>
<td align="right">514</td>
</tr>
<tr>
<td><div align="left">�����, 900&nbsp;�</div></td>
<td align="right">8</td>
<td align="right">25,79</td>
<td align="right">206,3</td>
</tr>
<tr>
<td><div align="left">���, 1,5&nbsp;��</div></td>
<td align="right">7</td>
<td align="right">59,99</td>
<td align="right">419,9</td>
</tr>
<tr>
<td><div align="left">��������, 1&nbsp;��</div></td>
<td align="right">10</td>
<td align="right">24,69</td>
<td align="right">246,9</td>
</tr>
<tr>
<td><div align="left">��� ������, 100&nbsp;���������</div></td>
<td align="right">6</td>
<td align="right">37,49</td>
<td align="right">224,9</td>
</tr>
<tr>
<td><div align="left">��������� ������, 36&nbsp;��</div></td>
<td align="right">4</td>
<td align="right">57,77</td>
<td align="right">231,1</td>
</tr>
<tr>
<td><div align="left">����� ������� �&nbsp;���� ��� �����, ��</div></td>
<td align="right">20</td>
<td align="right">18,99</td>
<td align="right">379,8</td>
</tr>
<tr>
<td><div align="left">������ ������ (����� ������, �������� ����, �����), �����</div></td>
<td align="right">32</td>
<td align="right">11,99</td>
<td align="right">383,7</td>
</tr>
<tr>
<td><div align="left">�������, 100&nbsp;�</div></td>
<td align="right">16</td>
<td align="right">19,99</td>
<td align="right">319,8</td>
</tr>
<tr>
<td><div align="left">������ ������������, 100&nbsp;�</div></td>
<td align="right">16</td>
<td align="right">10,99</td>
<td align="right">175,8</td>
</tr>
<tr>
<td><div align="left">������, ��</div></td>
<td align="right">4,38</td>
<td align="right">49,99</td>
<td align="right">219</td>
</tr>
<tr>
<td><div align="left">�������� �������, �����</div></td>
<td align="right">32</td>
<td align="right">51,79</td>
<td align="right">1657</td>
</tr>
<tr>
<td><div align="left">�������� ��&nbsp;�������, �����</div></td>
<td align="right">32</td>
<td align="right">52,19</td>
<td align="right">1670</td>
</tr>
<tr>
<td><div align="left">���������&nbsp;�/� (����������), ��</div></td>
<td align="right">80</td>
<td align="right">3,41</td>
<td align="right">272,8</td>
</tr>
<tr>
<td><div align="left">������ �������, 500&nbsp;�</div></td>
<td align="right">7</td>
<td align="right">48,99</td>
<td align="right">342,9</td>
</tr>
<tr>
<td><div align="left">����, 1&nbsp;��</div></td>
<td align="right">4</td>
<td align="right">7,99</td>
<td align="right">31,96</td>
</tr>
<tr>
<td><div align="left">������, ��</div></td>
<td align="right">24</td>
<td align="right">20,29</td>
<td align="right">487</td>
</tr>
<tr>
<td><div align="left">�������� ������ �&nbsp;������������, ����� (����������� ����)</div></td>
<td align="right">45</td>
<td align="right">24,04</td>
<td align="right">1082</td>
</tr>
<tr>
<td><div align="left">�������� �&nbsp;������, �����</div></td>
<td align="right">4</td>
<td align="right">29,99</td>
<td align="right">120</td>
</tr>
<tr>
<td><div align="left">��������� �&nbsp;������, �����</div></td>
<td align="right">5</td>
<td align="right">22,29</td>
<td align="right">111,5</td>
</tr>
<tr>
<td><div align="left">������� ���������, �����</div></td>
<td align="right">7</td>
<td align="right">29,99</td>
<td align="right">209,9</td>
</tr>
<tr>
<td><div align="left">������ ��������, �����</div></td>
<td align="right">16</td>
<td align="right">32,69</td>
<td align="right">523</td>
</tr>
<tr>
<td><div align="left">�����, �</div></td>
<td align="right">15</td>
<td align="right">130</td>
<td align="right">1950</td>
</tr>
<tr>
<td colspan="4"><strong>��������� ��� ������� (2262&nbsp;�.) </strong></td>
</tr>
<tr>
<td><div align="left">����� �����������, 100&nbsp;��</div></td>
<td align="right">3</td>
<td align="right">60,69</td>
<td align="right">182,1</td>
</tr>
<tr>
<td><div align="left">������ �����������, 100&nbsp;��</div></td>
<td align="right">6</td>
<td align="right">41,99</td>
<td align="right">251,9</td>
</tr>
<tr>
<td><div align="left">������� �����������, 50&nbsp;��</div></td>
<td align="right">6</td>
<td align="right">62,69</td>
<td align="right">376,1</td>
</tr>
<tr>
<td><div align="left">�������� ��� ����� ������, ��</div></td>
<td align="right">2</td>
<td align="right">20,99</td>
<td align="right">41,98</td>
</tr>
<tr>
<td><div align="left">����� ��� ������, 10&nbsp;��</div></td>
<td align="right">2</td>
<td align="right">17,99</td>
<td align="right">35,98</td>
</tr>
<tr>
<td><div align="left">��������� ������, �������</div></td>
<td align="right">24</td>
<td align="right">4,25</td>
<td align="right">102</td>
</tr>
<tr>
<td><div align="left">�������� �������, �����</div></td>
<td align="right">8</td>
<td align="right">11,39</td>
<td align="right">91,12</td>
</tr>
<tr>
<td><div align="left">����� ��� ������ 30�, 50��</div></td>
<td align="right">4</td>
<td align="right">23,82</td>
<td align="right">95,28</td>
</tr>
<tr>
<td><div align="left">����� ��� ������ 120�, 25��</div></td>
<td align="right">2</td>
<td align="right">113,9</td>
<td align="right">227,7</td>
</tr>
<tr>
<td><div align="left">��������, �����</div></td>
<td align="right">20</td>
<td align="right">24,8</td>
<td align="right">496</td>
</tr>
<tr>
<td><div align="left">����� �����, 5&nbsp;����.</div></td>
<td align="right">5</td>
<td align="right">40</td>
<td align="right">200</td>
</tr>
<tr>
<td><div align="left">����� 80&nbsp;�,&nbsp;��</div></td>
<td align="right">12</td>
<td align="right">12</td>
<td align="right">144</td>
</tr>
<tr>
<td><div align="left">������ ������, ��</div></td>
<td align="right">5</td>
<td align="right">3,5</td>
<td align="right">17,5</td>
</tr>
<tr>
<td colspan="4"><strong>���������� (4673&nbsp;�.) </strong></td>
</tr>
<tr>
<td><div align="left">����� �������, ��</div></td>
<td align="right">1</td>
<td align="right">36,16</td>
<td align="right">36,16</td>
</tr>
<tr>
<td><div align="left">�������� �6,&nbsp;��</div></td>
<td align="right">100</td>
<td align="right">0,54</td>
<td align="right">54</td>
</tr>
<tr>
<td><div align="left">������ ��� ���������� Svetocopy, �����</div></td>
<td align="right">5</td>
<td align="right">107</td>
<td align="right">535</td>
</tr>
<tr>
<td><div align="left">������ �1,&nbsp;��</div></td>
<td align="right">40</td>
<td align="right">8,9</td>
<td align="right">356</td>
</tr>
<tr>
<td><div align="left">�����-������� 70&times;100, ��</div></td>
<td align="right">200</td>
<td align="right">0,14</td>
<td align="right">28</td>
</tr>
<tr>
<td><div align="left">�����-������� 100&times;150, ��</div></td>
<td align="right">200</td>
<td align="right">0,29</td>
<td align="right">58</td>
</tr>
<tr>
<td><div align="left">�����-������� 150&times;200, ��</div></td>
<td align="right">200</td>
<td align="right">0,55</td>
<td align="right">110</td>
</tr>
<tr>
<td><div align="left">�����-������� 180&times;250, ��</div></td>
<td align="right">200</td>
<td align="right">0,75</td>
<td align="right">150</td>
</tr>
<tr>
<td><div align="left">����� 6&nbsp;��,&nbsp;��</div></td>
<td align="right">3</td>
<td align="right">43,91</td>
<td align="right">131,7</td>
</tr>
<tr>
<td><div align="left">�������� �������, ��</div></td>
<td align="right">60</td>
<td align="right">1,74</td>
<td align="right">104,4</td>
</tr>
<tr>
<td><div align="left">����� &#8470;&nbsp;3,&nbsp;��</div></td>
<td align="right">7</td>
<td align="right">8,96</td>
<td align="right">62,72</td>
</tr>
<tr>
<td><div align="left">����� &#8470;&nbsp;4,&nbsp;��</div></td>
<td align="right">7</td>
<td align="right">13,1</td>
<td align="right">91,7</td>
</tr>
<tr>
<td><div align="left">����-��������, ��</div></td>
<td align="right">10</td>
<td align="right">10,85</td>
<td align="right">108,5</td>
</tr>
<tr>
<td><div align="left">����� ��������, ��</div></td>
<td align="right">4</td>
<td align="right">27,86</td>
<td align="right">111,4</td>
</tr>
<tr>
<td><div align="left">������� (������ ������), ��</div></td>
<td align="right">40</td>
<td align="right">15</td>
<td align="right">600</td>
</tr>
<tr>
<td><div align="left">������������� ������ �4&nbsp;100&nbsp;��,&nbsp;�����</div></td>
<td align="right">3</td>
<td align="right">321</td>
<td align="right">963</td>
</tr>
<tr>
<td><div align="left">�������, ��</div></td>
<td align="right">20</td>
<td align="right">23,8</td>
<td align="right">476</td>
</tr>
<tr>
<td><div align="left">����� ���������� (16+8)�37��, 1000&nbsp;��</div></td>
<td align="right">1</td>
<td align="right">120,5</td>
<td align="right">120,5</td>
</tr>
<tr>
<td><div align="left">����� ���������� 25&times;40,&nbsp;1000&nbsp;��</div></td>
<td align="right">1</td>
<td align="right">145</td>
<td align="right">145</td>
</tr>
<tr>
<td><div align="left">�����-������ �&nbsp;�����������, 100&nbsp;��</div></td>
<td align="right">2</td>
<td align="right">60</td>
<td align="right">120</td>
</tr>
<tr>
<td><div align="left">������� ��� �����, ��.</div></td>
<td align="right">1</td>
<td align="right">28,17</td>
<td align="right">28,17</td>
</tr>
<tr>
<td><div align="left">����� ���������, ��</div></td>
<td align="right">60</td>
<td align="right">2,8</td>
<td align="right">168</td>
</tr>
<tr>
<td><div align="left">������� �5,&nbsp;12&nbsp;������</div></td>
<td align="right">50</td>
<td align="right">2,3</td>
<td align="right">115</td>
</tr>
<tr>
<td colspan="4"><strong>������� ���������� (1690&nbsp;�.)</strong> </td>
</tr>
<tr>
<td><div align="left">���������� (1800&nbsp;����� ���������, 1800&nbsp;�����, 200&nbsp;���������� ��&nbsp;������� ������)</div></td>
<td align="right">1</td>
<td align="right">970</td>
<td align="right">970</td>
</tr>
<tr>
<td><div align="left">������, ��</div></td>
<td align="right">70</td>
<td align="right">10</td>
<td align="right">700</td>
</tr>
<tr>
<td><div align="left">������ ��&nbsp;���������� (������� ������� ������� ��� ��������� �������)</div></td>
<td align="right">1</td>
<td align="right">20</td>
<td align="right">20</td>
</tr>

<tr>
<td colspan="4"><strong>����������� �������� (3457&nbsp;�.) </strong></td>
</tr>
<tr>
<td><div align="left">����-����, 2&nbsp;�</div></td>
<td align="right">12</td>
<td align="right">39,55</td>
<td align="right">474,6</td>
</tr>
<tr>
<td><div align="left">��� �������, �����</div></td>
<td align="right">1</td>
<td align="right">43,99</td>
<td align="right">43,99</td>
</tr>
<tr>
<td><div align="left">���� ��������, 6&nbsp;�</div></td>
<td align="right">8</td>
<td align="right">39,99</td>
<td align="right">319,9</td>
</tr>
<tr>
<td><div align="left">����� &laquo;�������&raquo;, ��</div></td>
<td align="right">12</td>
<td align="right">18,39</td>
<td align="right">220,7</td>
</tr>
<tr>
<td><div align="left">���� �����������������, ��</div></td>
<td align="right">12</td>
<td align="right">12,99</td>
<td align="right">155,9</td>
</tr>
<tr>
<td><div align="left">��������� R20, 2&nbsp;��</div></td>
<td align="right">4</td>
<td align="right">76,9</td>
<td align="right">307,6</td>
</tr>
<tr>
<td><div align="left">���� ����������� ���������� 76&times;76</div></td>
<td align="right">1</td>
<td align="right">719</td>
<td align="right">719</td>
</tr>
<tr>
<td><div align="left">���� �����������, ��</div></td>
<td align="right">4</td>
<td align="right">249</td>
<td align="right">996</td>
</tr>
<tr>
<td><div align="left">������� �������, ��</div></td>
<td align="right">4</td>
<td align="right">54,87</td>
<td align="right">219,5</td>
</tr>
<tr>
<td colspan="4"><strong>��������� �&nbsp;������������ ��������� (3894&nbsp;�.) </strong></td>
</tr>
<tr>
<td><div align="left">����� �������� ��� ��������, ��</div></td>
<td align="right">1</td>
<td align="right">250</td>
<td align="right">250</td>
</tr>
<tr>
<td><div align="left">�������� ���������� ��� ���������, ��</div></td>
<td align="right">3</td>
<td align="right">130</td>
<td align="right">390</td>
</tr>
<tr>
<td><div align="left">����� ����������� ��� ����������� ������� 3,2&times;50��, ��</div></td>
<td align="right">4</td>
<td align="right">48,36</td>
<td align="right">193,4</td>
</tr>
<tr>
<td><div align="left">�������� ������� ��� ����������� �������, 2�</div></td>
<td align="right">1</td>
<td align="right">36,27</td>
<td align="right">36,27</td>
</tr>
<tr>
<td><div align="left">������� (2,7&times;1,2&nbsp;�), ��</div></td>
<td align="right">3</td>
<td align="right">212,4</td>
<td align="right">637,3</td>
</tr>
<tr>
<td><div align="left">����� �������� 25&times;100&nbsp;&times;&nbsp;3000, ��</div></td>
<td align="right">18</td>
<td align="right">27,78</td>
<td align="right">500</td>
</tr>
<tr>
<td><div align="left">����� ��� ������������ ��������� 14&nbsp;��,&nbsp;��</div></td>
<td align="right">9</td>
<td align="right">41</td>
<td align="right">369</td>
</tr>
<tr>
<td><div align="left">������� ��� �������, d=8��, �</div></td>
<td align="right">12</td>
<td align="right">10</td>
<td align="right">120</td>
</tr>
<tr>
<td><div align="left">������ ��� ������ ����� (���������, ��������� �&nbsp;��.), �</div></td>
<td align="right">31</td>
<td align="right">17,41</td>
<td align="right">539,7</td>
</tr>
<tr>
<td><div align="left">����� ��� ����������, ��</div></td>
<td align="right">1</td>
<td align="right">170</td>
<td align="right">170</td>
</tr>
<tr>
<td><div align="left">����� ��� ���������, ��</div></td>
<td align="right">1</td>
<td align="right">124</td>
<td align="right">124</td>
</tr>
<tr>
<td><div align="left">����� �������� 10w40&nbsp;������������� (��� �����������) 1�,&nbsp;��</div></td>
<td align="right">1</td>
<td align="right">123,5</td>
<td align="right">123,5</td>
</tr>
<tr>
<td><div align="left">����� ��� ���� ��������� 1�,&nbsp;��</div></td>
<td align="right">1</td>
<td align="right">52,25</td>
<td align="right">52,25</td>
</tr>
<tr>
<td><div align="left">�������� ��� ������� �&nbsp;���������, 0,1&nbsp;�</div></td>
<td align="right">1</td>
<td align="right">70</td>
<td align="right">70</td>
</tr>
<tr>
<td><div align="left">������� ����� ��� ���������, ��</div></td>
<td align="right">4</td>
<td align="right">50</td>
<td align="right">200</td>
</tr>
<tr>
<td><div align="left">���� ��������������� ��� ���������</div></td>
<td align="right">1</td>
<td align="right">119</td>
<td align="right">119</td>
</tr>
<tr>
<td colspan="4"><strong>��������� (6873&nbsp;�.) </strong></td>
</tr>
<tr>
<td><div align="left">�������� ����� ��&nbsp;�������</div></td>
<td align="right">1</td>
<td align="right">3000</td>
<td align="right">3000</td>
</tr>
<tr>
<td><div align="left">����� ����� �&nbsp;������ �&nbsp;��������</div></td>
<td align="right">1</td>
<td align="right">3000</td>
<td align="right">3000</td>
</tr>
<tr>
<td><div align="left">������ ��� ���������� �����������, �</div></td>
<td align="right">52,8</td>
<td align="right">16,53</td>
<td align="right">872,8</td>
</tr>
<tr>
<td><div align="right"><strong>�����:</strong></div></td>
<td align="right"> </td>
<td align="right"> </td>
<td align="right"><strong>42457</strong></td>
</tr>
</table>


            </td>
	    <td width="150" class="side">
<? show_menu("inc/luna.menu");?>
	     </tr>
<? show_footer(); ?>

