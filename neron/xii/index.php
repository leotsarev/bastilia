<?
define("ROOT","../../");
require_once("../../funcs.php");
show_header("���� ������",NERON);
show_menu("inc/main.menu"); ?>

    <td class="box">
            <div class="boxheader"><a href="http://bastilia.ru/neron/">���� ������</a> :: XII ������</div>
            <h2>������ XII ������ </h2>
            <p class="epigraph">&laquo;���� ���� ����; �� �� �������, �� �����; �, ���� �� �� ��� �������, ��� �����! ��, ��� �� ����, � �� ����� � �� �������, �� �������� ���� �� ��� ����&raquo;.
(���������� ������ ���������, 3:15-16)</p>
<h4>I. ���� </h4>
<p class="syltext">
&laquo;Quo vadis?&raquo; - ������������ � ����� ���� ����-�����������, ���������� (����������) �������� ������� ���������� � ����� &laquo;����������� ������������&raquo; ������������ - ������� ���� ������ ��������� ��[������]������� ���������� ������, ������������������ ������� ����������� � �����������. ���������� �� ���� ���� � &laquo;�������&raquo; ������������, ���������� ������ ������� � ���� ����� �������������.
</p>
<h4>II. ��������� ���������</h4>
<div class="syltext">
<ul>
	<li>������������ ����� ������� ��������� &laquo;Quo vadis?&raquo; (<a href="http://lib.aldebaran.ru/author/senkevich_genrik/senkevich_genrik_kamo_gryadeshi/senkevich_genrik_kamo_gryadeshi__1.html">&laquo;���� �������?&raquo;</a>).</li>
	<li>������ �������� ����� "������". <a href="http://www.ancientrome.ru/antlitr/tacit/index.htm">����� XII-XVI.</a></li>
	<li>��� �������� ��������� "����� ���������� �������" ����� VI. <a href="http://www.ancientrome.ru/antlitr/svetoni/vita-caesarum/nero-f.htm">"�����"</a></li>
	<li><a href="http://bible.skiftel.ru/new_page.htm">����� �����</a></li>
	<li>��� �������� ������. <a href="http://lib.ru/POEEAST/PETRONIJ/satirikon.txt">���������</a></li>
	<li>����� ����� ������ �������. <a href="http://www.ancientrome.ru/antlitr/seneca/epist/epist-f.htm">������������ ������ � �������.</a></li>
	<li>���������� �.�. <a href="http://www.sno.7hits.net/lib/svenz/index.htm">������ ������������: �������� �������</a></li>
	<li>������ �. ������������ ���������� ������ ������� �������. </li>
	<li>��������� �. �. ��������� ������������.</li>
</ul>
[������ �������� ������� ���������]
</div>
<h4>III. ������������</h4>
<p class="syltext">�������� �������� ���� - �������� �����������������, �������������, ������������ ���������. ��� ������ ������� - �������� ���� ��������� � ������ �� ������������ "������� ����������������". ������ � ����������� ������������ ��������� "����������", � ����������� ������������ ������� ��������, � ������ ������� ���� � �����, � ��������� ����������� ���������. ����� ������� ������ - ��� ����� ����� ��������, "����������" ����������� � �������� �������������� ������������, ����� �� ������� �������� ������������. ����� �������, ���� ���� � ������ ����� ��������� � ������. <br />
����� ����... [����� ����� ��������]
</p>
<h4>IV. ����������� ����������</h4>
<p class="syltext">
<br />���� ���������� 6-9 ���� 2006 ���� � ������������� �������. ������ �������������� �������� [����� ��������]. ���������� ���������� - 50-80 �������. ������� ����� - 300 ������. ����� ������ �� ����������� ������, �������������, ������-��������� ���������, ���������� �, ����������... [����������������, ����� ��� ��������]
<br />���� ���������� [���������] ��� "��������". ������ ���������� ������:
<br />����� (��������� ������) - ����� �����������, ��� ��������������� �������; +7 921 9857287
<br />����� (����� �����) - ��� ������� ����������� �����, [������������] ���������� ������; +7 909 5778179
<br />��� (��������� ������) [���������]; +7 911 7023480
<br />����������� �����: <a href="mailto:neron@bastilia.ru">neron@bastilia.ru</a>
<br />������ ����� �������� � ������� ������ ���������� ��� ������������ �������� �����. ����� ����� ������ ��������� ������ �� �������� � ������������ � ������ �������. ����� ����, ������ ������� �� ������� ����� "����������� ������" � 8 ����� ������ ���������� ���������������� ������ �� ���� "�� ������ �� ����� �����?"
</p>
<h4>V. ��[��������� � ��������][�� ������ �������������� ������ �. ���������� ������� ���� ����������� "���� � ����"]</h4>
<p class="syltext">
�� ���� ������������ ����� ��� � ���������� ����� �������������� ����������������� �����. ��������� ���������� ������� ��� ���� ����� �������������, ��� �� �����, ���������. ��� ��������, ��� � ����� ������ �� ������ �������� ������ �� ������������ �� ��������� ��� ��������� � ����� ��������� ��������. ������� ��������� �� ����� ����������� ���������� ���� � ����, �� ���� ����������� ���������� � ��������. ����� �� ���������� � ����� ��� ������� ������ ������ ����� ��������� � ��� ������. ������� ������ ��������� � ���������� ��������� ��������� ����� ��������� �� ��������� ����������.<br />
������ ������� �������� ������ � ���� �������. �� �� ������ ����������, ��� ������������ � ���������, �������� ��� � ������ ����-������ �� �������.<br />
�� �� �������� �������� � � ������� ���������. ������ ������� �������� ������� ��������� ����������� ����������� ����� ��� �������� ��������, ���������� ��� ������� ������� ��� ������������ �����. ������ ������������� ����� ���� �������, �������� ��� ��������� ����������� ����� ������ ���������, ��� ���� ��������, ��� ��������, �������� �������� �������. ������ ��������� ������������������ �������� ����������� � ������������������ �� �������� (��������). ������ ������ � ������ ������������ � ����������. ��� ������� ��������� ���������� ����� �� ������ ��������� ��� �� �������� � ��������� � ��������� 1:1. �� ��������� ��������� � ������� �� ������� �� �������.<br />
</p>
<h4>VI. �������</h4>
<p class="syltext">� ������ ������������ (��������� �1): ������������� ������, ���� ������� ������, ������ (����� �������� �����), �������, �����, �����, ���������, ������������� ������� (�� ������ ������). ������� ����� (��������� �2): ������� �����, ��������� �����, ������� �����, ���� �����, �������������� ������������ ������ (�����, �����, �������, �������� � ������)... [����� ����� ��������]</p>
<h4>VII. ���������</h4>
<p class="syltext">�������������� ������������� ������������ ����, ���������� ���������� � ������� ����� � ��������� � ��������� II ����������. ������ �� �� ����� ������ ������ ������ ������������ ���������... [���������] ������� ���������� - ��� �������������, ������������ ������ ����� � ������������� �����. ��� ����� ������� � ���������� ����������� ����������� ���������� �������� �� ������� �������������� ����������� ��� � ������������.<br/>
[������, � ������� ���� ����, ��� ������ ��� � XVIII ���� � ����� ������������� �����������, ����������� � ��������� ���������]</p>
<h4>VIII. ������ � ���������</h4>
<p class="syltext">������ � ����� ���� - �� ����������� �����������, � ������������ ������������ �����. �������� ������ ��������, ��� �������, ������������, ������������� �� ����������� ���� ��� �� ������� ����������. ������� ������ ������������ �� �������������: ������� ������ ������ � �������, ������������ ���������, ������������ ������� � ������ �� ���� ������� ����������� ����. � �����, �� ���� ������ ��������� �� ����� ������ ������������ ��������� �� ���� ����������� ����. <br />
���������� ������������ �� ���� ������� �������: � �������������, � ��������� �� ������. ������ � ������ ���������� ����� ������������ �� ��������, � ��������� ����� � ������������ ����� � ���������. ����� [���������]</p>
<h4>IX. ������</h4>
<p class="syltext">������ �� ���� ����������, "�����������". ����������� � ������ ������ ���������� - ���������� (�����������, ��������� � ��������� ����� �� �������, ������� ��� ������� ������� ����������, � ������ ������). �����  ����������� [����� ����� �� �������� �����������. ������������� �������� �� ������, ��� ��, ���������� ��������� �� ����� ������������� ������ ���� ��������� �����������] <br />
����������, ��� �������� ������� �������, �������� � ���� �� ����� ����� ������ � ������� �� ���������� ������. ������� ����� ����������� ���� �� ���� �������� ������ (���� ��, �������, �� ����������� � �� ���������) [������� ����������: "������� � ��������"] ������ ���� �������� � ������. � ������ ��� ����������� ��� ����� ������� ����� ������� ������������. ������������ ����� ����� ����������� ������������� �������. �� ������� ���������� ��� ������� ���������� ���, �������, �� �������, �� ���������, ������������ � ����� ��� �������, ��������, ����� ���������� ���������.<br />
�������, ������� ����� (�������� ��������� "�������" � "�������" ������) � ������� ���������� ��������� ��������� ��������, � ������ �������, �������. "�������" ������������ ����, ��� "�������" (��������� ����������� � ������ "�������").</p>
<h4>X. ������</h4>
<p class="syltext">���� ������������ ��������� ������� ������������ ����������� ����� ��������� � ����� ����������� ��������� ��� ������������� �� ��������� (������������ ���� ���������� �������). � ���� ������� �� ������ ����� ����������� ��� ��������� ��� ����, � ��� ����� � ���� ������ ������������ �����������. ������� [���������] ����� ������� ���, ��� ������ ���������� ������ �� ��������, � �� �������� �����. ������ ���� ���� ������ ����� ����������� �� ���� ����� ����� ����� � ��������, ��� ����� ��������� ��� ������ ������. ������� ������ ���������. ��������� ������ ������ ����������� �����. </p>
<h4>XI. ������</h4>
<p class="syltext">������ ��� � ���������������� �������� ����� �������� ��� "���������� ���" ������� ����������. ���� ������ ����������� �����. ������������� ��������� �����, �������� �� ������������ �������������� ������, ��������� ���������� ������ ������ ������� ������. ��� ���� ������������� ������ � ����������� ������� - ����������. ��� ������ ������ �����, ��� ���������, ������ ��� �����, � ��������� ����� ������ � ������ �������������.
<br />
[����� ������� �������������� ��� ���������� �������� ������������ ���������� �������������]</p>
<h4>XII. ���������</h4>
<p class="syltext">������ ��������������� �������� ������ �������� ���������. ��������� �� ��������� ������� � ��������� ������������, �� ����, ��� ���, ���� � �������������� �����, �� ����� �������� ��������. ������� ������� ������������, ��� ���... [��������� ������������ ������ �� ���-�� ������������ ������] [�]�������� ������� � ������ ���������. ��������� ����� � ����������, ����, �������, � ������ ����� ������ ��������. ������ � ���������� ��� ����������, �� �������� ����, ����������.
���������� ������ ������������� ��������� ���� ������� �������� � ����������� "�� �����".</p>
 </td>
<? show_menu_ex("inc/neron.menu");?>
    </tr>
<? show_footer(); ?>