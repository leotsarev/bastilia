<?php

define('ROOT', '../../');
define('HOST', 'bastilia.ru');
define('SCHEME', 'http');
define('PORT', 80);
define('OPENID_PAGE', '/news/openid/');

require_once ROOT . 'funcs.php';
require_once 'openid.php';
require_once 'user_funcs.php';

$status = 0;

class BastiliaActionHandler extends SimpleActionHandler {

    function doValidLogin($login) {
        global $status;
        global $ljuser;

        $uri = $this->query['open_id'];

        if ( preg_match ('#^http://(.*)\.livejournal\.com/$#', $uri, $matches) )
        {
        	$ljuser = $matches[1];
        } elseif ( preg_match ('#^http://users\.livejournal\.com/(.*)/$#', $uri, $matches) )
        {
        	$ljuser = $matches[1];
        } else
        {
        	$ljuser = '';
        	$status = 3;
        }

        $status = 4;

        set_username ($ljuser);
        header ("Location: http://bastilia.ru/news/");
        die();
    }

    function doInvalidLogin() {
        global $status;
        $status = 1;
    }

    function doUserCancelled() {
        global $status;
        $status = 2;
    }

    function doErrorFromServer($message) {
        global $status;
        $status = 3;
    }
};

$handler = new BastiliaActionHandler($_REQUEST);

$ljuser = array_key_exists ('lj_user', $_REQUEST) ? $_REQUEST['lj_user'] : '';

$identity_url = isset( $ljuser ) ? get_lj_path($ljuser, false) : null;

if ($identity_url)
{
	$ret = openid_find_identity_info($identity_url);
  if( !$ret )
  {
  	$status = 3;
  }
  else
  {
  	openid_request ($handler, $ret);
  }
}
else
{
	openid_check_request($handler);
}

show_header("������������� ������ :: ����",BEFREE);
show_menu("inc/main.menu"); ?>
<td class="box">
 <div class="boxheader"><a href="/news/">������������� ������</a> :: ����</div>
 <?php switch ($status) {
   case 0: ?>
 <p>������ �������� ������������� ������ ��� ������������� �������������� ������.  ������� ������ �� ������������ �� �����, ��� ���
 	������������ ������� � ������ ����� ������. ���� �� �������, ��� ������������ � �������������, ������� � ���� ����� ���� ��� � ��.</p>

 <?php break; case 1: case 2: case 3: ?>
 <p>��������� �����-�� ������! �� �� ������������ ���� ���, ��� �� ���������� ��� �����������, ��� ���-�� ��� ����� �� ���. ���� ������ - ����������
 	��� ���.</p>
 <?php } ?>
<form action="/news/openid/" method="get" name="openid_login">
 		��� � ��: <input type="text" name="lj_user"> <input type="submit" value="�����">
 	</form>
</td>
<?php
right_block ('');
echo '</tr>';
show_footer();
?>