<?php
define('ROOT', '../');

require_once ROOT . 'appcode/users.php';
require_once ROOT . 'appcode/news.php';
function add_some_news ($date, $author, $header, $text, $tags)
{
	$date_array = explode (".", $date, 3);
	$timestamp = mktime (0, 0, 0, $date_array[1], $date_array[0], $date_array[2]);
	$date = date ('Y-m-j-H-i-s', $timestamp);
	$header = quote_smart ($header);

	$text = str_replace ("\n", '', $text);
	$text = quote_smart ($text);
	$author = quote_smart ($author);

	$sql =
	  'INSERT INTO ' . NEWS_TABLE . " SET `news_date` = '$date', `news_author` = $author, `news_header` = $header, `news_text` = $text";
	do_query_or_die ($sql);

	$id = last_insert_id();
	global $tags_list;
  $tags = explode (",", $tags);
  foreach ($tags as $tag)
  {
		$tid = $tags_list[trim($tag)];
		$sql = 'INSERT INTO ' . NEWSTAGS_TABLE . " SET `tid` = '$tid', `nid` = '$id'";
		do_query_or_die ($sql);
  }
}
$tags = get_all_tags();
$tag_list = array();
foreach ($tags as $tag)
{
	$tags_list[$tag['tag_name']] = $tag['id'];
}

$file = fopen(ROOT . "news/news.csv","rb");
echo "<table>";
$array = fgetcsv ($file, 100000);
while ($array)
{
	add_some_news($array[0], $array[1], $array[2], $array[3], $array[4], $array[5]);
	$array = fgetcsv ($file, CSV_HARD_LIMIT);
}
echo "</table>";
?>